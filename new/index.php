<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/select2.min.css">
  <link rel="stylesheet" href="/css/main.css">
  <link rel="stylesheet" href="/css/jquery.fileupload.css">
  <link rel="stylesheet" href="/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/remodal.css">
  <link rel="stylesheet" href="/css/remodal-default-theme.css">
  <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
  <title>Добавить новость | Мой район Онлайн</title>
</head>
<body>

  <?php // Подключение svg иконок, путь при программированиии надо заменить
  include '../include/svg.php';?>

  <div class="remodal-bg">
  <div class="remodal" data-remodal-id="modal">
  <button data-remodal-action="close" class="remodal-close"></button>
  <h1>Готово !</h1>
  <p>
    Ваша новость отправлена к нам в редакцию. В ближайшее время с Вами обязательно свяжется наш главный редактор.
  </p>
  <br>
  <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
</div>
  
  <header class="header">
    <div class="header__box">
      <div class="header__logo header__el">
        <svg class="svg-icon svg-icon--full-size">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
        </svg>
      </div>
               
      <div class="header__search header__el">
        <button class="header__search-btn" id="header-loupe">
          <svg class="svg-icon svg-icon--full-size">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
          </svg>
        </button>
        <div class="header__form-container">
          <form class="header__form" action="/search/">
            <input type="text" name="q" class="header__search-input" placeholder="... Поиск" autocomplete="off">
          </form>
          <div class="header__close-search"></div>
        </div>
      </div>

      <!-- search mobile -->
      <div class="search-mobile header__el">
        <div class="search-mobile__form-container">
          <button class="search-mobile__btn">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
            </svg>
          </button>

          <form class="search-mobile__form" action="/search/">
            <input type="text" name="q" class="search-mobile__input" placeholder="... Выбери город и район" autocomplete="off">
          </form>
        </div>
        <div class="search-mobile__close"></div>
      </div>
      <!-- end search mobile -->
      
      <ul class="search__suggest">
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
      </ul>

      <div id="burger" class="burger burger--header">
        <span></span>
        <span></span>
        <span></span>
      </div>

      <div class="burger-menu">
        <div class="burger-menu__header">
          <div class="burger-menu__logo">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
            </svg>
          </div>
        </div>
        <div class="burger-menu__body">
          <ul class="burger-menu__menu">
            <li><a href="/new/">Отправь новость</a></li>
            <li><a href="#">Вступай в группы</a></li>
            <li><a href="#">О пректе</a></li>
            <li><a href="#">Работа у нас</a></li>
            <li><a href="#">Реклама на сайте</a></li>
            <li><a href="#">Статистика</a></li>
            <li><a href="#">О нас</a></li>
          </ul>
        </div>
      </div>

    </div>
  </header>

  <main class="main">
    
    <div class="addnews">

      <div class="addnews__body">
        <div class="addnews__info">
          <h2 class="addnews__info-title">Отправьте  нам новость</h2>
            <div class="addnews__info-text">
              <p>У вас есть интересная информация? Думаете, мы могли бы об этом написать? Нам интересно все. Поделитесь информацией и обязательно оставьте координаты для связи.</p>
              <p>Координаты нужны, чтобы связаться с вами для уточнений и подтверждений.</p>
              <p>Ваше сообщение попадет к нам напрямую, мы гарантируем вашу конфиденциальность как источника, если вы не попросите об обратном.</p>
              <p>Мы не можем гарантировать, что ваше сообщение обязательно станет поводом для публикации, однако обещаем отнестись к информации серьезно.</p>
            </div>
          <div class="addnews__info-notice"><span>*</span> обязательное поле</div>
        </div>

        
        <div class="addnews__form">
          
          <div class="addnews__form-item addnews__city-block">
            
            <span class="addnews__required">*</span>
            <select name="city" id="city" class="addnews__input addnews__city select2" required="required">
              <option value="1" selected="selected">Краснодар</option>
            </select>
          
            <select name="district" id="district" class="addnews__input addnews__city addnews__city--district select2">
              <option value="0" selected="">Район города</option>
              
              <option value="12">Витаминкомбинат</option>
              <option value="8">Восточка</option>
              <option value="3">Гидрострой</option>
              <option value="1">Комсомольский</option>
              <option value="11">район Красной Площади</option>
              <option value="10">район Краевой больницы</option>
              <option value="18">район Московской</option>
              <option value="14">Славянский</option>
              <option value="17">Пашковка</option>
              <option value="6">Фестивальный</option>
              <option value="13">Центральный</option>
              <option value="15">Черемушки</option>
              <option value="9">Юбилейный</option>
              <option value="16">Яблоновский</option>
            </select>

          </div>

          <div class="addnews__form-item">
            <span class="addnews__required">*</span>
            <input type="text" name="name" id="username" placeholder="Введите имя" class="addnews__input" required="required">
          </div>

          <div class="addnews__form-item ">
            <span class="addnews__required">*</span>
            <input type="tel" name="phone" id="phone" class="addnews__input" placeholder="+7 (XXX) XXX-XX-XX" required="required">
            <label for="phone" class="addnews__phone-label">Если вы не введете номер телефона, мы не сможем вам начислить гонорар</label>
          </div>

          <div class="addnews__form-item">
            <textarea name="news-text" id="news-text" placeholder="Текст новости..." class="addnews__input addnews__textarea"></textarea>
          </div>
          
          <div class="addnews__file">
            <span class="addnews__file-tip">Прикрепить фото или видео</span>

            <label class="addnews__file-button">
              <span>Выбрать</span>
              <input  id="fileupload" name="files[]" multiple="" type="file">
            </label>
          </div>
          <div id="progress" class="progress">
          <div class="progress-bar progress-bar-success"></div>
          </div>
          <div class="fileholder"></div>
          <!-- <a data-remodal-target="modal"> --><button class="addnews__submit">Отправить новость</button><!-- </a> -->

        </div>
      </div>

      <div class="addnews__confirm"><strong>Нажимая кнопку «Отправить»,</strong> вы соглашаетесь с <a href="#">соглашением и правилами пользования сайта</a></div>
      <div class="addnews__notice">* Мы не используем в макретинговых целях и не передаем ваши персональные данные третьим лицам</div>

    </div>

</main>

  <script src="/js/jquery.js" type="text/javascript"></script>

  <script src="/js/maskedinput.min.js" type="text/javascript"></script>
  
  <script src="/js/jquery.fancybox.min.js" type="text/javascript"></script>

  <script src="/js/jquery.ui.widget.min.js"></script>
  <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
  <script src="/js/jquery.iframe-transport.min.js"></script>
  <!-- The basic File Upload plugin -->
  <script src="/js/jquery.fileupload.min.js"></script>

  <!-- The File Upload processing plugin -->
  <script src="/js/jquery.fileupload-process.js"></script>

 <script src="/js/remodal.min.js"></script>
  <!-- <script src="../js/select2.min.js" type="text/javascript"></script> -->

  <script src="/js/new.js" type="text/javascript"></script>
  <!-- Yandex.Metrika counter --> 
  <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter49868365 = new Ya.Metrika2({ id:49868365, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/49868365" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->
  <script src="/js/main.js" type="text/javascript"></script>
</div> <!-- remodal-bg -->
</body>
</html>