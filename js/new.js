$( document ).ready(function() {

	  $('.addnews__submit').click(function () {
		var mainarr = {};
		var imgs = [];


		if ($('#username').val() == '') {
			$('#username').css('border', '2px solid rgba(255, 80, 80, 0.9)');
			$('#username').css('box-shadow', '0 0 10px rgba(255,0,0,0.5)');
			 throw new Error('Введите имя!'); 

		} else{
			
			$('#username').css('border','');
			$('#username').css('box-shadow','');
			mainarr['username'] = $('#username').val();
		}

		if ($('#phone').val() == '') {
			$('#phone').css('border', '2px solid rgba(255, 80, 80, 0.9)');
			$('#phone').css('box-shadow', '0 0 10px rgba(255,0,0,0.5)');
			 throw new Error('Введите телефон!'); 

		} else{

			$('#phone').css('border', '');
			$('#phone').css('box-shadow', '');
			mainarr['phone'] = $('#phone').val();
		}		

		mainarr['city'] = $('#city option:selected').text();
		mainarr['raion'] = $('#district option:selected').text();
		mainarr['text'] = $('#news-text').val();

		$('.fileholder').each(function(){

              $(this).find('.attachment').each(function(){

                  imgs.push($(this).data('src'));

              });

          });

          mainarr['attachments'] = imgs;

		//console.log(mainarr);

		 $.ajax({
              type: "POST",
              dataType: "json",
              context: this,
              url: "/ajax/sendnews.php",
              data: {myData:mainarr},
              //contentType: "application/json; charset=utf-8",
              success: function(data){
                  if(data.status){

                      //alert(data.mess);
                    //$(this).removeClass('disabled');
              }
            },
              error: function(e){
                  console.log('error');
                  console.log(e);
            }
          });

		$('[data-remodal-id=modal]').remodal().open();


	})

	
	$(document).on('closing', '.remodal', function (e) {


	  		window.location.replace('/new');

	});

    $('#phone').mask('+7 (999) 999-99-99', {placeholder:'X'});

    // $('.select2').select2({
   //   placeholder: 'Выберите автора поста'
  //  });

    $('#fileupload').fileupload({

        dataType: 'json',

        dropZone: $(this),

        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,

        url: '/server/php/',

        error: function (jqXHR, textStatus, errorThrown) {

           alert(errorThrown);
        },

        done: function (e, data) {

          //console.log(data);

            $.each(data.result.files, function (index, file) {


             var tt = "/server/php/files/"+file.name;

           // console.log(file);
             if(file.type.includes('image')) {
             // Add pics to div
             $('.fileholder').append('<div class="img-holder"><img class="responsive-img filelist attachment" data-src="' + tt +'" src="' + file.thumbnailUrl + '">'+
                                   '<i class="fa fa-3x fa-times-circle img-close-btn"></i></div>'
                                   );

             };

             if(file.type.includes('video')) {

	          $('.fileholder').append('<div class="img-holder"><video class="responsive-img filelist attachment" data-src="' + tt +'" src="' + tt + '"></video>'+
	                   '<i class="fa fa-3x fa-times-circle img-close-btn"></i></div>'
	                   );
	      }
    })
    }
}).on('fileuploadprogressall', function (e, data) {
	$('.progress').show();
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
        if (data.loaded == data.total) {
        	$('.progress').hide();
	        $('#progress .progress-bar').css(
	            'width',
	            '0'
	        );
        }
    });


    //Remove pics
    $('.fileholder').on('click','.img-close-btn',function(){

      $(this).parent().remove();

    });

});