$( document ).ready(function() {

  $('.fancybox').fancybox();
  
  $('[data-fancybox]').fancybox({
    loop: "true",
    buttons : [
      'slideShow',
      'fullScreen',
      'close'
    ],
    animationEffect : "fade",
    transitionEffect : "tube",
  });

  // intro screen collapse
  $(window).scroll(function() {
    var element = $('.intro__body');
    var scrollTop = $(window).scrollTop();
    element['fade' + (scrollTop > 200 ? 'Out' : 'In')](500);
    //remove color carousel
    if ( scrollTop > 200 ) {
      $('#headerIntro').removeClass('header--intro').css( 'background-color', '#fff' );
    } else {
      $('#headerIntro').addClass('header--intro');
    }
  });


  $('.js-hide-intro').click(function() {
    var destination = $('#city-choice').offset().top - $('.header').height();
    $('html').animate({ scrollTop: destination }, 600);
  });

  //header color carousel
  if ( $('*').is( '.header--intro' ) ) {
    var arrD = ["#17A086", "#FF8929", "#BF3A2B", "#9A59B5", "#F44336", "#3598DC"], i = -1;
    setInterval(function(){
      $(".header--intro").css( 'background-color', arrD[i = ++i == arrD.length ? 0 : i] );
    }, 4000);
  }
  //end color carousel

  var burger = $('#burger');

  var asideOpen = false;  
  function openSidebar() {
    burger.toggleClass('burger-active');
    $(document.body).toggleClass('_fixed');
    if(asideOpen == false) {
      $('.burger-menu').show();
      asideOpen = true;
    } else {
      $('.burger-menu').hide();
      asideOpen = false;
    }; 
  };

  if(burger) {
    burger.click( function(){ 
      openSidebar();
    });
  }

  //Header search form  
  var searchBox = $('.header__search');
  var inputBox = $('.header__search-input');
  var isOpen = false;
  
  function closeSearch() {
    searchBox.removeClass('header__search--open');
    inputBox.focusout();
    isOpen = false;
  };

  $("#header-loupe").click(function() {
    if(isOpen == false) {
      searchBox.addClass('header__search--open');
      inputBox.focus();
      isOpen = true;
    } else {
      closeSearch();
    }
  });

  $('.header__close-search').click(function(){ 
    closeSearch();
  });
  // End header search

  // Header mobile search form
  var mobileSearch = $('.search-mobile');
  var mobileSearchIsOpen = false;

  function closeMobileSearch() {
    mobileSearch.removeClass('search-mobile--open');
    mobileSearchIsOpen = false;
  };
  
  $(".search-mobile__btn").click(function() {
    if(isOpen == false) {
      mobileSearch.addClass('search-mobile--open');
      mobileSearchIsOpen = true;
    } else {
      closeSearch();
    }
  });

  $('.search-mobile__close').click(function(){ 
    closeMobileSearch();
  });
  // End header search mobile

  //landing price, see more info
  $('.news-type__btn').click( function() {
    
    var moreInfo = $(this).parents('.news-type').find('.news-type__more');
    console.log($(this));
    if ( $(this).hasClass('news-type__btn--close') ) {
      $(this).removeClass( 'news-type__btn--close' );
      moreInfo.animate( { maxHeight: '0' }, 200 );
    } else {
      $(this).addClass( 'news-type__btn--close' );
      moreInfo.animate( { maxHeight: '100%' }, 500 );
    }
    
    
  });

  /*$('.news-type__btn--close').click( function() {
    var moreInfo = $(this).parents('.news-type').find('.news-type__more');
    $(this).removeClass( 'news-type__btn--close' );
    moreInfo.animate( { maxHeight: '0' }, 500 );
  });*/

  // scrolled map when footer appears
  if ( $('*').is( '.footer' ) ) {
    var footer = $('.footer');
    var fixCol = $('.col--fixed');
    var footerPos = footer.offset().top;
    var winHeight = $(window).height();
    var scrollToElem = footerPos - winHeight;

    function makeColScrolled() {
      var winScrollTop = $(window).scrollTop();

      if(winScrollTop > scrollToElem) {
        fixCol.addClass('col--scrolled');
      } else {
        fixCol.removeClass('col--scrolled');
      }
    }
  };

  $(document).ready( makeColScrolled );
  $(window).scroll( makeColScrolled );
  $(window).resize( makeColScrolled );
  // end case with footer and map


  //feed show more
  $('.news__text').each( function(indx) {
    if( $(this).prop('scrollHeight') > 130 ) {
      $(this).after( 
        '<a href="#" class="news__show-more">Показать полностью…</a>' 
      );
    }
  });

  $( '.news__show-more' ).click( function( event ) {
    event.preventDefault();
    $(this).hide();
    var openText = $(this).parent().find('.news__text');
    openText.animate( {maxHeight: '5000px' }, 500);
  });
  //end show more
	
});