<svg style="position: absolute; width: 0; height: 0;" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <defs>
    <symbol viewbox="0 0 40 40" id="icon-logo">
      <path d="M4.84389 5.9664L4.82837 3.24768L3.64941 5.42959H2.84439L1.66614 3.34167V5.9664H0V0H1.50421L3.27089 3.17941L4.991 0H6.49345L6.51003 5.96605H4.84389V5.9664Z" transform="translate(11.1575 2.49646)" />
      <path d="M3.25431 5.96605C2.63343 5.96605 2.07605 5.83953 1.58218 5.58363C1.08759 5.32809 0.700249 4.97462 0.419091 4.52466C0.139344 4.06755 0 3.55397 0 2.9832C0 2.41244 0.139697 1.90207 0.419091 1.45068C0.700249 0.993567 1.08759 0.637955 1.58218 0.383131C2.07605 0.127234 2.63343 0 3.25431 0C3.87589 0 4.43256 0.127234 4.92679 0.383131C5.42067 0.637955 5.80837 0.993567 6.08847 1.45068C6.36927 1.90207 6.50862 2.41244 6.50862 2.9832C6.50862 3.55397 6.36892 4.06719 6.08847 4.52466C5.80837 4.97462 5.42067 5.32809 4.92679 5.58363C4.43256 5.83917 3.87589 5.96605 3.25431 5.96605ZM3.25431 4.41029C3.49631 4.41029 3.71291 4.35239 3.90552 4.23803C4.10307 4.11794 4.25935 3.95318 4.37541 3.74089C4.49077 3.52359 4.54827 3.27162 4.54827 2.9832C4.54827 2.69478 4.49077 2.44603 4.37541 2.23302C4.25935 2.01644 4.10307 1.84989 3.90552 1.73517C3.71291 1.61651 3.49631 1.55575 3.25431 1.55575C3.01301 1.55575 2.79253 1.61651 2.59498 1.73517C2.40378 1.84953 2.24927 2.01608 2.13462 2.23302C2.01785 2.44603 1.96105 2.69442 1.96105 2.9832C1.96105 3.27198 2.01785 3.52359 2.13462 3.74089C2.24891 3.95318 2.40378 4.11794 2.59498 4.23803C2.79253 4.35239 3.01301 4.41029 3.25431 4.41029Z" transform="translate(21.9695 2.49683)" />
      <path d="M3.25466 5.96604C2.63308 5.96604 2.075 5.83881 1.58182 5.58327C1.08794 5.32737 0.699896 4.97462 0.419444 4.52287C0.139697 4.06683 0 3.55397 0 2.98249C0 2.41172 0.139697 1.90136 0.419444 1.45068C0.699896 0.99428 1.08794 0.637955 1.58182 0.38313C2.075 0.127233 2.63308 0 3.25466 0C3.87483 0 4.43221 0.127233 4.92679 0.38313C5.42208 0.637955 5.80731 0.99428 6.08705 1.45068C6.36821 1.90136 6.50791 2.41172 6.50791 2.98249C6.50791 3.55397 6.36821 4.06647 6.08705 4.52287C5.80731 4.97462 5.42208 5.32737 4.92679 5.58327C4.43221 5.83845 3.87483 5.96604 3.25466 5.96604ZM3.25466 4.4085C3.49525 4.4085 3.71291 4.35239 3.90446 4.23767C4.10307 4.11758 4.25865 3.95282 4.37435 3.74017C4.48936 3.52323 4.54792 3.27019 4.54792 2.98213C4.54792 2.69406 4.48936 2.4446 4.37435 2.23195C4.25865 2.01429 4.10272 1.8481 3.90446 1.73481C3.71291 1.61579 3.49525 1.55575 3.25466 1.55575C3.01266 1.55575 2.79218 1.61579 2.59392 1.73481C2.40307 1.84775 2.24926 2.01394 2.13285 2.23195C2.01855 2.4446 1.95999 2.69406 1.95999 2.98213C1.95999 3.27019 2.01855 3.52323 2.13285 3.74017C2.24926 3.95282 2.40307 4.11758 2.59392 4.23767C2.79218 4.35239 3.01266 4.4085 3.25466 4.4085Z" transform="translate(21.9702 23.8052)" />
      <path d="M3.34426 0C3.9856 0 4.54439 0.0911363 5.02134 0.273409C5.49793 0.454253 5.86516 0.715868 6.12233 1.05718C6.38056 1.39707 6.50967 1.79557 6.50967 2.25054C6.50967 2.70479 6.38091 3.10257 6.12233 3.44425C5.86516 3.78377 5.49757 4.0461 5.02134 4.22731C4.54439 4.40958 3.9856 4.50143 3.34426 4.50143H2.3424V5.96676H0V0H3.34426ZM3.19539 2.94853C3.51289 2.94853 3.75171 2.88921 3.9094 2.76983C4.0685 2.64546 4.14752 2.47177 4.14752 2.25054C4.14752 2.02859 4.0685 1.85847 3.9094 1.73838C3.75171 1.61365 3.51289 1.55218 3.19539 1.55218H2.3424V2.94853H3.19539Z" transform="translate(0 13.5768)" />
      <path d="M4.26464 4.92638H2.21399L1.85205 5.9664H0L2.35933 0H4.14893L6.50862 5.9664H4.62623L4.26464 4.92638ZM3.763 3.47748L3.23914 1.97748L2.71527 3.47748H3.763Z" transform="translate(11.158 13.5768)" />
      <path d="M4.26464 4.92709H2.21399L1.85205 5.96676H0L2.35933 0H4.14893L6.50862 5.96676H4.62623L4.26464 4.92709ZM3.763 3.4782L3.23914 1.97749L2.71527 3.4782H3.763Z" transform="translate(11.158 34.0332)" />
      <path d="M3.25466 5.9664C2.63379 5.9664 2.0757 5.83917 1.58112 5.58327C1.08795 5.32702 0.700603 4.97498 0.42015 4.52359C0.139698 4.06755 0 3.55361 0 2.98284C0 2.41208 0.139698 1.90172 0.42015 1.45032C0.700603 0.994282 1.08795 0.639029 1.58112 0.383132C2.0757 0.128307 2.63379 0 3.25466 0C3.87554 0 4.4315 0.128307 4.9275 0.383132C5.42138 0.639386 5.80872 0.994282 6.08917 1.45032C6.36821 1.90172 6.50862 2.41208 6.50862 2.98284C6.50862 3.55361 6.36821 4.06719 6.08917 4.52359C5.80872 4.97498 5.42138 5.32702 4.9275 5.58327C4.4315 5.83881 3.87554 5.9664 3.25466 5.9664ZM3.25466 4.40922C3.49596 4.40922 3.71221 4.35204 3.90517 4.23803C4.10308 4.11937 4.26076 3.95354 4.37506 3.7416C4.49077 3.52323 4.54792 3.27091 4.54792 2.98284C4.54792 2.69442 4.49077 2.44568 4.37506 2.23374C4.26076 2.01537 4.10308 1.85025 3.90517 1.73517C3.71221 1.61651 3.49596 1.55575 3.25466 1.55575C3.01266 1.55575 2.79218 1.61651 2.59604 1.73517C2.40378 1.84989 2.24962 2.01501 2.13356 2.23374C2.01926 2.44568 1.9607 2.69442 1.9607 2.98284C1.9607 3.27091 2.01926 3.52323 2.13356 3.7416C2.24927 3.95354 2.40378 4.11937 2.59604 4.23803C2.79253 4.35204 3.01302 4.40922 3.25466 4.40922Z" transform="translate(33.491 13.5771)" />
      <path d="M6.50967 0V5.9664H4.28263V3.76698H2.22669V5.9664H0V0H2.22669V2.11329H4.28263V0H6.50967Z" transform="translate(0 23.8048)" />
      <path d="M6.50932 0V5.9664H4.28299V3.76698H2.22634V5.9664H0V0H2.22634V2.11329H4.28299V0H6.50932Z" transform="translate(33.4907 23.8048)" />
      <path d="M6.50932 0V5.96676H4.28299V3.76769H2.22634V5.96676H0V0H2.22634V2.11365H4.28299V0H6.50932Z" transform="translate(33.4907 34.0332)" />
      <path d="M0 0H2.17165V3.10257L4.42868 0H6.50826V5.9664H4.3359V2.87277L2.0891 5.96676H0V0Z" transform="translate(21.97 13.5768)" />
      <path d="M0 0H2.17165V3.10293L4.42868 0H6.50826V5.96676H4.3359V2.87205L2.0891 5.96676H0V0Z" transform="translate(21.97 34.0332)" />
      <path d="M6.50932 0.000357202V5.85811H4.44314V1.53145H3.01866L3.00067 2.20908C2.97738 3.0461 2.90542 3.73302 2.78759 4.26841C2.66836 4.80414 2.46093 5.22302 2.16354 5.52323C1.86686 5.81987 1.44213 5.96712 0.890393 5.96712C0.611704 5.96712 0.313966 5.92995 0 5.85775L0.107242 4.27663C0.184146 4.29271 0.261756 4.302 0.338307 4.302C0.558083 4.302 0.72459 4.21837 0.83783 4.05039C0.956008 3.88313 1.03644 3.65976 1.07771 3.38063C1.11934 3.1015 1.14933 2.73624 1.16696 2.28413L1.24775 0H6.50932V0.000357202Z" transform="translate(0.000244141 34.0328)" />
      <path d="M4.81391 4.02716H6.02814V5.04646H4.69255L4.50664 6.54074H3.45256L3.63953 5.04646H2.26726L2.07958 6.54074H1.03538L1.222 5.04646H0V4.02716H1.3437L1.53949 2.51287H0.316788V1.495H1.66049L1.84675 0H2.89236L2.70575 1.495H4.07732L4.26358 0H5.30884L5.12293 1.495H6.33576L6.34493 2.51287H5.00087L4.81391 4.02716ZM3.75912 4.02716L3.95597 2.51287H2.58475L2.38826 4.02716H3.75912Z" transform="translate(0 2.20947)" />
      <path d="M4.42903 0L2.17201 3.10293V0H0V5.9664H2.0891L4.3359 2.87205V5.9664H6.50861V0H4.42903Z" transform="translate(33.4915 2.49646)" />
      <path d="M1.74445 0H0.189439L0 1.55683H1.55502L1.74445 0Z" transform="translate(36.043)" />
      <path d="M1.55501 1.55647H0L0.189436 0H1.74445L1.55501 1.55647Z" transform="translate(24.5215 11.0208)" />
      <path d="M1.55501 1.5579H0L0.189436 0H1.74445L1.55501 1.5579Z" transform="translate(24.5215 31.5894)" />
      <path d="M6.24951 0.421104C5.69566 -0.140368 4.79821 -0.140368 4.24436 0.421104L3.33245 1.34498L2.42053 0.421104C1.86704 -0.140368 0.969237 -0.140368 0.415387 0.421104C-0.138462 0.982219 -0.138462 1.89144 0.415387 2.45255L3.33456 5.40609L6.24951 2.45255C6.80335 1.8918 6.80335 0.982577 6.24951 0.421104Z" transform="translate(11.0803 24.085)" />
    </symbol>
    <symbol viewBox="-1 -1 24 24" id="icon-loupe">
      <path d="M17.3456 15.8303C20.3764 12.0418 20.1599 6.41328 16.5879 2.84133C12.7995 -0.94711 6.62977 -0.94711 2.84133 2.84133C-0.94711 6.62977 -0.94711 12.7995 2.84133 16.5879C6.41328 20.1599 11.9336 20.3764 15.8303 17.3456L20.4846 22L22 20.4846L17.3456 15.8303ZM15.0726 4.46495C17.9951 7.38745 17.9951 12.2583 15.0726 15.1808C12.1501 18.1033 7.27921 18.1033 4.3567 15.1808C1.43419 12.2583 1.43419 7.38745 4.3567 4.46495C7.27921 1.54244 12.1501 1.43419 15.0726 4.46495Z"/>
    </symbol>
    <!-- Social media -->
    <symbol viewBox="0 0 39 23" id="icon-vk">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M18.543 0.00561278C16.4746 -0.0166729 14.7146 0.00561279 13.7245 0.489899C13.0651 0.808756 12.5589 1.53561 12.8664 1.56904C13.2518 1.6239 14.1211 1.81076 14.5827 2.43818C15.1771 3.24218 15.1548 5.05847 15.1548 5.05847C15.1548 5.05847 15.4846 10.0565 14.3523 10.6848C13.5704 11.1133 12.5041 10.2442 10.1934 6.26961C9.01658 4.2219 8.12586 1.96504 8.12586 1.96504C8.12586 1.96504 7.96056 1.54676 7.65224 1.32647C7.27882 1.05133 6.76152 0.963041 6.76152 0.963041L1.2502 0.99647C1.2502 0.99647 0.425433 1.02904 0.128241 1.38133C-0.146682 1.71218 0.105973 2.3619 0.105973 2.3619C0.105973 2.3619 4.41825 12.4565 9.30263 17.5428C13.7802 22.2108 18.8624 21.903 18.8624 21.903H21.1723C21.1723 21.903 21.8652 21.8259 22.2172 21.4513C22.5469 21.099 22.5358 20.439 22.5358 20.439C22.5358 20.439 22.4921 17.3448 23.9224 16.8828C25.3415 16.4422 27.1453 19.8768 29.0706 21.1985C30.5334 22.2005 31.6442 21.9802 31.6442 21.9802L36.7924 21.903C36.7924 21.903 39.4766 21.7376 38.2004 19.6128C38.1019 19.4482 37.4639 18.0493 34.3832 15.1873C31.1492 12.1925 31.5783 12.6768 35.4726 7.4919C37.8373 4.33247 38.7837 2.40561 38.4865 1.56904C38.2004 0.787327 36.4627 0.99647 36.4627 0.99647L30.6653 1.02904C30.6653 1.02904 30.2362 0.974184 29.9176 1.16104C29.6093 1.34876 29.4114 1.78933 29.4114 1.78933C29.4114 1.78933 28.4873 4.23304 27.266 6.31333C24.6924 10.695 23.6475 10.9376 23.2295 10.6625C22.2497 10.0239 22.4921 8.10818 22.4921 6.7539C22.4921 2.4939 23.1413 0.73247 21.2383 0.269613C20.6113 0.115327 20.1488 0.0167556 18.543 0.00561278Z" transform="translate(0 0.857142)"/>
    </symbol>
    <symbol viewBox="0 0 24 23" id="icon-fb">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M22.0641 22.9389C22.0641 22.9389 22.9206 22.9389 22.9206 22.0817V0.857143C22.9206 0.857143 22.9206 0 22.0641 0H0.856459C0.856459 0 0 0 0 0.857143V22.0817C0 22.0817 0 22.9389 0.856459 22.9389H11.259C11.259 22.9389 12.1155 22.9389 12.1155 22.0817V14.4566H8.84037V11.1789H12.1155V8.66743C12.1155 5.63143 13.7547 3.97714 16.4637 3.97714C17.7587 3.97714 18.9937 4.07486 19.3183 4.11857V7.23429H17.321C15.8496 7.23429 15.3897 7.93286 15.3897 8.96229V10.3217C15.3897 10.3217 15.3897 11.1789 16.2462 11.1789H19.0759L18.6177 14.4566H16.2462C16.2462 14.4566 15.3897 14.4566 15.3897 15.3137V22.0817C15.3897 22.0817 15.3897 22.9389 16.2462 22.9389H22.0641Z" transform="translate(0.875488)"/>
    </symbol>
    <symbol viewBox="0 0 22 23" id="icon-inst">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M6.32581 0C2.83831 0 0 2.77286 0 6.18086V15.0094C0 18.4174 2.83831 21.1903 6.32581 21.1903H15.3623C18.8498 21.1903 21.6873 18.4174 21.6873 15.0094V6.18086C21.6873 2.77286 18.8498 0 15.3623 0H6.32581ZM17.1694 3.53137C17.667 3.53137 18.073 3.92823 18.073 4.41509C18.073 4.90109 17.667 5.29794 17.1694 5.29794C16.6718 5.29794 16.2659 4.90109 16.2659 4.41509C16.2659 3.92823 16.6718 3.53137 17.1694 3.53137ZM10.8438 5.29797C13.8337 5.29797 16.266 7.67397 16.266 10.5951C16.266 13.5163 13.8337 15.8923 10.8438 15.8923C7.85471 15.8923 5.42236 13.5163 5.42236 10.5951C5.42236 7.67397 7.85471 5.29797 10.8438 5.29797ZM10.8438 7.06372C8.84649 7.06372 7.22949 8.64343 7.22949 10.5951C7.22949 12.5469 8.84649 14.1266 10.8438 14.1266C12.8419 14.1266 14.4589 12.5469 14.4589 10.5951C14.4589 8.64343 12.8419 7.06372 10.8438 7.06372Z" transform="translate(0.100464 0.857142)"/>
    </symbol>
    <symbol viewBox="0 0 14 24" id="icon-ok">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M6.54607 3.55969C7.87787 3.55969 8.96129 4.63455 8.96129 5.95626C8.96129 7.27284 7.87787 8.34769 6.54607 8.34769C5.21428 8.34769 4.13086 7.27284 4.13086 5.95626C4.13086 4.63455 5.21428 3.55969 6.54607 3.55969ZM6.54601 11.9074C9.85708 11.9074 12.5438 9.24086 12.5438 5.95629C12.5438 2.67086 9.85708 0 6.54601 0C3.23494 0 0.543945 2.67086 0.543945 5.95629C0.543945 9.23657 3.23494 11.9074 6.54601 11.9074ZM8.90467 16.4579C10.0909 16.1922 11.2351 15.7268 12.2903 15.0659C13.0876 14.5688 13.3266 13.5214 12.8255 12.7302C12.3237 11.9399 11.2685 11.6974 10.4711 12.2005C8.07905 13.6894 5.00778 13.6894 2.62083 12.2005C1.81919 11.6974 0.764028 11.9399 0.262143 12.7302C-0.239742 13.5214 6.65347e-05 14.5688 0.801712 15.0659C1.85173 15.7268 3.0011 16.1922 4.1873 16.4579L0.92333 19.6962C0.257861 20.3571 0.257861 21.4319 0.928468 22.0919C1.26077 22.4228 1.69757 22.5899 2.13351 22.5899C2.56944 22.5899 3.01052 22.4228 3.34283 22.0919L6.54599 18.9145L9.75343 22.0919C10.4198 22.7528 11.498 22.7528 12.1686 22.0919C12.8341 21.4319 12.8341 20.3571 12.1686 19.6962L8.90467 16.4579Z" transform="translate(0.0717773 0.857142)"/>
    </symbol>
    <symbol viewBox="0 0 28 23" id="icon-teleg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M25.0361 0.00215836C24.6867 0.0175869 24.3578 0.113587 24.0649 0.22073H24.0606C23.7643 0.332158 22.3546 0.891873 20.2134 1.73873C18.0723 2.58987 15.2931 3.69387 12.5344 4.79102C7.02479 6.98016 1.60854 9.13502 1.60854 9.13502L1.67363 9.11187C1.67363 9.11187 1.30022 9.22673 0.909672 9.47959C0.710974 9.60216 0.49172 9.77102 0.30073 10.0359C0.109739 10.2999 -0.0452796 10.7062 0.0121031 11.1244C0.105457 11.8333 0.592783 12.2593 0.942218 12.4933C1.29594 12.7307 1.63338 12.8422 1.63338 12.8422H1.64109L6.71989 14.4562C6.94771 15.1462 8.26837 19.2407 8.58526 20.1836C8.77197 20.7476 8.95439 21.0999 9.18221 21.369C9.29184 21.507 9.42202 21.6219 9.57618 21.7136C9.63699 21.7479 9.70208 21.7753 9.76717 21.7976C9.78773 21.8096 9.80828 21.813 9.83227 21.8173L9.77917 21.8053C9.79544 21.8096 9.80828 21.8207 9.82028 21.825C9.86053 21.8362 9.88879 21.8404 9.94189 21.8482C10.7461 22.0779 11.3927 21.6064 11.3927 21.6064L11.4287 21.5799L14.4272 19.0033L19.4537 22.6419L19.5676 22.6873C20.6159 23.121 21.6762 22.8793 22.2364 22.4533C22.8016 22.0239 23.0209 21.4762 23.0209 21.4762L23.0577 21.3879L26.9418 2.61302C27.0514 2.1493 27.0796 1.71559 26.958 1.29387C26.8364 0.872158 26.5229 0.477873 26.1452 0.267015C25.7633 0.0518726 25.3856 -0.0132702 25.0361 0.00215836ZM24.9307 2.01477C24.9264 2.07648 24.9384 2.06877 24.9102 2.18791V2.19906L21.0621 20.7785C21.0458 20.8051 21.0176 20.8625 20.9405 20.9199C20.8591 20.9816 20.794 21.0202 20.4574 20.8933L14.3098 16.4456L10.5962 19.6393L11.3764 14.9388C11.3764 14.9388 21.0056 6.4702 21.4201 6.10591C21.8346 5.74163 21.6959 5.66448 21.6959 5.66448C21.725 5.22048 21.0707 5.5342 21.0707 5.5342L8.4062 12.9382L8.40192 12.9185L2.33219 10.9899V10.9865C2.32791 10.9865 2.31934 10.9822 2.31592 10.9822C2.31934 10.9822 2.34846 10.9711 2.34846 10.9711L2.38101 10.9556L2.41355 10.9445C2.41355 10.9445 7.83323 8.78963 13.3428 6.60048C16.1015 5.5042 18.8807 4.39934 21.0176 3.5482C23.1553 2.70134 24.7355 2.07991 24.8245 2.04563C24.9102 2.01477 24.8691 2.01477 24.9307 2.01477Z" transform="translate(0.62207)"/>
    </symbol>
    <!-- Cities -->
    <!-- Krasnodar -->
    <symbol viewBox="-71 -26.5 600 400" id="icon-krr">
      <line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="86.971" y1="194.959" x2="162.5" y2="135"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="86.539" y1="202.641" x2="168.333" y2="140.667"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="86.25" y1="209.845" x2="177.5" y2="143.75"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="85.674" y1="216.473" x2="189.667" y2="145.5"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="85.674" y1="224.254" x2="188.5" y2="157"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="85.385" y1="230.45" x2="187" y2="169.25"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M85.242,236.934
  c0,0,96.258-54.116,96.258-53.684"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="84.809" y1="244.571" x2="158.167" y2="205"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="84.521" y1="252.064" x2="138.833" y2="226"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="84.233" y1="258.98" x2="139.333" y2="237"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="83.801" y1="266.618" x2="140.5" y2="247.334"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="83.513" y1="274.255" x2="140.833" y2="259.5"/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" points="83,288.667 86.971,188.807 
  78.757,188.662 74.723,276.993 82.937,275.983 "/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" points="78.325,192.12 51.956,219.354 
  46.192,287.8 74.723,276.993 "/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="51.812" y1="223.965" x2="78.181" y2="198.028"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M58.151,218.057
  c-0.144,0.434-5.187,62.538-5.187,62.538"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="64.204" y1="212.15" x2="59.305" y2="277.713"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="71.265" y1="205.233" x2="66.654" y2="274.688"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="46.769" y1="283.333" x2="74.867" y2="270.941"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="46.192" y1="287.8" x2="44" y2="325"/>
<polyline fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="1,360 
  225.875,345 224.125,360.5 85.625,373.5 "/>
<polyline fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="1,355.5 
  36.625,353.25 52.875,354.625 222.625,343.75 226.125,344.5 "/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="8,354.625 
  68,305.75 101.625,303.625 52.25,354.5 "/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M224.125,360.5l31-2.75
  L256.25,301l-154.625,2.625c0.5-0.75,6.125-12,6.125-12l2.75-1.75l23.25-4.625l-1.75,6.625l-2.75,10.875"/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="256,315.625 
  144.375,319.25 138.125,326.125 255.875,321.375 "/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="98.75,303.75 
  99.25,295.125 107.75,291.625 "/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M110.5,289.875l8.5-6.75
  l48.375-12.75c0,0,31.875-9.125,31.5-9.125s-65.125,24-65.125,24"/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="
  123.625,281.625 125.5,275.125 137.75,268.5 152.375,258.625 195.75,243 196.125,247.375 199.625,247.5 198.875,261.25 "/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="199.625,247.5 
  205,248.375 235.5,277.125 236.25,301.25 "/>
<polyline fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="135.75,269.5 
  142.5,256.75 139.417,240.583 141.167,236.833 140.917,240.833 151.333,256.417 138.25,268.083 "/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M141.417,236.417
  c0,0.25,0.833,4,0.833,4s8.5,8.417,19.583,2l5.25-0.667c0,0-0.5,5.251-0.833,5.417s-13.417,8.5-13.417,8.5l-0.5-1.917l-2.75,0.167"
  />
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M139.417,239.833
  L138.667,221c0,0,16.167-14.75,38.667-28l0.167,9c0,0-28.583,23-35.667,34.333c0,0,13.083-1.25,25.583-1.75L167,241.417"/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M172.917,206.083
  c0,0,2.667,3.333,5.25,3.833c0,0,3.167-0.167,3.417-0.25s13.75-4.917,19.167-5l-0.667-1.833l-5.667,0.5c0,0-0.167-1.916-0.417-1.833
  s-12.25,6.584-15,6.75c0,0-3.916-1.751-4.583-3.584"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M177.333,193
  c0,0,41.333-16.084,45.583-17.417c0.5-0.25,13.75,0,13.75,0S186,196.584,177.5,202"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M194.417,201.5
  c0,0,16.167-16.334,37-23.667"/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M167.083,241.75
  c0,0,18.75-32.334,33.167-36.917"/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M166.75,246.583
  c0,0,11.917,1.416,23.25-3.917l7.833-3.083l12.5-5.75c0,0-10.583,7.917-14.583,9.167"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="210.333" y1="233.833" x2="208.5" y2="251.083"/>
<line fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="210.083" y1="239.25" x2="236.584" y2="199.25"/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M206.5,235.166
  c0.083-0.25,31.084-43.75,31.084-43.75"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M216.167,144.5
  c0,0-0.334,31.667-0.917,33.75"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="220.5" y1="144.583" x2="219.417" y2="176.75"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M189.5,188.25
  c0,0,7.084-31.833,10.167-37.167c0,0,1.166-3.5,9.583-6.167"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M202.167,147.75
  l0.25-5.667c0,0,1.75-2.167,8.25-3.083c0,0,12.417-0.5,21.167,1.667c0,0,5.25,1.167,6.833,2.167l0.417,6.25
  c0,0-7.667-4.583-20.25-4.917C218.833,144.167,208.667,143.25,202.167,147.75z"/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
  223.5,139.5 223.5,144.5 230.584,146 230.75,140.333 223.792,139.25 "/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="229.583" y1="143" x2="232.417" y2="143.333"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M218.917,116.333
  c-0.083,0.25-1.083,22.083-1.083,22.083"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="221.831" y1="119.5" x2="221.914" y2="138.5"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M185.581,189.5
  l0.417-6.167l-3.5-0.333c0,0-2.5,1.25-2.5-1.417s2.75-0.583,2.583-0.333l3.833,0.25l3.75-37.417"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="194.497" y1="143.167" x2="189.414" y2="187.083"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M200,136.063L198.813,141
  c-2.125,3.063-3.375,0-3.375,0c-3.063-4.625-0.125-14.125-0.125-14.125l-12-0.25c0,0-9.938,11.375-5.875,16.875l1.688,0.313
  c0,0,9.188,1.125,13.688-0.375L197,142.5"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M182.875,126.938
  c0,0-6.438-1.125-6.625-1.063s-8.875,4-8.688,3.813s7.688-4.875,7.688-4.875l0.813-1.25l5.5,1.875"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M176.688,142.875
  l-5.313-0.938l-3.125-1.5c0,0-4.063-3.188-4.25-3.125s-1.5-1.625-1.5-1.625l0.438-6.375l8.938,5.313l3.063,1"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M162.938,129.313
  l1.875-3.438c0,0,6.063-6.5,6.25-6.625s8.938-6.313,8.938-6.313l3.875-2.75l0.563-1.375"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M194.625,137.5
  c0.188-0.125,4.938-1.438,4.938-1.438"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="194.5" y1="135.313" x2="197.125" y2="134.5"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M195.125,131
  c0,0,1.688,4.563,3.938,4.625c0,0,2.313,0.25,3.375-2.063l-1.875-0.938"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M195.563,126.688
  c0,0,1.25-4.5,3.938-5.625l6.25-0.125l1.625,2l-0.125,1.563l-1.375,0.563"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M199,122.688
  c0,0,1.688-0.688,2.688,0.563c0,0,3.063,1.5,3.25,1.5s1.875,0.813,1.875,0.813l-0.25,1.438l-0.813,0.813"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M199.375,126.125
  c0,0,0.836,0.188,2.5,0.188c0.375,0,4.375,1.938,4.375,1.938l-0.5,1.25l-0.688,0.688"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M199.313,129.313
  c0,0,0.438-0.875,2.375-0.063s3.688,2.063,3.688,2.063l-0.813,2.313l-1.25,0.75"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M202.625,134.75
  c0,0-8.188,34.75-11.313,44.625"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M203.75,118.938
  c0.25-0.125,3-2.125,4.063-1.375c0,0,1.875,1-1.188,3.688"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M200.938,120.625
  c0,0,2.438-15.5,1.875-18.75c0,0-1.313-4.5-5.313-4.25l4.438-0.438c0,0,3.875,1.875,4.063,6.313c0,0-1.375,10.25-2.688,17.125"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="208.563" y1="111.875" x2="205.125" y2="112.5"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M201.25,113.063
  l-13,4.875c0,0-3.063-6.563-3.938-7.688c0,0-0.625-7.063,12.938-12.625"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M189.875,118.563
  c0.25,0.125,3.938,5.375,3.75,5.438s-2.5,1.688-2.5,1.688"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M222.875,112
  l-4.375,3.25l4,4.563l1.5-1.188c0,0,9.875-0.625,13.875,2.75l3.5-2.938l-1.813-3.688C239.563,114.75,226.813,113.563,222.875,112z"
  />
<path fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M217.125,106.5
  l4.625,2.5c0,0,10.875-0.375,16.125,2.688l2.688-1.438l-2.625-2.438c0,0-14.176-0.114-15.938-1.875
  C221.813,105.75,217.125,106.5,217.125,106.5z"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M213.563,101.75
  l3.563,1.563c0,0,11.375-1,17.375,1.625l2.375-1.5l-3.125-1.625c0,0-13.585-1.125-16.375-1.125
  C217.063,100.688,213.563,101.75,213.563,101.75z"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M212.438,101.75
  c0,0,5.938,3.813,7.875,11.875"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M238.813,122.063
  c0,0-1,15.688-1.188,19.875"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M238.667,142.833
  c0,0,4.146-19.146,2.458-30.583c0,0-4.063-13.688-6.688-16.625"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M220.813,99
  c0,0-6.25-6.813-13.188-6.063l-0.813,2l-4.25,0.438l-0.25,1.5l6.25,2.438c0,0,7-2.25,12.5-0.188"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M202.813,95.25
  c0.188,0,5.375,1.375,6.688,3.5"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M222,98.813l2-1.625
  c0,0,4.5,0.125,6.125-3.375l-5-0.188"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M230.75,93.563
  c0.188,0,1.938,0.5,1.938,0.5s-0.813,4.5-4,5.625c0,0-2.375-1.563-6.25-0.438"/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="233.25,94.313 
  232.813,98.688 230.688,100.063 228.688,99.688 "/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M208.75,92.625
  c-0.063-0.188-0.063-3.313-0.063-3.313l0.938-5"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M210.375,84.125
  c0.375,0,4.813,4.938,4.938,10.188"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M210.563,81.375
  c0,0,1.938,3.313,4.125,4.875l3.188,2.313c0,0,4.313,0.625,4.75,0.563s1.938-0.75,1.938-0.75s2.938-0.063,2.75,0
  s-2.75,1.875-2.75,1.875l0.375,6.313"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="193.438" y1="99.25" x2="194.25" y2="85.875"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="196.125" y1="97.563" x2="196.313" y2="85.813"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M211.813,78.063l1.125-2
  c0,0-19.313,0.5-25.125,1.75s-5.5,1.438-5.5,1.438L210.25,78"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M209.688,79.625
  c0,0,0.313,2.625,2.375,1.875l0.375-1.375"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M208.938,80.75
  c-0.25,0-1.375,0.563-1.375,0.563l-1,1.188c0,0-20.688,0.563-22.5-0.063s-2.5-2.563-2.5-2.563l0.063-4.063l21.75-3.75"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M182.438,82
  c0,0,0.177,3.75,5.438,3.75c0.188,0,3.938,0.438,3.938,0.438s2.5-1.438,9,0.688c0,0,6.375,2.563,7.625,2.313"/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="192.063,73.813 
  192.188,70.125 199.938,69.563 207.875,69.25 "/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="199.313,69.313 
  205.688,66.625 210.875,65.938 "/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M203.75,74.625
  c0.5-0.063,10.688-0.75,10.688-0.75l-0.875,1.313"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M213.563,76.938l0.813,2
  c0,0,4.5,0.875,5.438,2.688"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M226,70.75
  c0,0-3.5-2.313-7.313-1l-3,2.063L215,73.063"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M215.125,72
  c0,0-0.75-2.438-4.563-3.438l12.5-0.75"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M218.688,66.188
  c0,0,7.875,0.625,8.063,4.625c0,0,1.188-2.375,2.25-3.25c0,0,1.063-4.438-5.25-4.813c-0.5,0-4.375,0.188-5.188,0.375
  s-3.938,1-4.5,1.125s-0.75,0.563-1.438,0.875s-2.375,2.5-2.625,2.563"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M229,67.938
  c0.125,0.25,0.875,4.563,0.375,5.938"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M227.5,70.25
  c0,0,2.438,3.313,1.75,7.313c0,0-0.375,1.625-0.563,1.938s-0.563,1.688-0.625,1.875S228,82.438,228,82.625s-0.125,1.625-0.125,1.625
  l-0.25,0.938c0,0,1.375,2.125-0.563,3.063"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M217.438,74.438
  c0.25-0.063,4.75-1.375,7.063,1.313l-1.625,1.125"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M229.625,76.25
  c-0.313,0.063-2.75,0.063-2.75,0.063"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M226.094,75.219c0,0,2.578,4.172,2.328,5.797
  c0,0-1.555,0.773-2.055,0.711c0,0-0.117-0.727-0.305-0.727S225,81,225,81l-0.375,0.813"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M223.5,83.563c0.188,0,1.5,0.438,1.5,0.438h1
  l0.688,0.125l1.375-0.063"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M225.313,84.75c0,0,1.875-0.063,2.375,0.688"
  />
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M218.563,75.938c0,0,1.375,0.5,1.563,0.5
  s3.563-0.625,3.313-0.563"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M229.875,93.188
  c0.125-0.188,3.438-4.5,3.438-4.5s4.313,4.375,5.563,6.875s4.563,11.25,3.125,17.313"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M233.875,88.625
  c0,0,4-14,6.688-15.063"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M240.125,71.875
  c0,0,10.5,8.5,4.938,30.25l0.25,4.125"/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M239.313,74
  c0,0,1.188-10.531,3.313-16.969c0,0,0.625-5.859,1-7.047s0.844-6.555,0.844-6.555S254,41.938,259,44.563V64.5v4l-1.313,9.438
  c0,0-4.375,22.875-16.188,38.25"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M243,55.813
  c0,0,7.75,1.313,13.813,10.813L259.5,64.5"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M254.813,42.688
  c-0.063-0.188,0.063-7.25,0.063-7.25s-4.75-0.875-6.75,3.625"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M254.188,30.5
  c-0.188,0.063-1.25-0.063-2.5-1.563"/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M252.5,28.563l-3.063,0.906l-0.781,2.578
  c0,0-1.078,0.039-1.266-0.086S246.5,31.5,246.5,31.5v-1"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M244.563,31
  c0,0,0.5,5.125,3.125,6.563l-1.188,5.5"/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="254.375,35.063 
  255.625,27 255.063,26.375 254.438,26.25 249.125,27.375 "/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M243.875,31.438
  l-1.938-5.688l0.375-1.313l0.875-0.625c0,0,1.938-0.563,2.313-0.25s0.5,1.313,0.5,1.5s-0.375,0.688-0.375,0.688l-1.125,0.125"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M254.625,25.938
  c0,0-0.313-1.188-0.5-1.188s-1.875-0.125-2.125,0s-2.75,0.75-2.75,0.75s-1.063,0.375-1.125,0.563s-0.063,1.313-0.063,1.313"/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M253.094,24.063
  c0,0-1.219-0.563-1.406-0.563s-4.688,0-4.688,0l-0.063,0.563l0.313,2.125"/>
<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M250.844,22.719
  c0,0-0.922-3.203-1.109-3.203s-0.898-0.414-1.086-0.352s-1.949,0.918-1.949,0.918s-1.381,0.365-1.443,0.553S244,22.438,244,22.438
  V23.5"/>
<line fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="247" y1="19.5" x2="247" y2="16.5"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="248.813" y1="18.875" x2="248.75" y2="15.938"/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" points="246.875,13.75 221.125,2.875 
  243.563,14 "/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" points="221.375,2.563 246.125,10.688 
  247.938,13.125 "/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" points="246.25,10.438 249,0.688 
  248.438,12.938 "/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" points="249.25,0.5 250.313,11.438 
  248.688,13.313 "/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M250.313,11.438c0,0,11.75,2,11.5,2.063
  s-12.875,0.25-12.875,0.25"/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" points="249.188,18.5 257.625,26.813 
  249.063,15.375 "/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" points="257.625,26.813 252.125,15.625 
  261.813,13.5 "/>
<path fill="none" stroke-linecap="round" stroke-linejoin="round" d="M244.063,14.375c0,0-3.188,8.875-3.125,8.625
  s6.5-7.75,6.5-7.75"/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="241.375" y1="22.875" x2="247.313" y2="18.563"/>
<polyline fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="73.833,305 
  74.667,292.167 91.667,286.333 92.5,297.833 96,299.667 94.5,303.5 "/>
<line fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="92.667" y1="286.167" x2="115.667" y2="285"/>
<path fill="none" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M239.084,149.083
  c0,0,5.75,13.751,5.583,42.584l-3.5,109"/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
  240.5,136.25 478.5,184.771 478.5,181.745 240.75,132.5 "/>
<polyline fill="none" stroke-linecap="round" stroke-linejoin="round" points="477.9,185.06 375.736,242.266 
  244,232.5 "/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="375.736" y1="242.266" x2="403.547" y2="301.776"/>
<line fill="none" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round" x1="256.5" y1="306.25" x2="461.667" y2="299.5"/>
<line fill="none" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round" x1="255.75" y1="330.25" x2="460.667" y2="317.833"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="239.25" y1="145.25" x2="466.949" y2="191.256"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="242.75" y1="161.25" x2="450.09" y2="200.117"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="244.25" y1="177.75" x2="433.52" y2="209.699"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="245.25" y1="194" x2="415.939" y2="219.786"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="244.75" y1="208.25" x2="400.954" y2="227.854"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="243.75" y1="224.5" x2="384.815" y2="237.223"/>
<polyline fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="250.663,233.62 
  268.098,305.811 271.988,305.669 254.842,234.047 "/>
<polyline fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="289.568,236.501 
  307.004,304.082 310.894,303.948 293.748,236.9 "/>
<polyline fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="320.693,238.878 
  338.128,303.218 342.019,303.092 324.871,239.26 "/>
<polyline fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="358.734,241.545 
  376.169,302.353 380.06,302.233 362.914,241.905 "/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="247.5" y1="301.25" x2="242" y2="279.5"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="270.836" y1="234.628" x2="290.001" y2="305.234"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="305.419" y1="237.222" x2="324.584" y2="303.505"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="340.866" y1="239.814" x2="360.031" y2="302.641"/>
<line fill="none" stroke-linecap="round" stroke-linejoin="round" x1="243" y1="251.5" x2="383.086" y2="257.828"/>
    </symbol>

    <!-- Rostov-na-Donu -->
    <symbol viewBox="-9 -124.451 600 400" id="icon-rostov">
      <path fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M6.5,193.549
  c0.75,0,20-2.25,21.25-2.5s15-2.75,15-2.75l6.75-0.25c0,0,14-0.75,14.75-0.75s24-4,24-4l8.25-1.75"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M6,202.799l18.25-1.5
  c0,0,35.5-3,36.5-3s16.75-1.75,17.5-1.75s18-1.25,18-1.25l0.25-13.75c0,0,71-9.25,72-9.25s54-2.75,54-2.75s73.5,10.75,81,11.5
  s44.75,5.25,55.5,5.75s38.25,1.5,38.25,1.5s52,2.5,54.25,3s39.25,2.75,40,2.75s35.75,0.75,35.75,0.75s28.75,0.25,29.5,0.25
  s25.75,0.25,25.75,0.25"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M97.5,195.049
  c1.25,0,34-1.5,34-1.5s39-0.75,42.75-0.5s52.5,1.5,52.5,1.5s86.75,3.5,88,3.5s105.75,5.5,107,5.5s40,3.25,41,3.5
  s10.75,1.75,10.75,1.75l86.25-0.25l15.5-0.75"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M96.5,79.924l-23.75,0.625l-6.875,0.625
  c0,0-11.875,5.75-13.5,6.375s-7.25,2.125-8.375,2.375s-4,0.25-5.375,1.125s-5.125,2.5-5.875,3s-4.5,3.25-5.125,3.75s-5,3-5.5,3.125
  s-2.5,1.625-2.5,1.625l2.125,1.125l6.5,3.5c0,0,3.25,1.125,3.625,0.75s5.5-5.375,6-5.375s5.375,0.125,5.375,0.125l8.25-3.5
  l8.875-4.125c0,0,12-1.25,12.5-1.25S90,95.049,90,95.049"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M19.5,103.174
  c-0.625,0.25-8.5,5.25-8.5,5.625s-0.25,2.875-0.25,2.875l16.875,2.5l3.375-5.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M378.834,169.883
  c-0.5,0-30.334-2.334-30.834-2.334s-15.332-2.499-16.166-2.666s-12-0.5-12-0.5s-22.668-4.5-23.334-4.5s-18.166-2.667-18.833-2.667
  s-13.833-1.167-14.833-1.167S249,157.383,249,157.383s-59.5,0.5-60.5,0.5s-13.667-0.834-14.5-0.834s-9.5,2.667-9.5,2.667l-29,6.333
  l13.5-10.5c0,0,30-11,31-11.5s12.333-4,12.333-4l8-3.833l8.667-0.167l-1.167,2c0,0-11,3.167-10.5,3.167s7.167,1.333,7,0.833
  s7.833-4.666,7.833-4.666s14.333-1.833,20.333-1.5s20.833,1.833,21.833,2.333s12.5,2.167,12.833,3s0,4.333,0,4.333
  s4.333-3.5,5.667-3s7,1.334,5.5,1.667s-6,2.333-6,2.333s10.333,4.168,13.833,5.334s21,6.666,21,6.666s26.501,4.166,30.167,4.5
  s17.333,2.167,17.833,2.167s9.833,0.333,10.5,0.333s11.167-0.833,11.167-0.833"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M1,193.216l22-7.5
  c0,0,33.333-12.667,34.667-13.167s18.833-6,19.5-6.5s23.833-14.334,24.5-14.667S118,139.549,118,139.549l8.5-5.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M332.375,142.424
  c0,0,5.125,8.292,5.625,11.125"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M296.667,133.882c0.5,0.5,13.5,14.5,14,15
  s4.833,4,5.333,4s3.667,1.167,3.667,1.167l2.833,2.5l2.167-4.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M322.5,156.549
  c1.167,0,6.333-0.667,6.333-0.667l4.667-1.833"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M407.5,174.716
  c-3,4.667-11.167,5.5-11.167,5.5c-13.334,0.334-14.833-15.167-14.833-15.167c0.167-18.333,15.333-18.5,15.333-18.5
  c11.333-0.667,15.334,12.167,15.334,12.167"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M415.667,159.216
  c0,0-4.001-17.001-19.334-16.834c0,0-18.999,0.668-18.166,23.834c0,0,1.167,16.166,18,17.166c0,0,10.666,1.334,16.833-8.166"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M385.667,153.216
  c0,0,14.688,3.666,15.183,3.666s6.271,0.334,6.271,0.334l10.563,3.166l5.775,0.167"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="345.333,146.382 351.167,147.549 
  357.333,148.216 364,148.382 373,150.049 380.333,152.382 "/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="344,154.716 348.167,154.549 
  361,155.382 372.333,156.216 378.833,157.049 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M354,160.549c1,0,12.333-0.333,12.333-0.333
  l5.5-1l6.5-0.167"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M381.5,165.049l15.833,0.833l14.334,0.167
  c0,0,14.332-1.167,15.166-1.167"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M384.667,171.549
  c0.5,0,21.5,0.333,21.5,0.333l-0.5,3l10-0.166l10.833-2.834l7-1"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M410.333,179.216c0,0,6.834-1.833,7.334-2
  s19-1,19.5-1s15.333-2.5,15.833-2.667s10.167-0.999,10.667-0.833s17,3.333,17,3.333l3,1"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M408.167,184.216c0.5,0,15.333,2,15.333,2
  s23.833-0.166,24.833-0.5s16.334,0,16.334,0l8.833,1.166l5.5-0.333l9-1"/>
<polyline fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="370.833,142.549 
  370.333,135.216 413.167,135.882 416.167,137.049 420,140.716 421.667,146.382 426.667,160.382 454,160.049 461.333,150.049 "/>
<polyline fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="373.333,142.716 
  374.162,138.216 411.923,138.382 415.731,140.216 417.719,142.049 424.344,163.716 454.816,163.882 464.754,150.882 
  467.238,149.549 491.75,150.382 "/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M370,137.549
  c0,0-16.667,0-17.333,0s-3.376-11.75-3.376-11.75l7.959-0.958l1.75,5.271l42.667-0.198l0.833-8.849l25.833,0.659l-0.666,25.996
  l2.833-0.252"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M467.737,138.382l2.637-1.5
  l12.023,0.167l54.352,0.833c0,0,6.258,0.667,6.588,1.167s0.33,2.5,0.33,2.5l-70.658-1.833l-3.952,1.333l-2.307,3.333"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M466.25,131.633
  l23.402-1.459c0,0,2.618-4.625,3.117-4.625s42.23,0,42.23,0l-5.653,23.917"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M472.167,150.382c0,0,5,6.166,4.5,6.5
  s-4,1-4.5,1s-11.334,1.167-11.334,1.167l15.667-0.667l0.667,3.667c0,0-0.334,2.333-0.834,2.5s-3.666,1.833-4.166,1.833
  s-16.001,0-16.834,0.167"/>
<circle fill="none"  stroke-linecap="round" stroke-linejoin="round" cx="399.375" cy="162.174" r="2.25"/>
<circle fill="none"  stroke-linecap="round" stroke-linejoin="round" cx="511.375" cy="169.174" r="2.25"/>
<circle fill="none"  stroke-linecap="round" stroke-linejoin="round" cx="510.125" cy="168.924" r="4"/>
<circle fill="none"  stroke-linecap="round" stroke-linejoin="round" cx="398.125" cy="161.924" r="4"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M486.625,171.549
  c0,0-0.25-14.5,10.625-21.125c0,0,19.875-11.875,34,7.5c0,0,6.5,14-3.75,26.875c0,0-8.25,9-22.5,7.5
  C505,192.299,488.75,190.674,486.625,171.549z"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M485.875,182.299c0,0-10.875-19.5,8.5-34.125
  c0,0,9.125-7.75,25.875-3.875c0,0,15,4.625,17.625,21.875c0,0,0.625,12.25-7,20.875c0,0-6.625,9.125-22.75,7.375
  C508.125,194.424,492.375,197.674,485.875,182.299z"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="487.25,170.799 502.625,167.924 
  487.5,167.174 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="475.625" y1="165.174" x2="482.375" y2="166.549"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="477.25" y1="163.424" x2="483.25" y2="164.549"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M488,165.049h9.198l7.861,0.375l3.871-0.25
  l4.232,0.125c0,0,4.597-0.5,4.96-0.5c0.362,0,8.224-1.25,8.224-1.25l6.049-1.75l0.604,2.375c0,0-5.686,1.75-6.047,1.75
  c-0.363,0-6.049,1.25-6.049,1.25l-6.533,0.125"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="487.375,174.049 498.25,171.174 
  502.625,170.049 506.125,170.174 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M496,151.674c0,0,7,7.625,6.375,13.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M499.875,149.049
  c0,0,6.875,10.125,6.125,16.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="475.625" y1="154.549" x2="490.25" y2="151.674"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="477" y1="156.424" x2="487.625" y2="154.799"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="492.625" y1="154.549" x2="498.375" y2="154.299"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M502.875,153.674l7.5-0.125
  c0,0,15,0.875,19.125,2.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M523.375,150.799l-10.5,0.25
  c0,0,10.375,1.375,14.375,2.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M534.125,155.049c0,0,11,3.875,11.125,7.375
  l-0.625,1.375l-1.125,0.875c0,0-1-0.125-1.375-0.125s-1.625-0.5-1.625-0.5l-1-1.5l-0.25-1.375l0.5-1.75c0,0-3.25-1.625-4.5-2.125"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="477.875,156.549 480.25,159.049 
  484.375,160.549 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="488.375" y1="161.549" x2="500.625" y2="165.174"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="514.25,170.799 523.75,175.549 
  527,177.424 530.5,179.049 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M488.125,176.799
  c0.625-0.125,16.75,2.25,16.75,2.25s6,0.625,6.375,0.75s3.5,2.375,3.5,2.375l1.875,1.375"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M491.5,184.424
  c1-0.25,11.25-0.75,11.75-0.75s9,0.625,9,0.625l11.25-2.25l6.375-1.375"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M535.708,179.924
  l15.518-0.5c0,0,20.358,7.625,20.73,7.75s4.842,2.125,4.842,2.125l6.827,2"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M528.25,189.674
  c0.375-0.125,8.5-0.75,8.5-0.75l4.125-0.5l5.125,0.625l3.75,1l2,0.875l3,3.875"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M412.084,178.799
  c-0.718,0.86-1.02,2.136-1.647,3.083c-0.493,0.743-0.923,1.585-1.519,2.25"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M414.001,178.382
  c-1.051,1.979-1.882,4.1-3.083,6"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M416.001,178.049
  c-0.006,1.313-1.081,3.25-1.686,4.42c-0.353,0.684-0.805,1.594-1.314,2.163"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M369.583,129.799
  c0.25-0.167,2.333-1.666,3.5-2.25s4.084-0.918,5.667-1.834s3.333-2,3.833-2.75s2.501-3.082,3.667-3.916s4.25-2.167,5-2.417
  s5.666-1.916,7.333-2.583s8.25-2.333,8.75-2.5s3.5-1.25,3.5-1.25l3.083-1.084"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M386.166,114.465
  c0-0.25,0.417-3.5,0.667-3.333s2,0.917,2.25,0.917s1.167,0.667,2,0s2.417-1.834,2.917-2.334s3.332-2.083,4.166-2.333
  s6-1.834,7.167-2.167s4.25-2.25,5.25-1.833s2.251,1.584,2.667,2.167s0.916,2.333,0.916,2.333s-0.166,6.083,0.167,7.083
  s1.25,1.917,1.25,2.167s0.667,4,0.667,4"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M423.416,100.716
  c0.918,0.25,3.084,0.665,4.084,0.833c1,0.166,3.458,0.208,3.708,0.208s2.271,0.146,2.271,0.146l1.426-0.26
  c0,0,3.84-0.84,3.423-0.423s-0.999,1.582-1.915,1.832s-4.665,0.332-5.248,0.498s-1.999,0.666-2.416,0.916s-2.082,0.499-2.249,0.749
  s0,1.25,0,1.25l0.833,1.167c0,0,0.249,0.417-0.167,1s-1.166,1.333-1.333,1.583s-0.667,0.666-0.5,1.25s-0.084,0.834,0.583,1.417
  s1.166,0.75,1.5,1.25s0.834,1.416,0.834,1.833s0.25,1.417,0.25,1.667s0,1.917,0,1.917l0.333,0.916l-0.5,1.251"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M416.666,68.132l-0.333,1l0.167,0.667
  l1,0.167c0,0,0.666,0.5,0.666,0.75s-0.5,1-0.5,1l-1.416-0.083c0,0-0.584-2.25-0.834-2.167s-0.916-0.083-1.083,0.417
  s-0.667,1.417-0.667,1.833s0.5,3.083,0.5,3.083l2.25,0.833c0,0,3.916,2.584,4.25,2.667s3.583,0.75,4,0.75s3.584,0.583,3.917,0.583
  s5.833-1.333,5.833-1.333s2.334-1.167,2.584-1.167s1.833-1.25,1.833-1.25L439,74.298l-0.667-1.667l-3.917-2.5
  c0,0-3.166-2.417-3.416-2.5s-1.168-0.25-2.334-0.25s-2.833,0-3.333-0.25s-1.917-1.917-2.667-1.917s-1.833-0.75-2.833-0.583
  s-3.583,0-4,0.083s-2.749,0.167-3.083,0.167s-1.334,0.167-1.334,0.167"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M417.083,67.632
  c0.25,0,1.751,0.417,1.917,0.75s1.582,1.667,1.666,1.917s0.584,1.917,0.584,1.917s0.084,0.5,0.75,0.333s3.832-0.583,4.416-0.75
  s2.75-0.333,3-0.417s2.834-0.167,3.084-0.083s2,0.667,2,0.667l1.666,1.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M385,76.049h10c0,0,4.75-0.833,4.75-0.583
  s0.999,2.25,1.166,2.667s0.416,1.667,0.75,2.167s1.417,1.749,1.667,1.916s2.5,0.333,2.75,0.25s2.083-0.666,2.083-0.666"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M398.666,78.882l1.667,3.5l5.583,10.083
  l0.5,2.834l0.167,2.333l-1.5,1c0,0-2.999,0.583-3.583,0.75s-4.166,1.083-5.25,1.5s-3.251,0.917-4.334,1.5s-3.916,2.5-4.833,3.5
  s-3.583,3.5-4,4s-2.833,2-4,2.5s-2.584,1.333-3.667,2s-2.167,1.166-3,1.833s-1.5,1.334-2.25,2.084s-1.082,1-1.666,1.75
  s-0.75,1.25-1.25,1.75s-1.084,1.166-1.834,1.666s-1.416,0.75-1.833,0.917s-1.751,0.501-2.167,0.667s-3.833-0.25-3.833-0.25
  l-2.167-1.417c0,0-0.75-1.166-1.083-1.083s-1.417,0.75-1.75,1s-0.333,0.25-0.333,0.25l-0.334,1.666"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M411.25,96.715l2.583-0.166l4.583,2.083
  L422,99.549c0,0,0.833,1.666,1.083,1.916s1.5,2,2.167,2.25s-0.167,3.084-0.334,3.834S424,118.465,424,118.465l-0.917,0.167
  l-0.667-0.917c0,0-0.333-1.25-0.333-1.666s-0.75-0.5-1-0.667s-0.833-0.083-1.083-0.083s-1.334,0.416-1.75,0.666
  s-0.584,1.25-0.667,1.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M425.75,79.299c0,0,0.166,6.416,0.083,6.833
  s-0.166,2.917-0.333,3.25s-0.5,1.833-0.5,1.833l-0.5,2"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M418.583,93.215
  c0.5-0.166,1.75-0.75,1.75-0.75l6.75,3l7.833,2.834l3.412,2.921"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M417.166,76.298
  c0,0.25,0.334,5.25,0.334,5.583s0.833,6.583,0.833,6.583l0.75,4.167"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="360.75,70.965 360.5,72.132 
  362.166,72.465 363.666,72.632 363.5,75.632 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M406.166,57.048
  c-0.666,0.083-3.916,0.333-4.166,0.333s-2.918,0.333-3.334,0.417s-2.083,1-2.333,1.333s-0.917,1.667-0.917,1.667l-0.916,0.917
  c0,0-0.917,1.167-0.667,1.167s1.334,0.167,1.667,0.083s1.583-0.25,1.833-0.417S399,61.715,399,61.715l0.75-0.5l2.166,1.917
  L404,64.798c0,0,1.916,0.834,2.166,0.917s2.334,0.25,2.334,0.25l1.666-0.25c0,0,2.084-1.584,2.084-1.917s0.083-1.667,0.083-1.667
  s-2.084,0.083-2.417,0s-3-1.167-3-1.167l-1.5-1.5l-0.583-0.75l1.833-1.25L406.166,57.048z"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M395.5,62.965l-0.667,2.083L396,66.215
  l-0.167,1.25l0.333,2.417c-0.333,0.083-1.749,0.75-2.166,0.917s-1.25,0.583-1.667,0.583s-7.417-0.167-7.417-0.167
  s-4.666-1.25-5.166-1.25s-2.667-0.25-3.167,0s-2.001,0.417-2.417,0.417s-7.75-0.583-7.75-0.583s-5.416-0.167-5.666-0.167
  s-2-0.167-2-0.167l-1.25,1.333l1.25,4.417l2,1.583c0.583-0.167,1.166-1.333,1.166-1.333l1.584,0.167l1.416-0.417l1.5-0.417
  l3.834,2.583l8,2.917l5.833,1.416l11.083,0.417l5.167,0.25"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M398.25,69.215
  c0.25-0.083,2.333-0.083,2.583-0.083s1.667-0.333,1.667-0.333l1.25-1.917"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="407.084" y1="66.632" x2="406.084" y2="69.466"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M397.417,70.466c0.25,0.083,2.75,1,2.75,1
  l3.167-0.167l2.917-0.667l2.166-1.583l2-3.167"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="365.876,70.924 366.396,72.153 
  366.5,72.882 366.5,74.549 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M28.625,96.674c0,0-7.75-7.5-7.375-7.5
  s10-1.25,10-1.25l3.125,4.75"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M55.75,86.174l-10-0.25
  c0,0-3.875,1.375-4.5,1.375s-6.125,0.125-6.125,0.125l-3.875,0.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M59.625,95.674c0.625,0,12.5,1.5,13.75,1.5
  s5.25-0.25,5.25-0.25s7.125,7.375,7.125,8.375s1.25,5,1.5,5.5s3.25,2.625,3.25,3s1.25,1.375,1.25,1.375l9.875,3l4.75,7.875l3,1.25
  l6.625,1.25"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M72.25,36.049l-5,2.5l-2.375-0.875
  c0,0-1.875-0.875-2.375-0.625s-3,1.625-3.5,1.875s-7,2-7.375,2s-4.875,0.75-5.25,0.75s-6,0.375-6.375,0.5s-2.375,1.125-2.375,1.125
  s-0.5,1.875-0.875,2s-2.25,1.25-2.25,1.25s-1.5,1.25-1,1.625s1.875,2,1.875,2l0.875,1c0,0-1,1.125-0.75,1.5s1,2.75,1.75,3.125
  s4.125,2,5,2s3.625-0.25,4-0.25s3.375-0.75,3.75-0.625s9.75,1.375,11,2.375s5.25,2.125,5.75,2.125s3.125-0.375,3.875-0.625
  s5.75-1,5.75-1"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="36,50.049 42.021,50.049 46,51.049 
  "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="46.375" y1="41.674" x2="47.375" y2="47.424"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="50.625" y1="49.049" x2="62.25" y2="47.424"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="48.25" y1="56.799" x2="47.875" y2="52.549"/>
<circle fill="none"  stroke-linecap="round" stroke-linejoin="round" cx="48.063" cy="49.861" r="2.313"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M77.75,34.299l4.25-1.25l0.5-2.375
  c0,0,0.25-1.875,0.875-1.875S95,27.924,95,27.924l4.5,1.75c0,0,1.375-1.75,1.75-1.875s9.75-1.75,10.375-1.5s16.875,6.75,16.875,6.75
  s6.375,7.25,6.75,7.25s4.5-0.125,4.875,0s5.375-0.625,5.875,0.125s9.625,3.125,10.125,3.5s6.5,3.25,7,3.625s6,4.375,6,4.375
  s3.375,1.125,3.375,1.75s-0.25,1.5-0.75,2.375s-1.875,2.5-2,2.875s-0.375,3.125-0.375,3.125s7.625,0,8.25,0.25
  s14.375,3.625,18.25,3.875s7.125,0.125,8.375-0.125s11.5-0.25,11.875-0.25s18.25,5.875,19.875,6.125s7.875,2.75,8.25,3
  s3.875,4,3.875,4"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M237.375,72.049
  c0.75,0,10.875-0.125,11.375-0.125s4.875,1.625,4.875,1.625s9.375,3.25,9.75,3.5s12.375,6.5,12.875,6.875s5.375,2.875,5.375,2.875"
  />
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M86.625,28.299c0,0-1.5-6.25-1.5-6.875
  s0.375-11.625,0.375-12s2.25-6.5,2.875-6.875s5.25-1.875,5.625-1.75s6.5-0.625,8.25-0.125s6.5,2.625,7.25,3.5s5.25,3.75,5.75,4.125
  s5,2.25,5,2.25s2.625,1.25,2.625,1.625s0.625,2.125,0,2.375s-1.75,0.875-1.375,1s3.875,2,3.75,2.625s-1.25,1.5,0.125,1.75
  s4.5,1.75,4.5,1.75s0.375,1.875,0.75,2.125s3,2.125,3,2.125l0.875,0.75c0,0,3-0.25,4.25,0.125s5.125,1.5,5.125,1.5
  s1.75,1.125,2.125,1.125s4.75,1.125,4.75,1.125s0,0.5,0.25,1.75s1.375,3,2.25,3.75s2.75,1.875,3.125,1.875s2.625-1,3.25,0.125
  s2.375,2.5,2.375,2.5s2,1.25,1.75,1.75s0.125,2.375,0.125,2.375s1.5,0.125,1.625,0.625s1.375,1.5,1.375,1.5l-1.125,1.875"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M85.875,8.299c-0.375-0.25-2-5.25-2.25-4.875
  s-1.125,2.875-1.5,4.125s-0.375,5.75-0.25,6.125s1.25,1.625,1.25,1.625l2.25,1.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M85.125,21.424c-0.625,0.125-1.375,2-1.375,2
  s-0.125,1.125,0.125,1.625s1.75,1.5,1.75,1.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M94.875,12.611
  C94.5,12.236,94.5,8.924,94.5,8.924V5.549c0,0,0.125-3.375,0.875-3.125s3.25,1.375,2.875,3.25s-0.125,3.625-0.125,4.625
  s0.625,2.875,0.625,2.875"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M100.875,13.799c0,0,13.5,6,21.875,16.375"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M98.25,9.299c0,0,12.875,0.125,41.625,30.375
  "/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="99.375,21.674 98.375,21.049 
  97,21.049 96.625,22.174 97.125,23.424 98,23.674 98.75,23.299 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M96.75,17.674c-0.625,0.125-3.5,1.5-3.5,1.5
  s-0.375,1.875,0,2.875s3.625,3.5,3.625,3.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M123.333,119.716
  c0,0,3.167-0.668,3.667-0.834s4,3.334,4,3.334"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M206,67.674c0.75,0.25,2.625,1.75,2.625,1.75
  s-0.875,1.5-0.375,2.25s0.125,5.25,3.625,6.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M467,149.049h-4l-2.332,0.25l-1.25-1.583
  c0,0,0.917-16.75-11.5-20l-2.584-0.833l-2.25-0.75l-1.666-0.417"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="446.501,127.466 447.084,130.383 
  437.584,147.049 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M428.001,143.716l3.75-12.833l5.25-5.5
  l1.417-1.917l2.416-1.333c0,0,4.834-2.584,5.084-2.584s13.5,1,13.5,1s2.833,3.25,3.083,3.417s3.417,6.083,3.417,6.083l0.5,18.584"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M461.834,123.466
  c-0.25,0-10.916-0.417-10.583-0.167s-0.749,0,2.667,3.667s3.75,4.417,5.25,5.417s4.583,2.166,4.833,2.583s2,1.083,2,1.083"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M460.084,120.633
  c0.25,0,4.334-0.084,5.667-0.084s4-0.167,4.5-0.25s2.167-0.667,3-0.5s3.333,1.167,4.083,1.667s2.334,1.751,2.417,2.417
  s0.333,2.25,0.333,2.583s0.001,1.333,0.084,1.75s0.166,2.333,0.166,2.333"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M447.168,115.716
  c0.25-0.083,2.666-0.583,4.166-0.583s7.75,0.083,8.417,0.333s4.167,0.667,4.667,1.167s1.833,1.833,1.833,1.833l0.583,2"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M430.084,123.882l0.5-6.666l5.083-11.584
  l2-1.666c0,0,2.334-2,2.667-2.417s6.667-8.083,6.667-8.083l2.083-1.417l2-0.583l6.5-0.334l2-0.333"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M434.417,121.299l-5,3.5l0.084,1.333
  l1.166,1.834l0.75,0.416l1-0.333l-0.583-2.917l1.667,2.25l1.25-0.583l-0.417-2.167c0,0,0.834-1.083,0.917-0.833
  s0.75,1.583,0.75,1.583l2-2c0,0-0.334-1.083-0.667-1.083s-1-0.333-1-0.333"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M438.001,122.216l3.166-4.5l2.25-3.75
  l2.084-2.083c0,0,0.25,2.082,0.25,2.416s1,1.5,1,1.5l-0.583,1.5c0,0-2.084,0.666-2.334,1s-4.166,5.417-4.166,5.417l-2.334,4.167
  c0,0-1.749,2.667-1.916,3s-2.25,3.25-2.25,3.25l-2.25,13.916c0,0,5.5,0.583,6.75,1.5s4.416,1.167,4.666,1.167
  s13.167-0.583,13.167-0.583l5.25-0.5"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="450.667,99.049 448.001,104.133 
  446.667,108.549 445.501,111.883 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M447.584,100.299
  c-0.417,0.334-7,6.084-7,6.084s-3.167,6.25-3.25,6.583s-1.584,2.583-1.667,3.083s-0.999,4.751-0.916,5.084"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M457.584,91.132l0.417,11.167l6.583-5.583
  l6.083,6.333c0,0,1.75-1.333,1.5-1.583s-2-3.667-2.083-4s-0.667-3.417-0.667-3.417l-1.083-2.25"/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="461.584,102.133 459.084,103.133 
  460.251,104.966 468.001,105.549 469.251,103.383 467.834,102.216 "/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="458.917,106.716 456.667,107.966 
  458.417,110.383 465.667,110.966 468.251,109.799 467.084,107.633 "/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="468.751,81.966 467.001,80.133 
  465.667,78.133 464.334,79.133 461.417,81.466 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M469.625,86.508l2.063-2.772l-0.521-1.687
  H465l-4.749-1H459l-0.666,2l1.083,4.667l0.667-1.917l5.583-0.25C465.667,85.549,469.166,85.467,469.625,86.508z"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M461.917,91.466l1.667-0.083
  c0,0,0.917,0.416,1.167,0.916"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="459.417,88.049 460,92.965 
  462.084,96.049 465,96.049 468.334,92.466 469.292,90.799 470.5,89.633 470.5,86.549 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="466" y1="88.049" x2="468" y2="88.049"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M470.834,87.299c0,0,2.584-0.416,2.917-0.333
  s2.916,1,2.916,1l1.25,1.667c0,0,0.917,0.75,1.167,0.75s4.167,1.417,4.5,1.75s5.917,0.833,5.917,0.833l7.25,0.25l-0.417,7.083
  l-15.083,1.25l-4.084-0.75c0,0-1.832-1.332-2.166-1.416s-1.084-2.834-1.084-2.834"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="471.584,102.966 468.334,115.049 
  469.417,117.383 470.251,120.299 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M496.751,93.216l4.083-0.333l3.167-1.334
  l3,0.334c0,0,2.5,1.083,2.5,1.333s-0.5,0.833-0.5,0.833l0.25,1.417c-0.834,0.583-1.417-0.333-1.417-0.083s0.249,1.75,0.083,2
  s-0.749,0.416-0.916,0.666s-0.75,1.584-0.75,1.584s-1-1.417-1.25-1.417s-3.334,0.083-3.334,0.083s-0.833,0.584-1.083,0.667
  s-3.75,0.417-3.75,0.417"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="504.209,91.216 504.417,88.049 
  507,88.049 506.667,91.966 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M496.417,124.799
  c0,0,1.584-9.416,11.834-8.416s10.833,8.416,10.833,8.416"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M499.334,124.633c0,0,1.583-6,8.333-5.75
  s8.167,5.916,8.167,5.916"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="486.084,92.133 488.084,89.799 
  488.417,84.466 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M489.167,82.133
  c0.417-0.084,15.834,0.334,17.417,0.75s6.583,1.25,8.583,1.25s8.834-0.667,12.834-1.917s8.166-3.5,8.166-3.5l0.667,1.083
  c0,0,1.166,6.334,1.583,7.167s5.75,3.917,7.167,4.167s7.084,0.583,10.167,0.083s6.833-1.75,6.833-1.75s6.5,15.25,6.25,15.25
  s-9.75,4.167-9.75,4.167s-6.001,1.166-7.167,1.583s-5.333,1.917-5.333,1.917s-8.917-1-10-0.75s-2.417,0.5-5.5,1.333
  s-9.416,1.834-10.583,1.917s-5.666-0.168-6-0.084"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M537.251,110.716l0.083-8.083l-29.833,0.25
  c0,0-3.167,1.999-1.917,8.916L537.251,110.716z"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M503.084,98.466
  c0.083,0.25-0.25,18.333-0.25,18.333"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="505.5" y1="98.549" x2="505.001" y2="116.383"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="505,114.049 514,114.049 
  514.084,117.549 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M491.584,92.299
  c0.25,0,7.499-1.333,7.833-1.333s4.75-0.583,4.75-0.583"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M507.501,90.383
  c2.083-0.084,9.75-0.5,11.583-1s8.917-3.167,9.5-3.417s7.334-3.833,6.917-3.583s-3.667,5.666-3.667,5.666l-0.083,3.25l4.333,0.75
  c0,0,1.5-3.25,1.75-2.833s4.917,3.083,4.75,3.333s-1.917,1.918-1.5,2.084s3.583,1.499,3.75,2.416s0.334,1.083,0.167,2.5
  s0,2.917,0,2.917"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M508.667,96.799
  c1.25,0,6.666,0.418,7.5,0.334s6.75-0.5,6.75-0.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M520.167,93.383
  c2.667,1.666,10.333,3.584,11.75,4.5s7.418,2.334,7.584,2.75s1.75,1.082,3.083,1.666s5.251,1.501,5.917,1.584s5.333,0,5.333,0"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M495.251,100.633c0,0,0.167,8.166,0.25,9.416
  s-0.583,7.416-0.75,8s-1.25,2.417-1.5,2.667s-3.25,0.417-3.25,0.667s-0.25,1.25-0.25,1.25l1.5,0.416l0.333,3.084"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M488.751,101.216
  c0.083,0.25,1.334,7.666,1.25,8.333s-0.334,3.667-0.334,3.917s-0.333,1.417-0.333,1.417l-3.167,0.083l-2.083,0.75
  c0,0-1.417,1.583-1.75,2.25s-0.583,1.667-0.75,2.167s-1.75,4.083-1.75,4.083"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M489.334,114.883
  c0.667,0.5,1.833,1.834,1.75,2.5s-0.583,3-0.583,3l-2.834,9.166"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M469.167,115.799
  c0.25-0.083,3.084-0.499,4.417,0.084s2.917,1.916,2.917,2.333s0.333,2.583,0.333,2.583"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M109,180.049
  c0,0.375-0.25,13.875-0.25,13.875"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="127.5" y1="178.174" x2="127" y2="193.299"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="147.5" y1="175.174" x2="147" y2="193.049"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="165.5" y1="173.174" x2="165" y2="193.049"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="183.5" y1="172.174" x2="183" y2="193.049"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="201.75" y1="170.799" x2="200.25" y2="193.924"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="219.375" y1="169.799" x2="218.375" y2="193.549"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="235.625" y1="171.424" x2="235.5" y2="194.674"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="252" y1="173.924" x2="252.75" y2="195.549"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="263.75" y1="175.924" x2="263.125" y2="196.049"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="277" y1="177.549" x2="276.75" y2="195.799"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="290.375" y1="179.299" x2="290.25" y2="197.174"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="303.5" y1="181.049" x2="303.875" y2="197.674"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="318.125" y1="182.674" x2="317.875" y2="197.924"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="333" y1="184.799" x2="333.375" y2="199.299"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="347.375" y1="186.174" x2="347.625" y2="199.549"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="359" y1="186.799" x2="358.875" y2="200.174"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="366.375" y1="187.174" x2="366.25" y2="200.549"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="378.25" y1="187.674" x2="378.875" y2="201.424"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="389.875" y1="188.174" x2="391.75" y2="201.299"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="402.75" y1="188.799" x2="402.625" y2="202.174"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="414.5" y1="189.549" x2="414.5" y2="203.549"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="426.5" y1="189.549" x2="426.5" y2="203.549"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="438.625" y1="190.674" x2="438.75" y2="204.299"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="450.625" y1="191.299" x2="450" y2="205.299"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="463.5" y1="192.674" x2="463.25" y2="206.924"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="475" y1="193.299" x2="476" y2="208.549"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="486.75" y1="193.674" x2="487.75" y2="208.299"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="499" y1="194.299" x2="500.75" y2="208.299"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="511.25" y1="194.799" x2="513" y2="208.049"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="523.875" y1="195.049" x2="525.25" y2="208.299"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="536.75" y1="194.924" x2="537.875" y2="208.424"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="549" y1="195.299" x2="550.125" y2="208.049"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="563.125" y1="195.299" x2="562.625" y2="208.049"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="575.125" y1="195.549" x2="574.625" y2="207.674"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M93.667,182.383
  c0.109,1.366-0.195,2.802-0.254,4.167c-0.06,1.395-0.026,2.8-0.079,4.187c-0.051,1.33-0.34,2.962-0.167,4.229"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M91,182.883
  c-0.012,3.998-0.5,8.102-0.5,12.064c0-0.077,0-0.398,0-0.398"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M88.125,183.924
  c-0.498,2.957-0.521,6.109-0.521,9.11c0,0.837-0.104,1.876-0.104,2.533c0-0.089,0-0.019,0-0.019"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M86,183.966
  c-0.409,0.779-0.16,2.244-0.167,3.163c-0.012,1.697-0.083,3.392-0.083,5.093c0,0.902,0.234,2.735-0.083,3.411"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M83.833,184.216
  c0.001,2.629-0.095,5.271-0.167,7.896c-0.023,0.823-0.041,1.627-0.083,2.441c-0.014,0.267,0,1.284,0,1.079"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M82.083,184.55
  c-0.312,2.61-0.178,5.294-0.334,7.922c-0.064,1.088-0.166,2.504-0.166,3.411"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M80,184.883
  c0.129,3.684,0.03,7.882-0.667,11.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M77.667,185.55
  c0,3.765-0.359,7.514-0.332,11.264c0.014,0.106-0.059-0.075-0.085-0.098"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M75.583,185.633
  c0,3.487-0.083,6.997-0.083,10.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M73.333,186.424
  c0,3.42,0.167,6.59,0.167,10.005c0-0.099,0,0.12,0,0.12"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M71.083,186.55
  c-0.198,2.244,0.01,4.487,0.102,6.731c0.041,0.991-0.086,2.494,0.236,3.448c0.042,0.073-0.003-0.257-0.004-0.347"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M68.75,186.883
  c-0.299,0.889-0.001,2.021,0.083,2.936c0.12,1.293,0.135,2.583,0.186,3.879c0.041,1.034-0.136,2.62,0.236,3.577
  c-0.004-0.101-0.016-0.209-0.004-0.309"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M66.375,187.092
  c-0.337,0.92,0.047,2.292,0.127,3.264c0.096,1.16,0.008,2.395,0.008,3.561c0,1.061-0.01,2.249-0.01,3.281c0-0.077,0-0.648,0-0.648"
  />
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M63.833,187.716
  c-0.622,1.683,0.003,4.472,0.087,6.258c0.057,1.213-0.004,2.443-0.004,3.659"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M61.083,187.633
  c-0.24,0.738-0.058,1.636,0.002,2.395c0.1,1.257,0.114,2.533,0.168,3.793c0.062,1.431-0.004,2.879-0.004,4.313"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M58.292,187.841
  c0.216,0.552,0.122,1.505,0.144,2.165c0.041,1.18,0.029,2.372,0.077,3.552c0.04,0.992-0.012,2.002-0.012,2.993
  c0,0.24,0,0.998,0,0.998"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M55.167,188.383
  c0,3.4,0.333,6.687,0.333,10.064c0-0.077,0,0.102,0,0.102"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M52.5,188.133
  c-0.001,3.398,0.254,6.924,0.519,10.308c0.087-0.077,0.03-0.137,0.065-0.225"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M49.917,188.3
  c0,2.196,0.171,4.373,0.167,6.566c-0.002,1.086-0.208,2.816,0.171,3.826c-0.003-0.075-0.005-0.151-0.004-0.227"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M48,188.049c0,2.065,0.112,4.148,0.249,6.208
  c0.069,1.044,0.131,2.084,0.221,3.126c0.039,0.45-0.055,1.121,0.116,1.529c-0.001-0.038-0.002-0.075-0.002-0.113"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M45.958,188.174
  c0,2.504,0.123,5.001,0.211,7.501c0.026,0.745,0.011,1.499,0.081,2.239c0.027,0.289,0.113,0.726,0,0.968"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M43.75,188.591
  c-0.001,2.485,0.085,4.972,0.083,7.459c0,0.918,0.053,1.829,0.084,2.743c0.005,0.14,0.005,0.733,0.041,0.631"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M41.667,188.757
  c0,2.553,0.164,5.12,0.281,7.671c0.038,0.829,0.096,1.661,0.094,2.489c0,0.215-0.035,0.749,0.042,0.799"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M39.167,189.277
  c0,3.514,0.333,6.999,0.333,10.513c0-0.094,0-0.241,0-0.241"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M36.458,189.34
  c0,3.527,0.042,7.267,0.042,10.787c0-0.054,0-0.578,0-0.578"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M34.125,190.257
  c0.005,2.248,0.389,4.492,0.459,6.742c0.026,0.831,0.292,2.115,0.166,2.842"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M32.458,190.674
  c-0.076,0.096-0.042,0.57-0.042,0.799c0.001,0.596-0.044,1.187-0.042,1.782c0.005,1.256-0.083,2.508-0.083,3.765
  c0,0.734-0.044,1.464-0.042,2.196c0,0.245,0.046,0.756-0.042,0.875"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M30.917,190.966
  c-0.25,0.375-0.131,1.338-0.176,1.799c-0.122,1.253-0.275,2.485-0.333,3.743c-0.045,0.973-0.15,1.952-0.251,2.921
  c-0.028,0.264-0.103,1.229-0.157,1.245"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M28.5,191.757
  c-0.257,3.058-0.702,6.136-0.708,9.209"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M26.417,191.674
  c-0.251,0.367-0.134,1.219-0.175,1.67c-0.118,1.295-0.264,2.584-0.283,3.886c-0.013,0.9-0.115,1.799-0.157,2.696
  c-0.022,0.459,0.02,1.011-0.081,1.447c-0.025-0.034-0.043-0.073-0.054-0.116"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M24.292,191.882
  c-0.186,0.637-0.064,1.471-0.114,2.132c-0.099,1.33-0.177,2.674-0.346,3.996c-0.099,0.771-0.257,1.555-0.292,2.332
  c-0.013,0.286,0.06,0.682-0.04,0.915"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M22.042,192.174
  c0,2.006-0.249,4.007-0.428,6.003c-0.077,0.85,0.026,1.947-0.24,2.753c0.013,0.059-0.026-0.041-0.04-0.048"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M19.75,192.466
  c0.005,2.229-0.142,4.445-0.345,6.67c-0.071,0.784-0.263,1.587-0.238,2.371"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M17.625,192.757
  c0.053,1.727-0.316,3.526-0.428,5.253c-0.067,1.044,0.169,2.477-0.155,3.456"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M15.417,193.174
  c0,1.782-0.147,3.549-0.209,5.329c-0.028,0.78-0.059,1.56-0.092,2.34c-0.014,0.347-0.003,0.834-0.116,1.123"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M12.417,193.466
  c0.001,0.79,0.205,8.646-0.25,8.625"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M9.917,193.924
  c-0.015,0.481,0.075,0.95,0.126,1.427c0.115,1.073,0.082,2.155,0.082,3.232c0,1.107-0.141,2.338,0.083,3.424"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M102.875,103.674l23.375,7
  l15.75,3l4.75,1.625c0,0,13.375,4.25,14.125,4.625s23.625,9.375,46,8.25c0,0,18.125-2.5,18.625-2.625s7.875-1.125,8.625-1
  s15.5,0.125,16.25,0.125s8.5,2.875,8.5,2.875l8,5.25l14.25,1.625l11.375,0.5l12.125-4.25c0,0,11.25,13.5,12.625,17.625l3.75,2.875
  c0,0,4.75,0.75,5.125,0.75s3.625,0.125,3.625,0.125l2.625,1.5l3.25,0.625l7.125-1.125l4.625-3.125l-9-9.75l-3.375,1.5
  c0,0-2.25,0.75-2.625,0.75s-5,0.375-5,0.375l-1.25-4l-5.25-5.5l-6.75-8.625l-3.375-7.125l-19.25-0.625l-2.125-4.75
  c0,0-3.625-4.875-3.625-5.25s-1.125-3.625-1.125-3.625L282,97.049l-3.375-6l-1.75-2.375L273,86.799c0,0-14.875-6.625-28.125-7.75
  l-25.375-0.75l-9.875-0.375l-8.125-1.375l-11.75-3.75l-11.375-6.125l-5-2L161,58.924c0,0-48.875-32.75-80.625-19.875"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M82.375,42.424
  c0,0,15,1.375,22.75,15.25l-1.875,1.875l-2.625,0.25l-1.75-2.125l-4.75-7.125"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M103.875,59.549
  l5.625,8.375l2.625,4.25l0.125,1.625l4.125,7.375c0,0,0.25,2.375-2.625,4.375l-2.875,1.5l-2.25,0.25l-8.25-6.625
  c0,0-12.75-14.75-15.75-16.625"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M91.25,48.549
  c0,0-2,3-2,3.375S89,57.299,89,57.299l-1.875-1.375L86,51.424l1.625-2.5"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M84.875,55.174l4,4.875
  c0,0,9.25,10.5,9.75,11s8.25,8.875,8.25,8.875l1.875,1.125l2.5,0.75l0.875,1.875l-1.625,1.5"/>
<polyline fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="75.313,58.361 
  80.094,65.205 81.75,67.549 84,67.549 88,70.924 89.75,71.674 100.75,83.299 "/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M75.25,41.174
  c0,0-11.375,3.875-5.5,13.5c0,0,1.25,3.125,11,4.25c0,0-9.5-9.75-5.25-14"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M80,44.674
  c0,0,3,6.875,0.375,11.75"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M67.625,51.424
  c0,0-6.5-4.875-6.5-9.75c0,0,5.5,3.375,7.125,6.125"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M76.5,42.799
  c0,0-3.375-4.25-3.75-6.625c0,0-1-4.25-1.5-5.875c0,0,6.75,2.125,9.25,10.375"/>
<polygon fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="93.75,54.799 
  92.375,55.424 92.25,56.549 92.625,57.049 93.625,56.924 94.375,56.174 "/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M107.25,57.049
  c0.375,0,30.875,16.25,30.875,16.25s-2,4-0.375,9.375c0,0-4.625,1.75-5.25,9.875l-19.25,0.875c0,0-12.625-0.75-14.75-0.625
  s-3.875-0.625-4.625-0.375S91.5,93.049,91,94.049s-1.125,3.5-1.25,4.125s-0.5,2.25-0.75,2.875s-0.625,3-0.75,4s0.875,3,1.5,4
  s20.5,14.75,22.5,16.5s4.625,3.875,5.125,4s9.625,4,11.25,5.5s3.625,2.125,4,2.125s-1.625-10.75-3.75-13.875c0,0-2.5-0.25-3.25-0.5
  s-2.75-2.875-3-3.25s-1.125-2.625-1.5-2.625s-4.875-1-6.25-1.5s-6.375-5.75-6.75-6.125s-4.75-5.375-4.75-5.375"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="112.25" y1="71.049" x2="131.25" y2="91.674"/>
<polyline fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="129.875,123.174 
  136.625,122.299 145.875,137.049 133.125,136.549 "/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M134.75,95.674
  c0.125,0.375,2,6.875,2.125,7.25s6.375,7.125,6.875,7.125s3.5,0.25,3.5,0.25s10.625,0.75,16.125-4.25
  c0.25-0.625,9.5-11.875,9.625-21.625"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M99.25,100.174
  c0,0,15.5,2.75,20.75,5.125s14.5,3.25,17.75-0.75"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M205,121.924
  c0,0,13.625-1.25,21.625-3s20.5-2.625,27.625,1.125"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M243.375,83.299
  c0,0,1.25,13.125,9.625,20.5c0,0,3.25,16,6.25,18.75c0.375-0.25,2.125,1.125,3.625,1.25s4.125,0.75,4.125,0.75"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M260.125,103.424
  l8.375,23.5c0,0,2.5,2.5,3.25,2.5s4.625-1.125,4.625-1.125l19.5,2.125l5.125-0.125"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M309.75,121.924
  c0,0-8,2.75-5.125,8.75"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M277.625,88.299
  l12.625-2.875c0,0,16.625,7.5,17,8s5.75,6.125,5.875,7.25s1.5,4.75,1.5,5.875s0,2.375,2.125,4s5,3.125,5.625,3.125
  s2.375-0.5,2.375-0.125s1,1.625,1.125,2s3.375,1.625,3.125,2s-4.75,5.75-5.5,6.125s-4,1.625-5,2s-3.5,0-3.5,0"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M287.125,100.174
  c0.375,0.25,4.5,2.625,4.875,2.75s2.875,1.375,4.625,1.5s12,0.75,12,0.75l1.5,5.75"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M295,108.299
  c0.875,0.5,5,2.625,5,2.625l6.5,2c0,0,7.375,2.625,7.5,3s1.75,2.875,2.25,3.25s8.375-0.625,8.375-0.625"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M112.875,107.174
  c0.625,0.375,20,10.75,20.375,10.75s5.25,0.375,5.625,0.375s6.125,2.875,6.125,2.875s28.375,7.625,29.375,8s23.25,5.625,40.625,2.25
  l17.25-3.125l10.25-3.125"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M186.167,132.383
  c-1.167,0.166-23.333,10.5-25,11s-30.333,13.167-32,14s-13.667,7-14.833,7.333s-10,5.333-10,5.333l-7.833,11.5"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="139.5" y1="125.549" x2="145" y2="121.174"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M320.167,131.216
  c0,0,20.666,7.333,29.333,8.833s13.667,2.333,13.667,2.333s7.166,0.167,7.666,0.167s7.834,2.333,7.834,2.333l7.833,1.334"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M321.167,125.382
  c0,0,12.833,6,13.333,6s11,3.334,11,3.334l6.167,0.666"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M163.375,106.049
  c0,0,1.625-4.25,11.125-2s11.25,4.75,23,4.75s16.5,0.25,23.5-0.75s12.25-2.25,16-2.25s11.5,1.5,11.5,3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="143.417" y1="159.883" x2="140.667" y2="164.383"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="147.333" y1="156.967" x2="144.167" y2="163.633"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="150.833" y1="155.133" x2="147.5" y2="163.05"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="154.75" y1="153.717" x2="151.583" y2="162.133"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="158.417" y1="152.133" x2="155.417" y2="161.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="161.917" y1="150.883" x2="159.333" y2="160.383"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="165.833" y1="149.467" x2="163.333" y2="159.633"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="169.5" y1="148.05" x2="166.917" y2="158.8"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="173.25" y1="146.633" x2="171" y2="157.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="177.25" y1="145.217" x2="174.917" y2="156.717"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="181.083" y1="143.55" x2="178.667" y2="156.467"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="184.667" y1="142.55" x2="182.083" y2="157.133"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="188.333" y1="141.55" x2="185.917" y2="157.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="192.333" y1="140.3" x2="189.75" y2="157.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="196.167" y1="138.3" x2="193.5" y2="157.217"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="200" y1="136.633" x2="199.167" y2="140.217"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="204.083" y1="136.467" x2="203.083" y2="139.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="199.083" y1="141.717" x2="197.083" y2="157.383"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="202.25" y1="142.383" x2="200.083" y2="157.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="206.583" y1="140.8" x2="204.083" y2="157.217"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="210" y1="138.8" x2="207.917" y2="157.133"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="214.25" y1="137.383" x2="211.667" y2="157.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="218" y1="136.883" x2="215.75" y2="157.05"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="221.917" y1="136.383" x2="219.75" y2="156.883"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="225.833" y1="136.3" x2="223.833" y2="157.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="229.667" y1="136.05" x2="227.917" y2="157.383"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="233.917" y1="136.133" x2="232.083" y2="157.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="237.917" y1="136.383" x2="236.167" y2="157.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="241.75" y1="136.8" x2="240.417" y2="156.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="245.583" y1="137.216" x2="244" y2="157.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="249" y1="137.466" x2="247.583" y2="156.883"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="252.417" y1="138.05" x2="251.167" y2="156.8"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="255.833" y1="138.8" x2="254.583" y2="156.716"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="259.667" y1="139.383" x2="258.583" y2="156.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="263.083" y1="140.133" x2="262.25" y2="155.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="266" y1="140.883" x2="265.083" y2="155.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="269.75" y1="144.05" x2="268.583" y2="155.716"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="273" y1="142.8" x2="272.583" y2="146.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="275.583" y1="143.133" x2="274.917" y2="145.133"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="272.583" y1="147.05" x2="271.333" y2="156.383"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="275.667" y1="148.133" x2="274.583" y2="156.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="278.417" y1="149.133" x2="277.583" y2="156.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="281.5" y1="150.216" x2="280.5" y2="157.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="284" y1="151.216" x2="283" y2="157.8"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="286.917" y1="152.216" x2="286" y2="158.05"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="289.917" y1="153.216" x2="288.667" y2="158.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="292.917" y1="154.383" x2="291.917" y2="158.8"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="295.334" y1="155.133" x2="294.5" y2="159.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="297.917" y1="155.716" x2="297.167" y2="159.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="300.584" y1="156.8" x2="299.75" y2="160.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="303.5" y1="157.716" x2="302.5" y2="160.383"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="306.334" y1="158.716" x2="305.584" y2="161.633"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="308.917" y1="159.05" x2="308.084" y2="161.883"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="311.75" y1="159.716" x2="311.167" y2="162.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="314.917" y1="160.133" x2="314.084" y2="162.883"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="317.584" y1="160.55" x2="317" y2="163.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="320.167" y1="160.966" x2="319" y2="163.966"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="323.25" y1="161.466" x2="322.25" y2="164.05"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="326.917" y1="161.883" x2="326.167" y2="164.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="329.584" y1="162.216" x2="329" y2="164.383"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="332.75" y1="162.633" x2="332" y2="164.883"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="336.084" y1="162.883" x2="335.25" y2="165.133"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="338.917" y1="163.383" x2="338.167" y2="165.883"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="341.584" y1="163.8" x2="340.75" y2="166.383"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="344.167" y1="164.3" x2="343.25" y2="166.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="347.75" y1="164.633" x2="346.584" y2="167.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="351.084" y1="165.05" x2="350.25" y2="167.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="354.417" y1="165.216" x2="353.667" y2="167.716"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="358.084" y1="165.466" x2="357.167" y2="168.05"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="361.417" y1="165.55" x2="360.584" y2="167.966"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="364.834" y1="165.883" x2="363.334" y2="168.8"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="368" y1="165.466" x2="366.834" y2="168.716"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="371" y1="165.05" x2="369.834" y2="168.716"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="374.5" y1="165.05" x2="373.584" y2="169.133"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="377.5" y1="164.8" x2="376.084" y2="169.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="418.916" y1="177.216" x2="415.833" y2="184.8"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="421.416" y1="177.216" x2="418.833" y2="184.883"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="424.666" y1="177.216" x2="421.833" y2="185.633"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="429.5" y1="176.8" x2="426.833" y2="185.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="432.333" y1="176.633" x2="430" y2="185.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="435.083" y1="176.216" x2="433.083" y2="185.716"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="438.25" y1="176.05" x2="436.583" y2="185.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="441.416" y1="175.716" x2="440.166" y2="185.8"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="444.583" y1="175.3" x2="443" y2="185.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="448.833" y1="174.55" x2="446.25" y2="185.633"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="456.583" y1="173.633" x2="454" y2="185.008"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="459.833" y1="173.175" x2="457.083" y2="184.716"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="463.25" y1="172.716" x2="460.75" y2="185.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="466.833" y1="173.466" x2="464.5" y2="185.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="470.416" y1="174.216" x2="467.75" y2="185.883"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="473.916" y1="174.966" x2="471.333" y2="186.133"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="477.666" y1="175.466" x2="474.916" y2="186.716"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="481.083" y1="176.133" x2="479.166" y2="185.466"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="484.166" y1="177.966" x2="482.333" y2="185.633"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="486" y1="183.05" x2="485.083" y2="185.966"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="490.5" y1="177.383" x2="489.833" y2="180.3"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="494.166" y1="177.55" x2="492.333" y2="183.966"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="497.166" y1="178.175" x2="495.666" y2="183.716"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="500" y1="178.383" x2="498.666" y2="183.633"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="503.166" y1="178.883" x2="502.416" y2="183.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="506.5" y1="179.3" x2="505" y2="183.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="509.75" y1="179.55" x2="508.25" y2="183.966"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="512.666" y1="181.05" x2="511.666" y2="183.883"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="526.25" y1="191.633" x2="525.166" y2="194.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="531" y1="189.716" x2="529.166" y2="194.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="534.833" y1="189.3" x2="533.416" y2="193.8"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="539.083" y1="189.05" x2="537.166" y2="194.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="542.916" y1="189.05" x2="540.75" y2="194.55"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="546.666" y1="189.216" x2="544.833" y2="194.216"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="550.25" y1="190.633" x2="548.666" y2="194.633"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="552.75" y1="192.05" x2="551.583" y2="194.55"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M478.167,81.632c0,0,0.166,2,0.333,1.667
  s0.917-0.833,2.25-1.25s2.334-0.749,3.667-0.583s2.832,0.166,2.916,0.416s1,1.917,1,1.917"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M478.333,84.549l-1.083,1.667
  c0,0-4.083-3.417,1.5-8.667c0,0,3.333-2.75,7.083-1.167c0.25-0.083,3,1.75,3.667,5.5"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="481.167,84.216 482.833,82.882 
  483.333,85.132 484.083,85.299 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="484.833" y1="83.132" x2="486.083" y2="83.799"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="481.75,87.049 482.917,86.299 
  484.583,86.466 485.667,87.299 "/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="478.417,87.382 479.75,89.299 
  481.25,90.049 483.833,89.716 485.917,89.549 486.917,88.632 "/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="460.813,87.674 462.25,87.049 
  464,87.049 463.375,89.799 "/>
<line  stroke-linecap="round" stroke-linejoin="round" x1="397.25" y1="63.174" x2="399.875" y2="63.924"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="427" y1="176.799" x2="424.5" y2="185.799"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="451.75" y1="174.049" x2="449.375" y2="184.799"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="454.625" y1="173.424" x2="451.875" y2="184.924"/>
    </symbol>
    <!-- Sochi -->
    <symbol viewBox="-6.5 -40.25 600 400" id="icon-sochi">
      <polygon fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" points="292,0.75 
  290.625,4.625 286.125,5.375 289.625,8.375 288.875,12.375 292.375,9.375 295.75,12.375 295.25,7.125 298.75,4.25 294,4.625 "/>
<path fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M292.375,9.375
  c-0.25,0.75-0.25,13.5-0.25,13.5"/>
<circle fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" cx="292.063" cy="24.938" r="2.063"/>
<line fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="291.25" y1="27" x2="289.75" y2="69.875"/>
<line fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="293.125" y1="27" x2="294.75" y2="69.375"/>
<path fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M288.875,73.25
  c0,0-0.25-5.125,3.5-5c0,0,2.75-0.875,3,5.125"/>
<path fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M287.625,74.5
  c0,0,4.375-2,9.5-0.625"/>
<line fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="289.125" y1="74.375" x2="287" y2="127.75"/>
<line fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="291.5" y1="73.875" x2="290.625" y2="127.5"/>
<line fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" x1="295.625" y1="74" x2="298" y2="127.25"/>
<path fill="none"  stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" d="M285.25,137.375
  L285.125,128c0,0,8.75-1.5,14.75-0.125L300,137"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="278.063,148.313 276,145.75 
  276,139.25 310,139.25 310,143.75 307.5,148.125 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="278.5" y1="144.25" x2="306.5" y2="144.25"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="279.5" y1="146.75" x2="306.5" y2="146.75"/>
<rect x="278.5" y="148.75" fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="2" height="21"/>
<rect x="286.5" y="148.75" fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="2" height="21"/>
<rect x="295.5" y="148.75" fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="2" height="21"/>
<rect x="304.5" y="148.75" fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="2" height="21"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M290,137.75v-7c0,0,3-3.375,5,0.125v7.875"/>
<rect x="276.5" y="169.75" fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" width="32" height="5"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="267,195.75 267,175.25 318,175.25 
  318,194.75 "/>
<circle fill="none"  stroke-linecap="round" stroke-linejoin="round" cx="275.25" cy="181.75" r="1.625"/>
<circle fill="none"  stroke-linecap="round" stroke-linejoin="round" cx="293.25" cy="181.75" r="1.625"/>
<circle fill="none"  stroke-linecap="round" stroke-linejoin="round" cx="310.25" cy="181.75" r="1.625"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M269.5,174.875l0.188-2.063l-0.406-1.219
  l-0.516-1.609l0.117-1.492l-0.379-1.121l0.123-2.123l0.249-1.624l1.375,0.126c0,0,0.375-1,0.125-1.375s0.125-1.5,0.125-1.5
  l1.125-0.625c0,0,0.875-0.25,0.875,0.125s0.625,1,0.625,1L273,163.75v1l-0.75,2.25l0.75,0.75l0.75,1.375l-0.5,1.375l0.25,1.25l0.5,1
  l-0.25,1.5"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M316.004,174.875l-0.188-2.063l0.406-1.219
  l0.516-1.609l-0.117-1.492l0.379-1.121l-0.123-2.123l-0.249-1.624l-1.374,0.126c0,0-0.375-1-0.125-1.375s-0.125-1.5-0.125-1.5
  l-1.125-0.625c0,0-0.875-0.25-0.875,0.125s-0.377,1-0.377,1L313,163.75v1l0.502,2.25l-0.874,0.75l-0.812,1.375l0.469,1.375
  l-0.266,1.25l-0.508,1l0.246,1.5"/>
<rect x="244.5" y="197.75" fill="none"  stroke-width="2" width="93" height="2"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M248.5,194.375l-0.875-1.625l0.375-1.188
  l0.875-1.031c0,0-1.125-0.391-1.125-0.766s0.75-0.633,0.75-0.633l1.5-0.883h0.5l-1.25-3c1.125-0.5,3.625-0.875,3.625-0.875
  c0.25,1.25,0,2.375,0,2.375l0.625,1c0.625,0.75,1.125,0.75,1.5,1.125s3.375-1.125,3.25-0.75s0.75,1.125,0.75,1.125
  s-0.75,0.5-0.5,1.125s0.875,0.75,0.875,1.25s0.5,1.25,0.875,1.375s2.25,0.375,2.25,0.375s-0.25,1,0.125,1S264,194,264,194
  c0.375,0.375,0.75,1.75,0.75,1.75"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M335.875,194.375l0.875-1.625l-0.375-1.188
  l-0.875-1.031c0,0,1.125-0.391,1.125-0.766s-0.75-0.633-0.75-0.633l-1.5-0.883h-0.5l1.25-3c-1.125-0.5-3.625-0.875-3.625-0.875
  c-0.25,1.25,0,2.375,0,2.375l-0.625,1c-0.625,0.75-1.125,0.75-1.5,1.125s-3.375-1.125-3.25-0.75s-0.75,1.125-0.75,1.125
  s0.75,0.5,0.5,1.125s-0.875,0.75-0.875,1.25s-0.5,1.25-0.875,1.375s-2.25,0.375-2.25,0.375s0.25,1-0.125,1S320.375,194,320.375,194
  c-0.375,0.375-0.75,1.75-0.75,1.75"/>
<rect x="271" y="185.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="9" height="11"/>
<rect x="273" y="189.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="5" height="5"/>
<rect x="288" y="185.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="9" height="11"/>
<rect x="290" y="189.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="5" height="5"/>
<rect x="306" y="185.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="9" height="11"/>
<rect x="308" y="189.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="5" height="5"/>
<path fill="none"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M526.5,221.25H406.125
  l-112.766-21.344l-110.391,23.656c0,0-113.031,0.125-113.531,0.25"/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="70,222.5 85.5,212.125 234.625,211 
  "/>
<polyline fill="none"  stroke-linecap="round" stroke-linejoin="round" points="350,209.75 509.625,207.875 
  527,220.375 "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M290.938,123.75
  c0.188,0,3.625,2.875,3.625,2.875l3.063-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M290.938,120.75
  c0.188,0,3.625,2.875,3.625,2.875l3.063-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M290.938,117.75
  c0.188,0,3.625,2.875,3.625,2.875l3.063-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M290.938,114.75
  c0.188,0,3.625,2.875,3.625,2.875l3.063-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M290.938,111.75
  c0.175,0,3.388,2.875,3.388,2.875l2.862-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M290.938,108.75
  c0.175,0,3.388,2.875,3.388,2.875l2.862-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M290.938,105.75
  c0.175,0,3.388,2.875,3.388,2.875l2.862-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.313,102.75
  c0.156,0,3.015,2.875,3.015,2.875l2.547-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.313,99.75
  c0.156,0,3.015,2.875,3.015,2.875l2.547-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.313,96.75
  c0.156,0,3.015,2.875,3.015,2.875l2.547-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.5,93.75c0.14,0,2.71,2.875,2.71,2.875
  l2.29-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.5,90.75c0.14,0,2.71,2.875,2.71,2.875
  l2.29-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.5,87.75c0.14,0,2.71,2.875,2.71,2.875
  l2.29-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.5,84.75
  c0.128,0,2.473,2.875,2.473,2.875l2.089-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.5,81.75
  c0.128,0,2.473,2.875,2.473,2.875l2.089-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.5,78.75
  c0.128,0,2.473,2.875,2.473,2.875l2.089-2.813"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M291.5,74.75
  c0.128,0,2.473,2.875,2.473,2.875l2.089-2.813"/>
<line fill="none"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" x1="79.75" y1="258" x2="180" y2="257"/>
<line fill="none"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" x1="1.75" y1="259" x2="69.375" y2="258.125"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="207" y1="256.5" x2="382.5" y2="255.5"/>
<line fill="none"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" x1="407.75" y1="255.75" x2="574.75" y2="255"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="73.5" y1="223.75" x2="73.5" y2="237.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="522.5" y1="220.75" x2="522.5" y2="253.75"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M84.5,257.75v-23
  c0,0,5.167-2.667,11-0.083v22.083"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M102.5,257.75v-23
  c0,0,5.167-2.667,11-0.083v22.083"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M119.5,256.75v-22.077
  c0,0,5.167-2.496,11-0.078v21.155"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M137.5,256.75v-22.029
  c0,0,5.167-2.603,11-0.081v22.11"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M155.5,256.75v-22
  c0,0,5.167-2.667,11-0.083v22.083"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M173.5,255.75v-22
  c0,0,5.167-2.667,11-0.083v22.083"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M510.5,254.75v-23
  c0,0-6-2.667-12-0.083v22.083"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M491.5,254.75v-23
  c0,0-6-2.667-12-0.083v22.083"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M473.5,254.75v-23.028
  c0,0-6-2.604-12-0.081v22.109"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M454.5,254.75v-22.979
  c0,0-6-2.715-12-0.084v23.063"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M435.5,254.75v-22.931
  c0,0-6-2.82-12-0.088v23.019"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M416.5,254.75v-23.913
  c0,0-5.836-2.858-12-0.089v24.002"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M207.5,255.75v-22
  c0,0,1.167-5.75,7.083-5.75s6.917,6.416,6.917,6.416v21.334"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M230.5,255.75v-27
  c0,0,1.167-5.75,7.083-5.75s6.917,6.416,6.917,6.416v26.334"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M255.5,255.75v-31
  c0,0,1.086-5.75,8.099-5.75c7.014,0,7.901,6.416,7.901,6.416v30.334"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M379.5,255.75v-22
  c0,0-1.167-5.75-7.084-5.75c-5.916,0-6.916,6.416-6.916,6.416v21.334"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M356.5,255.75v-27
  c0,0-1.167-5.75-7.084-5.75c-5.916,0-6.916,6.416-6.916,6.416v26.334"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M331.5,255.75v-31
  c0,0-1.086-5.75-8.099-5.75c-7.014,0-7.901,6.416-7.901,6.416v30.334"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M282.5,255.75v-35
  c0,0,1.277-7.75,10.631-7.75c9.357,0,10.369,8.416,10.369,8.416v34.334"/>
<circle fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" cx="392.583" cy="230.5" r="2.417"/>
<circle fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" cx="196.583" cy="230.5" r="2.417"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M224.333,234.917c0,0,1.75,0.666,2.667,0
  l-1.333-4.583L224.333,234.917z"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M248.333,234.917c0,0,1.75,0.666,2.667,0
  l-1.333-4.583L248.333,234.917z"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M275.333,234.917c0,0,1.75,0.666,2.667,0
  l-1.333-4.583L275.333,234.917z"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M308.334,234.917c0,0,1.749,0.666,2.666,0
  l-1.333-4.583L308.334,234.917z"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M335.334,234.917c0,0,1.749,0.666,2.666,0
  l-1.333-4.583L335.334,234.917z"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M360.334,234.917c0,0,1.749,0.666,2.666,0
  l-1.333-4.583L360.334,234.917z"/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="180.833,255.916 181.667,258.25 
  205.5,258.25 206.583,255.833 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="182" y1="275.75" x2="182" y2="258.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="205" y1="258.75" x2="205" y2="274.75"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M189,274.75v-11c0,0,0.375-3.5,4.875-3.5
  s4.125,3.583,4.125,3.583v10.917"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M194,273.75v-10c0,0,0.417-1.751,3.417-1.834
  "/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="407.584,254.916 406.752,257.25 
  382.918,257.25 381.834,254.833 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="406" y1="274.75" x2="406" y2="257.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="383" y1="257.75" x2="383" y2="273.75"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M399,273.75v-11c0,0-0.375-3.5-4.875-3.5
  S390,262.833,390,262.833v10.917"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M394,272.75v-10c0,0-0.416-1.751-3.416-1.834
  "/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M211,273.75v-10c0,0,0.083-3.167,4.083-3.167
  S219,263.5,219,263.5v10.25"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M368,273.75v-10c0,0,0.084-3.167,4.084-3.167
  S376,263.5,376,263.5v10.25"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M234,273.75v-10c0,0,0.176-3.167,4.592-3.167
  c4.417,0,4.408,2.917,4.408,2.917v10.25"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M345,273.75v-10c0,0,0.176-3.167,4.592-3.167
  c4.417,0,4.408,2.917,4.408,2.917v10.25"/>
<rect x="258" y="260.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="5" height="7"/>
<rect x="270" y="260.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="5" height="7"/>
<rect x="311" y="260.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="5" height="7"/>
<rect x="324" y="260.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="5" height="7"/>
<polyline fill="none"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="84.25,275.75 
  277.25,274.5 576.5,273.25 "/>
<polyline fill="none"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="2,287.25 
  277.25,285.5 351.5,285 "/>
<rect x="212" y="242.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="14"/>
<rect x="235" y="242.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="9" height="14"/>
<rect x="258" y="242.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="14" height="14"/>
<rect x="286" y="241.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="14" height="14"/>
<rect x="315" y="241.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="14" height="14"/>
<rect x="343" y="242.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="9" height="13"/>
<rect x="366" y="242.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="9" height="13"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="212.5" y1="246.25" x2="218.5" y2="246.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="215" y1="246.75" x2="215" y2="255.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="235.5" y1="246.25" x2="243.5" y2="246.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="239" y1="246.75" x2="239" y2="255.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="258.5" y1="246.25" x2="270.5" y2="246.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="286.5" y1="246.25" x2="299.5" y2="246.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="315.5" y1="246.25" x2="328.5" y2="246.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="343.5" y1="245.25" x2="351.5" y2="245.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="365.5" y1="246.25" x2="374.5" y2="246.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="261" y1="242.75" x2="261" y2="255.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="268" y1="242.75" x2="268" y2="255.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="290" y1="241.75" x2="290" y2="254.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="297" y1="241.75" x2="297" y2="254.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="319" y1="241.75" x2="319" y2="255.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="325" y1="241.75" x2="325" y2="255.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="348" y1="242.75" x2="348" y2="255.75"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="370" y1="242.75" x2="370" y2="255.75"/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="220,236.25 212,236.25 212,230.25 
  216.083,230.25 220,230.25 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="216" y1="235.75" x2="216" y2="230.75"/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="243,236.25 235,236.25 235,230.25 
  239.083,230.25 243,230.25 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="239" y1="235.75" x2="239" y2="230.75"/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="351,235.25 343,235.25 343,229.25 
  347.084,229.25 351,229.25 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="347" y1="234.75" x2="347" y2="229.75"/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="375,235.25 368,235.25 368,229.25 
  371.451,229.25 375,229.25 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="371" y1="234.75" x2="371" y2="229.75"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M258,239.25v-6.5c0,0,0.667-4.418,6-4.334
  s6,4.667,6,4.667v6.167H258z"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="264" y1="228.75" x2="264" y2="238.75"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M286,239.25v-6.5c0,0,0.741-4.418,7-4.334
  c6.258,0.084,7,4.667,7,4.667v6.167H286z"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="293" y1="228.75" x2="293" y2="238.75"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M316,238.25v-6.5c0,0,0.539-4.418,6.5-4.334
  c5.959,0.084,6.5,4.667,6.5,4.667v6.167H316z"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="322" y1="227.75" x2="322" y2="237.75"/>
<rect x="9" y="259.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="9" y="274.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="11" y="261.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="13"/>
<rect x="29" y="259.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="29" y="274.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="31" y="261.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="13"/>
<rect x="48" y="259.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="48" y="273.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="50" y="261.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="12"/>
<rect x="90" y="259.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="90" y="273.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="92" y="261.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="12"/>
<rect x="109" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="109" y="273.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="111" y="260.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="13"/>
<rect x="127" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="127" y="273.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="129" y="260.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="13"/>
<rect x="145" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="145" y="273.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="147" y="260.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="13"/>
<rect x="164" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="164" y="273.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="166" y="260.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="13"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M121,274.75v-11c0,0,1.125-4.125,7.875-1.75"
  />
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M103,274.75v-11c0,0,1.125-4.125,7.875-1.75"
  />
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M139,274.75v-11c0,0,1.125-4.125,7.875-1.75"
  />
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M158,274.75v-11c0,0,1.125-4.125,7.875-1.75"
  />
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M175,274.75v-11c0,0,0.946-4.125,6.625-1.75"
  />
<rect x="564" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="564" y="272.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="1"/>
<rect x="566" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="14"/>
<rect x="545" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="6" height="2"/>
<rect x="545" y="272.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="6" height="1"/>
<rect x="547" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="2" height="14"/>
<rect x="527" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="6" height="2"/>
<rect x="527" y="271.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="6" height="1"/>
<rect x="529" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="2" height="13"/>
<rect x="509" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="509" y="271.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="1"/>
<rect x="511" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="13"/>
<rect x="502" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="502" y="271.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="1"/>
<rect x="504" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="13"/>
<rect x="487" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="487" y="271.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="1"/>
<rect x="489" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="13"/>
<rect x="469" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="6" height="2"/>
<rect x="469" y="272.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="6" height="1"/>
<rect x="471" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="2" height="14"/>
<rect x="452" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="452" y="272.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="1"/>
<rect x="454" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="14"/>
<rect x="435" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="2"/>
<rect x="435" y="272.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="7" height="1"/>
<rect x="437" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="3" height="14"/>
<rect x="417" y="256.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="6" height="2"/>
<rect x="417" y="272.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="6" height="1"/>
<rect x="419" y="258.25" fill="none"  stroke-linecap="round" stroke-linejoin="round" width="2" height="14"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M465,272.75v-11.553
  c0,0-1.069-4.779-7.495-2.174"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M481,272.75v-11.553
  c0,0-1.071-4.779-7.495-2.174"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M447,272.75v-11.528
  c0,0-1.07-4.882-7.494-2.254"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M429,272.75v-11.528
  c0,0-1.07-4.882-7.494-2.254"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M413,273.75v-12.403
  c0,0-0.9-4.897-6.306-2.158"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M277.25,139.125l0.125-1.375
  c0.25-0.625,0.25-1.5,0.25-1.5L277.5,135l0.125-0.875l0.25-1.25l-0.375-1.25L278,131l-0.25-1l0.5-2.125c0,0,1-1.125,0.875-0.625
  s0.25,2.875,0.25,3.25s0.125,2.625,0.125,2.625l-0.375,1.625L279,135.875l0.125,1.125c0.375,0.75,0.625,2.125,0.625,2.125"/>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M305.25,139.125l0.125-1.375
  c0.25-0.625,0.25-1.5,0.25-1.5L305.5,135l0.125-0.875l0.25-1.25l-0.375-1.25L306,131l-0.25-1l0.5-2.125c0,0,1-1.125,0.875-0.625
  s0.25,2.875,0.25,3.25s0.125,2.625,0.125,2.625l-0.375,1.625L307,135.875l0.125,1.125c0.375,0.75,0.625,2.125,0.625,2.125"/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="289,274.25 284,274.25 284,264.251 
  284,260.25 289,260.25 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="288.5" y1="264.25" x2="284.5" y2="264.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="287" y1="273.75" x2="287" y2="264.75"/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="296,274.25 291,274.25 291,264.251 
  291,260.25 296,260.25 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="295.5" y1="264.25" x2="291.5" y2="264.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="294" y1="273.75" x2="294" y2="264.75"/>
<polygon fill="none"  stroke-linecap="round" stroke-linejoin="round" points="303,274.25 298,274.25 298,264.251 
  298,260.25 303,260.25 "/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="302.5" y1="264.25" x2="298.5" y2="264.25"/>
<line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="301" y1="273.75" x2="301" y2="264.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="253" y1="344.75" x2="288.25" y2="285.5"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="301.75" y1="285.5" x2="330.5" y2="346.25"/>
<g>
  <defs>
    <polygon id="SVGID_4_" points="343.875,209.125 509.625,207.875 525.875,219.875 406.125,220.25     "/>
  </defs>
  <clipPath id="SVGID_5_">
    <use xlink:href="#SVGID_4_"  overflow="visible"/>
  </clipPath>
  <g clip-path="url(#SVGID_5_)">
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="334.206" y1="200.75" x2="361.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="337.206" y1="200.75" x2="364.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="340.206" y1="200.75" x2="367.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="343.206" y1="200.75" x2="370.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="346.206" y1="200.75" x2="373.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="349.206" y1="200.75" x2="376.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="352.206" y1="200.75" x2="379.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="355.206" y1="200.75" x2="382.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="358.206" y1="200.75" x2="385.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="361.206" y1="200.75" x2="388.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="364.206" y1="200.75" x2="391.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="367.206" y1="200.75" x2="394.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="370.206" y1="200.75" x2="397.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="373.206" y1="200.75" x2="400.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="376.206" y1="200.75" x2="403.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="379.206" y1="200.75" x2="406.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="382.206" y1="200.75" x2="409.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="385.206" y1="200.75" x2="412.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="388.206" y1="200.75" x2="415.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="391.206" y1="200.75" x2="418.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="394.206" y1="200.75" x2="421.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="397.206" y1="200.75" x2="424.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="400.206" y1="200.75" x2="427.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="403.206" y1="200.75" x2="430.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="406.206" y1="200.75" x2="433.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="409.206" y1="200.75" x2="436.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="412.206" y1="200.75" x2="439.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="415.206" y1="200.75" x2="442.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="418.206" y1="200.75" x2="445.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="421.206" y1="200.75" x2="448.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="424.206" y1="200.75" x2="451.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="427.206" y1="200.75" x2="454.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="430.206" y1="200.75" x2="457.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="433.206" y1="200.75" x2="460.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="436.206" y1="200.75" x2="463.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="439.206" y1="200.75" x2="466.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="442.206" y1="200.75" x2="469.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="445.206" y1="200.75" x2="472.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="448.206" y1="200.75" x2="475.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="451.206" y1="200.75" x2="478.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="454.206" y1="200.75" x2="481.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="457.206" y1="200.75" x2="484.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="460.206" y1="200.75" x2="487.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="463.206" y1="200.75" x2="490.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="466.206" y1="200.75" x2="493.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="469.206" y1="200.75" x2="496.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="472.206" y1="200.75" x2="499.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="475.206" y1="200.75" x2="502.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="478.206" y1="200.75" x2="505.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="481.206" y1="200.75" x2="508.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="484.206" y1="200.75" x2="511.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="487.206" y1="200.75" x2="514.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="490.206" y1="200.75" x2="517.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="493.206" y1="200.75" x2="520.794" y2="220.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="496.206" y1="200.75" x2="523.794" y2="220.75"/>
  </g>
</g>
<g>
  <defs>
    <polygon id="SVGID_3_" points="85.5,212.125 236.875,211.25 182.969,223.438 69,223.25    "/>
  </defs>
  <clipPath id="SVGID_6_">
    <use xlink:href="#SVGID_3_"  overflow="visible"/>
  </clipPath>
  <g clip-path="url(#SVGID_6_)">
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="259.492" y1="205.75" x2="230.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="256.492" y1="205.75" x2="227.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="253.492" y1="205.75" x2="224.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="250.492" y1="205.75" x2="221.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="247.492" y1="205.75" x2="218.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="244.492" y1="205.75" x2="215.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="241.492" y1="205.75" x2="212.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="238.492" y1="205.75" x2="209.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="235.492" y1="205.75" x2="206.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="232.492" y1="205.75" x2="203.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="229.492" y1="205.75" x2="200.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="226.492" y1="205.75" x2="197.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="223.492" y1="205.75" x2="194.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="220.492" y1="205.75" x2="191.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="217.492" y1="205.75" x2="188.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="214.492" y1="205.75" x2="185.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="211.492" y1="205.75" x2="182.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="208.492" y1="205.75" x2="179.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="205.492" y1="205.75" x2="176.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="202.492" y1="205.75" x2="173.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="199.492" y1="205.75" x2="170.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="196.492" y1="205.75" x2="167.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="193.492" y1="205.75" x2="164.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="190.492" y1="205.75" x2="161.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="187.492" y1="205.75" x2="158.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="184.492" y1="205.75" x2="155.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="181.492" y1="205.75" x2="152.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="178.492" y1="205.75" x2="149.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="175.492" y1="205.75" x2="146.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="172.492" y1="205.75" x2="143.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="169.492" y1="205.75" x2="140.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="166.492" y1="205.75" x2="137.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="163.492" y1="205.75" x2="134.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="160.492" y1="205.75" x2="131.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="157.492" y1="205.75" x2="128.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="154.492" y1="205.75" x2="125.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="151.492" y1="205.75" x2="122.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="148.492" y1="205.75" x2="119.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="145.492" y1="205.75" x2="116.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="142.492" y1="205.75" x2="113.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="139.492" y1="205.75" x2="110.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="136.492" y1="205.75" x2="107.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="133.492" y1="205.75" x2="104.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="130.492" y1="205.75" x2="101.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="127.492" y1="205.75" x2="98.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="124.492" y1="205.75" x2="95.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="121.492" y1="205.75" x2="92.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="118.492" y1="205.75" x2="89.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="115.492" y1="205.75" x2="86.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="112.492" y1="205.75" x2="83.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="109.492" y1="205.75" x2="80.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="106.492" y1="205.75" x2="77.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="103.492" y1="205.75" x2="74.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="100.492" y1="205.75" x2="71.508" y2="225.75"/>
    
      <line fill="none"  stroke-linecap="round" stroke-linejoin="round" x1="97.492" y1="205.75" x2="68.508" y2="225.75"/>
  </g>
</g>
<line fill="none"  stroke-width="2" x1="246.5" y1="198.75" x2="246.5" y2="208.75"/>
<line fill="none"  stroke-width="2" x1="335.5" y1="198.75" x2="335.5" y2="207.75"/>
<line fill="none"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" x1="1.5" y1="277.25" x2="61.5" y2="277.25"/>
<line fill="none"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" x1="451.75" y1="283.75" x2="576.75" y2="283"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="263.5" y1="294.75" x2="281.5" y2="294.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="260.5" y1="300.75" x2="278.5" y2="300.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="252.5" y1="308.75" x2="273.5" y2="308.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="241.5" y1="319.75" x2="267.5" y2="319.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="325.5" y1="294.75" x2="307.5" y2="294.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="328.5" y1="300.75" x2="310.5" y2="300.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="334.5" y1="308.75" x2="313.5" y2="308.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="327.5" y1="319.75" x2="318.5" y2="319.75"/>
<line fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="356.5" y1="335.75" x2="326.5" y2="335.75"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M141.25,302.5
  c2.103-1.617,4.193-2.957,7.049-2.762c3.046,0.208,4.117,2.103,6.506,3.206c6.666,3.079,10.489-3.063,16.463-3.2
  c4.922-0.112,7.086,4.912,12.5,2.568c3.284-1.422,5.286-3.243,9.219-3.063c2.795,0.128,4.681,1.623,7.213,2.688
  c2.773,1.167,5.407,1.683,8.3,0.062"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M100.25,312.5
  c2.103-1.617,4.193-2.957,7.049-2.762c3.046,0.208,4.117,2.103,6.506,3.206c6.666,3.079,10.489-3.063,16.463-3.2
  c4.922-0.112,7.086,4.912,12.5,2.568c3.284-1.422,5.286-3.243,9.219-3.063c2.795,0.128,4.681,1.623,7.213,2.688
  c2.773,1.167,5.407,1.683,8.3,0.062"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M161.25,324.5
  c2.103-1.617,4.193-2.957,7.049-2.762c3.046,0.208,4.117,2.103,6.506,3.206c6.666,3.079,10.489-3.063,16.463-3.2
  c4.922-0.112,7.086,4.912,12.5,2.568c3.284-1.422,5.286-3.243,9.219-3.063c2.795,0.128,4.681,1.623,7.213,2.688
  c2.773,1.167,5.407,1.683,8.3,0.062"/>
<path fill="none"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M454.25,341.5
  c2.103-1.617,4.193-2.957,7.049-2.762c3.047,0.208,4.117,2.103,6.507,3.206c6.665,3.079,10.489-3.063,16.463-3.2
  c4.922-0.112,7.086,4.912,12.5,2.568c3.284-1.422,5.286-3.243,9.219-3.063c2.796,0.128,4.681,1.623,7.213,2.688
  c2.773,1.167,5.407,1.683,8.3,0.062"/>
<g>
  <defs>
    <path id="SVGID_2_" d="M475.922,208.018l24.199-0.324l1.624-2.924c0,0,13.806-2.437,14.293-2.437s17.054-9.744,17.054-9.744
      l17.703-28.261c0,0-16.729-27.285-17.541-27.285s-92.901-6.334-92.901-6.334l-25.499,5.847l10.07,38.005l19.652,19.165
      L475.922,208.018z"/>
  </defs>
  <clipPath id="SVGID_7_">
    <use xlink:href="#SVGID_2_"  overflow="visible"/>
  </clipPath>
  <path clip-path="url(#SVGID_7_)" fill="none"  stroke-linecap="round" stroke-linejoin="round" d="
    M431.061,159.312c0.447-0.021,0.956-0.018,1.504,0.025c-0.452-0.183-0.825-0.263-1.369-0.461c0.057-0.159,0.121-0.32,0.196-0.484
    c0.14,0.078,0.516,0.23,0.7,0.279c-0.222-0.168-0.506-0.316-0.642-0.41c0.134-0.283,0.293-0.577,0.485-0.883
    c0.424,0.191,0.901,0.436,1.397,0.735c-0.335-0.376-0.626-0.623-1.203-1.037c0.29-0.435,0.641-0.894,1.057-1.374
    c0.264,0.137,0.539,0.291,0.821,0.466c-0.212-0.243-0.405-0.429-0.666-0.643c0.098-0.11,0.198-0.222,0.304-0.334
    c0.487,0.212,1.069,0.498,1.675,0.872c-0.366-0.417-0.679-0.676-1.382-1.181c0.052-0.053,0.1-0.105,0.153-0.159
    c0.286-0.289,0.589-0.552,0.899-0.803c0.572,0.188,1.439,0.524,2.37,1.055c-0.453-0.483-0.82-0.741-1.877-1.435
    c0.428-0.308,0.879-0.578,1.345-0.82c0.304,0.299,0.631,0.603,1.015,0.944c-0.28-0.337-0.576-0.692-0.856-1.024
    c0.266-0.135,0.534-0.264,0.81-0.38c0.433,0.421,0.999,1.036,1.548,1.808c-0.205-0.609-0.425-0.997-1.055-2.01
    c0.559-0.211,1.132-0.384,1.714-0.523c0.347,0.629,0.684,1.215,1.159,1.948c-0.306-0.686-0.647-1.432-0.913-2.009
    c0.596-0.135,1.202-0.239,1.813-0.313c0.153,0.373,0.307,0.792,0.443,1.247c0.001-0.428-0.037-0.764-0.14-1.283
    c0.216-0.022,0.433-0.046,0.65-0.061c0.176,0.579,0.38,1.399,0.489,2.348c0.118-0.648,0.108-1.103,0.038-2.379
    c0.356-0.02,0.712-0.024,1.068-0.027c0.04,0.529,0.092,1.065,0.178,1.711c0.013-0.568,0.022-1.175,0.028-1.712
    c0.578,0.003,1.155,0.022,1.727,0.063c-0.068,0.573-0.077,0.925-0.034,1.375c0.1-0.494,0.226-0.948,0.354-1.352
    c0.342,0.028,0.679,0.062,1.013,0.102c-0.138,0.722-0.191,1.528-0.068,2.399c0.246-0.682,0.444-1.343,0.917-2.285
    c0.314,0.05,0.62,0.104,0.924,0.158c-8.961-9.19-19.08-5.598-19.08-5.598c0.099-0.035,0.194-0.067,0.293-0.104
    c-0.191,0.064-0.293,0.104-0.293,0.104c0.802-0.563,1.583-1.062,2.346-1.512c0.537-0.069,1.283-0.127,2.111-0.078
    c-0.412-0.167-0.736-0.24-1.396-0.332c0.34-0.188,0.676-0.364,1.01-0.53c0.296,0.069,0.611,0.148,0.939,0.236
    c-0.197-0.137-0.374-0.269-0.591-0.409c0.681-0.328,1.348-0.614,1.997-0.856c0.492,0.171,1.179,0.455,1.898,0.893
    c-0.321-0.39-0.598-0.618-1.283-1.109c0.814-0.271,1.603-0.479,2.363-0.623c0.981,0.802,1.392,1.377,1.874,1.951
    c-0.146-0.841-0.446-1.543-0.798-2.119c0.891-0.105,1.742-0.125,2.558-0.069c0.305,0.577,0.669,1.36,0.994,2.307
    c-0.066-0.722-0.175-1.215-0.468-2.26c0.323,0.036,0.643,0.08,0.953,0.14c0.12,0.472,0.236,1.008,0.33,1.601
    c0.038-0.523,0.036-0.939-0.009-1.531c1.229,0.267,2.357,0.712,3.384,1.294c0.183,0.62,0.382,1.422,0.524,2.361
    c0.053-0.671,0.04-1.158-0.042-2.073c0.948,0.595,1.801,1.308,2.564,2.1c-0.055,0.498-0.134,1.055-0.254,1.657
    c0.205-0.473,0.342-0.861,0.494-1.403c0.929,1.007,1.712,2.133,2.351,3.302c-0.106-0.586-0.213-1.185-0.315-1.782
    c0.745-0.133,1.06-0.205,1.495-0.377c-0.61,0.069-1.145,0.07-1.551,0.05c-0.041-0.239-0.082-0.479-0.122-0.718
    c0.527-0.028,0.811-0.064,1.189-0.161c-0.467-0.009-0.884-0.052-1.233-0.104c-0.146-0.883-0.286-1.76-0.421-2.608
    c0.653,0.098,0.969,0.118,1.391,0.057c-0.554-0.135-1.049-0.317-1.457-0.493c-0.027-0.188-0.058-0.375-0.086-0.559
    c0.302,0.089,0.524,0.137,0.809,0.171c-0.313-0.131-0.595-0.27-0.845-0.404c-0.156-1.038-0.3-2.009-0.423-2.866
    c0.226,0.082,0.416,0.133,0.645,0.172c-0.245-0.136-0.473-0.279-0.68-0.42c-0.036-0.257-0.071-0.505-0.104-0.737
    c0.196,0.117,0.359,0.189,0.559,0.254c-0.222-0.176-0.422-0.358-0.6-0.534c-0.193-1.387-0.308-2.255-0.308-2.255
    c0.349,0.354,0.67,0.724,0.983,1.101c-0.006,0.161-0.014,0.33-0.024,0.507c0.036-0.146,0.071-0.28,0.098-0.418
    c0.463,0.563,0.891,1.145,1.278,1.745c0.006,0.329-0.003,0.701-0.029,1.11c0.08-0.296,0.128-0.539,0.178-0.87
    c0.104,0.165,0.205,0.328,0.302,0.494c-0.068,0.383-0.168,0.846-0.314,1.353c0.18-0.347,0.292-0.62,0.458-1.101
    c0.492,0.869,0.915,1.76,1.273,2.661c-0.146,0.318-0.332,0.685-0.557,1.073c0.235-0.267,0.402-0.488,0.646-0.854
    c0.058,0.154,0.115,0.306,0.17,0.46c-0.206,0.395-0.476,0.854-0.81,1.329c0.328-0.263,0.549-0.494,0.938-0.975
    c0.115,0.333,0.224,0.667,0.322,1.003c-0.157,0.179-0.33,0.366-0.52,0.558c0.203-0.134,0.368-0.256,0.559-0.415
    c0.239,0.827,0.426,1.65,0.567,2.457c-0.283,0.301-0.666,0.68-1.137,1.069c0.389-0.202,0.647-0.375,1.184-0.778
    c0.147,0.901,0.239,1.778,0.278,2.615c-0.279,0.255-0.623,0.551-1.033,0.851c0.352-0.156,0.602-0.298,1.042-0.583
    c0.007,0.232,0.011,0.46,0.011,0.684c-0.204,0.084-0.426,0.17-0.662,0.25c0.233-0.024,0.427-0.053,0.659-0.104
    c-0.001,0.331-0.015,0.652-0.033,0.966c0.066-0.564,0.165-1.138,0.317-1.719c0.294,0.063,0.59,0.126,0.875,0.188
    c-0.298-0.113-0.573-0.213-0.842-0.309c0.095-0.35,0.211-0.702,0.346-1.057c0.396,0.288,0.691,0.463,1.073,0.641
    c-0.355-0.321-0.673-0.643-0.95-0.948c0.222-0.535,0.499-1.078,0.834-1.628c0.2,0.635,0.484,1.29,0.903,1.93
    c-0.076-0.871-0.199-1.687-0.12-3.062c0.239-0.312,0.499-0.627,0.783-0.944c0.137,0.917,0.231,1.345,0.45,1.9
    c-0.071-0.936-0.037-1.771,0.021-2.403c0.139-0.144,0.279-0.284,0.429-0.429c0.023,0.631,0.051,1.31,0.08,1.952
    c0.088-0.822,0.141-1.516,0.175-2.192c0.422-0.392,0.884-0.789,1.388-1.189c-0.139,1.098-0.172,1.557-0.103,2.202
    c0.231-1.185,0.588-2.163,0.842-2.77c0.25-0.186,0.509-0.372,0.779-0.56c-0.125,0.576-0.257,1.188-0.377,1.768
    c0.274-0.735,0.49-1.366,0.686-1.98c0.164-0.112,0.335-0.225,0.507-0.336c-0.094,0.303-0.185,0.605-0.274,0.899
    c0.175-0.368,0.333-0.709,0.479-1.034c0.867-0.56,1.823-1.126,2.876-1.698c0,0-2.899,3.606-5.763,7.436
    c0.088-0.064,0.175-0.133,0.265-0.195c0.058,0.404,0.119,0.826,0.18,1.229c0.002-0.497-0.006-0.932-0.022-1.346
    c0.294-0.205,0.597-0.398,0.906-0.584c-0.004,0.493-0.007,1.033-0.009,1.541c0.098-0.623,0.163-1.15,0.215-1.665
    c0.319-0.185,0.644-0.363,0.979-0.524c-0.226,0.81-0.316,1.242-0.376,1.842c0.288-0.827,0.604-1.53,0.877-2.069
    c0.276-0.12,0.56-0.232,0.847-0.334c-0.075,0.379-0.15,0.771-0.223,1.147c0.156-0.44,0.287-0.831,0.401-1.206
    c0.756-0.257,1.543-0.453,2.361-0.567c-0.138,0.523-0.205,0.892-0.254,1.363c0.184-0.521,0.379-0.994,0.568-1.408
    c0.301-0.033,0.608-0.054,0.917-0.068c-0.443,0.913-0.625,1.347-0.804,1.983c0.459-0.822,0.927-1.499,1.306-1.996
    c0.22,0,0.439,0.006,0.662,0.017c-0.188,0.418-0.387,0.864-0.573,1.288c0.3-0.469,0.546-0.878,0.771-1.275
    c0.503,0.031,1.013,0.087,1.534,0.179c-0.423,0.494-0.821,1.101-1.091,1.839c0.545-0.48,1.021-0.97,2.074-1.628
    c0.279,0.069,0.563,0.151,0.848,0.239c-0.375,0.399-0.799,0.853-1.192,1.278c0.556-0.44,0.998-0.82,1.427-1.205
    c0.343,0.112,0.689,0.236,1.039,0.378c-0.724,0.399-1.021,0.584-1.385,0.914c0.746-0.349,1.438-0.562,1.928-0.688
    c0.216,0.096,0.437,0.203,0.656,0.311c-0.207,0.165-0.416,0.333-0.619,0.494c0.267-0.153,0.507-0.298,0.736-0.438
    c0.32,0.158,0.642,0.328,0.967,0.511c-0.228,0.112-0.415,0.219-0.621,0.329c0.322-0.06,0.631-0.111,0.922-0.157
    c0.285,0.168,0.572,0.343,0.86,0.53c-0.633,0.044-0.952,0.091-1.366,0.217c0.787,0.003,1.479,0.096,1.973,0.188
    c0.646,0.443,1.299,0.934,1.961,1.48c0,0-0.006-0.003-0.013-0.006c-0.377-0.154-9.71-3.836-19.779,4.674
    c0.253,0.005,0.511,0.017,0.778,0.041c-0.271,0.439-0.563,0.922-0.839,1.379c0.405-0.501,0.733-0.929,1.045-1.358
    c0.231,0.024,0.47,0.054,0.711,0.092c-0.115,0.188-0.229,0.378-0.341,0.563c0.156-0.192,0.299-0.373,0.436-0.55
    c0.338,0.053,0.683,0.113,1.039,0.191c-0.323,0.414-0.52,0.723-0.729,1.123c0.373-0.391,0.741-0.734,1.088-1.037
    c0.262,0.065,0.514,0.146,0.764,0.229c-0.323,0.284-0.664,0.586-0.985,0.875c0.444-0.289,0.821-0.548,1.179-0.807
    c0.463,0.169,0.909,0.365,1.329,0.595c-0.976,0.381-1.396,0.577-1.93,0.949c1.018-0.317,1.94-0.471,2.604-0.547
    c0.221,0.146,0.435,0.302,0.644,0.463c-0.44,0.154-0.92,0.325-1.372,0.489c0.597-0.104,1.092-0.211,1.574-0.328
    c0.331,0.269,0.646,0.557,0.944,0.867c-1.125,0.484-1.815,0.616-2.541,0.809c1.195,0.323,2.334,0.207,3.237-0.026
    c0.27,0.333,0.523,0.678,0.765,1.04c-0.482,0.102-1.027,0.22-1.535,0.334c0.634-0.026,1.151-0.068,1.667-0.127
    c0.252,0.39,0.487,0.794,0.708,1.212c-0.301,0.064-0.608,0.134-0.906,0.199c0.358-0.014,0.677-0.035,0.981-0.06
    c0.135,0.259,0.262,0.524,0.388,0.793c-0.6,0.125-1.487,0.312-2.288,0.49c0.934-0.036,1.619-0.111,2.408-0.22
    c0.128,0.291,0.253,0.58,0.369,0.88c-0.586-0.026-1.228,0.024-1.904,0.21c0.657,0.171,1.287,0.289,2.238,0.691
    c0.15,0.435,0.287,0.878,0.417,1.326c-0.468-0.112-0.992-0.235-1.487-0.347c0.586,0.249,1.072,0.435,1.559,0.601
    c0.291,1.037,0.536,2.097,0.732,3.162c-0.389-0.307-0.855-0.719-1.319-1.238c0.273,0.618,0.539,0.951,1.472,2.14
    c0.057,0.343,0.106,0.684,0.154,1.022c-0.318-0.263-0.653-0.54-0.973-0.803c0.369,0.419,0.69,0.765,1.009,1.089
    c0.084,0.629,0.154,1.252,0.214,1.867c-0.286-0.252-0.594-0.548-0.901-0.889c0.214,0.484,0.423,0.793,0.953,1.475
    c0.042,0.501,0.075,0.994,0.104,1.478c-0.216-0.23-0.438-0.467-0.653-0.692c0.239,0.347,0.455,0.648,0.668,0.93
    c0.228,4.177-0.01,7.533-0.03,8.258c-0.265-2.672-0.861-5.175-1.69-7.479c0.251,0.128,0.515,0.261,0.811,0.4
    c-0.29-0.207-0.597-0.42-0.89-0.625c-0.155-0.418-0.313-0.832-0.483-1.237c0.168,0.095,0.341,0.193,0.512,0.301
    c-0.223-0.279-0.435-0.504-0.698-0.74c-0.145-0.327-0.292-0.651-0.445-0.97c0.394,0.128,0.807,0.28,1.229,0.46
    c-0.444-0.409-0.83-0.675-1.521-1.053c-0.332-0.656-0.683-1.292-1.05-1.903c0.506,0.069,1.051,0.17,1.618,0.311
    c-0.568-0.376-1.033-0.584-1.986-0.91c-0.109-0.174-0.225-0.344-0.337-0.516c0.295,0.028,0.604,0.052,0.949,0.069
    c-0.35-0.084-0.717-0.168-1.068-0.25c-0.229-0.34-0.466-0.67-0.705-0.994c0.553-0.063,1.1-0.142,1.775-0.27
    c-0.661,0.005-1.38,0.018-1.951,0.031c-0.17-0.228-0.342-0.449-0.518-0.668c0.953-0.355,1.65-0.482,2.272-0.664
    c-0.994-0.241-1.98-0.187-2.838-0.021c-0.176-0.205-0.353-0.406-0.531-0.602c0.347-0.189,0.715-0.383,1.106-0.567
    c-0.465,0.073-0.85,0.165-1.332,0.324c-0.16-0.172-0.326-0.335-0.49-0.5c0.823-0.513,1.479-0.94,2.377-1.616
    c-1.002,0.528-2.139,1.148-2.592,1.396c-0.289-0.286-0.581-0.564-0.877-0.827c1.127-1.078,1.453-1.389,1.845-1.974
    c-0.957,0.814-1.861,1.333-2.311,1.569c-0.244-0.207-0.492-0.403-0.737-0.594c0.563-0.809,1.018-1.489,1.604-2.483
    c-0.671,0.845-1.419,1.807-1.818,2.321c-0.273-0.206-0.547-0.406-0.822-0.592c0.243-0.537,0.478-1.09,0.743-1.771
    c-0.304,0.552-0.628,1.146-0.908,1.663c-0.15-0.1-0.302-0.2-0.451-0.291c0.208-0.81,0.522-1.84,0.987-2.954
    c-0.545,0.713-0.85,1.27-1.516,2.641c-0.278-0.157-0.557-0.305-0.833-0.44c0.114-1.552,0.365-2.335,0.573-3.194
    c-0.759,0.924-1.197,1.92-1.452,2.797c-0.118-0.049-0.234-0.095-0.351-0.136c0.153-0.698,0.287-1.388,0.423-2.256
    c-0.214,0.734-0.439,1.526-0.621,2.172c-0.048-0.018-0.092-0.052-0.139-0.067c-0.032-0.542-0.013-1.303,0.005-1.942
    c-0.165,0.553-0.198,0.695-0.302,1.456c-0.005-0.001-0.01-0.003-0.015-0.004c0.309,0.062,0.402,1.177,0.604,1.714
    c-0.494,0.115-1.126,0.392-1.686,0.527c0.668-0.045,1.188-0.073,1.729-0.151c0.146,0.399,0.284,0.835,0.42,1.265
    c-0.506,0.019-1.071,0.141-1.639,0.447c0.521,0.053,1.009,0.063,1.858,0.321c0.071,0.249,0.14,0.506,0.207,0.766
    c-0.521-0.035-1.188-0.072-1.798-0.103c0.711,0.159,1.266,0.255,1.856,0.337c0.057,0.215,0.107,0.429,0.16,0.648
    c-0.358-0.021-0.737-0.043-1.099-0.061c0.421,0.095,0.786,0.167,1.139,0.225c0.123,0.532,0.24,1.077,0.35,1.634
    c-0.348-0.034-0.725-0.019-1.117,0.067c0.011,0.009,0.024,0.021,0.041,0.041c0.046,0.05,0.092,0.104,0.139,0.161
    c0.308,0.095,0.64,0.207,1.071,0.417c0.045,0.242,0.086,0.492,0.13,0.739c-0.146-0.037-0.301-0.075-0.459-0.115
    c0.049,0.072,0.098,0.145,0.148,0.22c0.116,0.042,0.232,0.083,0.349,0.124c0.019,0.109,0.034,0.222,0.053,0.332
    c0.01-0.069,0.021-0.084,0.062-0.03c0.275,0.349,0.729,0.99,1.258,1.751c0.525,0.764,1.149,1.628,1.655,2.499
    c0.08-0.184,0.08-0.297,0.156-0.157c0.181,0.325,0.426,0.763,0.711,1.276c0.043-0.105,0.075-0.13,0.126-0.046
    c0.226,0.389,0.604,1.045,1.051,1.814c0.221,0.386,0.457,0.799,0.699,1.222c0.222,0.437,0.448,0.883,0.67,1.317
    c0.035-0.139,0.053-0.183,0.107-0.088c0.289,0.503,0.805,1.508,1.381,2.628c0.05-0.087,0.07-0.118,0.132-0.012
    c0.195,0.355,0.471,0.857,0.742,1.488c0.036-0.094,0.06-0.113,0.099-0.027c0.343,0.783,1.146,2.626,1.901,4.357
    c0.051-0.093,0.064-0.131,0.125-0.017c0.127,0.251,0.287,0.569,0.438,0.955c0.139,0.389,0.3,0.836,0.473,1.322
    c0.067-0.16,0.081-0.214,0.12-0.126c0.35,0.828,1.167,2.979,1.8,4.903c0.144-0.154,0.161-0.27,0.221-0.11
    c0.125,0.335,0.261,0.794,0.413,1.337c0.086-0.104,0.129-0.127,0.156-0.029c0.16,0.604,0.475,1.788,0.818,3.087
    c0.062-0.073,0.087-0.096,0.12,0.014c0.093,0.317,0.225,0.751,0.341,1.279c0.109,0.53,0.237,1.148,0.374,1.812
    c0.065-0.102,0.093-0.126,0.119-0.029c0.193,0.714,0.538,2.308,0.898,3.93c0.07-0.076,0.098-0.104,0.138,0.01
    c0.287,0.749,0.389,2.245,0.684,3.936c0.196-0.23,0.249-0.339,0.279-0.198c0.131,0.617,0.327,1.876,0.544,3.25
    c0.111-0.093,0.149-0.134,0.172-0.006c0.117,0.648,0.157,1.803,0.301,3.152c0.09-0.096,0.125-0.117,0.142-0.019
    c0.054,0.341,0.118,0.878,0.188,1.519c0.648-0.938,1.266-1.769,1.596-2.151c0.054-0.058,0.062-0.005,0.104,0.161
    c0.518-0.682,0.942-1.257,1.262-1.598c0.073-0.078,0.083-0.038,0.124,0.068c0.977-1.224,2.011-2.523,2.489-3.044
    c0.056-0.061,0.075-0.026,0.102,0.086c0.366-0.444,0.673-0.814,0.909-1.057c0.071-0.072,0.086-0.035,0.123,0.064
    c0.684-0.778,1.31-1.453,1.644-1.789c0.071-0.071,0.076-0.011,0.097,0.162c0.843-0.935,1.527-1.752,1.988-2.131
    c0.075-0.063,0.085-0.029,0.104,0.057c0.927-0.993,1.831-1.967,2.256-2.389c0.058-0.058,0.068-0.027,0.077,0.074
    c0.731-0.821,1.358-1.527,1.71-1.924c0.063-0.067,0.074-0.041,0.096,0.039c0.698-0.828,1.336-1.583,1.661-1.969
    c0.05-0.058,0.076-0.033,0.106,0.058c0.285-0.354,0.528-0.653,0.717-0.866c0.063-0.071,0.076-0.041,0.105,0.046
    c0.674-0.751,1.248-1.498,1.702-2.108c-0.572-0.714-0.84-1.81-0.869-3.145c0.142-0.139,0.299-0.282,0.473-0.427
    c-0.172,0.069-0.311,0.14-0.474,0.239c-0.001-0.153,0.001-0.31,0.006-0.469c0.216-0.167,0.475-0.349,0.77-0.522
    c-0.261,0.064-0.456,0.139-0.759,0.283c0.014-0.277,0.035-0.563,0.064-0.854c0.28-0.163,0.635-0.349,1.051-0.516
    c-0.326,0.033-0.56,0.091-1.021,0.24c0.071-0.612,0.177-1.251,0.31-1.904c0.17-0.066,0.345-0.138,0.533-0.218
    c-0.17,0.045-0.343,0.092-0.518,0.138c0.057-0.268,0.116-0.538,0.18-0.813c0.353-0.057,0.718-0.123,1.139-0.209
    c-0.364,0.025-0.747,0.057-1.108,0.084c0.051-0.212,0.104-0.427,0.159-0.641c0.343-0.1,0.949-0.246,1.684-0.307
    c-0.422-0.081-0.707-0.077-1.586-0.056c0.045-0.17,0.093-0.341,0.142-0.509c0.868-0.136,1.337-0.102,1.84-0.098
    c-0.588-0.281-1.176-0.396-1.683-0.431c0.087-0.287,0.177-0.573,0.271-0.858c0.769,0.062,1.2,0.177,1.667,0.276
    c-0.515-0.351-1.048-0.567-1.524-0.703c0.124-0.363,0.255-0.725,0.389-1.085c0.354,0.088,0.725,0.173,1.153,0.266
    c-0.364-0.126-0.75-0.255-1.112-0.377c0.045-0.119,0.09-0.24,0.136-0.358c0.244,0.004,1.032,0.034,1.969,0.268
    c-0.414-0.234-0.705-0.318-1.813-0.662c0.154-0.391,0.314-0.773,0.479-1.152c0.693,0.123,1.315,0.222,2.152,0.314
    c-0.723-0.172-1.522-0.355-2.08-0.483c0.042-0.094,0.082-0.19,0.124-0.285c0.238-0.004,1.03,0,1.979,0.208
    c-0.418-0.221-0.71-0.296-1.803-0.599c0.066-0.146,0.136-0.294,0.205-0.438c1.255,0.068,1.813,0.249,2.43,0.397
    c-0.705-0.575-1.492-0.862-2.135-1.007c0.118-0.233,0.235-0.461,0.354-0.685c0.667,0.204,1.27,0.374,2.085,0.569
    c-0.696-0.258-1.464-0.536-1.998-0.73c0.049-0.093,0.098-0.187,0.147-0.277c0.329,0.024,1.029,0.104,1.836,0.343
    c-0.386-0.241-0.666-0.339-1.644-0.688c0.084-0.15,0.169-0.296,0.253-0.44c0.252,0.067,0.518,0.133,0.811,0.204
    c-0.25-0.093-0.51-0.188-0.764-0.282c0.08-0.134,0.16-0.269,0.24-0.398c1.136,0.101,1.667,0.283,2.251,0.438
    c-0.692-0.422-1.426-0.653-2.034-0.78c0.123-0.19,0.246-0.371,0.368-0.546c0.635,0.065,1.042,0.171,1.476,0.261
    c-0.386-0.294-0.795-0.505-1.188-0.657c0.063-0.085,0.127-0.163,0.189-0.244c-0.369-0.011-0.725-0.012-1.068-0.002
    c-0.894-0.972-1.163-1.263-1.674-1.614c0.502,0.617,0.864,1.207,1.102,1.639c-0.369,0.022-0.725,0.056-1.065,0.102
    c-0.479-0.846-0.656-1.403-0.889-1.985c-0.125,0.769-0.055,1.487,0.103,2.113c-0.211,0.042-0.416,0.086-0.617,0.137
    c-0.153-0.504-0.327-1.068-0.494-1.597c0.089,0.625,0.181,1.144,0.285,1.648c-0.303,0.082-0.587,0.179-0.862,0.28
    c-0.177-0.314-0.358-0.638-0.535-0.948c0.138,0.366,0.269,0.691,0.396,1.001c-0.121,0.049-0.246,0.093-0.363,0.145
    c-0.728-1.044-0.952-1.362-1.406-1.767c0.484,0.795,0.778,1.528,0.937,1.985c-0.202,0.102-0.397,0.211-0.583,0.324
    c-0.485-0.471-1.12-1.083-1.708-1.642c0.548,0.701,1,1.229,1.492,1.775c-0.355,0.231-0.681,0.486-0.983,0.759
    c-0.292-0.386-0.644-0.9-0.962-1.508c0.085,0.517,0.223,0.855,0.645,1.805c-0.11,0.11-0.218,0.221-0.321,0.337
    c-0.348-0.377-0.728-0.786-1.09-1.172c0.348,0.503,0.653,0.92,0.96,1.317c-0.166,0.195-0.322,0.396-0.471,0.603
    c-0.494-0.528-0.786-0.942-1.115-1.363c0.113,0.751,0.388,1.411,0.715,1.971c-0.173,0.283-0.331,0.575-0.477,0.875
    c-0.282-0.376-0.61-0.865-0.911-1.438c0.089,0.537,0.232,0.88,0.692,1.913c-0.057,0.133-0.112,0.266-0.165,0.399
    c-0.191-0.293-0.389-0.595-0.58-0.884c0.18,0.393,0.345,0.737,0.51,1.063c-0.175,0.455-0.328,0.919-0.464,1.391
    c-0.237-0.322-0.485-0.658-0.725-0.977c0.235,0.446,0.451,0.829,0.665,1.192c-0.073,0.265-0.142,0.534-0.206,0.805
    c-0.185-0.277-0.375-0.591-0.554-0.934c0.07,0.429,0.177,0.736,0.453,1.373c-0.107,0.489-0.202,0.979-0.29,1.466
    c-0.154-0.207-0.311-0.417-0.461-0.619c0.148,0.283,0.291,0.543,0.431,0.787c-0.292,1.652-0.506,3.263-0.834,4.64
    c-2.115-7.767,1.675-19.055,8.252-19.368c1.337-0.063,2.504,0.091,3.519,0.372c-0.353-0.295-0.702-0.563-1.046-0.809
    c-0.517-0.09-1.052-0.139-1.594-0.15c0.023-0.259,0.044-0.521,0.065-0.798c-0.041-0.021-0.081-0.044-0.12-0.064
    c-0.057,0.289-0.114,0.578-0.17,0.858c-0.487-0.003-0.984,0.025-1.483,0.075c-0.044-0.529-0.1-1.071-0.178-1.677
    c-0.023-0.007-0.049-0.016-0.072-0.022c-0.006,0.585-0.012,1.184-0.016,1.728c-0.127,0.015-0.255,0.031-0.384,0.049
    c-0.151-0.632-0.27-0.987-0.483-1.427c0.08,0.531,0.121,1.032,0.141,1.479c-0.379,0.061-0.758,0.132-1.135,0.215
    c-0.219-0.768-0.569-1.584-1.13-2.368c-0.017,0.001-0.032,0.004-0.048,0.004c0.108,0.744,0.248,1.457,0.268,2.579
    c-0.252,0.066-0.503,0.138-0.752,0.209c-0.094-0.389-0.189-0.783-0.282-1.167c0.038,0.438,0.079,0.835,0.124,1.213
    c-0.498,0.148-0.987,0.31-1.467,0.478c-0.854-0.852-1.192-1.16-1.757-1.503c0.531,0.601,0.951,1.191,1.267,1.68
    c-0.331,0.124-0.656,0.25-0.973,0.378c-0.442-0.467-0.97-0.914-1.605-1.293c0.295,0.49,0.599,0.948,0.925,1.573
    c-0.665,0.282-1.287,0.567-1.849,0.841c-0.486-0.114-0.821-0.159-1.242-0.172c0.317,0.108,0.612,0.225,0.892,0.341
    c-1.65,0.816-2.705,1.466-2.705,1.466c0.096-0.16,0.192-0.311,0.291-0.466c-0.191,0.291-0.291,0.466-0.291,0.466
    c0.057-0.724,0.149-1.398,0.27-2.031c0.31-0.398,0.688-0.844,1.134-1.293c-0.359,0.19-0.633,0.374-1.001,0.672
    c0.104-0.438,0.224-0.854,0.356-1.245c0.501-0.248,1.018-0.518,1.607-0.845c-0.494,0.183-1.012,0.375-1.508,0.563
    c0.156-0.42,0.33-0.811,0.519-1.175c0.862-0.257,1.495-0.351,2.152-0.483c-0.584-0.177-1.153-0.268-1.695-0.302
    c0.331-0.506,0.698-0.948,1.098-1.33c0.493,0.02,1.062,0.067,1.668,0.164c-0.415-0.205-0.756-0.325-1.325-0.472
    c0.786-0.658,1.675-1.105,2.629-1.364c0.58,0.281,1.022,0.547,1.494,0.805c-0.232-0.359-0.487-0.681-0.754-0.972
    c0.214-0.037,0.432-0.065,0.649-0.085c0.283,0.28,0.584,0.564,0.923,0.881c-0.234-0.298-0.479-0.615-0.719-0.917
    c1.492-0.097,3.046,0.316,4.601,0.889c0.006,0.002-0.019,0.117-0.012,0.117c0.002,0,0.003,0,0.004,0
    c2.54,0,4.637,1.428,6.299,3.121c-0.505-0.854-1.01-1.785-1.526-2.703c0.616-0.352,0.879-0.554,1.208-0.841
    c-0.515,0.252-0.981,0.4-1.348,0.512c-0.11-0.198-0.22-0.406-0.33-0.604c0.456-0.19,0.696-0.315,1-0.519
    c-0.415,0.138-0.797,0.226-1.121,0.288c-0.406-0.732-0.806-1.46-1.188-2.166c0.604-0.119,0.891-0.205,1.242-0.389
    c-0.528,0.054-1.026,0.05-1.438,0.021c-0.086-0.157-0.17-0.312-0.252-0.465c0.293-0.017,0.507-0.045,0.766-0.104
    c-0.315-0.017-0.608-0.05-0.87-0.092c-0.465-0.865-0.896-1.677-1.274-2.395c0.225,0.002,0.408-0.015,0.622-0.051
    c-0.259-0.044-0.505-0.098-0.731-0.157c-0.114-0.216-0.222-0.423-0.324-0.618c0.212,0.04,0.378,0.055,0.575,0.05
    c-0.252-0.087-0.485-0.186-0.698-0.283c-0.606-1.163-0.979-1.894-0.979-1.894c0.417,0.203,0.819,0.428,1.212,0.662
    c0.047,0.144,0.093,0.297,0.138,0.456c-0.011-0.144-0.024-0.271-0.043-0.401c0.585,0.354,1.145,0.732,1.675,1.139
    c0.107,0.288,0.22,0.618,0.322,0.988c-0.022-0.286-0.055-0.515-0.116-0.821c0.144,0.112,0.285,0.223,0.423,0.339
    c0.06,0.36,0.116,0.797,0.147,1.293c0.049-0.364,0.062-0.64,0.059-1.114c0.706,0.61,1.359,1.266,1.959,1.945
    c-0.029,0.328-0.079,0.709-0.156,1.125c0.125-0.312,0.202-0.56,0.304-0.959c0.1,0.116,0.196,0.232,0.294,0.352
    c-0.06,0.414-0.151,0.903-0.296,1.429c0.207-0.336,0.327-0.609,0.52-1.154c0.206,0.258,0.408,0.519,0.6,0.782
    c-0.083,0.207-0.177,0.427-0.284,0.656c0.139-0.183,0.245-0.342,0.363-0.542c0.471,0.655,0.894,1.321,1.271,1.988
    c-0.154,0.354-0.373,0.808-0.665,1.301c0.278-0.3,0.452-0.535,0.8-1.059c0.413,0.748,0.77,1.494,1.065,2.221
    c-0.164,0.312-0.378,0.681-0.644,1.075c0.262-0.247,0.438-0.451,0.735-0.843c0.078,0.202,0.155,0.402,0.226,0.601
    c-0.154,0.139-0.323,0.282-0.508,0.428c0.2-0.092,0.362-0.18,0.552-0.3c0.132,0.384,0.246,0.758,0.345,1.121
    c0.167-0.407,0.348-0.83,0.54-1.27c0.115,0.059,0.237,0.122,0.36,0.185c0.03-0.06,0.059-0.115,0.089-0.175
    c0.098,0.134,0.185,0.251,0.265,0.355c0.129,0.065,0.257,0.132,0.39,0.198c-0.048-0.09-0.096-0.178-0.14-0.263
    c-0.289-0.189-0.57-0.367-0.855-0.541c0.132-0.297,0.27-0.601,0.411-0.909c0.145,0.113,0.267,0.209,0.384,0.299
    c0.059-0.11,0.117-0.22,0.175-0.329c-0.128-0.151-0.246-0.295-0.346-0.423c0.175-0.369,0.698-0.745,0.886-1.126
    c0.132,0.102,0.713,0.21,0.787,0.323c-0.039-0.063-0.255-0.126-0.219-0.189c-0.146-0.13-0.462-0.261-0.612-0.39
    c0.155-0.312,0.227-0.623,0.389-0.937c0.117,0.185,0.208,0.371,0.351,0.557c0.041-0.073,0.06-0.146,0.102-0.221
    c0.264,0.384,0.548,0.813,0.841,1.234c0.052,0.044,0.097,0.09,0.151,0.133c-0.305-0.601-0.585-1.11-0.873-1.612
    c0.044-0.078,0.088-0.158,0.135-0.237c-0.087-0.256-0.173-0.539-0.259-0.865c0.305-0.568,0.618-1.138,0.941-1.698
    c0.144,0.196,0.293,0.398,0.44,0.602c0.021-0.035,0.043-0.071,0.063-0.107c-0.135-0.24-0.265-0.467-0.395-0.687
    c0.123-0.211,0.248-0.426,0.374-0.635c0.152,0.283,0.274,0.507,0.385,0.699c0.062-0.105,0.123-0.206,0.185-0.31
    c-0.112-0.346-0.195-0.657-0.255-0.912c0.396-0.649,0.8-1.279,1.207-1.88c0.049,0.319,0.1,0.646,0.148,0.965
    c0.01-0.016,0.02-0.03,0.028-0.045c-0.005-0.407-0.016-0.779-0.031-1.134c0.182-0.265,0.364-0.524,0.549-0.776
    c0.02,0.403,0.041,0.688,0.08,0.945c0.018-0.028,0.037-0.059,0.056-0.088c0.029,0.114,0.059,0.225,0.09,0.339
    c0.005-0.099,0.012-0.198,0.019-0.292c-0.008-0.062-0.016-0.124-0.022-0.183c0.016-0.025,0.031-0.049,0.046-0.075
    c0.052-0.494,0.124-0.926,0.196-1.269c0.232-0.301,0.464-0.589,0.696-0.864c0.007,0.229,0.017,0.47,0.025,0.711
    c0.054-0.081,0.107-0.162,0.159-0.24c0.018-0.252,0.033-0.496,0.046-0.739c0.573-0.656,1.143-1.22,1.694-1.651
    c-1.066,4.794-3.588,10.339-5.754,14.542c0.101-0.064,0.199-0.131,0.303-0.192c0.013,0.267,0.017,0.587,0.009,0.943
    c0.061-0.019,0.121-0.041,0.183-0.062c0.019,0.119,0.037,0.239,0.057,0.361c0.087-0.372,0.177-0.851,0.312-1.563
    c0.433-0.238,0.875-0.465,1.34-0.669c-0.062,0.61-0.099,1.044-0.109,1.413c0.093-0.021,0.185-0.045,0.277-0.065
    c0.147-0.623,0.313-1.171,0.465-1.611c0.219-0.088,0.441-0.168,0.666-0.247c-0.038,0.372-0.067,0.762-0.093,1.209
    c0.088-0.415,0.178-0.854,0.261-1.264c0.182-0.06,0.364-0.121,0.55-0.177c-0.026,0.409-0.02,0.703,0.021,1.061
    c0.069-0.409,0.155-0.788,0.246-1.134c0.448-0.125,0.908-0.229,1.373-0.313c-0.029,0.479-0.049,0.975-0.054,1.564
    c0.085-0.531,0.174-1.099,0.251-1.601c0.179-0.03,0.358-0.059,0.541-0.082c-0.151,0.588-0.259,1.269-0.259,2.006
    c0.101-0.004,0.2-0.01,0.3-0.014c0.015,0.169,0.033,0.339,0.051,0.51c0.194-0.679,0.451-1.452,1.031-2.609
    c0.182-0.012,0.367-0.02,0.552-0.023c-0.082,0.223-0.164,0.456-0.252,0.711c0.121-0.232,0.245-0.473,0.368-0.712
    c0.166-0.003,0.332,0.018,0.501,0.02c-0.492,1.061-0.733,2.105-0.874,2.105c0.063,0,0.127,0,0.192,0
    c0.543,0,1.112-1.631,1.49-2.066c0.226,0.016,0.451,0.029,0.677,0.055c-0.076,0.158-0.152,0.314-0.232,0.488
    c0.104-0.158,0.211-0.322,0.318-0.485c0.226,0.028,0.453,0.06,0.682,0.099c-0.234,0.441-0.463,0.895-0.721,1.453
    c0.312-0.473,0.643-0.978,0.928-1.418c0.16,0.029,0.32,0.06,0.48,0.094c-0.354,0.533-0.671,1.165-0.898,1.892
    c0.061,0.007,0.122,0.015,0.184,0.021c0.419-0.489,0.864-1.007,1.655-1.676c0.19,0.054,0.384,0.116,0.576,0.179
    c-0.341,0.468-0.891,1.155-1.619,1.837c0.057-0.027,0.109-0.056,0.161-0.082c0.029-0.049,0.06-0.103,0.089-0.147
    c0.044,0.007,0.088,0.014,0.132,0.021c0.438-0.262,0.863-0.619,1.839-1.428c0.416,0.154,0.835,0.319,1.257,0.51
    c-0.402,0.312-0.8,0.628-1.271,1.038c0.498-0.314,1.034-0.683,1.488-0.977c0.146,0.068,0.376,0.112,0.521,0.187
    c-0.682,0.517-1.013,1.06-1.229,1.035v0.001c0-0.002-0.119,0.104-0.133,0.118c-0.063,0.067-0.177,0.223-0.24,0.298
    c0.798-0.526,1.53-0.886,2.049-1.098c0.247,0.136,0.486,0.276,0.733,0.428c-0.397,0.191-0.811,0.399-1.283,0.658
    c0.487-0.177,1.002-0.37,1.465-0.542c0.247,0.154,0.492,0.315,0.739,0.484c-0.186,0.071-0.376,0.145-0.581,0.226
    c0.221-0.051,0.45-0.105,0.678-0.16c0.3,0.206,0.598,0.422,0.897,0.649c-0.8,0.231-1.18,0.361-1.596,0.584
    c-0.036,0.023-0.071,0.049-0.108,0.072c0.18-0.037,0.353-0.07,0.522-0.098c0.095-0.049,0.191-0.102,0.278-0.148
    c0.056,0.027,0.113,0.057,0.168,0.085c0.488-0.061,0.918-0.08,1.254-0.085c0.297,0.242,0.596,0.496,0.894,0.763
    c-0.215,0.066-0.393,0.126-0.553,0.186c0.059,0.035,0.116,0.067,0.175,0.104c-0.122,0.044-0.234,0.087-0.344,0.133
    c0.381-0.063,0.736-0.102,1.052-0.125c0.627,0.578,1.252,1.21,1.876,1.901c0,0-9.845-3.092-20.836-0.241
    c0.394,0.05,0.789,0.109,1.187,0.191c0.026,0.197,0.061,0.446,0.099,0.718c0.052,0.014,0.104,0.028,0.156,0.045
    c-0.007-0.233-0.018-0.467-0.033-0.713c0.8,0.177,1.6,0.416,2.389,0.708c0.012,0.216,0.016,0.547-0.026,0.923
    c0.056,0.027,0.111,0.056,0.166,0.085c0.082-0.188,0.164-0.441,0.296-0.841c0.453,0.182,0.902,0.38,1.344,0.594
    c-0.095,0.28-0.21,0.625-0.327,0.982c0.039,0.024,0.076,0.05,0.114,0.074c0.168-0.319,0.312-0.621,0.453-0.94
    c0.605,0.305,1.195,0.638,1.763,0.994c-0.184,0.249-0.389,0.527-0.577,0.789c0.288-0.25,0.517-0.47,0.73-0.69
    c0.218,0.139,0.432,0.281,0.643,0.428c-0.543,0.425-0.815,0.643-1.011,0.864c0.04,0.031,0.08,0.063,0.119,0.097
    c0.553-0.31,1.081-0.497,1.402-0.595c0.476,0.354,0.928,0.724,1.352,1.108c-0.191,0.095-0.386,0.195-0.573,0.292
    c0.245-0.073,0.461-0.142,0.664-0.211c0.188,0.171,0.365,0.346,0.541,0.521c-0.674,0.25-0.933,0.367-1.231,0.604
    c0.562-0.159,1.106-0.243,1.54-0.286c0.176,0.188,0.343,0.379,0.507,0.57c-0.788,0.281-1.333,0.388-1.79,0.44
    c0.088,0.107,0.177,0.215,0.26,0.323c0.922,0.142,1.644,0.033,2.071-0.076c0.274,0.376,0.521,0.756,0.732,1.14
    c-0.495-0.03-1.138-0.069-1.725-0.102c0.705,0.13,1.246,0.207,1.816,0.275c0.211,0.404,0.384,0.813,0.514,1.22
    c-0.424-0.085-0.902-0.182-1.348-0.269c0.525,0.167,0.963,0.294,1.389,0.41c0.066,0.224,0.124,0.446,0.162,0.67
    c-0.385-0.165-0.89-0.402-1.44-0.717c0.368,0.321,0.646,0.5,1.481,1.023c0.019,0.164,0.032,0.327,0.035,0.488
    c-0.941-0.434-1.369-0.625-1.81-0.767c0.011,0.028,0.02,0.055,0.028,0.086c0.746,0.403,1.363,0.804,1.771,1.087
    c-0.012,0.17-0.027,0.339-0.059,0.506c-0.367-0.262-0.771-0.548-1.154-0.817c0.418,0.361,0.774,0.657,1.127,0.941
    c-0.07,0.341-0.181,0.676-0.334,1.002c-0.194-0.168-0.391-0.338-0.582-0.502c0.196,0.208,0.377,0.395,0.551,0.571
    c-0.052,0.106-0.111,0.211-0.173,0.313c-0.225-0.248-0.484-0.561-0.746-0.913c0,0.042,0.002,0.085,0,0.127
    c0.12,0.246,0.287,0.517,0.604,1.013c-0.117,0.174-0.245,0.345-0.389,0.514c-0.112-0.205-0.229-0.412-0.344-0.621
    c-0.006,0.021-0.012,0.043-0.019,0.064c0.094,0.23,0.188,0.448,0.279,0.655c-0.443,0.498-1.012,0.967-1.72,1.397
    c0.77-9.067-6.742-11.669-12.718-12.32c0.1,0.127,0.193,0.263,0.274,0.409c0.241,0.424,0.378,0.873,0.423,1.314
    c0.109,0.143,0.22,0.29,0.329,0.444c0.004,0.005,0.008,0.01,0.013,0.015c0.113,0.162,0.229,0.331,0.345,0.506
    c0.012,0.018,0.022,0.035,0.034,0.052c0.793,1.208,1.583,2.719,2.337,4.609c-0.201,0.021-0.408,0.042-0.606,0.063
    c0.235,0.024,0.447,0.038,0.651,0.05c0.056,0.145,0.113,0.289,0.17,0.437c0.031,0.081,0.057,0.166,0.08,0.251
    c-0.56-0.111-0.843-0.139-1.223-0.101c0.511,0.154,0.964,0.351,1.328,0.536c0.043,0.237,0.064,0.486,0.07,0.742
    c-0.264-0.103-0.543-0.21-0.808-0.312c0.297,0.182,0.557,0.33,0.808,0.467c0,0.155-0.007,0.314-0.021,0.476
    c-0.389-0.152-0.867-0.341-1.311-0.51c0.489,0.298,0.881,0.513,1.291,0.717c-0.023,0.21-0.056,0.423-0.097,0.64
    c-0.428-0.282-0.94-0.512-1.531-0.572c0.398,0.336,0.787,0.616,1.365,1.286c-0.064,0.237-0.138,0.48-0.221,0.724
    c-0.294-0.349-0.547-0.438-0.78-0.689c0.291,0.525,0.453,0.767,0.666,1.021c-0.132,0.358-0.282,0.721-0.446,1.08
    c-0.343-0.282-0.766-0.627-1.155-0.943c0.401,0.447,0.73,0.784,1.076,1.118c-0.091,0.194-0.184,0.388-0.282,0.581
    c-0.439-0.382-0.683-0.559-1.027-0.73c0.339,0.346,0.634,0.696,0.878,1.017c-0.138,0.263-0.282,0.524-0.434,0.782
    c-0.273-0.133-0.571-0.236-0.896-0.287c0.222,0.203,0.438,0.389,0.686,0.641c-0.176,0.289-0.358,0.574-0.547,0.854
    c-0.189-0.525-0.299-0.778-0.484-1.085c0.145,0.49,0.245,0.949,0.315,1.334c-0.309,0.445-0.627,0.875-0.953,1.281
    c-0.037-0.438-0.076-0.696-0.158-1.02c0.009,0.468-0.01,0.897-0.038,1.264c-0.135,0.164-0.27,0.322-0.405,0.476
    c0.027-0.296,0.032-0.517,0.02-0.785c-0.068,0.379-0.151,0.722-0.235,1.026c-0.686,0.754-1.374,1.388-2.014,1.839
    c0.111-0.161,0.23-0.337,0.354-0.521c0.289-0.599,0.562-1.514,0.715-2.221c0.538-0.466,0.737-0.653,0.979-0.993
    c-0.32,0.237-0.625,0.42-0.889,0.558c0.05-0.254,0.099-0.517,0.146-0.781c0.576-0.476,0.862-0.698,1.102-1.036
    c-0.356,0.265-0.719,0.472-1.03,0.625c0.038-0.23,0.072-0.467,0.106-0.704c0.242-0.115,0.496-0.245,0.782-0.402
    c-0.249,0.079-0.511,0.166-0.762,0.248c0.073-0.522,0.138-1.059,0.195-1.605c0.853-0.289,1.106-0.377,1.464-0.629
    c-0.568,0.144-1.076,0.172-1.42,0.17c0.036-0.395,0.068-0.792,0.095-1.193c0.57-0.168,1.056-0.329,1.711-0.602
    c-0.602,0.121-1.271,0.263-1.697,0.356c0.018-0.285,0.035-0.569,0.047-0.858c0.54-0.158,1.14-0.318,1.792-0.749
    c-0.473,0.043-0.885,0.129-1.738,0.135c0.008-0.215-0.015-0.567-0.012-0.781c0.611-0.013,0.852-0.033,1.187-0.147
    c-0.494-0.152-0.92-0.361-1.18-0.508c0.001-0.299-0.002-0.299-0.006-0.596c0.521,0.08,0.989,0.137,1.606,0.167
    c-0.002-0.003-0.002-0.008-0.003-0.012c-0.57-0.154-1.196-0.502-1.611-0.601c-0.005-0.211-0.003-0.211-0.01-0.419
    c0.671,0.065,1.11,0.186,1.481,0.313c-0.019-0.074-0.034-0.15-0.053-0.226c-0.542-0.329-0.944-0.431-1.393-0.556
    c-0.015-0.271-0.104-0.961-0.125-1.226c0.219,0.031,0.592,0.103,1.036,0.256c-0.037-0.098-0.081-0.196-0.122-0.294
    c-0.227-0.135-0.526-0.3-0.962-0.546c-0.034-0.37-0.074-0.732-0.119-1.093c0.159,0.121,0.313,0.231,0.467,0.341
    c-0.077-0.144-0.16-0.287-0.243-0.433c-0.028-0.023-0.055-0.046-0.082-0.068c-0.399,0.175-0.834,0.226-1.264,0.167
    c0.309,1.146-0.077,2.329-1.024,2.867c-0.369,0.209-0.78,0.293-1.191,0.269c-0.106,0.243-0.232,0.519-0.38,0.823
    c0.099-0.027,0.132-0.026,0.094,0.049c-0.347,0.636-1.076,2.157-1.941,3.485c0.097-0.009,0.131-0.018,0.081,0.081
    c-0.22,0.415-0.584,1.042-1.099,1.734c0.149-0.024,0.195-0.041,0.155,0.028c-0.19,0.322-0.562,0.87-1.003,1.511
    c-0.476,0.612-1.024,1.319-1.54,1.983c0.186,0.029,0.281,0.001,0.193,0.102c-0.203,0.235-0.5,0.53-0.832,0.893
    c0.106,0.018,0.141,0.039,0.087,0.102c-0.252,0.282-0.669,0.759-1.184,1.29c-0.518,0.529-1.109,1.136-1.674,1.714
    c0.13,0.004,0.175,0.012,0.113,0.079c-0.321,0.354-0.986,1.01-1.729,1.743c0.091,0.025,0.126,0.038,0.057,0.113
    c-0.229,0.242-0.58,0.559-0.999,0.938c0.091,0.012,0.115,0.027,0.061,0.083c-0.501,0.502-1.727,1.599-2.864,2.649
    c0.099,0.024,0.135,0.028,0.063,0.106c-0.312,0.337-0.84,0.801-1.468,1.364c0.156,0.018,0.205,0.016,0.152,0.072
    c-0.494,0.54-1.899,1.781-3.085,2.915c0.186,0.073,0.281,0.053,0.19,0.154c-0.193,0.216-0.475,0.48-0.813,0.787
    c0.123,0.039,0.161,0.067,0.1,0.124c-0.371,0.337-1.103,1-1.904,1.727c0.089,0.025,0.119,0.039,0.054,0.104
    c-0.367,0.382-1.051,1.002-1.829,1.773c0.111,0.011,0.145,0.023,0.091,0.083c-0.373,0.458-1.292,1.385-2.165,2.387
    c0.095,0.02,0.132,0.026,0.082,0.108c-0.322,0.549-1.21,1.358-2.03,2.453c0.279,0.018,0.383-0.002,0.317,0.093
    c-0.215,0.317-0.656,0.854-1.131,1.498c0.018,0.499,0.032,1.053,0.066,1.643c0.086-0.083,0.12-0.098,0.135-0.005
    c0.081,0.507,0.174,1.479,0.269,2.594c0.088-0.048,0.122-0.054,0.138,0.057c0.091,0.724,0.121,2.12,0.136,3.744
    c0.089-0.05,0.135-0.049,0.146,0.028c0.118,0.926,0.174,3.375,0.241,5.448c0.124-0.067,0.175-0.093,0.19,0.034
    c0.05,0.437,0.108,1.118,0.131,1.941c0.101-0.063,0.131-0.063,0.141,0.027c0.045,0.422,0.068,1.167,0.08,2.048
    c0.27-0.457,0.495-0.842,0.65-1.131c-0.154,0.453-0.379,1.013-0.645,1.647c0.008,0.917,0.008,1.933,0.007,2.872
    c0.051-0.019,0.084-0.007,0.117,0.067c0.016,0.098,0.032,0.207,0.048,0.324c0.014-0.026,0.027-0.053,0.04-0.081
    c-0.012,0.042-0.023,0.082-0.035,0.123c0.101,0.797,0.176,2.001,0.219,3.367c0.467,0.372,0.986,0.841,1.505,1.382
    c0.24-0.889,0.527-1.784,0.863-2.666c-0.591-1.024-1.234-1.993-1.838-3.066c0.883,0.703,1.603,1.438,2.189,2.191
    c1.043-2.451,2.506-4.758,4.543-6.521c-1.285,1.92-2.779,4.397-3.853,7.505c0.203,0.325,0.386,0.652,0.549,0.98
    c0.216-0.102,0.435-0.191,0.654-0.267c-0.188,0.148-0.373,0.305-0.557,0.469c0.206,0.437,0.38,0.876,0.527,1.313
    c1-0.859,2.059-1.202,2.839-1.502c-1.022,0.946-1.873,1.633-2.564,2.438c0.316,1.239,0.448,2.461,0.521,3.613
    c1.081-3.231,4.303-5.232,7.136-6.26c-2.636,2.37-6.264,3.748-6.229,7.907c1.782-2.787,7.202-3.44,8.526-4.88
    c-1.319,2.539-7.915,2.901-7.737,6.904c0,0-0.043,0.199-1.064,0.343c-1.792,0.252-2.447,0.217-2.447,0.217
    c0.054-1.169-0.159-2.431-0.536-3.683c-0.207,1.079-0.354,2.401-0.44,4.076c-0.137,0.098-0.385,0.143-1.685,0.174
    c-0.86,0.021-1.128-0.152-1.128-0.152c-0.021-0.385-0.061-0.755-0.11-1.114c-0.39,0.013-0.815,0.026-1.262,0.041
    c-0.11,0.466-0.187,0.912-0.224,1.33c-0.159,0.103-1.057,0.317-3.948,0.279c0,0-0.791,0.003-1.297-0.178
    c-0.546,0.066-1.24,0.1-1.903,0.1c-0.435,0-1.432-0.13-1.748-0.384c0.078-0.307,0.144-0.604,0.2-0.896
    c-0.373,0.006-0.751,0.005-1.124-0.006c-0.001,0.26-0.002,0.521,0.001,0.781c-0.306,0.162-1.733,0.153-2.659,0.081
    c-0.273-0.021-1.893-0.102-1.893-0.343c0-0.004,0-0.008,0-0.012c-1,0.022-0.254,0.036-0.787,0.036c-1.26,0-1.858-0.134-2.083-0.259
    c0.348-4.311-1.621-5.533-7.276-5.101c3.771-1.227,6.824-0.45,7.971,1.225c-1.282-3.027-3.895-4.541-6.836-6.128
    c6.408,1.442,7.936,5.275,8.598,7.573c-0.97-5.555-2.924-8.871-5.487-12.403c3.097,2.148,5.414,6.635,6.765,11.379
    c-0.395-4.391-2.259-8.324-4.167-10.586c2.492,1.527,4.468,4.772,5.049,8.796c0.038-0.078,0.077-0.152,0.122-0.219l0.009-0.009
    c0.04-1.044,0.109-1.918,0.306-2.434c0.028-0.047,0.054-0.065,0.086-0.067c-0.007-0.444-0.015-0.91-0.023-1.371
    c0.02-0.462,0.05-0.919,0.082-1.343c0.009-0.123,0.02-0.241,0.029-0.357c-0.521-0.577-1.045-1.114-1.529-1.626
    c0.545,0.145,1.086,0.462,1.608,0.886c0.047-0.353,0.099-0.643,0.157-0.833c0.013-0.041,0.036-0.055,0.075-0.046
    c0.059-0.771,0.139-1.473,0.215-2.057c-0.78-0.938-1.702-1.769-2.811-2.41c1.028,0.192,2.016,0.73,2.928,1.557
    c0.013-0.082,0.023-0.161,0.033-0.23c0.018-0.101,0.062-0.085,0.171-0.026c0.099-1.342,0.402-2.606,0.51-3.163
    c0.018-0.078,0.073-0.05,0.188,0.047c0.231-1.349,0.547-2.486,0.729-3.104c0.03-0.099,0.067-0.074,0.156,0
    c0.102-0.429,0.207-0.875,0.313-1.316c0.118-0.438,0.265-0.863,0.39-1.262c0.257-0.796,0.489-1.474,0.64-1.84
    c0.029-0.07,0.056-0.056,0.117,0.024c0.104-0.312,0.205-0.576,0.297-0.777c0.035-0.063,0.06-0.025,0.129,0.056
    c0.29-0.793,0.56-1.653,0.852-2.378c0.307-0.717,0.577-1.323,0.759-1.65c0.038-0.071,0.063-0.018,0.163,0.147
    c0.312-0.815,0.628-1.468,0.883-1.867c0.058-0.093,0.08-0.053,0.154,0.05c0.395-0.845,0.84-1.696,1.215-2.385
    c-0.104-0.704-0.191-1.298-0.244-1.659c-0.009-0.093,0.041-0.056,0.22,0.04c-0.203-0.99-0.383-1.813-0.438-2.358
    c-0.013-0.126,0.03-0.104,0.153-0.059c-0.357-1.816-0.742-3.732-0.969-4.539c-0.023-0.104,0.031-0.09,0.182-0.022
    c-0.172-0.666-0.313-1.227-0.373-1.634c-0.023-0.143,0.036-0.096,0.203-0.047c-0.469-1.999-1.042-4.368-1.285-5.255
    c-0.02-0.092,0.027-0.06,0.194,0.021c-0.299-0.962-0.56-1.76-0.675-2.295c-0.026-0.125,0.013-0.105,0.124-0.071
    c-0.271-0.879-0.551-1.787-0.793-2.574c-0.29-0.772-0.533-1.426-0.68-1.815c-0.032-0.092,0.008-0.091,0.123-0.048
    c-0.25-0.633-0.46-1.161-0.58-1.541c-0.036-0.116,0.005-0.108,0.112-0.087c-0.423-1.153-0.798-2.179-0.99-2.706
    c-0.043-0.109,0.018-0.082,0.192-0.011c-0.609-1.367-1.18-2.503-1.389-3.181c-0.034-0.111,0-0.104,0.09-0.08
    c-0.327-0.741-0.651-1.476-0.931-2.105c-0.317-0.613-0.58-1.127-0.733-1.452c-0.041-0.088-0.009-0.086,0.097-0.047
    c-0.619-1.176-1.174-2.147-1.461-2.722c-0.05-0.099-0.021-0.101,0.063-0.086c-0.156-0.285-0.31-0.564-0.455-0.832
    c-0.162-0.257-0.317-0.503-0.462-0.729c-0.287-0.455-0.525-0.835-0.688-1.093c-0.049-0.079-0.017-0.096,0.082-0.088
    c-0.295-0.465-0.545-0.857-0.718-1.153c-0.06-0.101-0.024-0.101,0.07-0.092c-0.982-1.833-2.178-3.266-2.676-4.052
    c-0.064-0.102-0.027-0.104,0.076-0.104c-0.668-0.956-1.264-1.809-1.581-2.299c0.026,0.253,0.056,0.501,0.08,0.755
    c-0.289-0.232-0.592-0.474-0.883-0.702c0.331,0.354,0.62,0.649,0.905,0.926c0.033,0.37,0.067,0.74,0.1,1.112
    c-0.249-0.177-0.534-0.402-0.83-0.68c0.21,0.352,0.389,0.563,0.862,1.067c0.018,0.219,0.035,0.438,0.05,0.655
    c-0.213-0.246-0.452-0.547-0.688-0.895c0.15,0.439,0.302,0.714,0.728,1.429c0.024,0.346,0.047,0.69,0.068,1.034
    c-0.063-0.061-0.128-0.123-0.19-0.184c0.067,0.088,0.132,0.17,0.194,0.25c0.102,1.591,0.178,3.174,0.228,4.72
    c-0.215-0.293-0.615-0.934-1.136-1.827c0.089-0.29,0.182-0.597,0.277-0.94c-0.117,0.265-0.237,0.541-0.354,0.813
    c-0.143-0.246-0.293-0.508-0.452-0.787c0.239-0.706,0.325-1.002,0.382-1.441c-0.181,0.445-0.375,0.831-0.551,1.144
    c-0.065-0.111-0.131-0.23-0.198-0.35c0.258-0.516,0.496-1.026,0.771-1.682c-0.298,0.494-0.616,1.028-0.884,1.481
    c-0.133-0.233-0.268-0.478-0.407-0.728c0.106-0.187,0.215-0.383,0.33-0.597c-0.124,0.167-0.251,0.338-0.378,0.51
    c-0.101-0.182-0.201-0.363-0.303-0.549c0.41-0.825,0.482-1.043,0.64-1.614c-0.526,0.884-0.71,0.901-0.917,1.107
    c-0.118-0.217-0.239-0.436-0.359-0.659c0.762-1.073,1.11-1.541,1.322-2.051c-0.58,0.72-1.256,1.321-1.572,1.588
    c-0.063-0.119-0.132-0.244-0.195-0.364c0.231-0.243,0.471-0.501,0.735-0.804c-0.265,0.219-0.541,0.45-0.806,0.674
    c-0.102-0.19-0.202-0.38-0.306-0.574c0.405-0.4,0.79-0.801,1.257-1.329c-0.461,0.381-0.955,0.798-1.358,1.139
    c-0.119-0.224-0.237-0.447-0.358-0.676c0.843-0.891,1.064-1.065,1.609-1.396c-0.89,0.198-1.502,0.552-1.891,0.858
    c-0.11-0.208-0.218-0.422-0.327-0.632c0.278-0.176,0.565-0.368,0.89-0.593c-0.315,0.149-0.646,0.31-0.959,0.461
    c-0.066-0.129-0.133-0.255-0.199-0.385c1.088-1.091,1.651-1.354,2.24-1.71c-1.153-0.057-2.081,0.474-2.646,0.916
    c-0.119-0.229-0.232-0.461-0.348-0.688c0.355-0.22,0.717-0.455,1.138-0.748c-0.403,0.191-0.831,0.398-1.217,0.586
    c-0.159-0.312-0.316-0.626-0.473-0.939c0.792-0.448,1.397-0.804,2.236-1.39c-0.927,0.441-1.986,0.963-2.358,1.146
    c-0.053-0.106-0.108-0.214-0.16-0.32c1.278-1.167,1.969-1.514,2.605-1.826c-0.054-0.063-0.103-0.12-0.149-0.175
    c-1.329-0.019-2.383,0.644-2.915,1.064c-0.125-0.253-0.247-0.506-0.367-0.759c0.842-0.646,1.365-0.858,1.909-1.136
    c-0.921-0.117-1.735-0.009-2.373,0.155c-0.155-0.329-0.306-0.653-0.453-0.974c0.467-0.25,0.918-0.511,1.471-0.862
    c-0.524,0.222-1.095,0.468-1.56,0.67c-0.063-0.143-0.125-0.282-0.189-0.425c0.683-0.313,1.232-0.573,1.895-0.938
    c-0.02-0.021-0.039-0.043-0.059-0.064c-0.84,0.319-1.668,0.641-1.951,0.75c-0.051-0.115-0.105-0.234-0.157-0.351
    c-0.091,0.528-0.373,1.057-0.834,1.466c-0.973,0.863-2.361,0.884-3.106,0.046c-0.706-0.794-0.575-2.076,0.271-2.941
    c-0.445-0.066-0.853-0.267-1.157-0.61c-0.099-0.11-0.181-0.229-0.247-0.354c-0.052,0.057-0.105,0.108-0.156,0.167
    c-0.383-0.665-0.856-1.485-1.302-2.238c0.384,0.964,0.718,1.707,1.09,2.473c-0.345,0.384-0.683,0.784-1.017,1.21
    c-0.338-0.424-0.727-0.963-1.093-1.593c0.135,0.601,0.308,0.995,0.8,1.98c-0.134,0.178-0.268,0.355-0.396,0.538
    c-0.197-0.278-0.396-0.586-0.592-0.918c0.091,0.403,0.2,0.715,0.41,1.175c-0.359,0.514-0.706,1.055-1.048,1.62
    c-0.345-0.696-0.78-1.572-1.187-2.376c0.342,1.056,0.646,1.852,0.994,2.698c-0.361,0.617-0.714,1.266-1.053,1.95
    c-0.143-0.633-0.305-1.337-0.463-1.998c0.073,0.922,0.159,1.663,0.266,2.406c-0.16,0.333-0.318,0.672-0.472,1.019
    c-0.13-0.475-0.255-1.033-0.339-1.653c-0.115,0.662-0.094,1.11,0,2.442c-0.294,0.711-0.573,1.454-0.839,2.228
    c-0.09-0.374-0.172-0.785-0.231-1.227c-0.103,0.585-0.098,1.005-0.029,2.012c-0.104,0.316-0.203,0.639-0.299,0.967
    c-0.078-0.331-0.155-0.665-0.232-0.987c0.038,0.49,0.08,0.927,0.129,1.345c-0.693,2.386-1.258,5.035-1.652,7.985
    c0,0-0.311-2.58-0.36-6.172c0.188-0.391,0.295-0.678,0.39-1.044c-0.129,0.207-0.263,0.405-0.395,0.589
    c-0.001-0.223-0.002-0.449-0.002-0.68c0.4-0.581,0.586-0.91,0.769-1.375c-0.257,0.318-0.515,0.604-0.763,0.861
    c0.012-1.226,0.06-2.528,0.159-3.862c0.246-0.161,0.499-0.33,0.771-0.519c-0.248,0.122-0.504,0.246-0.757,0.371
    c0.03-0.409,0.065-0.815,0.106-1.229c0.46-0.244,0.737-0.428,1.062-0.698c-0.354,0.148-0.7,0.272-1.024,0.379
    c0.062-0.554,0.137-1.106,0.219-1.661c0.425-0.146,0.868-0.308,1.363-0.498c-0.437,0.098-0.893,0.201-1.333,0.304
    c0.029-0.194,0.057-0.387,0.09-0.579c0.815-0.315,1.423-0.455,2.051-0.636c-0.672-0.137-1.314-0.168-1.912-0.137
    c0.082-0.425,0.172-0.845,0.27-1.262c1.301-0.424,1.731-0.576,2.315-0.934c-0.83,0.216-1.591,0.323-2.18,0.379
    c0.155-0.606,0.335-1.194,0.528-1.773c0.374,0.007,0.767,0.009,1.2,0.005c-0.377-0.052-0.765-0.104-1.148-0.153
    c0.061-0.181,0.119-0.366,0.184-0.542c0.819,0.091,1.223,0.105,1.763,0.045c-0.606-0.111-1.155-0.256-1.624-0.401
    c0.093-0.233,0.191-0.463,0.292-0.689c0.511,0.104,1.049,0.208,1.669,0.309c-0.52-0.168-1.066-0.34-1.583-0.503
    c0.272-0.596,0.575-1.164,0.909-1.695c0.662,0.195,1.338,0.374,2.168,0.564c-0.672-0.264-1.391-0.543-2.025-0.787
    c0.135-0.209,0.278-0.409,0.426-0.606c0.522,0.146,1.072,0.285,1.712,0.434c-0.516-0.204-1.061-0.414-1.573-0.612
    c0.256-0.328,0.532-0.627,0.819-0.914c-0.176,0.012-0.354,0.021-0.528,0.034c-0.186-0.569-0.297-1.045-0.431-1.531
    c-0.12,0.549-0.165,1.079-0.163,1.586c-0.207,0.019-0.412,0.044-0.618,0.067c-0.093-0.677-0.198-1.323-0.371-2.142
    c0.041,0.737,0.092,1.539,0.132,2.169c-0.317,0.038-0.632,0.083-0.942,0.131c-0.236-0.555-0.526-1.341-0.739-2.262
    c-0.043,0.657,0.017,1.104,0.224,2.353c-0.339,0.063-0.671,0.132-0.997,0.212c-0.327-0.499-0.735-1.191-1.099-2.017
    c0.081,0.678,0.258,1.125,0.341,2.221c-0.093,0.028-0.186,0.055-0.276,0.082c-0.316,0.102-0.618,0.2-0.914,0.298
    c-0.157-0.408-0.314-0.871-0.451-1.379c-0.017,0.478,0.021,0.844,0.136,1.484c-0.455,0.153-0.879,0.309-1.279,0.468
    c-0.195-0.493-0.406-0.989-0.68-1.579c0.16,0.551,0.334,1.138,0.488,1.657c-0.164,0.066-0.326,0.138-0.48,0.209
    c-0.38-0.449-0.851-1.063-1.294-1.81c0.151,0.611,0.335,1.01,0.854,2.024c-0.311,0.159-0.598,0.33-0.86,0.518
    c-0.22-0.388-0.446-0.834-0.655-1.33c0.054,0.499,0.145,0.87,0.372,1.546c-0.267,0.214-0.506,0.449-0.721,0.71
    c-0.182-0.358-0.381-0.73-0.618-1.15c0.162,0.43,0.337,0.883,0.5,1.306c-0.313,0.418-0.564,0.901-0.747,1.473
    c-0.201-0.214-0.408-0.449-0.614-0.703c0.145,0.355,0.29,0.63,0.527,0.999c-0.061,0.229-0.112,0.473-0.156,0.731
    c-0.102-0.108-0.204-0.22-0.308-0.338c0.088,0.193,0.175,0.362,0.278,0.534c-0.014,0.089-0.025,0.18-0.035,0.271
    c0,0-0.272-0.081-0.659-0.264c-0.027-0.17-0.054-0.346-0.077-0.528c-0.028,0.16-0.048,0.31-0.061,0.461
    c-0.054-0.026-0.109-0.055-0.166-0.084c0.018-0.374,0.051-0.78,0.11-1.207c-0.158,0.345-0.262,0.637-0.369,1.059
    c-0.099-0.061-0.2-0.123-0.302-0.193c0.059-0.292,0.134-0.598,0.224-0.914c-0.145,0.293-0.246,0.532-0.356,0.822
    c-0.236-0.171-0.471-0.371-0.687-0.6c0.29-0.366,0.644-0.771,1.057-1.174c-0.444,0.222-0.751,0.438-1.298,0.896
    c-0.339-0.431-0.604-0.952-0.709-1.577c0.515-0.266,1.215-0.583,2.041-0.84c-0.612-0.004-1.03,0.075-2.088,0.33
    C430.9,160.13,430.946,159.734,431.061,159.312z M483.235,148.267c-0.039-0.019-0.172-0.077-0.387-0.16
    C482.979,148.162,483.106,148.209,483.235,148.267z M463.339,147.994c-0.013-0.485,0-0.93,0.024-1.338
    c-0.077,0.103-0.154,0.205-0.231,0.308C463.184,147.334,463.244,147.642,463.339,147.994z M462.235,148.906
    c0.032-0.022,0.068-0.044,0.102-0.067c-0.061-0.186-0.12-0.366-0.181-0.543c-0.042,0.057-0.083,0.114-0.123,0.172
    C462.099,148.612,462.166,148.758,462.235,148.906z M461.521,149.394c-0.021-0.045-0.042-0.084-0.062-0.127
    c-0.004,0.006-0.009,0.012-0.013,0.019C461.47,149.32,461.497,149.358,461.521,149.394z M460.702,158.165
    c0.052-0.701,0.088-1.389,0.1-2.254c-0.072,0.509-0.146,1.046-0.216,1.546c0.056,0.073,0.108,0.141,0.148,0.2
    c-0.055-0.008-0.114-0.016-0.173-0.021c-0.021,0.154-0.043,0.307-0.062,0.449C460.564,158.11,460.633,158.137,460.702,158.165z
     M507.526,173.983c0.079,0.554,0.161,1.021,0.253,1.472c0.05-0.063,0.1-0.122,0.151-0.182
    C507.8,174.853,507.661,174.408,507.526,173.983z M524.087,169.595c-0.002-0.085-0.002-0.168-0.003-0.25
    c-0.033,0.13-0.062,0.262-0.085,0.403C524.027,169.694,524.058,169.646,524.087,169.595z M517.622,175.137
    c0.026,0.095,0.054,0.19,0.081,0.284c-0.027-0.242-0.057-0.461-0.087-0.67C517.617,174.869,517.62,174.999,517.622,175.137z
     M517.472,174.6c0.041,0.005,0.081,0.009,0.123,0.015c-0.023-0.146-0.048-0.29-0.075-0.434c-0.051,0.014-0.101,0.028-0.151,0.041
    C517.401,174.344,517.436,174.47,517.472,174.6z M516.53,174.507c0.14,0.007,0.278,0.018,0.417,0.03
    c-0.012-0.066-0.021-0.129-0.029-0.187c-0.134,0.039-0.27,0.077-0.403,0.12C516.52,174.483,516.524,174.495,516.53,174.507z
     M515.281,171.387c0.032-0.097,0.063-0.198,0.091-0.306c-0.036,0.068-0.073,0.138-0.109,0.207
    C515.269,171.321,515.274,171.354,515.281,171.387z M515.137,167.012c0-0.004-0.004-0.01-0.007-0.015
    c0.004,0.007,0.007,0.014,0.007,0.021C515.137,167.016,515.137,167.014,515.137,167.012z M509.166,180.96
    c0.092,0.054,0.181,0.106,0.269,0.156c0.005-0.011,0.011-0.021,0.015-0.034C509.354,181.042,509.259,181,509.166,180.96z
     M510.123,179.584c0.236,0.123,0.453,0.232,0.661,0.336c0.049-0.014,0.097-0.023,0.146-0.034
    C510.661,179.785,510.388,179.683,510.123,179.584z M511.467,179.346c-0.002-0.018-0.004-0.034-0.006-0.051
    c-0.044-0.006-0.087-0.014-0.131-0.02C511.376,179.3,511.422,179.323,511.467,179.346z M510.517,177.954
    c0.301,0.125,0.599,0.236,0.956,0.406c0.02-0.117,0.046-0.23,0.08-0.343C511.228,177.965,510.883,177.937,510.517,177.954z
     M509.879,184.104c-0.075-0.098-0.147-0.198-0.213-0.308c-0.475-0.271-1.076-0.532-1.798-0.667c0.561,0.308,0.971,0.48,1.912,1.277
    c0.006-0.021,0.013-0.044,0.02-0.063C509.827,184.26,509.854,184.182,509.879,184.104z M503.267,194.852
    c0.063-0.077,0.078-0.044,0.118,0.046c0.6-0.808,1.127-1.533,1.389-1.911c0.063-0.084,0.069-0.01,0.118,0.185
    c0.334-0.518,0.647-1.001,0.914-1.412c0.254-0.419,0.458-0.771,0.64-1c0.06-0.075,0.074-0.046,0.106,0.029
    c0.71-1.123,1.26-2.296,1.557-2.801c0.04-0.068,0.057-0.046,0.088,0.046c0.508-0.958,0.773-1.839,1.007-2.306
    c0.038-0.081,0.056-0.063,0.097,0.003c0.2-0.454,0.349-0.903,0.473-1.301c-0.1,0.134-0.2,0.266-0.3,0.396
    c-0.521-0.43-1.448-1.029-2.683-1.187c0.585,0.376,1.155,0.672,2.234,1.763c-0.083,0.104-0.163,0.203-0.244,0.304
    c-0.423-0.287-0.949-0.645-1.437-0.971c0.495,0.437,0.899,0.769,1.33,1.104c-0.179,0.22-0.354,0.436-0.525,0.641
    c-0.967-0.599-1.217-0.759-1.667-0.916c0.716,0.502,1.21,1.019,1.401,1.233c-0.261,0.305-0.507,0.587-0.732,0.84
    c-0.555-0.338-0.867-0.609-1.217-0.878c0.229,0.501,0.548,0.913,0.882,1.246c-0.134,0.146-0.259,0.278-0.373,0.398
    c-0.27-0.421-0.643-0.994-0.987-1.515c0.293,0.652,0.543,1.132,0.84,1.663c-0.127,0.128-0.236,0.235-0.323,0.314
    c-0.172,0.157-0.344,0.313-0.514,0.472c-0.207-0.361-0.445-0.773-0.672-1.159c0.188,0.492,0.358,0.889,0.543,1.283
    c-0.133,0.129-0.265,0.257-0.394,0.394c-0.465-0.852-0.574-1.327-0.734-1.829c-0.06,0.91,0.158,1.699,0.373,2.235
    c-0.146,0.177-0.29,0.366-0.428,0.571c-0.143-0.304-0.311-0.716-0.438-1.19c-0.028,0.406,0.024,0.668,0.201,1.57
    c-0.146,0.248-0.282,0.52-0.412,0.819c-0.096-0.181-0.194-0.362-0.29-0.538c0.082,0.24,0.161,0.455,0.239,0.659
    c-0.373,0.902-0.675,2.063-0.873,3.623C502.856,195.397,503.09,195.077,503.267,194.852z M489.232,245.109
    c-0.042,0.143-0.083,0.287-0.123,0.433c0.083-0.057,0.167-0.111,0.251-0.165C489.318,245.287,489.275,245.197,489.232,245.109z
     M490.685,251.347c-0.027-1.125-0.146-2.11-0.332-2.996c-0.186,0.306-0.355,0.64-0.51,1.013
    C490.199,249.999,490.495,250.664,490.685,251.347z M489.244,248.405c0.252-0.472,0.524-0.873,0.811-1.218
    c-0.148-0.487-0.317-0.945-0.504-1.381c-0.239,0.244-0.475,0.501-0.704,0.768c-0.077,0.329-0.148,0.665-0.214,1.007
    C488.845,247.846,489.049,248.121,489.244,248.405z M488.619,249.881c-0.082-0.199-0.168-0.397-0.257-0.594
    c-0.093,0.749-0.158,1.523-0.188,2.325C488.298,250.971,488.448,250.397,488.619,249.881z M481.422,241.126
    c-0.164,0.08-0.326,0.16-0.481,0.244c0.154,0.345,0.302,0.702,0.439,1.071c0.005-0.011,0.009-0.021,0.014-0.031
    C481.39,241.911,481.396,241.47,481.422,241.126z M476.731,246.626C476.731,246.627,476.731,246.627,476.731,246.626
    c0,0.04,0.198,0.087,0.216,0.136c0.261,0.294,0.598,0.609,0.847,0.947c-0.089-1.225-0.154-2.449-0.271-3.656
    c-0.203,0.241-0.372,0.488-0.55,0.741c-0.009,0.471-0.174,1.014-0.166,1.596C476.832,246.469,476.731,246.547,476.731,246.626z
     M481.437,245.647c-0.054-0.289-0.112-0.576-0.174-0.862c-0.063,0.211-0.125,0.425-0.185,0.641
    C481.196,245.497,481.315,245.569,481.437,245.647z M480.678,249.059l0.192-0.415c0.028-1.033,0.259-1.667,0.445-2.208
    c-0.133-0.132-0.263-0.264-0.399-0.396c-0.301,1.193-0.539,2.471-0.691,3.829C480.392,249.545,480.549,249.27,480.678,249.059z
     M478.025,250.118c-0.018-0.33-0.036-0.662-0.057-0.994c-0.14-0.282-0.293-0.559-0.456-0.828c0.099,0.412,0.188,0.834,0.271,1.262
    C477.861,249.727,477.942,249.915,478.025,250.118z M477.169,240.975c-0.061,0.386-0.125,0.799-0.19,1.22
    c-0.044,0.422-0.089,0.851-0.131,1.261c0.053-0.011,0.084,0.001,0.11,0.063c0.007,0.045,0.011,0.098,0.015,0.147
    c0.164-0.174,0.33-0.342,0.501-0.5c-0.085-0.827-0.175-1.642-0.269-2.438C477.193,240.809,477.182,240.891,477.169,240.975z
     M477.4,237.189c0.028,0.118,0.057,0.239,0.085,0.362c0.003,0.006,0.005,0.015,0.005,0.024c0.317,1.381,0.612,2.979,0.878,4.858
    c0.591-0.424,1.222-0.767,1.892-1.015c-0.669-1.664-1.588-3.137-2.853-4.259C477.405,237.17,477.403,237.18,477.4,237.189z
     M478.462,243.114c0.256,1.921,0.481,4.121,0.672,6.658c0.061,0.121,0.12,0.242,0.177,0.365c0.299-1.676,0.632-3.235,1.025-4.651
    c-0.312-0.293-0.633-0.588-0.961-0.886c0.311,0.109,0.677,0.269,1.078,0.477c0.157-0.541,0.326-1.06,0.504-1.555
    c-0.17-0.626-0.368-1.236-0.593-1.825C479.646,242.127,479.017,242.603,478.462,243.114z M487.161,248.836
    c-0.062,0.095-0.12,0.191-0.181,0.288c0.023,0.107,0.046,0.222,0.067,0.34C487.085,249.255,487.12,249.047,487.161,248.836z
     M486.411,245.838c0.097,0.521,0.16,1.255,0.203,2.072c0.208-0.258,0.422-0.511,0.644-0.751
    C486.98,246.689,486.695,246.247,486.411,245.838z M480.529,227.042c-0.221,0.407-0.46,0.892-0.719,1.419
    c0.107-0.02,0.145-0.016,0.121,0.056c-0.108,0.391-0.459,1.08-0.73,1.92c0.097,0.001,0.131,0.012,0.107,0.097
    c-0.076,0.275-0.238,0.66-0.438,1.127c-0.202,0.465-0.452,1.011-0.628,1.629c0.098-0.008,0.141,0.008,0.131,0.069
    c-0.063,0.364-0.24,1.007-0.462,1.756c-0.19,0.602-0.331,1.292-0.483,1.957c1.471,0.806,2.59,2.283,3.447,4.15
    c0.181-0.05,0.363-0.093,0.549-0.131c0.013-0.152,0.028-0.285,0.05-0.394c0.009-0.049,0.031-0.066,0.069-0.062
    c-0.041-1.568-0.104-2.906-0.099-3.653c0-0.117,0.047-0.108,0.163-0.064c-0.054-1.558-0.21-3.036-0.214-3.692
    c-0.004-0.092,0.057-0.076,0.193-0.001c-0.093-1.58-0.193-2.937-0.215-3.681c-0.003-0.119,0.04-0.104,0.148-0.057
    c-0.093-1.37-0.261-2.881-0.372-4.007c-0.083,0.149-0.164,0.302-0.244,0.456c0.109,0.002,0.151,0.005,0.123,0.095
    C480.94,226.298,480.76,226.639,480.529,227.042z M449.813,151.637c-0.009,0.549-0.005,1.105,0.024,1.78
    c0.058-0.523,0.114-1.076,0.166-1.581C449.938,151.77,449.875,151.703,449.813,151.637z M458.019,153.24
    c-0.015,0.086-0.032,0.17-0.047,0.254c0.03-0.073,0.059-0.147,0.093-0.22C458.05,153.262,458.033,153.251,458.019,153.24z
     M458.065,152.919c0.011-0.014,0.019-0.027,0.027-0.04c0.005-0.093,0.008-0.185,0.015-0.277
    C458.093,152.707,458.081,152.814,458.065,152.919z M462.327,166.238c0.014,0.019,0.027,0.036,0.042,0.055
    c0.006-0.029,0.011-0.053,0.017-0.069C462.367,166.229,462.347,166.233,462.327,166.238z M464.728,172.971
    c-0.071-0.094-0.14-0.185-0.21-0.275c0.009,0.08,0.018,0.159,0.025,0.238C464.584,172.939,464.646,172.954,464.728,172.971z
     M455.611,164.313c0.537-0.541,1.016-0.864,1.447-1.081c-0.014-0.015-0.027-0.031-0.041-0.045
    c-0.078-0.081-0.152-0.157-0.225-0.231c-0.488,0.176-0.905,0.396-1.208,0.581C455.644,163.786,455.651,164.049,455.611,164.313z
     M455.125,162.685c0.349-0.27,0.68-0.463,0.989-0.609c-0.078-0.083-0.155-0.164-0.228-0.241c-0.338,0.153-0.688,0.242-1.029,0.277
    c-0.062,0.034-0.117,0.066-0.163,0.095c-0.012-0.028-0.021-0.057-0.033-0.084c-0.169,0.005-0.335-0.004-0.497-0.025
    c-0.02,0.021-0.042,0.04-0.062,0.062C454.491,162.222,454.847,162.398,455.125,162.685z M450.703,159.72
    c0.011-0.019,0.022-0.037,0.037-0.055c-0.056-0.107-0.11-0.216-0.164-0.32C450.619,159.475,450.661,159.598,450.703,159.72z
     M448.547,155.896c0.043-0.022,0.089-0.042,0.132-0.062c-0.041-0.16-0.089-0.315-0.146-0.482
    C448.539,155.537,448.544,155.718,448.547,155.896z M446.828,157.271c0.396,0.324,0.815,0.657,1.312,1.029
    c-0.421-0.43-0.866-0.881-1.281-1.296c0.089-0.076,0.182-0.146,0.276-0.218c-0.157-0.461-0.316-0.995-0.441-1.592
    C446.641,155.799,446.68,156.232,446.828,157.271z"/>
</g>
<g>
  <defs>
    <path id="SVGID_1_" d="M522.859,238.552l-0.162,15.754l16.079,0.487l13.155-19.652c0,0,12.019,0,12.506-0.162
      s15.916-26.312,15.916-26.312s-3.571-20.788-5.684-20.788s-22.089-1.3-22.738-1.3s-15.918,6.335-16.892,6.497
      s-10.231,9.744-10.231,9.744l4.547,23.063l-0.812,8.77L522.859,238.552z"/>
  </defs>
  <clipPath id="SVGID_8_">
    <use xlink:href="#SVGID_1_"  overflow="visible"/>
  </clipPath>
  <path clip-path="url(#SVGID_8_)" fill="none"  stroke-linecap="round" stroke-linejoin="round" d="
    M569.658,209.897c-0.323-0.015-0.691-0.013-1.087,0.019c0.326-0.132,0.596-0.19,0.989-0.333c-0.041-0.115-0.088-0.231-0.142-0.351
    c-0.102,0.057-0.373,0.167-0.506,0.202c0.16-0.121,0.365-0.229,0.463-0.296c-0.097-0.205-0.211-0.417-0.351-0.639
    c-0.306,0.139-0.651,0.315-1.01,0.531c0.242-0.271,0.453-0.449,0.87-0.749c-0.21-0.313-0.463-0.646-0.764-0.992
    c-0.19,0.099-0.39,0.21-0.594,0.336c0.153-0.176,0.293-0.31,0.481-0.464c-0.07-0.08-0.144-0.16-0.22-0.241
    c-0.352,0.153-0.772,0.359-1.21,0.63c0.265-0.301,0.49-0.488,0.999-0.853c-0.038-0.038-0.072-0.076-0.111-0.115
    c-0.207-0.209-0.426-0.398-0.649-0.58c-0.414,0.136-1.04,0.379-1.713,0.762c0.327-0.349,0.593-0.535,1.356-1.036
    c-0.31-0.223-0.635-0.418-0.972-0.593c-0.22,0.216-0.456,0.436-0.733,0.683c0.203-0.244,0.417-0.501,0.619-0.74
    c-0.192-0.098-0.386-0.19-0.585-0.274c-0.313,0.304-0.722,0.748-1.118,1.306c0.148-0.44,0.307-0.721,0.762-1.452
    c-0.403-0.152-0.817-0.277-1.238-0.378c-0.25,0.454-0.493,0.878-0.837,1.407c0.221-0.495,0.468-1.034,0.659-1.451
    c-0.43-0.098-0.868-0.173-1.31-0.227c-0.111,0.27-0.222,0.572-0.32,0.901c-0.001-0.31,0.026-0.552,0.101-0.927
    c-0.156-0.017-0.313-0.033-0.47-0.044c-0.127,0.418-0.274,1.011-0.354,1.696c-0.086-0.469-0.078-0.797-0.027-1.719
    c-0.258-0.015-0.515-0.018-0.772-0.021c-0.028,0.383-0.066,0.771-0.128,1.236c-0.01-0.41-0.017-0.849-0.021-1.236
    c-0.418,0.002-0.835,0.016-1.248,0.046c0.05,0.414,0.057,0.668,0.025,0.993c-0.072-0.357-0.163-0.686-0.256-0.977
    c-0.247,0.021-0.49,0.044-0.731,0.073c0.1,0.521,0.139,1.104,0.05,1.733c-0.178-0.492-0.321-0.97-0.663-1.65
    c-0.227,0.035-0.448,0.074-0.667,0.114c6.474-6.64,13.784-4.044,13.784-4.044c-0.071-0.025-0.14-0.049-0.211-0.076
    c0.138,0.047,0.211,0.076,0.211,0.076c-0.579-0.407-1.144-0.768-1.694-1.093c-0.388-0.05-0.927-0.092-1.525-0.057
    c0.298-0.12,0.532-0.173,1.009-0.239c-0.245-0.136-0.488-0.264-0.729-0.384c-0.214,0.051-0.441,0.107-0.679,0.171
    c0.143-0.099,0.271-0.193,0.427-0.295c-0.491-0.237-0.974-0.444-1.442-0.619c-0.355,0.123-0.852,0.329-1.372,0.645
    c0.232-0.281,0.432-0.446,0.928-0.801c-0.589-0.196-1.158-0.346-1.708-0.45c-0.709,0.579-1.005,0.994-1.354,1.409
    c0.104-0.607,0.322-1.114,0.576-1.531c-0.644-0.076-1.259-0.09-1.848-0.05c-0.221,0.417-0.483,0.982-0.719,1.667
    c0.049-0.521,0.127-0.878,0.338-1.633c-0.233,0.025-0.464,0.058-0.688,0.101c-0.086,0.341-0.17,0.729-0.238,1.156
    c-0.027-0.378-0.026-0.679,0.007-1.106c-0.888,0.193-1.703,0.515-2.445,0.936c-0.132,0.447-0.275,1.027-0.379,1.705
    c-0.038-0.484-0.028-0.836,0.03-1.497c-0.685,0.43-1.301,0.944-1.853,1.517c0.04,0.359,0.097,0.762,0.184,1.197
    c-0.148-0.342-0.247-0.622-0.356-1.014c-0.672,0.728-1.237,1.541-1.698,2.385c0.076-0.423,0.153-0.855,0.228-1.287
    c-0.538-0.096-0.766-0.148-1.08-0.272c0.44,0.05,0.827,0.051,1.12,0.036c0.029-0.173,0.06-0.347,0.088-0.519
    c-0.381-0.021-0.585-0.047-0.859-0.116c0.338-0.007,0.639-0.038,0.892-0.076c0.105-0.638,0.207-1.271,0.304-1.884
    c-0.472,0.07-0.699,0.085-1.005,0.041c0.4-0.098,0.758-0.229,1.053-0.356c0.021-0.136,0.042-0.271,0.063-0.403
    c-0.218,0.063-0.379,0.099-0.584,0.123c0.226-0.095,0.43-0.194,0.61-0.292c0.112-0.75,0.217-1.451,0.306-2.071
    c-0.163,0.06-0.301,0.097-0.466,0.125c0.177-0.099,0.341-0.202,0.491-0.304c0.025-0.186,0.051-0.365,0.074-0.532
    c-0.142,0.084-0.26,0.137-0.403,0.183c0.16-0.127,0.305-0.259,0.433-0.386c0.141-1.002,0.223-1.629,0.223-1.629
    c-0.252,0.256-0.484,0.522-0.71,0.795c0.004,0.116,0.01,0.238,0.018,0.366c-0.026-0.105-0.052-0.202-0.071-0.302
    c-0.334,0.406-0.644,0.827-0.923,1.261c-0.005,0.237,0.002,0.507,0.021,0.802c-0.058-0.213-0.092-0.389-0.128-0.628
    c-0.075,0.119-0.148,0.236-0.218,0.356c0.049,0.276,0.121,0.611,0.227,0.978c-0.13-0.251-0.211-0.448-0.331-0.795
    c-0.355,0.628-0.661,1.271-0.92,1.922c0.106,0.23,0.24,0.495,0.402,0.775c-0.17-0.192-0.291-0.353-0.466-0.617
    c-0.042,0.111-0.084,0.221-0.123,0.332c0.148,0.285,0.344,0.618,0.585,0.961c-0.237-0.19-0.396-0.357-0.678-0.704
    c-0.083,0.24-0.161,0.481-0.232,0.725c0.113,0.129,0.238,0.265,0.375,0.402c-0.146-0.097-0.266-0.185-0.403-0.3
    c-0.173,0.598-0.308,1.192-0.41,1.775c0.205,0.217,0.481,0.491,0.821,0.772c-0.28-0.146-0.468-0.271-0.855-0.563
    c-0.106,0.651-0.173,1.285-0.2,1.89c0.201,0.185,0.449,0.397,0.746,0.614c-0.254-0.112-0.435-0.215-0.753-0.421
    c-0.005,0.168-0.008,0.332-0.008,0.494c0.147,0.061,0.308,0.122,0.479,0.181c-0.169-0.018-0.309-0.038-0.477-0.075
    c0.001,0.239,0.011,0.472,0.024,0.698c-0.048-0.408-0.119-0.822-0.229-1.242c-0.212,0.045-0.426,0.091-0.632,0.136
    c0.215-0.082,0.414-0.154,0.607-0.224c-0.068-0.252-0.152-0.507-0.249-0.763c-0.287,0.208-0.5,0.334-0.775,0.463
    c0.257-0.232,0.486-0.465,0.687-0.686c-0.16-0.387-0.36-0.778-0.603-1.176c-0.145,0.458-0.351,0.932-0.653,1.394
    c0.056-0.629,0.145-1.218,0.087-2.212c-0.173-0.225-0.36-0.452-0.565-0.682c-0.099,0.662-0.167,0.972-0.325,1.373
    c0.052-0.676,0.026-1.279-0.016-1.736c-0.101-0.104-0.202-0.205-0.31-0.31c-0.018,0.455-0.037,0.945-0.058,1.41
    c-0.063-0.595-0.103-1.095-0.127-1.584c-0.305-0.283-0.639-0.57-1.002-0.859c0.1,0.793,0.124,1.125,0.073,1.591
    c-0.167-0.855-0.424-1.563-0.607-2.001c-0.181-0.134-0.368-0.269-0.563-0.404c0.091,0.416,0.186,0.858,0.272,1.277
    c-0.198-0.531-0.354-0.987-0.495-1.431c-0.118-0.081-0.242-0.162-0.366-0.243c0.067,0.219,0.133,0.438,0.198,0.65
    c-0.126-0.267-0.24-0.513-0.346-0.748c-0.627-0.403-1.317-0.813-2.078-1.227c0,0,2.095,2.605,4.163,5.372
    c-0.063-0.047-0.126-0.096-0.19-0.142c-0.042,0.293-0.086,0.598-0.13,0.888c-0.002-0.358,0.004-0.673,0.016-0.972
    c-0.212-0.148-0.431-0.288-0.654-0.422c0.003,0.356,0.005,0.746,0.006,1.113c-0.07-0.45-0.117-0.831-0.154-1.203
    c-0.231-0.134-0.466-0.263-0.708-0.379c0.163,0.585,0.229,0.897,0.271,1.331c-0.208-0.598-0.436-1.106-0.634-1.495
    c-0.199-0.087-0.404-0.168-0.611-0.241c0.055,0.273,0.108,0.557,0.161,0.828c-0.113-0.317-0.208-0.6-0.29-0.871
    c-0.547-0.186-1.115-0.327-1.706-0.41c0.1,0.379,0.147,0.645,0.184,0.985c-0.133-0.377-0.274-0.718-0.411-1.018
    c-0.217-0.023-0.439-0.038-0.662-0.049c0.32,0.659,0.451,0.973,0.58,1.433c-0.331-0.594-0.669-1.083-0.943-1.442
    c-0.158,0-0.317,0.005-0.478,0.013c0.135,0.302,0.279,0.624,0.414,0.93c-0.217-0.338-0.395-0.634-0.558-0.921
    c-0.363,0.022-0.731,0.063-1.108,0.129c0.306,0.357,0.593,0.795,0.788,1.329c-0.394-0.348-0.738-0.701-1.499-1.177
    c-0.201,0.051-0.406,0.109-0.612,0.173c0.271,0.289,0.577,0.616,0.861,0.924c-0.401-0.318-0.721-0.593-1.03-0.871
    c-0.248,0.082-0.498,0.171-0.751,0.273c0.523,0.289,0.737,0.422,1.001,0.66c-0.539-0.252-1.039-0.405-1.394-0.497
    c-0.155,0.069-0.314,0.146-0.474,0.225c0.149,0.119,0.301,0.24,0.447,0.356c-0.192-0.11-0.366-0.215-0.532-0.315
    c-0.231,0.114-0.463,0.236-0.698,0.368c0.164,0.082,0.3,0.158,0.449,0.238c-0.233-0.043-0.456-0.08-0.666-0.113
    c-0.206,0.121-0.414,0.247-0.622,0.383c0.457,0.031,0.688,0.065,0.987,0.156c-0.569,0.002-1.069,0.069-1.426,0.137
    c-0.466,0.32-0.938,0.674-1.416,1.069c0,0,0.004-0.002,0.009-0.004c0.272-0.112,7.016-2.771,14.29,3.376
    c-0.183,0.004-0.369,0.013-0.563,0.03c0.196,0.317,0.406,0.666,0.606,0.996c-0.293-0.362-0.53-0.671-0.755-0.981
    c-0.168,0.018-0.34,0.039-0.514,0.066c0.083,0.136,0.165,0.272,0.246,0.407c-0.113-0.14-0.216-0.27-0.314-0.397
    c-0.244,0.038-0.493,0.082-0.751,0.138c0.233,0.3,0.375,0.522,0.526,0.812c-0.27-0.282-0.536-0.53-0.786-0.749
    c-0.189,0.047-0.371,0.105-0.552,0.166c0.233,0.205,0.479,0.423,0.712,0.632c-0.321-0.209-0.594-0.396-0.852-0.583
    c-0.334,0.122-0.657,0.265-0.96,0.43c0.704,0.275,1.008,0.417,1.394,0.687c-0.734-0.229-1.401-0.341-1.881-0.396
    c-0.16,0.105-0.314,0.218-0.466,0.334c0.318,0.112,0.665,0.235,0.991,0.354c-0.431-0.075-0.788-0.152-1.137-0.236
    c-0.239,0.193-0.467,0.401-0.683,0.626c0.813,0.351,1.312,0.445,1.836,0.584c-0.863,0.234-1.687,0.15-2.339-0.019
    c-0.194,0.24-0.378,0.489-0.553,0.751c0.349,0.073,0.742,0.159,1.109,0.241c-0.458-0.019-0.832-0.049-1.204-0.091
    c-0.183,0.281-0.353,0.573-0.512,0.875c0.218,0.047,0.439,0.097,0.655,0.144c-0.26-0.01-0.489-0.025-0.709-0.043
    c-0.098,0.188-0.189,0.379-0.28,0.573c0.433,0.09,1.074,0.225,1.652,0.354c-0.674-0.026-1.169-0.081-1.739-0.159
    c-0.093,0.21-0.183,0.419-0.267,0.636c0.423-0.019,0.887,0.018,1.376,0.151c-0.476,0.124-0.931,0.209-1.617,0.5
    c-0.109,0.313-0.208,0.634-0.302,0.958c0.338-0.081,0.717-0.17,1.074-0.25c-0.423,0.18-0.774,0.313-1.126,0.434
    c-0.21,0.749-0.387,1.515-0.528,2.284c0.28-0.222,0.617-0.52,0.953-0.895c-0.198,0.446-0.39,0.688-1.063,1.546
    c-0.041,0.248-0.077,0.494-0.111,0.738c0.229-0.189,0.472-0.39,0.702-0.579c-0.267,0.303-0.499,0.552-0.729,0.786
    c-0.061,0.454-0.111,0.904-0.154,1.349c0.206-0.182,0.429-0.396,0.651-0.642c-0.155,0.35-0.306,0.573-0.689,1.065
    c-0.03,0.362-0.054,0.718-0.074,1.067c0.156-0.166,0.316-0.337,0.472-0.5c-0.173,0.25-0.328,0.469-0.482,0.672
    c-0.164,3.018,0.007,5.442,0.021,5.966c0.191-1.931,0.623-3.739,1.222-5.404c-0.182,0.093-0.372,0.188-0.586,0.29
    c0.21-0.15,0.432-0.304,0.644-0.452c0.111-0.302,0.226-0.601,0.349-0.894c-0.121,0.068-0.246,0.14-0.369,0.217
    c0.16-0.201,0.313-0.363,0.504-0.534c0.104-0.236,0.211-0.471,0.322-0.701c-0.285,0.093-0.583,0.203-0.889,0.333
    c0.321-0.296,0.6-0.488,1.099-0.761c0.24-0.475,0.493-0.934,0.759-1.375c-0.366,0.05-0.76,0.122-1.17,0.225
    c0.411-0.272,0.747-0.423,1.436-0.658c0.079-0.125,0.162-0.248,0.243-0.372c-0.213,0.021-0.436,0.037-0.686,0.05
    c0.253-0.061,0.518-0.121,0.771-0.181c0.165-0.245,0.337-0.484,0.51-0.718c-0.399-0.046-0.795-0.103-1.282-0.195
    c0.478,0.004,0.996,0.013,1.409,0.022c0.123-0.164,0.247-0.324,0.374-0.482c-0.688-0.257-1.192-0.349-1.642-0.479
    c0.718-0.175,1.431-0.135,2.05-0.016c0.127-0.148,0.255-0.294,0.384-0.435c-0.25-0.137-0.517-0.276-0.799-0.41
    c0.336,0.053,0.613,0.119,0.962,0.234c0.116-0.124,0.235-0.242,0.354-0.361c-0.595-0.37-1.069-0.68-1.718-1.168
    c0.725,0.382,1.545,0.83,1.873,1.009c0.209-0.207,0.42-0.408,0.633-0.598c-0.813-0.779-1.05-1.003-1.332-1.426
    c0.691,0.588,1.345,0.963,1.669,1.134c0.177-0.149,0.355-0.292,0.533-0.429c-0.407-0.585-0.735-1.076-1.159-1.795
    c0.485,0.61,1.025,1.306,1.314,1.678c0.197-0.149,0.395-0.294,0.594-0.428c-0.176-0.389-0.345-0.787-0.537-1.279
    c0.22,0.398,0.454,0.827,0.656,1.201c0.108-0.071,0.218-0.145,0.326-0.21c-0.15-0.585-0.378-1.329-0.714-2.134
    c0.394,0.515,0.614,0.917,1.096,1.907c0.2-0.113,0.401-0.22,0.602-0.318c-0.083-1.121-0.264-1.687-0.414-2.308
    c0.548,0.668,0.864,1.387,1.049,2.021c0.085-0.035,0.169-0.068,0.253-0.098c-0.11-0.505-0.207-1.003-0.305-1.63
    c0.154,0.53,0.317,1.103,0.448,1.569c0.034-0.013,0.066-0.038,0.101-0.049c0.022-0.392,0.009-0.941-0.004-1.403
    c0.119,0.399,0.144,0.502,0.218,1.052c0.004-0.001,0.007-0.002,0.011-0.003c-0.223,0.045-0.291,0.851-0.436,1.238
    c0.356,0.083,0.813,0.283,1.218,0.381c-0.483-0.032-0.858-0.053-1.249-0.109c-0.105,0.289-0.205,0.604-0.304,0.914
    c0.365,0.014,0.774,0.102,1.184,0.323c-0.376,0.038-0.729,0.045-1.342,0.231c-0.052,0.181-0.102,0.366-0.15,0.554
    c0.377-0.025,0.858-0.053,1.3-0.074c-0.514,0.115-0.915,0.185-1.342,0.243c-0.041,0.155-0.077,0.31-0.115,0.469
    c0.259-0.016,0.532-0.031,0.793-0.044c-0.304,0.068-0.567,0.121-0.822,0.162c-0.089,0.385-0.174,0.778-0.253,1.181
    c0.252-0.024,0.523-0.014,0.808,0.049c-0.008,0.006-0.018,0.016-0.029,0.029c-0.033,0.036-0.066,0.075-0.101,0.116
    c-0.223,0.069-0.462,0.15-0.774,0.302c-0.032,0.175-0.062,0.355-0.094,0.534c0.106-0.027,0.218-0.055,0.332-0.083
    c-0.035,0.052-0.07,0.104-0.107,0.158c-0.084,0.03-0.168,0.061-0.252,0.09c-0.014,0.079-0.024,0.16-0.038,0.24
    c-0.007-0.051-0.015-0.062-0.044-0.022c-0.199,0.252-0.527,0.716-0.909,1.265c-0.38,0.552-0.83,1.177-1.196,1.806
    c-0.058-0.133-0.058-0.214-0.112-0.113c-0.131,0.235-0.308,0.551-0.514,0.922c-0.031-0.076-0.055-0.094-0.091-0.033
    c-0.163,0.281-0.438,0.755-0.76,1.312c-0.159,0.278-0.33,0.577-0.505,0.882c-0.16,0.315-0.323,0.638-0.483,0.952
    c-0.026-0.101-0.039-0.132-0.078-0.063c-0.209,0.363-0.581,1.089-0.998,1.898c-0.035-0.063-0.051-0.085-0.095-0.009
    c-0.142,0.257-0.34,0.62-0.536,1.075c-0.026-0.067-0.043-0.081-0.071-0.02c-0.248,0.566-0.828,1.897-1.374,3.148
    c-0.036-0.067-0.046-0.095-0.09-0.012c-0.092,0.181-0.208,0.411-0.316,0.689c-0.1,0.281-0.217,0.604-0.342,0.955
    c-0.048-0.115-0.059-0.154-0.086-0.091c-0.253,0.599-0.844,2.152-1.301,3.543c-0.104-0.111-0.116-0.195-0.159-0.08
    c-0.091,0.242-0.188,0.573-0.299,0.966c-0.062-0.075-0.093-0.092-0.112-0.021c-0.116,0.438-0.344,1.292-0.592,2.23
    c-0.044-0.053-0.063-0.069-0.087,0.01c-0.066,0.229-0.162,0.543-0.246,0.925c-0.079,0.383-0.171,0.829-0.271,1.309
    c-0.047-0.073-0.066-0.091-0.086-0.021c-0.14,0.516-0.389,1.667-0.648,2.839c-0.051-0.055-0.071-0.074-0.1,0.008
    c-0.208,0.541-0.281,1.622-0.494,2.843c-0.142-0.166-0.18-0.245-0.202-0.144c-0.094,0.446-0.236,1.355-0.393,2.349
    c-0.08-0.067-0.108-0.097-0.124-0.005c-0.085,0.469-0.113,1.303-0.218,2.278c-0.064-0.069-0.09-0.085-0.102-0.014
    c-0.039,0.246-0.086,0.634-0.137,1.097c-0.469-0.677-0.914-1.277-1.152-1.554c-0.039-0.042-0.045-0.004-0.076,0.116
    c-0.374-0.492-0.681-0.908-0.911-1.154c-0.053-0.057-0.061-0.027-0.09,0.05c-0.705-0.885-1.452-1.823-1.798-2.199
    c-0.041-0.044-0.055-0.02-0.073,0.062c-0.265-0.321-0.486-0.588-0.657-0.764c-0.052-0.052-0.063-0.025-0.089,0.047
    c-0.494-0.563-0.946-1.05-1.188-1.292c-0.052-0.052-0.055-0.008-0.07,0.117c-0.608-0.676-1.104-1.267-1.437-1.54
    c-0.054-0.045-0.061-0.021-0.074,0.041c-0.67-0.718-1.323-1.421-1.63-1.726c-0.042-0.042-0.05-0.02-0.056,0.054
    c-0.528-0.594-0.981-1.104-1.235-1.39c-0.046-0.049-0.054-0.03-0.069,0.027c-0.505-0.598-0.965-1.144-1.2-1.422
    c-0.036-0.042-0.055-0.024-0.077,0.042c-0.206-0.257-0.381-0.473-0.518-0.626c-0.046-0.052-0.055-0.03-0.076,0.033
    c-0.486-0.543-0.901-1.083-1.229-1.523c0.413-0.516,0.606-1.308,0.628-2.271c-0.103-0.101-0.216-0.204-0.342-0.309
    c0.124,0.05,0.225,0.101,0.343,0.173c0-0.111-0.001-0.224-0.005-0.339c-0.155-0.12-0.343-0.252-0.556-0.377
    c0.188,0.046,0.329,0.1,0.548,0.204c-0.01-0.2-0.025-0.406-0.046-0.616c-0.203-0.118-0.459-0.252-0.76-0.373
    c0.236,0.024,0.404,0.065,0.737,0.174c-0.052-0.442-0.128-0.904-0.224-1.376c-0.123-0.048-0.249-0.1-0.385-0.157
    c0.122,0.032,0.247,0.066,0.374,0.1c-0.041-0.193-0.084-0.389-0.13-0.587c-0.255-0.041-0.519-0.089-0.823-0.151
    c0.264,0.019,0.54,0.041,0.801,0.061c-0.036-0.152-0.074-0.308-0.114-0.463c-0.248-0.071-0.687-0.178-1.217-0.221
    c0.305-0.059,0.511-0.057,1.146-0.041c-0.032-0.122-0.066-0.246-0.102-0.367c-0.628-0.098-0.966-0.073-1.33-0.07
    c0.425-0.203,0.85-0.287,1.216-0.312c-0.063-0.207-0.128-0.414-0.196-0.62c-0.555,0.045-0.866,0.128-1.204,0.199
    c0.372-0.253,0.757-0.409,1.102-0.508c-0.09-0.262-0.185-0.523-0.281-0.783c-0.255,0.063-0.523,0.125-0.833,0.191
    c0.264-0.091,0.542-0.184,0.804-0.272c-0.032-0.086-0.064-0.173-0.098-0.259c-0.177,0.003-0.746,0.024-1.423,0.193
    c0.299-0.169,0.51-0.229,1.311-0.479c-0.111-0.282-0.228-0.559-0.346-0.832c-0.501,0.089-0.95,0.16-1.556,0.227
    c0.522-0.124,1.101-0.257,1.503-0.349c-0.03-0.068-0.059-0.138-0.089-0.206c-0.173-0.003-0.745,0-1.431,0.15
    c0.302-0.16,0.513-0.214,1.303-0.433c-0.048-0.106-0.099-0.213-0.148-0.317c-0.906,0.05-1.31,0.181-1.755,0.287
    c0.509-0.415,1.077-0.622,1.542-0.727c-0.086-0.169-0.17-0.333-0.256-0.495c-0.481,0.147-0.917,0.271-1.506,0.411
    c0.503-0.186,1.058-0.387,1.443-0.527c-0.035-0.067-0.07-0.135-0.106-0.2c-0.238,0.018-0.744,0.075-1.326,0.247
    c0.278-0.174,0.48-0.244,1.188-0.497c-0.062-0.108-0.122-0.214-0.184-0.318c-0.182,0.049-0.373,0.096-0.585,0.147
    c0.181-0.066,0.368-0.135,0.552-0.204c-0.058-0.097-0.116-0.193-0.174-0.287c-0.82,0.072-1.204,0.204-1.626,0.316
    c0.5-0.305,1.03-0.472,1.47-0.563c-0.089-0.138-0.178-0.269-0.267-0.395c-0.458,0.047-0.753,0.123-1.065,0.188
    c0.278-0.213,0.574-0.365,0.857-0.475c-0.046-0.062-0.092-0.118-0.137-0.177c0.267-0.008,0.523-0.009,0.771-0.001
    c0.646-0.702,0.841-0.913,1.21-1.167c-0.363,0.446-0.625,0.872-0.796,1.185c0.267,0.016,0.523,0.04,0.77,0.073
    c0.346-0.611,0.474-1.014,0.642-1.435c0.091,0.556,0.04,1.074-0.073,1.526c0.152,0.03,0.3,0.063,0.445,0.1
    c0.111-0.364,0.236-0.772,0.357-1.154c-0.064,0.452-0.131,0.826-0.206,1.191c0.219,0.059,0.424,0.129,0.623,0.202
    c0.127-0.227,0.259-0.461,0.387-0.685c-0.1,0.265-0.194,0.499-0.287,0.723c0.088,0.035,0.178,0.067,0.263,0.104
    c0.525-0.754,0.688-0.984,1.016-1.276c-0.35,0.574-0.562,1.104-0.676,1.435c0.146,0.073,0.287,0.152,0.421,0.234
    c0.351-0.34,0.81-0.782,1.233-1.187c-0.396,0.507-0.722,0.889-1.078,1.283c0.258,0.167,0.492,0.352,0.711,0.548
    c0.211-0.278,0.465-0.65,0.695-1.089c-0.062,0.373-0.161,0.618-0.466,1.304c0.079,0.079,0.157,0.159,0.231,0.243
    c0.252-0.272,0.526-0.568,0.788-0.847c-0.251,0.363-0.473,0.665-0.693,0.952c0.119,0.141,0.232,0.285,0.34,0.435
    c0.356-0.381,0.567-0.681,0.806-0.984c-0.082,0.542-0.28,1.02-0.517,1.424c0.125,0.204,0.239,0.415,0.345,0.632
    c0.203-0.271,0.44-0.625,0.658-1.039c-0.064,0.388-0.168,0.636-0.501,1.382c0.041,0.097,0.081,0.192,0.12,0.289
    c0.138-0.212,0.28-0.43,0.419-0.639c-0.13,0.283-0.249,0.532-0.368,0.769c0.126,0.328,0.236,0.664,0.335,1.004
    c0.171-0.232,0.351-0.475,0.523-0.705c-0.17,0.322-0.326,0.599-0.48,0.861c0.053,0.191,0.103,0.386,0.148,0.581
    c0.134-0.2,0.271-0.427,0.4-0.674c-0.051,0.31-0.128,0.531-0.327,0.992c0.077,0.353,0.146,0.707,0.209,1.059
    c0.111-0.149,0.225-0.302,0.333-0.447c-0.107,0.204-0.21,0.393-0.311,0.568c0.211,1.194,0.365,2.357,0.603,3.353
    c1.527-5.611-1.21-13.767-5.962-13.993c-0.966-0.046-1.81,0.065-2.542,0.269c0.255-0.213,0.507-0.407,0.756-0.584
    c0.373-0.064,0.76-0.101,1.151-0.108c-0.018-0.188-0.032-0.377-0.048-0.576c0.029-0.016,0.059-0.032,0.087-0.047
    c0.041,0.209,0.083,0.418,0.123,0.62c0.352-0.002,0.711,0.019,1.071,0.055c0.032-0.383,0.072-0.774,0.129-1.212
    c0.017-0.005,0.035-0.011,0.052-0.017c0.004,0.423,0.009,0.855,0.012,1.248c0.092,0.011,0.184,0.023,0.277,0.036
    c0.109-0.457,0.194-0.714,0.349-1.031c-0.058,0.384-0.087,0.746-0.102,1.068c0.273,0.044,0.548,0.095,0.82,0.155
    c0.158-0.555,0.411-1.145,0.816-1.711c0.012,0.001,0.022,0.003,0.034,0.003c-0.078,0.537-0.179,1.053-0.193,1.863
    c0.182,0.048,0.363,0.1,0.543,0.15c0.068-0.28,0.138-0.565,0.204-0.843c-0.027,0.316-0.057,0.604-0.09,0.876
    c0.36,0.107,0.714,0.224,1.061,0.346c0.617-0.615,0.861-0.839,1.269-1.086c-0.384,0.434-0.687,0.86-0.915,1.213
    c0.239,0.09,0.475,0.181,0.703,0.273c0.319-0.337,0.7-0.66,1.16-0.935c-0.214,0.354-0.433,0.686-0.669,1.137
    c0.48,0.204,0.931,0.41,1.336,0.607c0.352-0.082,0.593-0.114,0.897-0.124c-0.229,0.078-0.442,0.162-0.645,0.246
    c1.192,0.59,1.955,1.06,1.955,1.06c-0.069-0.116-0.14-0.225-0.211-0.337c0.139,0.21,0.211,0.337,0.211,0.337
    c-0.041-0.523-0.108-1.011-0.195-1.468c-0.224-0.288-0.496-0.609-0.819-0.934c0.26,0.137,0.458,0.27,0.724,0.485
    c-0.075-0.317-0.162-0.617-0.258-0.9c-0.361-0.179-0.735-0.373-1.161-0.609c0.357,0.132,0.73,0.271,1.089,0.406
    c-0.112-0.304-0.238-0.586-0.374-0.849c-0.623-0.186-1.08-0.254-1.555-0.35c0.422-0.128,0.833-0.193,1.225-0.218
    c-0.239-0.366-0.505-0.686-0.793-0.961c-0.356,0.014-0.768,0.049-1.205,0.118c0.3-0.148,0.546-0.235,0.957-0.341
    c-0.567-0.476-1.21-0.799-1.899-0.985c-0.419,0.203-0.738,0.395-1.079,0.581c0.168-0.26,0.352-0.491,0.545-0.702
    c-0.155-0.026-0.313-0.047-0.47-0.062c-0.204,0.203-0.422,0.408-0.667,0.637c0.17-0.215,0.346-0.444,0.52-0.662
    c-1.078-0.07-2.2,0.537-3.323,0.95c-0.005,0.001,0.013,0.394,0.008,0.394c-0.001,0-0.002,0-0.003,0c-1.835,0-3.35,0.723-4.55,1.946
    c0.364-0.617,0.729-1.444,1.103-2.108c-0.445-0.254-0.636-0.477-0.873-0.684c0.372,0.182,0.709,0.251,0.974,0.331
    c0.08-0.144,0.159-0.313,0.238-0.456c-0.329-0.138-0.503-0.237-0.723-0.384c0.301,0.099,0.576,0.159,0.811,0.204
    c0.293-0.529,0.582-1.059,0.858-1.568c-0.436-0.086-0.644-0.149-0.897-0.281c0.382,0.038,0.741,0.034,1.039,0.014
    c0.063-0.113,0.123-0.226,0.183-0.336c-0.212-0.012-0.366-0.032-0.554-0.075c0.229-0.013,0.439-0.036,0.629-0.066
    c0.336-0.625,0.647-1.212,0.921-1.73c-0.162,0.002-0.295-0.011-0.449-0.036c0.187-0.032,0.364-0.071,0.528-0.114
    c0.082-0.155,0.16-0.306,0.234-0.446c-0.153,0.029-0.273,0.039-0.416,0.036c0.182-0.063,0.351-0.134,0.505-0.205
    c0.438-0.84,0.707-1.368,0.707-1.368c-0.301,0.147-0.592,0.31-0.875,0.479c-0.034,0.104-0.067,0.215-0.1,0.33
    c0.008-0.104,0.018-0.196,0.031-0.29c-0.423,0.255-0.827,0.529-1.21,0.822c-0.078,0.208-0.159,0.446-0.233,0.714
    c0.017-0.206,0.04-0.371,0.084-0.593c-0.104,0.081-0.206,0.16-0.306,0.244c-0.043,0.261-0.084,0.576-0.106,0.935
    c-0.035-0.264-0.044-0.462-0.042-0.805c-0.51,0.44-0.982,0.914-1.415,1.405c0.021,0.237,0.057,0.512,0.112,0.813
    c-0.09-0.225-0.146-0.404-0.219-0.692c-0.072,0.084-0.143,0.168-0.213,0.254c0.043,0.299,0.109,0.652,0.214,1.032
    c-0.149-0.243-0.236-0.44-0.375-0.834c-0.149,0.186-0.295,0.374-0.434,0.564c0.06,0.15,0.128,0.309,0.205,0.475
    c-0.1-0.132-0.177-0.247-0.263-0.392c-0.34,0.474-0.646,0.954-0.918,1.437c0.111,0.256,0.27,0.583,0.48,0.939
    c-0.201-0.217-0.327-0.387-0.578-0.765c-0.299,0.54-0.556,1.079-0.77,1.604c0.118,0.225,0.273,0.491,0.465,0.776
    c-0.189-0.178-0.316-0.325-0.531-0.608c-0.057,0.146-0.112,0.29-0.163,0.434c0.111,0.101,0.233,0.204,0.367,0.31
    c-0.145-0.066-0.262-0.13-0.398-0.217c-0.096,0.277-0.178,0.547-0.249,0.81c-0.121-0.294-0.252-0.6-0.391-0.917
    c-0.083,0.042-0.172,0.088-0.26,0.134c-0.022-0.044-0.043-0.084-0.064-0.127c-0.07,0.097-0.134,0.182-0.191,0.257
    c-0.093,0.048-0.186,0.096-0.281,0.144c0.034-0.065,0.068-0.129,0.101-0.19c0.209-0.137,0.412-0.265,0.618-0.391
    c-0.096-0.214-0.195-0.434-0.297-0.656c-0.104,0.081-0.193,0.15-0.277,0.216c-0.043-0.08-0.085-0.159-0.127-0.238
    c0.093-0.109,0.178-0.213,0.25-0.306c-0.126-0.267-0.504-0.538-0.64-0.813c-0.096,0.074-0.515,0.152-0.568,0.234
    c0.028-0.046,0.184-0.092,0.157-0.138c0.106-0.094,0.334-0.188,0.442-0.281c-0.111-0.225-0.163-0.45-0.28-0.677
    c-0.085,0.134-0.15,0.269-0.253,0.402c-0.03-0.053-0.044-0.105-0.074-0.159c-0.19,0.277-0.396,0.587-0.607,0.892
    c-0.037,0.032-0.069,0.064-0.109,0.096c0.221-0.434,0.423-0.802,0.631-1.165c-0.031-0.056-0.063-0.114-0.097-0.171
    c0.063-0.185,0.124-0.39,0.187-0.625c-0.22-0.411-0.446-0.822-0.68-1.228c-0.104,0.143-0.212,0.288-0.318,0.436
    c-0.016-0.026-0.031-0.052-0.046-0.078c0.098-0.174,0.191-0.337,0.285-0.496c-0.089-0.152-0.18-0.308-0.271-0.458
    c-0.11,0.204-0.198,0.365-0.278,0.505c-0.044-0.076-0.089-0.149-0.133-0.224c0.081-0.25,0.141-0.475,0.184-0.659
    c-0.285-0.469-0.577-0.924-0.872-1.358c-0.035,0.231-0.071,0.467-0.106,0.697c-0.008-0.011-0.015-0.021-0.021-0.032
    c0.003-0.294,0.011-0.563,0.022-0.819c-0.132-0.191-0.264-0.379-0.396-0.561c-0.015,0.291-0.03,0.497-0.059,0.683
    c-0.013-0.021-0.026-0.042-0.04-0.063c-0.021,0.083-0.042,0.162-0.064,0.245c-0.004-0.071-0.009-0.144-0.014-0.211
    c0.006-0.045,0.012-0.09,0.017-0.132c-0.012-0.019-0.022-0.036-0.033-0.055c-0.038-0.357-0.09-0.669-0.143-0.917
    c-0.167-0.217-0.335-0.425-0.503-0.624c-0.005,0.166-0.012,0.34-0.018,0.514c-0.039-0.059-0.078-0.117-0.115-0.174
    c-0.013-0.182-0.024-0.358-0.033-0.534c-0.414-0.474-0.825-0.881-1.225-1.192c0.771,3.463,2.593,7.469,4.157,10.506
    c-0.072-0.047-0.144-0.095-0.219-0.139c-0.009,0.192-0.012,0.424-0.006,0.681c-0.044-0.013-0.088-0.029-0.132-0.044
    c-0.014,0.086-0.027,0.173-0.041,0.261c-0.063-0.269-0.128-0.614-0.226-1.129c-0.313-0.172-0.632-0.336-0.968-0.483
    c0.045,0.441,0.071,0.755,0.079,1.021c-0.067-0.016-0.134-0.032-0.2-0.048c-0.106-0.449-0.227-0.846-0.336-1.164
    c-0.158-0.063-0.319-0.121-0.481-0.178c0.027,0.269,0.049,0.55,0.067,0.873c-0.063-0.3-0.129-0.616-0.188-0.913
    c-0.131-0.043-0.264-0.087-0.397-0.128c0.02,0.296,0.015,0.509-0.016,0.767c-0.05-0.296-0.112-0.569-0.178-0.819
    c-0.323-0.09-0.656-0.166-0.992-0.227c0.021,0.347,0.036,0.704,0.039,1.131c-0.062-0.384-0.126-0.794-0.182-1.156
    c-0.129-0.022-0.259-0.043-0.391-0.06c0.109,0.425,0.188,0.916,0.188,1.449c-0.073-0.003-0.145-0.007-0.217-0.01
    c-0.011,0.122-0.024,0.244-0.037,0.368c-0.14-0.49-0.325-1.049-0.745-1.886c-0.131-0.008-0.265-0.014-0.398-0.017
    c0.06,0.161,0.119,0.329,0.183,0.514c-0.088-0.168-0.178-0.342-0.267-0.515c-0.12-0.002-0.239,0.099-0.361,0.101
    c0.355,0.766,0.529,1.607,0.631,1.607c-0.046,0-0.092,0-0.139,0c-0.393,0-0.804-1.265-1.077-1.579
    c-0.163,0.011-0.325-0.022-0.488-0.005c0.055,0.115,0.109,0.207,0.168,0.332c-0.075-0.114-0.152-0.244-0.23-0.361
    c-0.163,0.021-0.327,0.038-0.492,0.066c0.169,0.318,0.334,0.644,0.521,1.047c-0.225-0.342-0.464-0.707-0.67-1.025
    c-0.116,0.021-0.231,0.042-0.348,0.066c0.256,0.386,0.485,0.842,0.649,1.367c-0.044,0.005-0.088,0.01-0.133,0.015
    c-0.303-0.354-0.624-0.728-1.196-1.211c-0.137,0.039-0.276,0.084-0.416,0.129c0.246,0.339,0.644,0.835,1.17,1.327
    c-0.041-0.02-0.079-0.04-0.116-0.059c-0.021-0.035-0.043-0.074-0.064-0.106c-0.031,0.005-0.063,0.01-0.095,0.015
    c-0.316-0.188-0.624-0.447-1.329-1.031c-0.301,0.111-0.603,0.23-0.908,0.368c0.291,0.226,0.578,0.454,0.919,0.75
    c-0.359-0.227-0.747-0.493-1.075-0.705c-0.105,0.049-0.271,0.081-0.376,0.135c0.492,0.373,0.731,0.766,0.889,0.748l0,0
    c0-0.001,0.086,0.075,0.096,0.086c0.045,0.049,0.128,0.161,0.174,0.215c-0.577-0.38-1.106-0.64-1.48-0.793
    c-0.179,0.099-0.352,0.2-0.53,0.31c0.287,0.138,0.586,0.288,0.928,0.476c-0.353-0.128-0.725-0.268-1.059-0.392
    c-0.179,0.111-0.355,0.228-0.534,0.35c0.134,0.052,0.271,0.104,0.42,0.163c-0.16-0.037-0.325-0.076-0.49-0.116
    c-0.216,0.149-0.432,0.305-0.648,0.47c0.578,0.167,0.853,0.261,1.153,0.422c0.026,0.017,0.052,0.035,0.078,0.052
    c-0.13-0.026-0.255-0.051-0.377-0.07c-0.068-0.035-0.139-0.073-0.201-0.107c-0.04,0.02-0.082,0.041-0.122,0.062
    c-0.353-0.044-0.663-0.058-0.905-0.062c-0.215,0.175-0.431,0.358-0.646,0.551c0.155,0.049,0.283,0.092,0.399,0.135
    c-0.043,0.025-0.084,0.049-0.127,0.074c0.089,0.032,0.17,0.063,0.249,0.097c-0.275-0.046-0.532-0.074-0.76-0.091
    c-0.453,0.418-0.905,0.874-1.355,1.374c0,0,7.112-2.234,15.053-0.175c-0.284,0.036-0.57,0.079-0.857,0.139
    c-0.019,0.143-0.043,0.322-0.071,0.519c-0.037,0.01-0.075,0.021-0.112,0.032c0.005-0.169,0.013-0.337,0.023-0.515
    c-0.577,0.127-1.155,0.3-1.726,0.511c-0.008,0.156-0.011,0.396,0.02,0.667c-0.04,0.02-0.081,0.04-0.12,0.062
    c-0.06-0.136-0.118-0.318-0.214-0.607c-0.327,0.131-0.651,0.274-0.971,0.429c0.068,0.203,0.151,0.452,0.236,0.71
    c-0.028,0.018-0.055,0.036-0.083,0.054c-0.121-0.23-0.225-0.448-0.327-0.68c-0.438,0.221-0.863,0.461-1.273,0.719
    c0.133,0.18,0.281,0.381,0.417,0.569c-0.208-0.18-0.373-0.339-0.527-0.498c-0.157,0.1-0.313,0.203-0.465,0.309
    c0.393,0.307,0.59,0.465,0.73,0.625c-0.029,0.022-0.058,0.045-0.086,0.069c-0.399-0.224-0.781-0.359-1.014-0.43
    c-0.343,0.256-0.67,0.522-0.976,0.801c0.138,0.068,0.278,0.142,0.414,0.211c-0.178-0.053-0.333-0.103-0.48-0.152
    c-0.135,0.123-0.264,0.25-0.391,0.376c0.487,0.181,0.674,0.266,0.89,0.437c-0.405-0.115-0.799-0.176-1.112-0.207
    c-0.127,0.136-0.248,0.273-0.366,0.412c0.569,0.203,0.963,0.28,1.293,0.318c-0.063,0.077-0.128,0.155-0.188,0.233
    c-0.666,0.103-1.188,0.023-1.496-0.056c-0.198,0.272-0.377,0.547-0.529,0.824c0.357-0.022,0.822-0.051,1.246-0.074
    c-0.51,0.094-0.9,0.15-1.313,0.199c-0.152,0.292-0.277,0.588-0.371,0.881c0.307-0.061,0.652-0.131,0.974-0.193
    c-0.38,0.12-0.695,0.212-1.003,0.296c-0.049,0.162-0.09,0.322-0.117,0.484c0.277-0.119,0.643-0.291,1.04-0.518
    c-0.266,0.231-0.466,0.36-1.07,0.739c-0.013,0.118-0.022,0.236-0.025,0.353c0.681-0.313,0.989-0.451,1.308-0.554
    c-0.008,0.021-0.014,0.039-0.021,0.062c-0.539,0.292-0.985,0.581-1.279,0.785c0.008,0.123,0.02,0.245,0.042,0.366
    c0.266-0.189,0.558-0.396,0.834-0.591c-0.302,0.261-0.56,0.475-0.814,0.68c0.051,0.247,0.131,0.488,0.241,0.725
    c0.141-0.122,0.282-0.244,0.421-0.363c-0.142,0.15-0.272,0.285-0.398,0.413c0.038,0.077,0.132,0.152,0.177,0.227
    c0.162-0.18-0.41-0.405,0.59-0.66c0,0.03,0,0.062,0,0.092c0,0.178-0.258,0.374-0.488,0.732c0.086,0.125,0.151,0.249,0.255,0.371
    c0.082-0.148,0.153-0.298,0.236-0.449c0.004,0.016,0.001,0.031,0.007,0.047c-0.068,0.166-0.139,0.323-0.205,0.474
    c0.32,0.359,0.729,0.698,1.24,1.009c-0.556-6.551,4.87-8.43,9.188-8.9c-0.072,0.092-0.14,0.189-0.198,0.295
    c-0.175,0.307-0.274,0.631-0.307,0.95c-0.079,0.103-0.159,0.209-0.237,0.321c-0.003,0.003-0.006,0.007-0.01,0.01
    c-0.082,0.117-0.165,0.239-0.249,0.366c-0.009,0.013-0.017,0.025-0.024,0.037c-0.573,0.873-1.144,1.964-1.688,3.33
    c0.146,0.015,0.295,0.03,0.438,0.045c-0.171,0.018-0.323,0.027-0.471,0.036c-0.041,0.104-0.082,0.209-0.123,0.315
    c-0.022,0.059-0.041,0.12-0.058,0.182c0.404-0.081,0.608-0.101,0.883-0.073c-0.369,0.111-0.696,0.254-0.959,0.388
    c-0.031,0.172-0.047,0.352-0.051,0.536c0.19-0.074,0.392-0.151,0.583-0.225c-0.214,0.131-0.402,0.238-0.583,0.337
    c0,0.112,0.005,0.227,0.015,0.344c0.28-0.11,0.626-0.246,0.946-0.368c-0.354,0.215-0.636,0.37-0.933,0.518
    c0.018,0.151,0.041,0.306,0.07,0.462c0.309-0.204,0.68-0.369,1.106-0.413c-0.288,0.242-0.569,0.445-0.986,0.929
    c0.046,0.172,0.099,0.348,0.159,0.523c0.212-0.252,0.395-0.317,0.563-0.498c-0.21,0.379-0.327,0.554-0.481,0.737
    c0.096,0.259,0.204,0.521,0.323,0.78c0.247-0.204,0.553-0.453,0.834-0.682c-0.29,0.323-0.527,0.566-0.777,0.809
    c0.065,0.14,0.133,0.279,0.204,0.419c0.317-0.275,0.493-0.403,0.742-0.527c-0.245,0.25-0.458,0.503-0.635,0.734
    c0.1,0.189,0.204,0.379,0.313,0.565c0.198-0.096,0.413-0.171,0.647-0.208c-0.16,0.147-0.315,0.281-0.495,0.463
    c0.127,0.209,0.259,0.415,0.396,0.618c0.137-0.38,0.216-0.563,0.35-0.784c-0.104,0.354-0.177,0.686-0.228,0.964
    c0.223,0.321,0.453,0.632,0.688,0.926c0.026-0.316,0.055-0.504,0.114-0.737c-0.007,0.338,0.007,0.648,0.027,0.913
    c0.098,0.119,0.195,0.233,0.293,0.344c-0.02-0.214-0.023-0.373-0.014-0.567c0.049,0.274,0.109,0.521,0.17,0.741
    c0.495,0.545,0.992,1.003,1.454,1.329c-0.08-0.116-0.166-0.243-0.255-0.376c-0.209-0.433-0.406-1.094-0.517-1.604
    c-0.389-0.337-0.532-0.472-0.708-0.718c0.231,0.172,0.452,0.304,0.643,0.403c-0.036-0.184-0.071-0.373-0.105-0.564
    c-0.416-0.344-0.623-0.505-0.796-0.749c0.258,0.191,0.52,0.341,0.744,0.452c-0.027-0.167-0.052-0.338-0.076-0.509
    c-0.175-0.084-0.358-0.178-0.565-0.291c0.18,0.058,0.369,0.12,0.551,0.18c-0.054-0.378-0.1-0.766-0.142-1.16
    c-0.616-0.209-0.799-0.272-1.058-0.454c0.411,0.104,0.777,0.124,1.026,0.122c-0.026-0.285-0.05-0.572-0.069-0.862
    c-0.411-0.121-0.763-0.237-1.235-0.435c0.435,0.088,0.918,0.19,1.226,0.258c-0.013-0.206-0.025-0.411-0.033-0.62
    c-0.391-0.114-0.823-0.229-1.295-0.541c0.342,0.031,0.64,0.093,1.256,0.098c-0.006-0.155,0.011-0.41,0.009-0.564
    c-0.442-0.01-0.615-0.024-0.857-0.106c0.356-0.11,0.664-0.262,0.853-0.367c-0.001-0.216,0.001-0.216,0.004-0.431
    c-0.377,0.058-0.715,0.099-1.161,0.121c0.002-0.002,0.002-0.006,0.003-0.009c0.411-0.111,0.864-0.362,1.164-0.434
    c0.003-0.152,0.002-0.152,0.007-0.303c-0.484,0.047-0.803,0.134-1.07,0.227c0.014-0.054,0.024-0.109,0.038-0.163
    c0.392-0.238,0.683-0.312,1.006-0.401c0.011-0.196,0.075-0.694,0.091-0.886c-0.158,0.022-0.428,0.074-0.749,0.185
    c0.027-0.07,0.059-0.142,0.088-0.212c0.164-0.098,0.381-0.217,0.695-0.395c0.024-0.268,0.054-0.529,0.086-0.789
    c-0.115,0.087-0.226,0.167-0.337,0.246c0.056-0.104,0.115-0.208,0.176-0.313c0.021-0.018,0.039-0.033,0.059-0.05
    c0.289,0.126,0.603,0.163,0.913,0.121c-0.223,0.828,0.056,1.683,0.74,2.071c0.267,0.15,0.563,0.212,0.861,0.193
    c0.076,0.176,0.167,0.375,0.274,0.595c-0.071-0.02-0.096-0.019-0.068,0.036c0.251,0.459,0.777,1.559,1.402,2.518
    c-0.069-0.006-0.094-0.013-0.059,0.059c0.159,0.3,0.423,0.753,0.794,1.253c-0.107-0.018-0.141-0.029-0.112,0.021
    c0.138,0.233,0.406,0.629,0.725,1.092c0.344,0.442,0.74,0.953,1.113,1.433c-0.135,0.021-0.203,0.001-0.14,0.073
    c0.146,0.17,0.361,0.384,0.601,0.646c-0.077,0.013-0.102,0.027-0.063,0.073c0.182,0.203,0.483,0.548,0.855,0.932
    c0.373,0.383,0.801,0.82,1.209,1.238c-0.094,0.003-0.127,0.009-0.082,0.057c0.232,0.257,0.713,0.729,1.249,1.26
    c-0.065,0.019-0.091,0.027-0.041,0.082c0.165,0.175,0.419,0.403,0.722,0.677c-0.065,0.009-0.083,0.021-0.044,0.061
    c0.362,0.362,1.247,1.155,2.069,1.914c-0.071,0.018-0.098,0.021-0.045,0.077c0.225,0.243,0.606,0.578,1.061,0.985
    c-0.113,0.013-0.148,0.011-0.11,0.052c0.357,0.391,1.372,1.287,2.229,2.106c-0.134,0.053-0.203,0.038-0.138,0.111
    c0.141,0.156,0.344,0.348,0.588,0.568c-0.09,0.028-0.117,0.049-0.072,0.09c0.268,0.243,0.797,0.723,1.376,1.247
    c-0.064,0.019-0.087,0.028-0.039,0.075c0.266,0.276,0.759,0.724,1.321,1.281c-0.08,0.008-0.104,0.017-0.065,0.06
    c0.27,0.331,0.934,1.001,1.564,1.725c-0.069,0.015-0.096,0.02-0.06,0.078c0.232,0.396,0.874,0.981,1.467,1.772
    c-0.202,0.013-0.276-0.001-0.229,0.067c0.155,0.229,0.475,0.616,0.817,1.082c-0.013,0.36-0.023,0.761-0.048,1.187
    c-0.063-0.06-0.087-0.07-0.098-0.003c-0.059,0.366-0.126,1.067-0.194,1.874c-0.063-0.035-0.088-0.039-0.1,0.04
    c-0.065,0.523-0.087,1.532-0.098,2.705c-0.064-0.036-0.098-0.035-0.105,0.021c-0.085,0.669-0.125,2.438-0.174,3.937
    c-0.09-0.049-0.127-0.067-0.138,0.024c-0.036,0.315-0.078,0.808-0.095,1.402c-0.072-0.045-0.095-0.045-0.102,0.021
    c-0.032,0.305-0.049,0.843-0.058,1.479c-0.194-0.33-0.357-0.608-0.47-0.817c0.111,0.327,0.273,0.731,0.466,1.19
    c-0.006,0.662-0.006,1.396-0.005,2.075c-0.037-0.014-0.062-0.005-0.085,0.048c-0.012,0.071-0.023,0.15-0.035,0.234
    c-0.01-0.019-0.02-0.038-0.028-0.059c0.008,0.03,0.017,0.06,0.025,0.089c-0.073,0.576-0.127,1.446-0.158,2.433
    c-0.337,0.27-0.713,0.607-1.087,0.999c-0.174-0.643-0.382-1.289-0.624-1.927c0.427-0.739,0.892-1.439,1.328-2.215
    c-0.638,0.508-1.158,1.039-1.582,1.583c-0.754-1.771-1.811-3.438-3.282-4.712c0.929,1.388,2.008,3.178,2.783,5.423
    c-0.146,0.234-0.278,0.471-0.396,0.708c-0.156-0.073-0.313-0.139-0.473-0.192c0.136,0.107,0.27,0.22,0.402,0.339
    c-0.149,0.314-0.274,0.633-0.381,0.948c-0.723-0.621-1.487-0.868-2.052-1.085c0.739,0.684,1.354,1.18,1.854,1.762
    c-0.229,0.896-0.324,1.778-0.377,2.61c-0.781-2.334-3.108-3.78-5.155-4.522c1.904,1.713,4.525,2.708,4.5,5.713
    c-1.287-2.014-5.203-2.485-6.159-3.525c0.953,1.834,5.718,2.096,5.59,4.988c0,0,0.031,0.144,0.769,0.247
    c1.295,0.183,1.769,0.157,1.769,0.157c-0.039-0.845,0.114-1.757,0.387-2.661c0.149,0.78,0.256,1.735,0.318,2.945
    c0.099,0.07,0.278,0.103,1.217,0.125c0.622,0.016,0.815-0.109,0.815-0.109c0.016-0.278,0.044-0.546,0.079-0.806
    c0.282,0.01,0.59,0.02,0.912,0.03c0.08,0.336,0.135,0.658,0.161,0.961c0.115,0.073,0.764,0.229,2.853,0.201
    c0,0,0.571,0.002,0.938-0.128c0.395,0.048,0.896,0.071,1.375,0.071c0.313,0,1.034-0.094,1.263-0.276
    c-0.057-0.222-0.104-0.437-0.145-0.647c0.27,0.004,0.542,0.004,0.812-0.004c0.001,0.188,0.002,0.376,0,0.564
    c0.221,0.117,0.989,0.11,1.657,0.059c0.197-0.016,1.105-0.073,1.105-0.248c0-0.003,0-0.006,0-0.009c1,0.017,0.445,0.026,0.83,0.026
    c0.91,0,1.476-0.097,1.639-0.187c-0.252-3.114,1.235-3.998,5.321-3.686c-2.724-0.886-4.897-0.325-5.726,0.885
    c0.926-2.187,2.83-3.28,4.955-4.427c-4.629,1.042-5.726,3.812-6.204,5.471c0.7-4.013,2.117-6.408,3.97-8.961
    c-2.237,1.553-3.909,4.794-4.885,8.222c0.285-3.173,1.632-6.015,3.01-7.648c-1.8,1.104-3.226,3.448-3.646,6.354
    c-0.027-0.056-0.056-0.109-0.088-0.157l-0.007-0.007c-0.028-0.754-0.079-1.386-0.221-1.758c-0.021-0.034-0.039-0.048-0.062-0.049
    c0.005-0.321,0.01-0.658,0.017-0.991c-0.015-0.333-0.036-0.663-0.06-0.97c-0.006-0.089-0.014-0.174-0.021-0.258
    c0.377-0.417,0.756-0.806,1.105-1.175c-0.394,0.104-0.785,0.333-1.162,0.64c-0.034-0.255-0.071-0.464-0.113-0.602
    c-0.01-0.03-0.026-0.04-0.055-0.033c-0.042-0.557-0.101-1.064-0.155-1.486c0.563-0.678,1.229-1.277,2.03-1.741
    c-0.742,0.14-1.456,0.528-2.115,1.125c-0.009-0.06-0.017-0.116-0.023-0.167c-0.013-0.072-0.045-0.062-0.124-0.019
    c-0.071-0.97-0.29-1.883-0.368-2.285c-0.013-0.057-0.053-0.036-0.136,0.033c-0.167-0.974-0.396-1.796-0.527-2.242
    c-0.021-0.07-0.049-0.053-0.112,0c-0.074-0.31-0.15-0.632-0.227-0.95c-0.085-0.317-0.19-0.624-0.281-0.912
    c-0.186-0.575-0.354-1.064-0.462-1.329c-0.021-0.051-0.04-0.04-0.085,0.018c-0.074-0.225-0.148-0.416-0.214-0.562
    c-0.025-0.045-0.044-0.019-0.094,0.04c-0.209-0.572-0.404-1.194-0.615-1.718c-0.222-0.518-0.417-0.956-0.548-1.192
    c-0.027-0.052-0.045-0.013-0.118,0.106c-0.225-0.589-0.453-1.061-0.638-1.349c-0.041-0.067-0.058-0.038-0.111,0.036
    c-0.285-0.61-0.606-1.226-0.878-1.724c0.075-0.509,0.139-0.938,0.177-1.198c0.006-0.067-0.029-0.04-0.159,0.029
    c0.147-0.716,0.276-1.31,0.316-1.704c0.009-0.091-0.021-0.075-0.11-0.043c0.258-1.312,0.536-2.696,0.699-3.279
    c0.018-0.074-0.022-0.064-0.131-0.016c0.124-0.481,0.226-0.887,0.27-1.181c0.017-0.103-0.026-0.069-0.146-0.034
    c0.338-1.443,0.752-3.155,0.928-3.796c0.015-0.066-0.02-0.043-0.14,0.016c0.216-0.695,0.403-1.271,0.487-1.658
    c0.019-0.091-0.01-0.076-0.09-0.052c0.196-0.635,0.398-1.291,0.573-1.859c0.209-0.559,0.385-1.03,0.491-1.312
    c0.022-0.066-0.006-0.065-0.09-0.035c0.181-0.457,0.333-0.839,0.42-1.113c0.025-0.084-0.004-0.078-0.082-0.063
    c0.306-0.833,0.577-1.574,0.716-1.955c0.031-0.079-0.013-0.06-0.139-0.008c0.44-0.987,0.853-1.809,1.003-2.298
    c0.024-0.08,0-0.075-0.064-0.058c0.236-0.536,0.471-1.066,0.672-1.521c0.229-0.443,0.419-0.814,0.53-1.049
    c0.029-0.063,0.006-0.063-0.07-0.034c0.447-0.85,0.849-1.551,1.056-1.966c0.036-0.071,0.015-0.073-0.046-0.063
    c0.113-0.206,0.224-0.407,0.329-0.601c0.117-0.186,0.229-0.363,0.333-0.526c0.208-0.329,0.38-0.604,0.498-0.79
    c0.035-0.057,0.012-0.069-0.06-0.063c0.213-0.336,0.394-0.619,0.519-0.833c0.043-0.072,0.018-0.072-0.051-0.066
    c0.71-1.324,1.573-2.359,1.934-2.927c0.046-0.074,0.02-0.075-0.056-0.076c0.482-0.69,0.913-1.307,1.143-1.66
    c-0.02,0.183-0.04,0.361-0.058,0.545c0.209-0.168,0.427-0.342,0.638-0.507c-0.239,0.256-0.448,0.469-0.654,0.668
    c-0.023,0.268-0.049,0.535-0.072,0.804c0.181-0.127,0.387-0.29,0.6-0.491c-0.151,0.255-0.28,0.407-0.622,0.771
    c-0.013,0.158-0.025,0.316-0.036,0.474c0.153-0.178,0.326-0.396,0.496-0.646c-0.108,0.317-0.218,0.516-0.525,1.032
    c-0.018,0.25-0.034,0.499-0.049,0.747c0.045-0.044,0.092-0.089,0.137-0.133c-0.048,0.063-0.095,0.123-0.14,0.181
    c-0.073,1.149-0.129,2.293-0.165,3.41c0.155-0.212,0.444-0.675,0.82-1.32c-0.063-0.209-0.131-0.431-0.2-0.679
    c0.085,0.19,0.172,0.391,0.257,0.587c0.103-0.178,0.211-0.367,0.326-0.569c-0.173-0.51-0.234-0.724-0.275-1.041
    c0.13,0.321,0.271,0.601,0.397,0.826c0.047-0.08,0.095-0.167,0.144-0.253c-0.187-0.372-0.358-0.741-0.558-1.215
    c0.215,0.357,0.445,0.743,0.639,1.07c0.096-0.168,0.193-0.345,0.294-0.525c-0.077-0.135-0.155-0.276-0.238-0.431
    c0.09,0.12,0.182,0.244,0.273,0.368c0.072-0.132,0.145-0.263,0.218-0.396c-0.296-0.597-0.348-0.754-0.462-1.167
    c0.381,0.639,0.513,0.651,0.663,0.801c0.085-0.157,0.173-0.315,0.259-0.477c-0.55-0.775-0.802-1.113-0.955-1.481
    c0.419,0.52,0.907,0.954,1.136,1.147c0.046-0.087,0.096-0.177,0.142-0.264c-0.167-0.176-0.34-0.362-0.531-0.58
    c0.191,0.157,0.391,0.325,0.582,0.486c0.073-0.138,0.146-0.274,0.221-0.415c-0.293-0.289-0.57-0.578-0.908-0.96
    c0.333,0.275,0.69,0.576,0.981,0.822c0.086-0.161,0.172-0.323,0.259-0.488c-0.608-0.644-0.769-0.77-1.162-1.008
    c0.643,0.144,1.085,0.398,1.365,0.62c0.08-0.15,0.158-0.305,0.236-0.456c-0.2-0.127-0.408-0.267-0.643-0.429
    c0.229,0.108,0.467,0.224,0.693,0.333c0.048-0.093,0.096-0.184,0.144-0.278c-0.786-0.788-1.193-0.979-1.618-1.235
    c0.833-0.04,1.503,0.343,1.912,0.662c0.086-0.166,0.168-0.333,0.251-0.497c-0.257-0.159-0.518-0.329-0.822-0.541
    c0.292,0.139,0.601,0.288,0.879,0.424c0.115-0.226,0.229-0.452,0.342-0.679c-0.572-0.324-1.01-0.581-1.615-1.004
    c0.669,0.318,1.435,0.695,1.703,0.828c0.038-0.077,0.079-0.154,0.116-0.231c-0.924-0.843-1.423-1.094-1.883-1.319
    c0.039-0.045,0.074-0.087,0.108-0.126c0.96-0.014,1.722,0.465,2.105,0.769c0.091-0.183,0.179-0.365,0.266-0.548
    c-0.608-0.467-0.986-0.62-1.379-0.82c0.665-0.085,1.253-0.007,1.714,0.111c0.112-0.237,0.221-0.472,0.327-0.703
    c-0.337-0.181-0.663-0.369-1.063-0.623c0.379,0.16,0.791,0.338,1.127,0.484c0.046-0.104,0.091-0.204,0.137-0.307
    c-0.493-0.227-0.89-0.415-1.368-0.679c0.014-0.016,0.028-0.031,0.042-0.046c0.606,0.23,1.205,0.462,1.41,0.541
    c0.036-0.083,0.076-0.169,0.113-0.253c0.065,0.382,0.27,0.764,0.603,1.059c0.703,0.624,1.706,0.639,2.244,0.033
    c0.51-0.573,0.416-1.5-0.196-2.125c0.322-0.048,0.616-0.192,0.836-0.44c0.071-0.08,0.131-0.165,0.179-0.256
    c0.037,0.041,0.076,0.078,0.113,0.121c0.276-0.48,0.618-1.073,0.94-1.617c-0.277,0.696-0.519,1.233-0.788,1.786
    c0.25,0.277,0.494,0.566,0.735,0.874c0.244-0.306,0.524-0.695,0.789-1.15c-0.098,0.434-0.223,0.719-0.578,1.431
    c0.097,0.129,0.193,0.257,0.287,0.389c0.143-0.201,0.286-0.423,0.427-0.663c-0.065,0.291-0.145,0.517-0.296,0.849
    c0.26,0.371,0.51,0.762,0.757,1.171c0.249-0.503,0.563-1.136,0.857-1.717c-0.247,0.763-0.467,1.338-0.718,1.949
    c0.261,0.446,0.516,0.914,0.76,1.409c0.104-0.457,0.221-0.966,0.335-1.443c-0.053,0.666-0.115,1.201-0.192,1.738
    c0.116,0.24,0.23,0.485,0.341,0.735c0.094-0.343,0.185-0.746,0.245-1.194c0.083,0.479,0.067,0.803,0,1.765
    c0.212,0.514,0.414,1.051,0.606,1.609c0.064-0.271,0.124-0.567,0.167-0.886c0.074,0.423,0.07,0.726,0.021,1.453
    c0.075,0.229,0.147,0.462,0.217,0.698c0.056-0.239,0.111-0.48,0.167-0.713c-0.027,0.354-0.058,0.669-0.093,0.972
    c0.501,1.724,0.909,3.638,1.193,5.769c0,0,0.225-1.863,0.261-4.459c-0.136-0.282-0.287-0.489-0.355-0.754
    c0.092,0.149,0.211,0.293,0.211,0.426c0-0.161,0-0.325,0-0.491c0-0.42-0.348-0.658-0.48-0.993c0.186,0.229,0.409,0.436,0.589,0.622
    c-0.009-0.886-0.024-1.827-0.097-2.791c-0.178-0.116-0.352-0.238-0.547-0.374c0.18,0.088,0.369,0.178,0.551,0.268
    c-0.021-0.295-0.045-0.589-0.075-0.887c-0.332-0.177-0.531-0.31-0.766-0.505c0.256,0.107,0.508,0.196,0.742,0.273
    c-0.045-0.399-0.1-0.799-0.159-1.2c-0.307-0.104-0.626-0.222-0.984-0.359c0.315,0.07,0.646,0.146,0.964,0.22
    c-0.021-0.141-0.041-0.28-0.065-0.419c-0.589-0.228-1.028-0.329-1.481-0.459c0.485-0.099,0.949-0.121,1.381-0.099
    c-0.059-0.308-0.124-0.61-0.194-0.912c-0.939-0.306-1.251-0.416-1.673-0.674c0.6,0.155,1.149,0.233,1.575,0.273
    c-0.112-0.438-0.242-0.863-0.382-1.281c-0.271,0.005-0.554,0.006-0.867,0.003c0.272-0.037,0.553-0.075,0.829-0.11
    c-0.043-0.131-0.086-0.265-0.132-0.392c-0.592,0.065-0.884,0.076-1.273,0.032c0.438-0.08,0.834-0.185,1.173-0.29
    c-0.067-0.168-0.139-0.334-0.211-0.498c-0.369,0.076-0.758,0.15-1.206,0.224c0.376-0.122,0.771-0.246,1.144-0.363
    c-0.196-0.431-0.415-0.842-0.656-1.226c-0.479,0.142-0.967,0.271-1.566,0.408c0.485-0.19,1.005-0.393,1.463-0.568
    c-0.097-0.151-0.201-0.296-0.308-0.438c-0.377,0.105-0.774,0.206-1.236,0.313c0.372-0.147,0.766-0.3,1.137-0.442
    c-0.186-0.237-0.385-0.453-0.592-0.66c0.127,0.008,0.255,0.015,0.381,0.024c0.135-0.411,0.215-0.755,0.312-1.106
    c0.087,0.396,0.119,0.779,0.118,1.146c0.149,0.014,0.298,0.032,0.446,0.049c0.067-0.488,0.144-0.956,0.269-1.547
    c-0.03,0.532-0.066,1.111-0.096,1.566c0.229,0.028,0.457,0.061,0.681,0.095c0.171-0.4,0.381-0.969,0.534-1.634
    c0.031,0.475-0.012,0.797-0.161,1.699c0.244,0.046,0.484,0.096,0.72,0.153c0.236-0.36,0.531-0.86,0.794-1.457
    c-0.059,0.49-0.187,0.813-0.246,1.604c0.067,0.021,0.134,0.04,0.199,0.06c0.229,0.073,0.447,0.145,0.661,0.215
    c0.113-0.295,0.227-0.629,0.325-0.996c0.013,0.345-0.015,0.609-0.098,1.072c0.329,0.111,0.635,0.224,0.924,0.338
    c0.142-0.356,0.294-0.715,0.491-1.141c-0.115,0.398-0.241,0.822-0.353,1.197c0.118,0.048,0.235,0.1,0.347,0.151
    c0.275-0.325,0.615-0.769,0.936-1.308c-0.109,0.441-0.242,0.729-0.617,1.463c0.224,0.114,0.432,0.238,0.621,0.374
    c0.159-0.28,0.322-0.603,0.474-0.961c-0.039,0.36-0.104,0.628-0.269,1.116c0.192,0.154,0.365,0.325,0.521,0.513
    c0.131-0.259,0.275-0.527,0.446-0.831c-0.117,0.311-0.243,0.638-0.361,0.943c0.226,0.303,0.408,0.651,0.54,1.064
    c0.146-0.154,0.295-0.325,0.443-0.508c-0.104,0.257-0.209,0.455-0.381,0.722c0.044,0.165,0.081,0.341,0.113,0.528
    c0.073-0.078,0.147-0.159,0.222-0.244c-0.063,0.14-0.126,0.262-0.201,0.386c0.011,0.064,0.019,0.13,0.025,0.196
    c0,0,0.197-0.059,0.477-0.19c0.02-0.123,0.039-0.25,0.056-0.382c0.021,0.115,0.035,0.224,0.044,0.333
    c0.039-0.02,0.079-0.039,0.12-0.061c-0.013-0.271-0.037-0.563-0.08-0.872c0.114,0.249,0.189,0.46,0.267,0.765
    c0.071-0.044,0.145-0.089,0.219-0.14c-0.043-0.211-0.097-0.432-0.162-0.66c0.104,0.212,0.178,0.384,0.258,0.594
    c0.171-0.123,0.34-0.268,0.496-0.434c-0.21-0.264-0.465-0.557-0.764-0.848c0.321,0.16,0.543,0.316,0.938,0.647
    c0.245-0.312,0.437-0.688,0.513-1.14c-0.372-0.192-0.878-0.422-1.475-0.607c0.442-0.002,0.744,0.055,1.508,0.239
    C569.773,210.488,569.74,210.203,569.658,209.897z M531.964,201.918c0.027-0.014,0.124-0.056,0.279-0.116
    C532.149,201.843,532.057,201.876,531.964,201.918z M546.338,201.721c0.009-0.351,0-0.671-0.018-0.966
    c0.056,0.073,0.111,0.147,0.167,0.222C546.45,201.244,546.406,201.467,546.338,201.721z M547.135,202.38
    c-0.022-0.017-0.049-0.031-0.073-0.049c0.044-0.134,0.087-0.265,0.131-0.392c0.03,0.04,0.06,0.082,0.089,0.124
    C547.234,202.168,547.186,202.272,547.135,202.38z M547.651,202.732c0.016-0.033,0.03-0.062,0.045-0.092
    c0.003,0.004,0.006,0.008,0.009,0.013C547.688,202.679,547.669,202.707,547.651,202.732z M548.243,209.069
    c-0.038-0.507-0.063-1.004-0.072-1.629c0.052,0.368,0.105,0.756,0.156,1.117c-0.041,0.053-0.079,0.102-0.107,0.145
    c0.039-0.006,0.082-0.011,0.125-0.016c0.016,0.111,0.031,0.222,0.044,0.325C548.343,209.029,548.293,209.049,548.243,209.069z
     M514.414,220.497c-0.058,0.4-0.116,0.738-0.183,1.063c-0.036-0.046-0.072-0.088-0.109-0.131
    C514.217,221.125,514.316,220.805,514.414,220.497z M502.45,217.326c0.001-0.061,0.001-0.121,0.002-0.18
    c0.023,0.094,0.044,0.188,0.061,0.291C502.493,217.398,502.471,217.363,502.45,217.326z M507.12,221.33
    c-0.019,0.069-0.039,0.138-0.059,0.206c0.02-0.175,0.041-0.333,0.063-0.484C507.124,221.137,507.122,221.231,507.12,221.33z
     M507.229,220.942c-0.029,0.004-0.058,0.007-0.089,0.011c0.018-0.105,0.035-0.21,0.055-0.313c0.037,0.01,0.072,0.021,0.109,0.03
    C507.279,220.758,507.255,220.849,507.229,220.942z M507.909,220.875c-0.101,0.006-0.201,0.013-0.302,0.022
    c0.009-0.048,0.016-0.093,0.021-0.135c0.097,0.028,0.194,0.056,0.291,0.087C507.917,220.858,507.913,220.867,507.909,220.875z
     M508.812,218.621c-0.023-0.069-0.045-0.143-0.065-0.221c0.026,0.05,0.053,0.1,0.079,0.149
    C508.82,218.574,508.816,218.598,508.812,218.621z M508.916,215.461c0-0.003,0.003-0.008,0.005-0.011
    c-0.003,0.005-0.005,0.01-0.005,0.015C508.916,215.463,508.916,215.462,508.916,215.461z M513.229,225.537
    c-0.066,0.039-0.131,0.077-0.194,0.113c-0.003-0.008-0.008-0.015-0.011-0.024C513.094,225.597,513.162,225.566,513.229,225.537z
     M512.538,224.544c-0.171,0.089-0.327,0.167-0.478,0.242c-0.035-0.01-0.07-0.017-0.105-0.024
    C512.149,224.688,512.347,224.615,512.538,224.544z M511.567,224.371c0.001-0.013,0.003-0.024,0.004-0.036
    c0.032-0.005,0.063-0.01,0.095-0.015C511.633,224.338,511.6,224.355,511.567,224.371z M512.254,223.366
    c-0.218,0.09-0.433,0.171-0.69,0.293c-0.015-0.084-0.033-0.166-0.059-0.247C511.74,223.374,511.989,223.354,512.254,223.366z
     M512.715,227.81c0.054-0.07,0.106-0.144,0.153-0.223c0.343-0.196,0.777-0.384,1.299-0.481c-0.405,0.222-0.701,0.347-1.381,0.923
    c-0.005-0.015-0.01-0.032-0.015-0.046C512.752,227.922,512.732,227.865,512.715,227.81z M517.491,235.574
    c-0.045-0.057-0.056-0.032-0.085,0.033c-0.434-0.584-0.814-1.108-1.003-1.381c-0.046-0.061-0.051-0.008-0.086,0.133
    c-0.241-0.374-0.468-0.723-0.66-1.02c-0.184-0.303-0.331-0.558-0.462-0.723c-0.043-0.055-0.054-0.033-0.077,0.021
    c-0.513-0.811-0.91-1.658-1.125-2.023c-0.028-0.049-0.041-0.033-0.063,0.033c-0.366-0.691-0.559-1.328-0.727-1.665
    c-0.028-0.059-0.041-0.046-0.07,0.002c-0.145-0.328-0.252-0.653-0.342-0.94c0.072,0.097,0.145,0.192,0.217,0.287
    c0.377-0.311,1.047-0.744,1.938-0.857c-0.423,0.271-0.835,0.485-1.614,1.273c0.06,0.075,0.117,0.146,0.176,0.22
    c0.306-0.208,0.687-0.466,1.038-0.701c-0.357,0.314-0.649,0.555-0.961,0.798c0.129,0.158,0.256,0.314,0.38,0.463
    c0.698-0.433,0.879-0.549,1.204-0.662c-0.518,0.362-0.874,0.735-1.013,0.891c0.188,0.221,0.366,0.425,0.529,0.607
    c0.401-0.244,0.627-0.44,0.879-0.635c-0.165,0.362-0.396,0.66-0.637,0.9c0.097,0.105,0.187,0.201,0.27,0.288
    c0.194-0.305,0.464-0.719,0.713-1.095c-0.211,0.472-0.392,0.818-0.606,1.202c0.092,0.092,0.171,0.17,0.233,0.227
    c0.124,0.114,0.249,0.226,0.371,0.341c0.149-0.261,0.322-0.559,0.485-0.837c-0.136,0.355-0.259,0.642-0.392,0.927
    c0.096,0.093,0.19,0.186,0.284,0.284c0.336-0.615,0.414-0.959,0.53-1.321c0.043,0.657-0.114,1.228-0.27,1.615
    c0.105,0.127,0.21,0.265,0.309,0.412c0.104-0.219,0.225-0.517,0.316-0.859c0.021,0.293-0.018,0.482-0.146,1.134
    c0.105,0.18,0.204,0.376,0.298,0.592c0.069-0.13,0.141-0.262,0.21-0.389c-0.06,0.174-0.116,0.329-0.173,0.477
    c0.27,0.652,0.487,1.49,0.631,2.617C517.788,235.968,517.619,235.736,517.491,235.574z M527.631,271.883
    c0.03,0.104,0.06,0.208,0.089,0.313c-0.06-0.04-0.121-0.08-0.182-0.119C527.568,272.012,527.6,271.946,527.631,271.883z
     M526.582,276.39c0.02-0.813,0.104-1.524,0.239-2.165c0.135,0.221,0.257,0.462,0.368,0.731
    C526.933,275.416,526.719,275.896,526.582,276.39z M527.622,274.265c-0.182-0.341-0.379-0.631-0.585-0.88
    c0.106-0.353,0.229-0.684,0.363-0.998c0.173,0.177,0.343,0.362,0.509,0.555c0.056,0.238,0.107,0.48,0.154,0.728
    C527.911,273.86,527.764,274.059,527.622,274.265z M528.074,275.33c0.059-0.144,0.121-0.287,0.186-0.429
    c0.066,0.541,0.114,1.101,0.135,1.68C528.306,276.118,528.197,275.703,528.074,275.33z M533.273,269.005
    c0.119,0.059,0.235,0.116,0.348,0.177c-0.111,0.249-0.218,0.508-0.317,0.774c-0.003-0.008-0.006-0.016-0.01-0.022
    C533.297,269.572,533.293,269.254,533.273,269.005z M536.662,272.979C536.662,272.979,536.662,272.979,536.662,272.979
    c0,0.029-0.144,0.063-0.155,0.099c-0.188,0.212-0.433,0.44-0.612,0.685c0.064-0.885,0.111-1.77,0.196-2.642
    c0.146,0.174,0.269,0.353,0.397,0.535c0.006,0.34,0.125,0.732,0.12,1.153C536.59,272.865,536.662,272.922,536.662,272.979z
     M533.263,272.271c0.039-0.208,0.081-0.416,0.126-0.622c0.046,0.152,0.09,0.307,0.134,0.462
    C533.437,272.163,533.351,272.216,533.263,272.271z M533.812,274.736l-0.14-0.3c-0.021-0.746-0.187-1.204-0.321-1.596
    c0.096-0.095,0.189-0.19,0.288-0.286c0.218,0.862,0.39,1.785,0.5,2.767C534.018,275.088,533.904,274.889,533.812,274.736z
     M535.728,275.502c0.013-0.238,0.026-0.479,0.041-0.719c0.101-0.203,0.212-0.403,0.329-0.598c-0.071,0.298-0.136,0.603-0.196,0.911
    C535.846,275.219,535.787,275.355,535.728,275.502z M536.347,268.896c0.043,0.279,0.09,0.577,0.137,0.882
    c0.032,0.305,0.064,0.614,0.095,0.911c-0.038-0.008-0.061,0-0.079,0.045c-0.006,0.033-0.008,0.071-0.011,0.107
    c-0.119-0.126-0.239-0.247-0.362-0.361c0.062-0.598,0.126-1.187,0.194-1.762C536.328,268.776,536.337,268.835,536.347,268.896z
     M536.179,266.161c-0.021,0.086-0.041,0.173-0.062,0.262c-0.002,0.005-0.003,0.011-0.003,0.018
    c-0.229,0.998-0.442,2.152-0.635,3.511c-0.427-0.307-0.883-0.555-1.366-0.733c0.483-1.202,1.147-2.267,2.061-3.077
    C536.176,266.147,536.177,266.154,536.179,266.161z M535.412,270.441c-0.185,1.388-0.348,2.978-0.485,4.811
    c-0.044,0.088-0.087,0.175-0.128,0.264c-0.216-1.21-0.456-2.337-0.74-3.36c0.225-0.212,0.457-0.425,0.693-0.64
    c-0.224,0.079-0.488,0.194-0.778,0.345c-0.113-0.392-0.235-0.766-0.364-1.124c0.123-0.452,0.266-0.893,0.429-1.318
    C534.557,269.729,535.012,270.072,535.412,270.441z M529.127,274.575c0.045,0.068,0.087,0.139,0.131,0.208
    c-0.017,0.078-0.033,0.16-0.049,0.246C529.183,274.878,529.157,274.728,529.127,274.575z M529.669,272.409
    c-0.069,0.376-0.115,0.907-0.146,1.497c-0.15-0.186-0.305-0.368-0.465-0.542C529.258,273.024,529.464,272.705,529.669,272.409z
     M533.919,258.83c0.159,0.294,0.332,0.645,0.519,1.025c-0.077-0.015-0.104-0.012-0.087,0.04c0.078,0.282,0.331,0.78,0.527,1.387
    c-0.07,0.001-0.095,0.009-0.077,0.07c0.055,0.199,0.172,0.477,0.316,0.814c0.146,0.335,0.326,0.729,0.454,1.177
    c-0.071-0.006-0.102,0.005-0.095,0.05c0.046,0.263,0.173,0.728,0.333,1.269c0.138,0.435,0.239,0.934,0.35,1.414
    c-1.063,0.582-1.871,1.649-2.49,2.998c-0.131-0.036-0.263-0.066-0.396-0.095c-0.01-0.109-0.021-0.206-0.036-0.284
    c-0.007-0.035-0.022-0.048-0.05-0.044c0.029-1.134,0.075-2.1,0.071-2.64c0-0.085-0.034-0.078-0.118-0.047
    c0.039-1.125,0.151-2.193,0.154-2.667c0.003-0.066-0.041-0.056-0.14-0.001c0.067-1.142,0.14-2.121,0.155-2.659
    c0.002-0.086-0.029-0.075-0.107-0.041c0.067-0.989,0.188-2.081,0.269-2.895c0.061,0.107,0.119,0.218,0.177,0.329
    c-0.079,0.002-0.109,0.004-0.089,0.068C533.621,258.293,533.752,258.539,533.919,258.83z M556.11,204.353
    c0.006,0.396,0.003,0.799-0.018,1.286c-0.042-0.378-0.083-0.777-0.12-1.143C556.02,204.448,556.065,204.4,556.11,204.353z
     M550.182,205.511c0.011,0.063,0.023,0.123,0.034,0.184c-0.022-0.053-0.043-0.106-0.067-0.158
    C550.159,205.526,550.171,205.519,550.182,205.511z M550.147,205.279c-0.008-0.01-0.013-0.021-0.02-0.029
    c-0.004-0.067-0.006-0.133-0.011-0.2C550.128,205.126,550.137,205.203,550.147,205.279z M547.069,214.901
    c-0.011,0.014-0.021,0.026-0.031,0.04c-0.004-0.021-0.008-0.038-0.012-0.05C547.04,214.895,547.055,214.898,547.069,214.901z
     M545.335,219.766c0.051-0.067,0.101-0.134,0.151-0.199c-0.007,0.059-0.013,0.115-0.019,0.173
    C545.438,219.743,545.394,219.754,545.335,219.766z M551.921,213.511c-0.388-0.392-0.734-0.625-1.046-0.781
    c0.01-0.011,0.02-0.022,0.03-0.032c0.056-0.059,0.109-0.114,0.162-0.168c0.353,0.127,0.653,0.287,0.872,0.42
    C551.897,213.13,551.892,213.32,551.921,213.511z M552.272,212.334c-0.252-0.194-0.491-0.334-0.715-0.439
    c0.057-0.061,0.112-0.119,0.164-0.175c0.244,0.11,0.497,0.175,0.744,0.2c0.044,0.025,0.084,0.048,0.117,0.068
    c0.009-0.021,0.016-0.041,0.024-0.061c0.122,0.004,0.242-0.003,0.359-0.019c0.014,0.016,0.03,0.029,0.044,0.045
    C552.73,212,552.474,212.128,552.272,212.334z M555.467,210.192c-0.008-0.014-0.017-0.027-0.026-0.04
    c0.04-0.077,0.079-0.155,0.118-0.23C555.527,210.016,555.497,210.104,555.467,210.192z M557.024,207.43
    c-0.031-0.017-0.064-0.03-0.095-0.045c0.029-0.115,0.063-0.228,0.105-0.349C557.03,207.171,557.026,207.301,557.024,207.43z
     M558.267,208.424c-0.286,0.234-0.59,0.475-0.948,0.743c0.305-0.311,0.626-0.636,0.926-0.937c-0.063-0.055-0.131-0.105-0.199-0.157
    c0.113-0.333,0.229-0.719,0.318-1.149C558.401,207.359,558.374,207.673,558.267,208.424z"/>
</g>
<path fill="none"  stroke-linecap="round" stroke-linejoin="round" d="M124.665,190.804
  c-0.447-0.021-0.956-0.018-1.504,0.025c0.452-0.183,0.825-0.263,1.369-0.461c-0.057-0.159-0.121-0.32-0.196-0.484
  c-0.14,0.078-0.516,0.23-0.7,0.279c0.222-0.168,0.506-0.316,0.642-0.41c-0.134-0.283-0.293-0.577-0.485-0.883
  c-0.424,0.191-0.901,0.436-1.397,0.735c0.335-0.376,0.626-0.623,1.203-1.037c-0.29-0.435-0.641-0.894-1.057-1.374
  c-0.264,0.137-0.539,0.291-0.821,0.466c0.212-0.243,0.405-0.429,0.666-0.643c-0.098-0.11-0.198-0.222-0.304-0.334
  c-0.487,0.212-1.069,0.498-1.675,0.872c0.366-0.417,0.679-0.676,1.382-1.181c-0.052-0.053-0.1-0.105-0.153-0.159
  c-0.286-0.289-0.589-0.552-0.899-0.803c-0.572,0.188-1.439,0.524-2.37,1.055c0.453-0.483,0.82-0.741,1.877-1.435
  c-0.428-0.308-0.879-0.578-1.345-0.82c-0.304,0.299-0.631,0.603-1.015,0.944c0.28-0.337,0.576-0.692,0.856-1.024
  c-0.266-0.135-0.534-0.264-0.81-0.38c-0.433,0.421-0.999,1.036-1.548,1.808c0.205-0.609,0.425-0.997,1.055-2.01
  c-0.559-0.211-1.132-0.384-1.714-0.523c-0.347,0.629-0.684,1.215-1.159,1.948c0.306-0.686,0.647-1.432,0.913-2.009
  c-0.596-0.135-1.202-0.239-1.813-0.313c-0.153,0.373-0.307,0.792-0.443,1.247c-0.001-0.428,0.037-0.764,0.14-1.283
  c-0.216-0.022-0.433-0.046-0.65-0.061c-0.176,0.579-0.38,1.399-0.489,2.348c-0.118-0.648-0.108-1.103-0.038-2.379
  c-0.356-0.02-0.712-0.024-1.068-0.027c-0.04,0.529-0.092,1.065-0.178,1.711c-0.013-0.568-0.022-1.175-0.028-1.712
  c-0.578,0.003-1.155,0.022-1.727,0.063c0.068,0.573,0.077,0.925,0.034,1.375c-0.1-0.494-0.226-0.948-0.354-1.352
  c-0.342,0.028-0.679,0.062-1.013,0.102c0.138,0.722,0.191,1.528,0.068,2.399c-0.246-0.682-0.444-1.343-0.917-2.285
  c-0.314,0.05-0.62,0.104-0.924,0.158c8.961-9.19,19.08-5.598,19.08-5.598c-0.099-0.035-0.194-0.067-0.293-0.104
  c0.191,0.064,0.293,0.104,0.293,0.104c-0.802-0.563-1.583-1.062-2.346-1.512c-0.537-0.069-1.283-0.127-2.111-0.078
  c0.412-0.167,0.736-0.24,1.396-0.332c-0.34-0.188-0.676-0.364-1.01-0.53c-0.296,0.069-0.611,0.148-0.939,0.236
  c0.197-0.137,0.374-0.269,0.591-0.409c-0.681-0.328-1.348-0.614-1.997-0.856c-0.492,0.171-1.179,0.455-1.898,0.893
  c0.321-0.39,0.598-0.618,1.283-1.109c-0.814-0.271-1.603-0.479-2.363-0.623c-0.981,0.802-1.392,1.377-1.874,1.951
  c0.146-0.841,0.446-1.543,0.798-2.119c-0.891-0.105-1.742-0.125-2.558-0.069c-0.305,0.577-0.669,1.36-0.994,2.307
  c0.066-0.722,0.175-1.215,0.468-2.26c-0.323,0.036-0.643,0.08-0.953,0.14c-0.12,0.472-0.236,1.008-0.33,1.601
  c-0.038-0.523-0.036-0.939,0.009-1.531c-1.229,0.267-2.357,0.712-3.384,1.294c-0.183,0.62-0.382,1.422-0.524,2.361
  c-0.053-0.671-0.04-1.158,0.042-2.073c-0.948,0.595-1.801,1.308-2.564,2.1c0.055,0.498,0.134,1.055,0.254,1.657
  c-0.205-0.473-0.342-0.861-0.494-1.403c-0.929,1.007-1.712,2.133-2.351,3.302c0.106-0.586,0.213-1.185,0.315-1.782
  c-0.745-0.133-1.06-0.205-1.495-0.377c0.61,0.069,1.145,0.07,1.551,0.05c0.041-0.239,0.082-0.479,0.122-0.718
  c-0.527-0.028-0.811-0.064-1.189-0.161c0.467-0.009,0.884-0.052,1.233-0.104c0.146-0.883,0.286-1.76,0.421-2.608
  c-0.653,0.098-0.969,0.118-1.391,0.057c0.554-0.135,1.049-0.317,1.457-0.493c0.027-0.188,0.058-0.375,0.086-0.559
  c-0.302,0.089-0.524,0.137-0.809,0.171c0.313-0.131,0.595-0.27,0.845-0.404c0.156-1.038,0.3-2.009,0.423-2.866
  c-0.226,0.082-0.416,0.133-0.645,0.172c0.245-0.136,0.473-0.279,0.68-0.42c0.036-0.257,0.071-0.505,0.104-0.737
  c-0.196,0.117-0.359,0.189-0.559,0.254c0.222-0.176,0.422-0.358,0.6-0.534c0.193-1.387,0.308-2.255,0.308-2.255
  c-0.349,0.354-0.67,0.724-0.983,1.101c0.006,0.161,0.014,0.33,0.024,0.507c-0.036-0.146-0.071-0.28-0.098-0.418
  c-0.463,0.563-0.891,1.145-1.278,1.745c-0.006,0.329,0.003,0.701,0.029,1.11c-0.08-0.296-0.128-0.539-0.178-0.87
  c-0.104,0.165-0.205,0.328-0.302,0.494c0.068,0.383,0.168,0.846,0.314,1.353c-0.18-0.347-0.292-0.62-0.458-1.101
  c-0.492,0.869-0.915,1.76-1.273,2.661c0.146,0.318,0.332,0.685,0.557,1.073c-0.235-0.267-0.402-0.488-0.646-0.854
  c-0.058,0.154-0.115,0.306-0.17,0.46c0.206,0.395,0.476,0.854,0.81,1.329c-0.328-0.263-0.549-0.494-0.938-0.975
  c-0.115,0.333-0.224,0.667-0.322,1.003c0.157,0.179,0.33,0.366,0.52,0.558c-0.203-0.134-0.368-0.256-0.559-0.415
  c-0.239,0.827-0.426,1.65-0.567,2.457c0.283,0.301,0.666,0.68,1.137,1.069c-0.389-0.202-0.647-0.375-1.184-0.778
  c-0.147,0.901-0.239,1.778-0.278,2.615c0.279,0.255,0.623,0.551,1.033,0.851c-0.352-0.156-0.602-0.298-1.042-0.583
  c-0.007,0.232-0.011,0.46-0.011,0.684c0.204,0.084,0.426,0.17,0.662,0.25c-0.233-0.024-0.427-0.053-0.659-0.104
  c0.001,0.331,0.015,0.652,0.033,0.966c-0.066-0.564-0.165-1.138-0.317-1.719c-0.294,0.063-0.59,0.126-0.875,0.188
  c0.298-0.113,0.573-0.213,0.842-0.309c-0.095-0.35-0.211-0.702-0.346-1.057c-0.396,0.288-0.691,0.463-1.073,0.641
  c0.355-0.321,0.673-0.643,0.95-0.948c-0.222-0.535-0.499-1.078-0.834-1.628c-0.2,0.635-0.484,1.29-0.903,1.93
  c0.076-0.871,0.199-1.687,0.12-3.062c-0.239-0.312-0.499-0.627-0.783-0.944c-0.137,0.917-0.231,1.345-0.45,1.9
  c0.071-0.936,0.037-1.771-0.021-2.403c-0.139-0.144-0.279-0.284-0.429-0.429c-0.023,0.631-0.051,1.31-0.08,1.952
  c-0.088-0.822-0.141-1.516-0.175-2.192c-0.422-0.392-0.884-0.789-1.388-1.189c0.139,1.098,0.172,1.557,0.103,2.202
  c-0.231-1.185-0.588-2.163-0.842-2.77c-0.25-0.186-0.509-0.372-0.779-0.56c0.125,0.576,0.257,1.188,0.377,1.768
  c-0.274-0.735-0.49-1.366-0.686-1.98c-0.164-0.112-0.335-0.225-0.507-0.336c0.094,0.303,0.185,0.605,0.274,0.899
  c-0.175-0.368-0.333-0.709-0.479-1.034c-0.867-0.56-1.823-1.126-2.876-1.698c0,0,2.899,3.606,5.763,7.436
  c-0.088-0.064-0.175-0.133-0.265-0.195c-0.058,0.404-0.119,0.826-0.18,1.229c-0.002-0.497,0.006-0.932,0.022-1.346
  c-0.294-0.205-0.597-0.398-0.906-0.584c0.004,0.493,0.007,1.033,0.009,1.541c-0.098-0.623-0.163-1.15-0.215-1.665
  c-0.319-0.185-0.644-0.363-0.979-0.524c0.226,0.81,0.316,1.242,0.376,1.842c-0.288-0.827-0.604-1.53-0.877-2.069
  c-0.276-0.12-0.56-0.232-0.847-0.334c0.075,0.379,0.15,0.771,0.223,1.147c-0.156-0.44-0.287-0.831-0.401-1.206
  c-0.756-0.257-1.543-0.453-2.361-0.567c0.138,0.523,0.205,0.892,0.254,1.363c-0.184-0.521-0.379-0.994-0.568-1.408
  c-0.301-0.033-0.608-0.054-0.917-0.068c0.443,0.913,0.625,1.347,0.804,1.983c-0.459-0.822-0.927-1.499-1.306-1.996
  c-0.22,0-0.439,0.006-0.662,0.017c0.188,0.418,0.387,0.864,0.573,1.288c-0.3-0.469-0.546-0.878-0.771-1.275
  c-0.503,0.031-1.013,0.087-1.534,0.179c0.423,0.494,0.821,1.101,1.091,1.839c-0.545-0.48-1.021-0.97-2.074-1.628
  c-0.279,0.069-0.563,0.151-0.848,0.239c0.375,0.399,0.799,0.853,1.192,1.278c-0.556-0.44-0.998-0.82-1.427-1.205
  c-0.343,0.112-0.689,0.236-1.039,0.378c0.724,0.399,1.021,0.584,1.385,0.914c-0.746-0.349-1.438-0.562-1.928-0.688
  c-0.216,0.096-0.437,0.203-0.656,0.311c0.207,0.165,0.416,0.333,0.619,0.494c-0.267-0.153-0.507-0.298-0.736-0.438
  c-0.32,0.158-0.642,0.328-0.967,0.511c0.228,0.112,0.415,0.219,0.621,0.329c-0.322-0.06-0.631-0.111-0.922-0.157
  c-0.285,0.168-0.572,0.343-0.86,0.53c0.633,0.044,0.952,0.091,1.366,0.217c-0.787,0.003-1.479,0.096-1.973,0.188
  c-0.646,0.443-1.299,0.934-1.961,1.48c0,0,0.006-0.003,0.013-0.006c0.377-0.154,9.71-3.836,19.779,4.674
  c-0.253,0.005-0.511,0.017-0.778,0.041c0.271,0.439,0.563,0.922,0.839,1.379c-0.405-0.501-0.733-0.929-1.045-1.358
  c-0.231,0.024-0.47,0.054-0.711,0.092c0.115,0.188,0.229,0.378,0.341,0.563c-0.156-0.192-0.299-0.373-0.436-0.55
  c-0.338,0.053-0.683,0.113-1.039,0.191c0.323,0.414,0.52,0.723,0.729,1.123c-0.373-0.391-0.741-0.734-1.088-1.037
  c-0.262,0.065-0.514,0.146-0.764,0.229c0.323,0.284,0.664,0.586,0.985,0.875c-0.444-0.289-0.821-0.548-1.179-0.807
  c-0.463,0.169-0.909,0.365-1.329,0.595c0.976,0.381,1.396,0.577,1.93,0.949c-1.018-0.317-1.94-0.471-2.604-0.547
  c-0.221,0.146-0.435,0.302-0.644,0.463c0.44,0.154,0.92,0.325,1.372,0.489c-0.597-0.104-1.092-0.211-1.574-0.328
  c-0.331,0.269-0.646,0.557-0.944,0.867c1.125,0.484,1.815,0.616,2.541,0.809c-1.195,0.323-2.334,0.207-3.237-0.026
  c-0.27,0.333-0.523,0.678-0.765,1.04c0.482,0.102,1.027,0.22,1.535,0.334c-0.634-0.026-1.151-0.068-1.667-0.127
  c-0.252,0.39-0.487,0.794-0.708,1.212c0.301,0.064,0.608,0.134,0.906,0.199c-0.358-0.014-0.677-0.035-0.981-0.06
  c-0.135,0.259-0.262,0.524-0.388,0.793c0.6,0.125,1.487,0.312,2.288,0.49c-0.934-0.036-1.619-0.111-2.408-0.22
  c-0.128,0.291-0.253,0.58-0.369,0.88c0.586-0.026,1.228,0.024,1.904,0.21c-0.657,0.171-1.287,0.289-2.238,0.691
  c-0.15,0.435-0.287,0.878-0.417,1.326c0.468-0.112,0.992-0.235,1.487-0.347c-0.586,0.249-1.072,0.435-1.559,0.601
  c-0.291,1.037-0.536,2.097-0.732,3.162c0.389-0.307,0.855-0.719,1.319-1.238c-0.273,0.618-0.539,0.951-1.472,2.14
  c-0.057,0.343-0.106,0.684-0.154,1.022c0.318-0.263,0.653-0.54,0.973-0.803c-0.369,0.419-0.69,0.765-1.009,1.089
  c-0.084,0.629-0.154,1.252-0.214,1.867c0.286-0.252,0.594-0.548,0.901-0.889c-0.214,0.484-0.423,0.793-0.953,1.475
  c-0.042,0.501-0.075,0.994-0.104,1.478c0.216-0.23,0.438-0.467,0.653-0.692c-0.239,0.347-0.455,0.648-0.668,0.93
  c-0.228,4.177,0.01,7.533,0.03,8.258c0.265-2.672,0.861-5.175,1.69-7.479c-0.251,0.128-0.515,0.261-0.811,0.4
  c0.29-0.207,0.597-0.42,0.89-0.625c0.155-0.418,0.313-0.832,0.483-1.237c-0.168,0.095-0.341,0.193-0.512,0.301
  c0.223-0.279,0.435-0.504,0.698-0.74c0.145-0.327,0.292-0.651,0.445-0.97c-0.394,0.128-0.807,0.28-1.229,0.46
  c0.444-0.409,0.83-0.675,1.521-1.053c0.332-0.656,0.683-1.292,1.05-1.903c-0.506,0.069-1.051,0.17-1.618,0.311
  c0.568-0.376,1.033-0.584,1.986-0.91c0.109-0.174,0.225-0.344,0.337-0.516c-0.295,0.028-0.604,0.052-0.949,0.069
  c0.35-0.084,0.717-0.168,1.068-0.25c0.229-0.34,0.466-0.67,0.705-0.994c-0.553-0.063-1.1-0.142-1.775-0.27
  c0.661,0.005,1.38,0.018,1.951,0.031c0.17-0.228,0.342-0.449,0.518-0.668c-0.953-0.355-1.65-0.482-2.272-0.664
  c0.994-0.241,1.98-0.187,2.838-0.021c0.176-0.205,0.353-0.406,0.531-0.602c-0.347-0.189-0.715-0.383-1.106-0.567
  c0.465,0.073,0.85,0.165,1.332,0.324c0.16-0.172,0.326-0.335,0.49-0.5c-0.823-0.513-1.479-0.94-2.377-1.616
  c1.002,0.528,2.139,1.148,2.592,1.396c0.289-0.286,0.581-0.564,0.877-0.827c-1.127-1.078-1.453-1.389-1.845-1.974
  c0.957,0.814,1.861,1.333,2.311,1.569c0.244-0.207,0.492-0.403,0.737-0.594c-0.563-0.809-1.018-1.489-1.604-2.483
  c0.671,0.845,1.419,1.807,1.818,2.321c0.273-0.206,0.547-0.406,0.822-0.592c-0.243-0.537-0.478-1.09-0.743-1.771
  c0.304,0.552,0.628,1.146,0.908,1.663c0.15-0.1,0.302-0.2,0.451-0.291c-0.208-0.81-0.522-1.84-0.987-2.954
  c0.545,0.713,0.85,1.27,1.516,2.641c0.278-0.157,0.557-0.305,0.833-0.44c-0.114-1.552-0.365-2.335-0.573-3.194
  c0.759,0.924,1.197,1.92,1.452,2.797c0.118-0.049,0.234-0.095,0.351-0.136c-0.153-0.698-0.287-1.388-0.423-2.256
  c0.214,0.734,0.439,1.526,0.621,2.172c0.048-0.018,0.092-0.052,0.139-0.067c0.032-0.542,0.013-1.303-0.005-1.942
  c0.165,0.553,0.198,0.695,0.302,1.456c0.005-0.001,0.01-0.003,0.015-0.004c-0.309,0.062-0.402,1.177-0.604,1.714
  c0.494,0.115,1.126,0.392,1.686,0.527c-0.668-0.045-1.188-0.073-1.729-0.151c-0.146,0.399-0.284,0.835-0.42,1.265
  c0.506,0.019,1.071,0.141,1.639,0.447c-0.521,0.053-1.009,0.063-1.858,0.321c-0.071,0.249-0.14,0.506-0.207,0.766
  c0.521-0.035,1.188-0.072,1.798-0.103c-0.711,0.159-1.266,0.255-1.856,0.337c-0.057,0.215-0.107,0.429-0.16,0.648
  c0.358-0.021,0.737-0.043,1.099-0.061c-0.421,0.095-0.786,0.167-1.139,0.225c-0.123,0.532-0.24,1.077-0.35,1.634
  c0.348-0.034,0.725-0.019,1.117,0.067c-0.011,0.009-0.024,0.021-0.041,0.041c-0.046,0.05-0.092,0.104-0.139,0.161
  c-0.308,0.095-0.64,0.207-1.071,0.417c-0.045,0.242-0.086,0.492-0.13,0.739c0.146-0.037,0.301-0.075,0.459-0.115
  c-0.049,0.072-0.098,0.145-0.148,0.22c-0.116,0.042-0.232,0.083-0.349,0.124c-0.019,0.109-0.034,0.222-0.053,0.332
  c-0.01-0.069-0.021-0.084-0.062-0.03c-0.275,0.349-0.729,0.99-1.258,1.751c-0.525,0.764-1.149,1.628-1.655,2.499
  c-0.08-0.184-0.08-0.297-0.156-0.157c-0.181,0.325-0.426,0.763-0.711,1.276c-0.043-0.105-0.075-0.13-0.126-0.046
  c-0.226,0.389-0.604,1.045-1.051,1.814c-0.221,0.386-0.457,0.799-0.699,1.222c-0.222,0.437-0.448,0.883-0.67,1.317
  c-0.035-0.139-0.053-0.183-0.107-0.088c-0.289,0.503-0.805,1.508-1.381,2.628c-0.05-0.087-0.07-0.118-0.132-0.012
  c-0.195,0.355-0.471,0.857-0.742,1.488c-0.036-0.094-0.06-0.113-0.099-0.027c-0.343,0.783-1.146,2.626-1.901,4.357
  c-0.051-0.093-0.064-0.131-0.125-0.017c-0.127,0.251-0.287,0.569-0.438,0.955c-0.139,0.389-0.3,0.836-0.473,1.322
  c-0.067-0.16-0.081-0.214-0.12-0.126c-0.35,0.828-1.167,2.979-1.8,4.903c-0.144-0.154-0.161-0.27-0.221-0.11
  c-0.125,0.335-0.261,0.794-0.413,1.337c-0.086-0.104-0.129-0.127-0.156-0.029c-0.16,0.604-0.475,1.788-0.818,3.087
  c-0.062-0.073-0.087-0.096-0.12,0.014c-0.093,0.317-0.225,0.751-0.341,1.279c-0.109,0.53-0.237,1.148-0.374,1.812
  c-0.065-0.102-0.093-0.126-0.119-0.029c-0.193,0.714-0.538,2.308-0.898,3.93c-0.07-0.076-0.098-0.104-0.138,0.01
  c-0.287,0.749-0.389,2.245-0.684,3.936c-0.196-0.23-0.249-0.339-0.279-0.198c-0.131,0.617-0.327,1.876-0.544,3.25
  c-0.111-0.093-0.149-0.134-0.172-0.006c-0.117,0.648-0.157,1.803-0.301,3.152c-0.09-0.096-0.125-0.117-0.142-0.019
  c-0.054,0.341-0.118,0.878-0.188,1.519c-0.648-0.938-1.266-1.769-1.596-2.151c-0.054-0.058-0.062-0.005-0.104,0.161
  c-0.518-0.682-0.942-1.257-1.262-1.598c-0.073-0.078-0.083-0.038-0.124,0.068c-0.977-1.224-2.011-2.523-2.489-3.044
  c-0.056-0.061-0.075-0.026-0.102,0.086c-0.366-0.444-0.673-0.814-0.909-1.057c-0.071-0.072-0.086-0.035-0.123,0.064
  c-0.684-0.778-1.31-1.453-1.644-1.789c-0.071-0.071-0.076-0.011-0.097,0.162c-0.843-0.935-1.527-1.752-1.988-2.131
  c-0.075-0.063-0.085-0.029-0.104,0.057c-0.927-0.993-1.831-1.967-2.256-2.389c-0.058-0.058-0.068-0.027-0.077,0.074
  c-0.731-0.821-1.358-1.527-1.71-1.924c-0.063-0.067-0.074-0.041-0.096,0.039c-0.698-0.828-1.336-1.583-1.661-1.969
  c-0.05-0.058-0.076-0.033-0.106,0.058c-0.285-0.354-0.528-0.653-0.717-0.866c-0.063-0.071-0.076-0.041-0.105,0.046
  c-0.674-0.751-1.248-1.498-1.702-2.108c0.572-0.714,0.84-1.81,0.869-3.145c-0.142-0.139-0.299-0.282-0.473-0.427
  c0.172,0.069,0.311,0.14,0.474,0.239c0.001-0.153-0.001-0.31-0.006-0.469c-0.216-0.167-0.475-0.349-0.77-0.522
  c0.261,0.064,0.456,0.139,0.759,0.283c-0.014-0.277-0.035-0.563-0.064-0.854c-0.28-0.163-0.635-0.349-1.051-0.516
  c0.326,0.033,0.56,0.091,1.021,0.24c-0.071-0.612-0.177-1.251-0.31-1.904c-0.17-0.066-0.345-0.138-0.533-0.218
  c0.17,0.045,0.343,0.092,0.518,0.138c-0.057-0.268-0.116-0.538-0.18-0.813c-0.353-0.057-0.718-0.123-1.139-0.209
  c0.364,0.025,0.747,0.057,1.108,0.084c-0.051-0.212-0.104-0.427-0.159-0.641c-0.343-0.1-0.949-0.246-1.684-0.307
  c0.422-0.081,0.707-0.077,1.586-0.056c-0.045-0.17-0.093-0.341-0.142-0.509c-0.868-0.136-1.337-0.102-1.84-0.098
  c0.588-0.281,1.176-0.396,1.683-0.431c-0.087-0.287-0.177-0.573-0.271-0.858c-0.769,0.062-1.2,0.177-1.667,0.276
  c0.515-0.351,1.048-0.567,1.524-0.703c-0.124-0.363-0.255-0.725-0.389-1.085c-0.354,0.088-0.725,0.173-1.153,0.266
  c0.364-0.126,0.75-0.255,1.112-0.377c-0.045-0.119-0.09-0.24-0.136-0.358c-0.244,0.004-1.032,0.034-1.969,0.268
  c0.414-0.234,0.705-0.318,1.813-0.662c-0.154-0.391-0.314-0.773-0.479-1.152c-0.693,0.123-1.315,0.222-2.152,0.314
  c0.723-0.172,1.522-0.355,2.08-0.483c-0.042-0.094-0.082-0.19-0.124-0.285c-0.238-0.004-1.03,0-1.979,0.208
  c0.418-0.221,0.71-0.296,1.803-0.599c-0.066-0.146-0.136-0.294-0.205-0.438c-1.255,0.068-1.813,0.249-2.43,0.397
  c0.705-0.575,1.492-0.862,2.135-1.007c-0.118-0.233-0.235-0.461-0.354-0.685c-0.667,0.204-1.27,0.374-2.085,0.569
  c0.696-0.258,1.464-0.536,1.998-0.73c-0.049-0.093-0.098-0.187-0.147-0.277c-0.329,0.024-1.029,0.104-1.836,0.343
  c0.386-0.241,0.666-0.339,1.644-0.688c-0.084-0.15-0.169-0.296-0.253-0.44c-0.252,0.067-0.518,0.133-0.811,0.204
  c0.25-0.093,0.51-0.188,0.764-0.282c-0.08-0.134-0.16-0.269-0.24-0.398c-1.136,0.101-1.667,0.283-2.251,0.438
  c0.692-0.422,1.426-0.653,2.034-0.78c-0.123-0.19-0.246-0.371-0.368-0.546c-0.635,0.065-1.042,0.171-1.476,0.261
  c0.386-0.294,0.795-0.505,1.188-0.657c-0.063-0.085-0.127-0.163-0.189-0.244c0.369-0.011,0.725-0.012,1.068-0.002
  c0.894-0.972,1.163-1.263,1.674-1.614c-0.502,0.617-0.864,1.207-1.102,1.639c0.369,0.022,0.725,0.056,1.065,0.102
  c0.479-0.846,0.656-1.403,0.889-1.985c0.125,0.769,0.055,1.487-0.103,2.113c0.211,0.042,0.416,0.086,0.617,0.137
  c0.153-0.504,0.327-1.068,0.494-1.597c-0.089,0.625-0.181,1.144-0.285,1.648c0.303,0.082,0.587,0.179,0.862,0.28
  c0.177-0.314,0.358-0.638,0.535-0.948c-0.138,0.366-0.269,0.691-0.396,1.001c0.121,0.049,0.246,0.093,0.363,0.145
  c0.728-1.044,0.952-1.362,1.406-1.767c-0.484,0.795-0.778,1.528-0.937,1.985c0.202,0.102,0.397,0.211,0.583,0.324
  c0.485-0.471,1.12-1.083,1.708-1.642c-0.548,0.701-1,1.229-1.492,1.775c0.355,0.231,0.681,0.486,0.983,0.759
  c0.292-0.386,0.644-0.9,0.962-1.508c-0.085,0.517-0.223,0.855-0.645,1.805c0.11,0.11,0.218,0.221,0.321,0.337
  c0.348-0.377,0.728-0.786,1.09-1.172c-0.348,0.503-0.653,0.92-0.96,1.317c0.166,0.195,0.322,0.396,0.471,0.603
  c0.494-0.528,0.786-0.942,1.115-1.363c-0.113,0.751-0.388,1.411-0.715,1.971c0.173,0.283,0.331,0.575,0.477,0.875
  c0.282-0.376,0.61-0.865,0.911-1.438c-0.089,0.537-0.232,0.88-0.692,1.913c0.057,0.133,0.112,0.266,0.165,0.399
  c0.191-0.293,0.389-0.595,0.58-0.884c-0.18,0.393-0.345,0.737-0.51,1.063c0.175,0.455,0.328,0.919,0.464,1.391
  c0.237-0.322,0.485-0.658,0.725-0.977c-0.235,0.446-0.451,0.829-0.665,1.192c0.073,0.265,0.142,0.534,0.206,0.805
  c0.185-0.277,0.375-0.591,0.554-0.934c-0.07,0.429-0.177,0.736-0.453,1.373c0.107,0.489,0.202,0.979,0.29,1.466
  c0.154-0.207,0.311-0.417,0.461-0.619c-0.148,0.283-0.291,0.543-0.431,0.787c0.292,1.652,0.506,3.263,0.834,4.64
  c2.115-7.767-1.675-19.055-8.252-19.368c-1.337-0.063-2.504,0.091-3.519,0.372c0.353-0.295,0.702-0.563,1.046-0.809
  c0.517-0.09,1.052-0.139,1.594-0.15c-0.023-0.259-0.044-0.521-0.065-0.798c0.041-0.021,0.081-0.044,0.12-0.064
  c0.057,0.289,0.114,0.578,0.17,0.858c0.487-0.003,0.984,0.025,1.483,0.075c0.044-0.529,0.1-1.071,0.178-1.677
  c0.023-0.007,0.049-0.016,0.072-0.022c0.006,0.585,0.012,1.184,0.016,1.728c0.127,0.015,0.255,0.031,0.384,0.049
  c0.151-0.632,0.27-0.987,0.483-1.427c-0.08,0.531-0.121,1.032-0.141,1.479c0.379,0.061,0.758,0.132,1.135,0.215
  c0.219-0.768,0.569-1.584,1.13-2.368c0.017,0.001,0.032,0.004,0.048,0.004c-0.108,0.744-0.248,1.457-0.268,2.579
  c0.252,0.066,0.503,0.138,0.752,0.209c0.094-0.389,0.189-0.783,0.282-1.167c-0.038,0.438-0.079,0.835-0.124,1.213
  c0.498,0.148,0.987,0.31,1.467,0.478c0.854-0.852,1.192-1.16,1.757-1.503c-0.531,0.601-0.951,1.191-1.267,1.68
  c0.331,0.124,0.656,0.25,0.973,0.378c0.442-0.467,0.97-0.914,1.605-1.293c-0.295,0.49-0.599,0.948-0.925,1.573
  c0.665,0.282,1.287,0.567,1.849,0.841c0.486-0.114,0.821-0.159,1.242-0.172c-0.317,0.108-0.612,0.225-0.892,0.341
  c1.65,0.816,2.705,1.466,2.705,1.466c-0.096-0.16-0.192-0.311-0.291-0.466c0.191,0.291,0.291,0.466,0.291,0.466
  c-0.057-0.724-0.149-1.398-0.27-2.031c-0.31-0.398-0.688-0.844-1.134-1.293c0.359,0.19,0.633,0.374,1.001,0.672
  c-0.104-0.438-0.224-0.854-0.356-1.245c-0.501-0.248-1.018-0.518-1.607-0.845c0.494,0.183,1.012,0.375,1.508,0.563
  c-0.156-0.42-0.33-0.811-0.519-1.175c-0.862-0.257-1.495-0.351-2.152-0.483c0.584-0.177,1.153-0.268,1.695-0.302
  c-0.331-0.506-0.698-0.948-1.098-1.33c-0.493,0.02-1.062,0.067-1.668,0.164c0.415-0.205,0.756-0.325,1.325-0.472
  c-0.786-0.658-1.675-1.105-2.629-1.364c-0.58,0.281-1.022,0.547-1.494,0.805c0.232-0.359,0.487-0.681,0.754-0.972
  c-0.214-0.037-0.432-0.065-0.649-0.085c-0.283,0.28-0.584,0.564-0.923,0.881c0.234-0.298,0.479-0.615,0.719-0.917
  c-1.492-0.097-3.046,0.57-4.601,1.143c-0.006,0.002,0.019,0.371,0.012,0.371c-0.002,0-0.003,0-0.004,0
  c-2.54,0-4.637,1.174-6.299,2.867c0.505-0.854,1.01-1.912,1.526-2.83c-0.616-0.352-0.879-0.617-1.208-0.904
  c0.515,0.252,0.981,0.368,1.348,0.479c0.11-0.198,0.22-0.422,0.33-0.62c-0.456-0.19-0.696-0.322-1-0.525
  c0.415,0.138,0.797,0.221,1.121,0.283c0.406-0.732,0.806-1.462,1.188-2.168c-0.604-0.119-0.891-0.206-1.242-0.39
  c0.528,0.054,1.026,0.049,1.438,0.021c0.086-0.157,0.17-0.312,0.252-0.465c-0.293-0.017-0.507-0.045-0.766-0.104
  c0.315-0.017,0.608-0.05,0.87-0.092c0.465-0.865,0.896-1.677,1.274-2.395c-0.225,0.002-0.408-0.015-0.622-0.051
  c0.259-0.044,0.505-0.098,0.731-0.157c0.114-0.216,0.222-0.423,0.324-0.618c-0.212,0.04-0.378,0.055-0.575,0.05
  c0.252-0.087,0.485-0.186,0.698-0.283c0.606-1.163,0.979-1.894,0.979-1.894c-0.417,0.203-0.819,0.428-1.212,0.662
  c-0.047,0.144-0.093,0.297-0.138,0.456c0.011-0.144,0.024-0.271,0.043-0.401c-0.585,0.354-1.145,0.732-1.675,1.139
  c-0.107,0.288-0.22,0.618-0.322,0.988c0.022-0.286,0.055-0.515,0.116-0.821c-0.144,0.112-0.285,0.223-0.423,0.339
  c-0.06,0.36-0.116,0.797-0.147,1.293c-0.049-0.364-0.062-0.64-0.059-1.114c-0.706,0.61-1.359,1.266-1.959,1.945
  c0.029,0.328,0.079,0.709,0.156,1.125c-0.125-0.312-0.202-0.56-0.304-0.959c-0.1,0.116-0.196,0.232-0.294,0.352
  c0.06,0.414,0.151,0.903,0.296,1.429c-0.207-0.336-0.327-0.609-0.52-1.154c-0.206,0.258-0.408,0.519-0.6,0.782
  c0.083,0.207,0.177,0.427,0.284,0.656c-0.139-0.183-0.245-0.342-0.363-0.542c-0.471,0.655-0.894,1.321-1.271,1.988
  c0.154,0.354,0.373,0.808,0.665,1.301c-0.278-0.3-0.452-0.535-0.8-1.059c-0.413,0.748-0.77,1.494-1.065,2.221
  c0.164,0.312,0.378,0.681,0.644,1.075c-0.262-0.247-0.438-0.451-0.735-0.843c-0.078,0.202-0.155,0.402-0.226,0.601
  c0.154,0.139,0.323,0.282,0.508,0.428c-0.2-0.092-0.362-0.18-0.552-0.3c-0.132,0.384-0.246,0.758-0.345,1.121
  c-0.167-0.407-0.348-0.83-0.54-1.27c-0.115,0.059-0.237,0.122-0.36,0.185c-0.03-0.06-0.059-0.115-0.089-0.175
  c-0.098,0.134-0.185,0.251-0.265,0.355c-0.129,0.065-0.257,0.132-0.39,0.198c0.048-0.09,0.096-0.178,0.14-0.263
  c0.289-0.189,0.57-0.367,0.855-0.541c-0.132-0.297-0.27-0.601-0.411-0.909c-0.145,0.113-0.267,0.209-0.384,0.299
  c-0.059-0.11-0.117-0.22-0.175-0.329c0.128-0.151,0.246-0.295,0.346-0.423c-0.175-0.369-0.698-0.745-0.886-1.126
  c-0.132,0.102-0.713,0.21-0.787,0.323c0.039-0.063,0.255-0.126,0.219-0.189c0.146-0.13,0.462-0.261,0.612-0.39
  c-0.155-0.312-0.227-0.623-0.389-0.937c-0.117,0.185-0.208,0.371-0.351,0.557c-0.041-0.073-0.06-0.146-0.102-0.221
  c-0.264,0.384-0.548,0.813-0.841,1.234c-0.052,0.044-0.097,0.09-0.151,0.133c0.305-0.601,0.585-1.11,0.873-1.612
  c-0.044-0.078-0.088-0.158-0.135-0.237c0.087-0.256,0.173-0.539,0.259-0.865c-0.305-0.568-0.618-1.138-0.941-1.698
  c-0.144,0.196-0.293,0.398-0.44,0.602c-0.021-0.035-0.043-0.071-0.063-0.107c0.135-0.24,0.265-0.467,0.395-0.687
  c-0.123-0.211-0.248-0.426-0.374-0.635c-0.152,0.283-0.274,0.507-0.385,0.699c-0.062-0.105-0.123-0.206-0.185-0.31
  c0.112-0.346,0.195-0.657,0.255-0.912c-0.396-0.649-0.8-1.279-1.207-1.88c-0.049,0.319-0.1,0.646-0.148,0.965
  c-0.01-0.016-0.02-0.03-0.028-0.045c0.005-0.407,0.016-0.779,0.031-1.134c-0.182-0.265-0.364-0.524-0.549-0.776
  c-0.02,0.403-0.041,0.688-0.08,0.945c-0.018-0.028-0.037-0.059-0.056-0.088c-0.029,0.114-0.059,0.225-0.09,0.339
  c-0.005-0.099-0.012-0.198-0.019-0.292c0.008-0.062,0.016-0.124,0.022-0.183c-0.016-0.025-0.031-0.049-0.046-0.075
  c-0.052-0.494-0.124-0.926-0.196-1.269c-0.232-0.301-0.464-0.589-0.696-0.864c-0.007,0.229-0.017,0.47-0.025,0.711
  c-0.054-0.081-0.107-0.162-0.159-0.24c-0.018-0.252-0.033-0.496-0.046-0.739c-0.573-0.656-1.143-1.22-1.694-1.651
  c1.066,4.794,3.588,10.339,5.754,14.542c-0.101-0.064-0.199-0.131-0.303-0.192c-0.013,0.267-0.017,0.587-0.009,0.943
  c-0.061-0.019-0.121-0.041-0.183-0.062c-0.019,0.119-0.037,0.239-0.057,0.361c-0.087-0.372-0.177-0.851-0.312-1.563
  c-0.433-0.238-0.875-0.465-1.34-0.669c0.062,0.61,0.099,1.044,0.109,1.413c-0.093-0.021-0.185-0.045-0.277-0.065
  c-0.147-0.623-0.313-1.171-0.465-1.611c-0.219-0.088-0.441-0.168-0.666-0.247c0.038,0.372,0.067,0.762,0.093,1.209
  c-0.088-0.415-0.178-0.854-0.261-1.264c-0.182-0.06-0.364-0.121-0.55-0.177c0.026,0.409,0.02,0.703-0.021,1.061
  c-0.069-0.409-0.155-0.788-0.246-1.134c-0.448-0.125-0.908-0.229-1.373-0.313c0.029,0.479,0.049,0.975,0.054,1.564
  c-0.085-0.531-0.174-1.099-0.251-1.601c-0.179-0.03-0.358-0.059-0.541-0.082c0.151,0.588,0.259,1.269,0.259,2.006
  c-0.101-0.004-0.2-0.01-0.3-0.014c-0.015,0.169-0.033,0.339-0.051,0.51c-0.194-0.679-0.451-1.452-1.031-2.609
  c-0.182-0.012-0.367-0.02-0.552-0.023c0.082,0.223,0.164,0.456,0.252,0.711c-0.121-0.232-0.245-0.473-0.368-0.712
  c-0.166-0.003-0.332,0.271-0.501,0.273c0.492,1.061,0.733,2.359,0.874,2.359c-0.063,0-0.127,0-0.192,0
  c-0.543,0-1.112-1.885-1.49-2.32c-0.226,0.016-0.451-0.098-0.677-0.072c0.076,0.158,0.152,0.251,0.232,0.425
  c-0.104-0.158-0.211-0.354-0.318-0.518c-0.226,0.028-0.453,0.044-0.682,0.083c0.234,0.441,0.463,0.888,0.721,1.446
  c-0.312-0.473-0.643-0.982-0.928-1.423c-0.16,0.029-0.32,0.058-0.48,0.092c0.354,0.533,0.671,1.164,0.898,1.891
  c-0.061,0.007-0.122,0.014-0.184,0.021c-0.419-0.489-0.864-1.007-1.655-1.676c-0.19,0.054-0.384,0.116-0.576,0.179
  c0.341,0.468,0.891,1.155,1.619,1.837c-0.057-0.027-0.109-0.056-0.161-0.082c-0.029-0.049-0.06-0.103-0.089-0.147
  c-0.044,0.007-0.088,0.014-0.132,0.021c-0.438-0.262-0.863-0.619-1.839-1.428c-0.416,0.154-0.835,0.319-1.257,0.51
  c0.402,0.312,0.8,0.628,1.271,1.038c-0.498-0.314-1.034-0.683-1.488-0.977c-0.146,0.068-0.376,0.112-0.521,0.187
  c0.682,0.517,1.013,1.06,1.229,1.035v0.001c0-0.002,0.119,0.104,0.133,0.118c0.063,0.067,0.177,0.223,0.24,0.298
  c-0.798-0.526-1.53-0.886-2.049-1.098c-0.247,0.136-0.486,0.276-0.733,0.428c0.397,0.191,0.811,0.399,1.283,0.658
  c-0.487-0.177-1.002-0.37-1.465-0.542c-0.247,0.154-0.492,0.315-0.739,0.484c0.186,0.071,0.376,0.145,0.581,0.226
  c-0.221-0.051-0.45-0.105-0.678-0.16c-0.3,0.206-0.598,0.422-0.897,0.649c0.8,0.231,1.18,0.361,1.596,0.584
  c0.036,0.023,0.071,0.049,0.108,0.072c-0.18-0.037-0.353-0.07-0.522-0.098c-0.095-0.049-0.191-0.102-0.278-0.148
  c-0.056,0.027-0.113,0.057-0.168,0.085c-0.488-0.061-0.918-0.08-1.254-0.085c-0.297,0.242-0.596,0.496-0.894,0.763
  c0.215,0.066,0.393,0.126,0.553,0.186c-0.059,0.035-0.116,0.067-0.175,0.104c0.122,0.044,0.234,0.087,0.344,0.133
  c-0.381-0.063-0.736-0.102-1.052-0.125c-0.627,0.578-1.252,1.21-1.876,1.901c0,0,9.845-3.092,20.836-0.241
  c-0.394,0.05-0.789,0.109-1.187,0.191c-0.026,0.197-0.061,0.446-0.099,0.718c-0.052,0.014-0.104,0.028-0.156,0.045
  c0.007-0.233,0.018-0.467,0.033-0.713c-0.8,0.177-1.6,0.416-2.389,0.708c-0.012,0.216-0.016,0.547,0.026,0.923
  c-0.056,0.027-0.111,0.056-0.166,0.085c-0.082-0.188-0.164-0.441-0.296-0.841c-0.453,0.182-0.902,0.38-1.344,0.594
  c0.095,0.28,0.21,0.625,0.327,0.982c-0.039,0.024-0.076,0.05-0.114,0.074c-0.168-0.319-0.312-0.621-0.453-0.94
  c-0.605,0.305-1.195,0.638-1.763,0.994c0.184,0.249,0.389,0.527,0.577,0.789c-0.288-0.25-0.517-0.47-0.73-0.69
  c-0.218,0.139-0.432,0.281-0.643,0.428c0.543,0.425,0.815,0.643,1.011,0.864c-0.04,0.031-0.08,0.063-0.119,0.097
  c-0.553-0.31-1.081-0.497-1.402-0.595c-0.476,0.354-0.928,0.724-1.352,1.108c0.191,0.095,0.386,0.195,0.573,0.292
  c-0.245-0.073-0.461-0.142-0.664-0.211c-0.188,0.171-0.365,0.346-0.541,0.521c0.674,0.25,0.933,0.367,1.231,0.604
  c-0.562-0.159-1.106-0.243-1.54-0.286c-0.176,0.188-0.343,0.379-0.507,0.57c0.788,0.281,1.333,0.388,1.79,0.44
  c-0.088,0.107-0.177,0.215-0.26,0.323c-0.922,0.142-1.644,0.033-2.071-0.076c-0.274,0.376-0.521,0.756-0.732,1.14
  c0.495-0.03,1.138-0.069,1.725-0.102c-0.705,0.13-1.246,0.207-1.816,0.275c-0.211,0.404-0.384,0.813-0.514,1.22
  c0.424-0.085,0.902-0.182,1.348-0.269c-0.525,0.167-0.963,0.294-1.389,0.41c-0.066,0.224-0.124,0.446-0.162,0.67
  c0.385-0.165,0.89-0.402,1.44-0.717c-0.368,0.321-0.646,0.5-1.481,1.023c-0.019,0.164-0.032,0.327-0.035,0.488
  c0.941-0.434,1.369-0.625,1.81-0.767c-0.011,0.028-0.02,0.055-0.028,0.086c-0.746,0.403-1.363,0.804-1.771,1.087
  c0.012,0.17,0.027,0.339,0.059,0.506c0.367-0.262,0.771-0.548,1.154-0.817c-0.418,0.361-0.774,0.657-1.127,0.941
  c0.07,0.341,0.181,0.676,0.334,1.002c0.194-0.168,0.391-0.338,0.582-0.502c-0.196,0.208-0.377,0.395-0.551,0.571
  c0.052,0.106,0.111,0.211,0.173,0.313c0.225-0.248,0.484-0.561,0.746-0.913c0,0.042-0.002,0.085,0,0.127
  c-0.12,0.246-0.287,0.517-0.604,1.013c0.117,0.174,0.245,0.345,0.389,0.514c0.112-0.205,0.229-0.412,0.344-0.621
  c0.006,0.021,0.012,0.043,0.019,0.064c-0.094,0.23-0.188,0.448-0.279,0.655c0.443,0.498,1.012,0.967,1.72,1.397
  c-0.77-9.067,6.742-11.669,12.718-12.32c-0.1,0.127-0.193,0.263-0.274,0.409c-0.241,0.424-0.378,0.873-0.423,1.314
  c-0.109,0.143-0.22,0.29-0.329,0.444c-0.004,0.005-0.008,0.01-0.013,0.015c-0.113,0.162-0.229,0.331-0.345,0.506
  c-0.012,0.018-0.022,0.035-0.034,0.052c-0.793,1.208-1.583,2.719-2.337,4.609c0.201,0.021,0.408,0.042,0.606,0.063
  c-0.235,0.024-0.447,0.038-0.651,0.05c-0.056,0.145-0.113,0.289-0.17,0.437c-0.031,0.081-0.057,0.166-0.08,0.251
  c0.56-0.111,0.843-0.139,1.223-0.101c-0.511,0.154-0.964,0.351-1.328,0.536c-0.043,0.237-0.064,0.486-0.07,0.742
  c0.264-0.103,0.543-0.21,0.808-0.312c-0.297,0.182-0.557,0.33-0.808,0.467c0,0.155,0.007,0.314,0.021,0.476
  c0.389-0.152,0.867-0.341,1.311-0.51c-0.489,0.298-0.881,0.513-1.291,0.717c0.023,0.21,0.056,0.423,0.097,0.64
  c0.428-0.282,0.94-0.512,1.531-0.572c-0.398,0.336-0.787,0.616-1.365,1.286c0.064,0.237,0.138,0.48,0.221,0.724
  c0.294-0.349,0.547-0.438,0.78-0.689c-0.291,0.525-0.453,0.767-0.666,1.021c0.132,0.358,0.282,0.721,0.446,1.08
  c0.343-0.282,0.766-0.627,1.155-0.943c-0.401,0.447-0.73,0.784-1.076,1.118c0.091,0.194,0.184,0.388,0.282,0.581
  c0.439-0.382,0.683-0.559,1.027-0.73c-0.339,0.346-0.634,0.696-0.878,1.017c0.138,0.263,0.282,0.524,0.434,0.782
  c0.273-0.133,0.571-0.236,0.896-0.287c-0.222,0.203-0.438,0.389-0.686,0.641c0.176,0.289,0.358,0.574,0.547,0.854
  c0.189-0.525,0.299-0.778,0.484-1.085c-0.145,0.49-0.245,0.949-0.315,1.334c0.309,0.445,0.627,0.875,0.953,1.281
  c0.037-0.438,0.076-0.696,0.158-1.02c-0.009,0.468,0.01,0.897,0.038,1.264c0.135,0.164,0.27,0.322,0.405,0.476
  c-0.027-0.296-0.032-0.517-0.02-0.785c0.068,0.379,0.151,0.722,0.235,1.026c0.686,0.754,1.374,1.388,2.014,1.839
  c-0.111-0.161-0.23-0.337-0.354-0.521c-0.289-0.599-0.562-1.514-0.715-2.221c-0.538-0.466-0.737-0.653-0.979-0.993
  c0.32,0.237,0.625,0.42,0.889,0.558c-0.05-0.254-0.099-0.517-0.146-0.781c-0.576-0.476-0.862-0.698-1.102-1.036
  c0.356,0.265,0.719,0.472,1.03,0.625c-0.038-0.23-0.072-0.467-0.106-0.704c-0.242-0.115-0.496-0.245-0.782-0.402
  c0.249,0.079,0.511,0.166,0.762,0.248c-0.073-0.522-0.138-1.059-0.195-1.605c-0.853-0.289-1.106-0.377-1.464-0.629
  c0.568,0.144,1.076,0.172,1.42,0.17c-0.036-0.395-0.068-0.792-0.095-1.193c-0.57-0.168-1.056-0.329-1.711-0.602
  c0.602,0.121,1.271,0.263,1.697,0.356c-0.018-0.285-0.035-0.569-0.047-0.858c-0.54-0.158-1.14-0.318-1.792-0.749
  c0.473,0.043,0.885,0.129,1.738,0.135c-0.008-0.215,0.015-0.567,0.012-0.781c-0.611-0.013-0.852-0.033-1.187-0.147
  c0.494-0.152,0.92-0.361,1.18-0.508c-0.001-0.299,0.002-0.299,0.006-0.596c-0.521,0.08-0.989,0.137-1.606,0.167
  c0.002-0.003,0.002-0.008,0.003-0.012c0.57-0.154,1.196-0.502,1.611-0.601c0.005-0.211,0.003-0.211,0.01-0.419
  c-0.671,0.065-1.11,0.186-1.481,0.313c0.019-0.074,0.034-0.15,0.053-0.226c0.542-0.329,0.944-0.431,1.393-0.556
  c0.015-0.271,0.104-0.961,0.125-1.226c-0.219,0.031-0.592,0.103-1.036,0.256c0.037-0.098,0.081-0.196,0.122-0.294
  c0.227-0.135,0.526-0.3,0.962-0.546c0.034-0.37,0.074-0.732,0.119-1.093c-0.159,0.121-0.313,0.231-0.467,0.341
  c0.077-0.144,0.16-0.287,0.243-0.433c0.028-0.023,0.055-0.046,0.082-0.068c0.399,0.175,0.834,0.226,1.264,0.167
  c-0.309,1.146,0.077,2.329,1.024,2.867c0.369,0.209,0.78,0.293,1.191,0.269c0.106,0.243,0.232,0.519,0.38,0.823
  c-0.099-0.027-0.132-0.026-0.094,0.049c0.347,0.636,1.076,2.157,1.941,3.485c-0.097-0.009-0.131-0.018-0.081,0.081
  c0.22,0.415,0.584,1.042,1.099,1.734c-0.149-0.024-0.195-0.041-0.155,0.028c0.19,0.322,0.562,0.87,1.003,1.511
  c0.476,0.612,1.024,1.319,1.54,1.983c-0.186,0.029-0.281,0.001-0.193,0.102c0.203,0.235,0.5,0.53,0.832,0.893
  c-0.106,0.018-0.141,0.039-0.087,0.102c0.252,0.282,0.669,0.759,1.184,1.29c0.518,0.529,1.109,1.136,1.674,1.714
  c-0.13,0.004-0.175,0.012-0.113,0.079c0.321,0.354,0.986,1.01,1.729,1.743c-0.091,0.025-0.126,0.038-0.057,0.113
  c0.229,0.242,0.58,0.559,0.999,0.938c-0.091,0.012-0.115,0.027-0.061,0.083c0.501,0.502,1.727,1.599,2.864,2.649
  c-0.099,0.024-0.135,0.028-0.063,0.106c0.312,0.337,0.84,0.801,1.468,1.364c-0.156,0.018-0.205,0.016-0.152,0.072
  c0.494,0.54,1.899,1.781,3.085,2.915c-0.186,0.073-0.281,0.053-0.19,0.154c0.193,0.216,0.475,0.48,0.813,0.787
  c-0.123,0.039-0.161,0.067-0.1,0.124c0.371,0.337,1.103,1,1.904,1.727c-0.089,0.025-0.119,0.039-0.054,0.104
  c0.367,0.382,1.051,1.002,1.829,1.773c-0.111,0.011-0.145,0.023-0.091,0.083c0.373,0.458,1.292,1.385,2.165,2.387
  c-0.095,0.02-0.132,0.026-0.082,0.108c0.322,0.549,1.21,1.358,2.03,2.453c-0.279,0.018-0.383-0.002-0.317,0.093
  c0.215,0.317,0.656,0.854,1.131,1.498c-0.018,0.499-0.032,1.053-0.066,1.643c-0.086-0.083-0.12-0.098-0.135-0.005
  c-0.081,0.507-0.174,1.479-0.269,2.594c-0.088-0.048-0.122-0.054-0.138,0.057c-0.091,0.724-0.121,2.12-0.136,3.744
  c-0.089-0.05-0.135-0.049-0.146,0.028c-0.118,0.926-0.174,3.375-0.241,5.448c-0.124-0.067-0.175-0.093-0.19,0.034
  c-0.05,0.437-0.108,1.118-0.131,1.941c-0.101-0.063-0.131-0.063-0.141,0.027c-0.045,0.422-0.068,1.167-0.08,2.048
  c-0.27-0.457-0.495-0.842-0.65-1.131c0.154,0.453,0.379,1.013,0.645,1.647c-0.008,0.917-0.008,1.933-0.007,2.872
  c-0.051-0.019-0.084-0.007-0.117,0.067c-0.016,0.098-0.032,0.207-0.048,0.324c-0.014-0.026-0.027-0.053-0.04-0.081
  c0.012,0.042,0.023,0.082,0.035,0.123c-0.101,0.797-0.176,2.001-0.219,3.367c-0.467,0.372-0.986,0.841-1.505,1.382
  c-0.24-0.889-0.527-1.784-0.863-2.666c0.591-1.024,1.234-1.993,1.838-3.066c-0.883,0.703-1.603,1.438-2.189,2.191
  c-1.043-2.451-2.506-4.758-4.543-6.521c1.285,1.92,2.779,4.397,3.853,7.505c-0.203,0.325-0.386,0.652-0.549,0.98
  c-0.216-0.102-0.435-0.191-0.654-0.267c0.188,0.148,0.373,0.305,0.557,0.469c-0.206,0.437-0.38,0.876-0.527,1.313
  c-1-0.859-2.059-1.202-2.839-1.502c1.022,0.946,1.873,1.633,2.564,2.438c-0.316,1.239-0.448,2.461-0.521,3.613
  c-1.081-3.231-4.303-5.232-7.136-6.26c2.636,2.37,6.264,3.748,6.229,7.907c-1.782-2.787-7.202-3.44-8.526-4.88
  c1.319,2.539,7.915,2.901,7.737,6.904c0,0,0.043,0.199,1.064,0.343c1.792,0.252,2.447,0.217,2.447,0.217
  c-0.054-1.169,0.159-2.431,0.536-3.683c0.207,1.079,0.354,2.401,0.44,4.076c0.137,0.098,0.385,0.143,1.685,0.174
  c0.86,0.021,1.128-0.152,1.128-0.152c0.021-0.385,0.061-0.755,0.11-1.114c0.39,0.013,0.815,0.026,1.262,0.041
  c0.11,0.466,0.187,0.912,0.224,1.33c0.159,0.103,1.057,0.317,3.948,0.279c0,0,0.791,0.003,1.297-0.178
  c0.546,0.066,1.24,0.1,1.903,0.1c0.435,0,1.432-0.13,1.748-0.384c-0.078-0.307-0.144-0.604-0.2-0.896
  c0.373,0.006,0.751,0.005,1.124-0.006c0.001,0.26,0.002,0.521-0.001,0.781c0.306,0.162,1.372,0.153,2.297,0.081
  c0.273-0.021,1.529-0.102,1.529-0.343c0-0.004,0-0.008,0-0.012c1,0.022,0.617,0.036,1.15,0.036c1.26,0,2.039-0.134,2.263-0.259
  c-0.348-4.311,1.712-5.533,7.368-5.101c-3.771-1.227-6.779-0.45-7.925,1.225c1.282-3.027,3.917-4.541,6.857-6.128
  c-6.408,1.442-7.923,5.275-8.585,7.573c0.97-5.555,2.929-8.871,5.492-12.403c-3.097,2.148-5.411,6.635-6.761,11.379
  c0.395-4.391,2.259-8.324,4.167-10.586c-2.492,1.527-4.465,4.772-5.046,8.796c-0.038-0.078-0.077-0.152-0.122-0.219l-0.009-0.009
  c-0.04-1.044-0.109-1.918-0.306-2.434c-0.028-0.047-0.054-0.065-0.086-0.067c0.007-0.444,0.015-0.91,0.023-1.371
  c-0.02-0.462-0.05-0.919-0.082-1.343c-0.009-0.123-0.02-0.241-0.029-0.357c0.521-0.577,1.045-1.114,1.529-1.626
  c-0.545,0.145-1.086,0.462-1.608,0.886c-0.047-0.353-0.099-0.643-0.157-0.833c-0.013-0.041-0.036-0.055-0.075-0.046
  c-0.059-0.771-0.139-1.473-0.215-2.057c0.78-0.938,1.702-1.769,2.811-2.41c-1.028,0.192-2.016,0.73-2.928,1.557
  c-0.013-0.082-0.023-0.161-0.033-0.23c-0.018-0.101-0.062-0.085-0.171-0.026c-0.099-1.342-0.402-2.606-0.51-3.163
  c-0.018-0.078-0.073-0.05-0.188,0.047c-0.231-1.349-0.547-2.486-0.729-3.104c-0.03-0.099-0.067-0.074-0.156,0
  c-0.102-0.429-0.207-0.875-0.313-1.316c-0.118-0.438-0.265-0.863-0.39-1.262c-0.257-0.796-0.489-1.474-0.64-1.84
  c-0.029-0.07-0.056-0.056-0.117,0.024c-0.104-0.312-0.205-0.576-0.297-0.777c-0.035-0.063-0.06-0.025-0.129,0.056
  c-0.29-0.793-0.56-1.653-0.852-2.378c-0.307-0.717-0.577-1.323-0.759-1.65c-0.038-0.071-0.063-0.018-0.163,0.147
  c-0.312-0.815-0.628-1.468-0.883-1.867c-0.058-0.093-0.08-0.053-0.154,0.05c-0.395-0.845-0.84-1.696-1.215-2.385
  c0.104-0.704,0.191-1.298,0.244-1.659c0.009-0.093-0.041-0.056-0.22,0.04c0.203-0.99,0.383-1.813,0.438-2.358
  c0.013-0.126-0.03-0.104-0.153-0.059c0.357-1.816,0.742-3.732,0.969-4.539c0.023-0.104-0.031-0.09-0.182-0.022
  c0.172-0.666,0.313-1.227,0.373-1.634c0.023-0.143-0.036-0.096-0.203-0.047c0.469-1.999,1.042-4.368,1.285-5.255
  c0.02-0.092-0.027-0.06-0.194,0.021c0.299-0.962,0.56-1.76,0.675-2.295c0.026-0.125-0.013-0.105-0.124-0.071
  c0.271-0.879,0.551-1.787,0.793-2.574c0.29-0.772,0.533-1.426,0.68-1.815c0.032-0.092-0.008-0.091-0.123-0.048
  c0.25-0.633,0.46-1.161,0.58-1.541c0.036-0.116-0.005-0.108-0.112-0.087c0.423-1.153,0.798-2.179,0.99-2.706
  c0.043-0.109-0.018-0.082-0.192-0.011c0.609-1.367,1.18-2.503,1.389-3.181c0.034-0.111,0-0.104-0.09-0.08
  c0.327-0.741,0.651-1.476,0.931-2.105c0.317-0.613,0.58-1.127,0.733-1.452c0.041-0.088,0.009-0.086-0.097-0.047
  c0.619-1.176,1.174-2.147,1.461-2.722c0.05-0.099,0.021-0.101-0.063-0.086c0.156-0.285,0.31-0.564,0.455-0.832
  c0.162-0.257,0.317-0.503,0.462-0.729c0.287-0.455,0.525-0.835,0.688-1.093c0.049-0.079,0.017-0.096-0.082-0.088
  c0.295-0.465,0.545-0.857,0.718-1.153c0.06-0.101,0.024-0.101-0.07-0.092c0.982-1.833,2.178-3.266,2.676-4.052
  c0.064-0.102,0.027-0.104-0.076-0.104c0.668-0.956,1.264-1.809,1.581-2.299c-0.026,0.253-0.056,0.501-0.08,0.755
  c0.289-0.232,0.592-0.474,0.883-0.702c-0.331,0.354-0.62,0.649-0.905,0.926c-0.033,0.37-0.067,0.74-0.1,1.112
  c0.249-0.177,0.534-0.402,0.83-0.68c-0.21,0.352-0.389,0.563-0.862,1.067c-0.018,0.219-0.035,0.438-0.05,0.655
  c0.213-0.246,0.452-0.547,0.688-0.895c-0.15,0.439-0.302,0.714-0.728,1.429c-0.024,0.346-0.047,0.69-0.068,1.034
  c0.063-0.061,0.128-0.123,0.19-0.184c-0.067,0.088-0.132,0.17-0.194,0.25c-0.102,1.591-0.178,3.174-0.228,4.72
  c0.215-0.293,0.615-0.934,1.136-1.827c-0.089-0.29-0.182-0.597-0.277-0.94c0.117,0.265,0.237,0.541,0.354,0.813
  c0.143-0.246,0.293-0.508,0.452-0.787c-0.239-0.706-0.325-1.002-0.382-1.441c0.181,0.445,0.375,0.831,0.551,1.144
  c0.065-0.111,0.131-0.23,0.198-0.35c-0.258-0.516-0.496-1.026-0.771-1.682c0.298,0.494,0.616,1.028,0.884,1.481
  c0.133-0.233,0.268-0.478,0.407-0.728c-0.106-0.187-0.215-0.383-0.33-0.597c0.124,0.167,0.251,0.338,0.378,0.51
  c0.101-0.182,0.201-0.363,0.303-0.549c-0.41-0.825-0.482-1.043-0.64-1.614c0.526,0.884,0.71,0.901,0.917,1.107
  c0.118-0.217,0.239-0.436,0.359-0.659c-0.762-1.073-1.11-1.541-1.322-2.051c0.58,0.72,1.256,1.321,1.572,1.588
  c0.063-0.119,0.132-0.244,0.195-0.364c-0.231-0.243-0.471-0.501-0.735-0.804c0.265,0.219,0.541,0.45,0.806,0.674
  c0.102-0.19,0.202-0.38,0.306-0.574c-0.405-0.4-0.79-0.801-1.257-1.329c0.461,0.381,0.955,0.798,1.358,1.139
  c0.119-0.224,0.237-0.447,0.358-0.676c-0.843-0.891-1.064-1.065-1.609-1.396c0.89,0.198,1.502,0.552,1.891,0.858
  c0.11-0.208,0.218-0.422,0.327-0.632c-0.278-0.176-0.565-0.368-0.89-0.593c0.315,0.149,0.646,0.31,0.959,0.461
  c0.066-0.129,0.133-0.255,0.199-0.385c-1.088-1.091-1.651-1.354-2.24-1.71c1.153-0.057,2.081,0.474,2.646,0.916
  c0.119-0.229,0.232-0.461,0.348-0.688c-0.355-0.22-0.717-0.455-1.138-0.748c0.403,0.191,0.831,0.398,1.217,0.586
  c0.159-0.312,0.316-0.626,0.473-0.939c-0.792-0.448-1.397-0.804-2.236-1.39c0.927,0.441,1.986,0.963,2.358,1.146
  c0.053-0.106,0.108-0.214,0.16-0.32c-1.278-1.167-1.969-1.514-2.605-1.826c0.054-0.063,0.103-0.12,0.149-0.175
  c1.329-0.019,2.383,0.644,2.915,1.064c0.125-0.253,0.247-0.506,0.367-0.759c-0.842-0.646-1.365-0.858-1.909-1.136
  c0.921-0.117,1.735-0.009,2.373,0.155c0.155-0.329,0.306-0.653,0.453-0.974c-0.467-0.25-0.918-0.511-1.471-0.862
  c0.524,0.222,1.095,0.468,1.56,0.67c0.063-0.143,0.125-0.282,0.189-0.425c-0.683-0.313-1.232-0.573-1.895-0.938
  c0.02-0.021,0.039-0.043,0.059-0.064c0.84,0.319,1.668,0.641,1.951,0.75c0.051-0.115,0.105-0.234,0.157-0.351
  c0.091,0.528,0.373,1.057,0.834,1.466c0.973,0.863,2.361,0.884,3.106,0.046c0.706-0.794,0.575-2.076-0.271-2.941
  c0.445-0.066,0.853-0.267,1.157-0.61c0.099-0.11,0.181-0.229,0.247-0.354c0.052,0.057,0.105,0.108,0.156,0.167
  c0.383-0.665,0.856-1.485,1.302-2.238c-0.384,0.964-0.718,1.707-1.09,2.473c0.345,0.384,0.683,0.784,1.017,1.21
  c0.338-0.424,0.727-0.963,1.093-1.593c-0.135,0.601-0.308,0.995-0.8,1.98c0.134,0.178,0.268,0.355,0.396,0.538
  c0.197-0.278,0.396-0.586,0.592-0.918c-0.091,0.403-0.2,0.715-0.41,1.175c0.359,0.514,0.706,1.055,1.048,1.62
  c0.345-0.696,0.78-1.572,1.187-2.376c-0.342,1.056-0.646,1.852-0.994,2.698c0.361,0.617,0.714,1.266,1.053,1.95
  c0.143-0.633,0.305-1.337,0.463-1.998c-0.073,0.922-0.159,1.663-0.266,2.406c0.16,0.333,0.318,0.672,0.472,1.019
  c0.13-0.475,0.255-1.033,0.339-1.653c0.115,0.662,0.094,1.11,0,2.442c0.294,0.711,0.573,1.454,0.839,2.228
  c0.09-0.374,0.172-0.785,0.231-1.227c0.103,0.585,0.098,1.005,0.029,2.012c0.104,0.316,0.203,0.639,0.299,0.967
  c0.078-0.331,0.155-0.665,0.232-0.987c-0.038,0.49-0.08,0.927-0.129,1.345c0.693,2.386,1.258,5.035,1.652,7.985
  c0,0,0.311-2.58,0.36-6.172c-0.188-0.391-0.295-0.678-0.39-1.044c0.129,0.207,0.263,0.405,0.395,0.589
  c0.001-0.223,0.002-0.449,0.002-0.68c-0.4-0.581-0.586-0.91-0.769-1.375c0.257,0.318,0.515,0.604,0.763,0.861
  c-0.012-1.226-0.06-2.528-0.159-3.862c-0.246-0.161-0.499-0.33-0.771-0.519c0.248,0.122,0.504,0.246,0.757,0.371
  c-0.03-0.409-0.065-0.815-0.106-1.229c-0.46-0.244-0.737-0.428-1.062-0.698c0.354,0.148,0.7,0.272,1.024,0.379
  c-0.062-0.554-0.137-1.106-0.219-1.661c-0.425-0.146-0.868-0.308-1.363-0.498c0.437,0.098,0.893,0.201,1.333,0.304
  c-0.029-0.194-0.057-0.387-0.09-0.579c-0.815-0.315-1.423-0.455-2.051-0.636c0.672-0.137,1.314-0.168,1.912-0.137
  c-0.082-0.425-0.172-0.845-0.27-1.262c-1.301-0.424-1.731-0.576-2.315-0.934c0.83,0.216,1.591,0.323,2.18,0.379
  c-0.155-0.606-0.335-1.194-0.528-1.773c-0.374,0.007-0.767,0.009-1.2,0.005c0.377-0.052,0.765-0.104,1.148-0.153
  c-0.061-0.181-0.119-0.366-0.184-0.542c-0.819,0.091-1.223,0.105-1.763,0.045c0.606-0.111,1.155-0.256,1.624-0.401
  c-0.093-0.233-0.191-0.463-0.292-0.689c-0.511,0.104-1.049,0.208-1.669,0.309c0.52-0.168,1.066-0.34,1.583-0.503
  c-0.272-0.596-0.575-1.164-0.909-1.695c-0.662,0.195-1.338,0.374-2.168,0.564c0.672-0.264,1.391-0.543,2.025-0.787
  c-0.135-0.209-0.278-0.409-0.426-0.606c-0.522,0.146-1.072,0.285-1.712,0.434c0.516-0.204,1.061-0.414,1.573-0.612
  c-0.256-0.328-0.532-0.627-0.819-0.914c0.176,0.012,0.354,0.021,0.528,0.034c0.186-0.569,0.297-1.045,0.431-1.531
  c0.12,0.549,0.165,1.079,0.163,1.586c0.207,0.019,0.412,0.044,0.618,0.067c0.093-0.677,0.198-1.323,0.371-2.142
  c-0.041,0.737-0.092,1.539-0.132,2.169c0.317,0.038,0.632,0.083,0.942,0.131c0.236-0.555,0.526-1.341,0.739-2.262
  c0.043,0.657-0.017,1.104-0.224,2.353c0.339,0.063,0.671,0.132,0.997,0.212c0.327-0.499,0.735-1.191,1.099-2.017
  c-0.081,0.678-0.258,1.125-0.341,2.221c0.093,0.028,0.186,0.055,0.276,0.082c0.316,0.102,0.618,0.2,0.914,0.298
  c0.157-0.408,0.314-0.871,0.451-1.379c0.017,0.478-0.021,0.844-0.136,1.484c0.455,0.153,0.879,0.309,1.279,0.468
  c0.195-0.493,0.406-0.989,0.68-1.579c-0.16,0.551-0.334,1.138-0.488,1.657c0.164,0.066,0.326,0.138,0.48,0.209
  c0.38-0.449,0.851-1.063,1.294-1.81c-0.151,0.611-0.335,1.01-0.854,2.024c0.311,0.159,0.598,0.33,0.86,0.518
  c0.22-0.388,0.446-0.834,0.655-1.33c-0.054,0.499-0.145,0.87-0.372,1.546c0.267,0.214,0.506,0.449,0.721,0.71
  c0.182-0.358,0.381-0.73,0.618-1.15c-0.162,0.43-0.337,0.883-0.5,1.306c0.313,0.418,0.564,0.901,0.747,1.473
  c0.201-0.214,0.408-0.449,0.614-0.703c-0.145,0.355-0.29,0.63-0.527,0.999c0.061,0.229,0.112,0.473,0.156,0.731
  c0.102-0.108,0.204-0.22,0.308-0.338c-0.088,0.193-0.175,0.362-0.278,0.534c0.014,0.089,0.025,0.18,0.035,0.271
  c0,0,0.272-0.081,0.659-0.264c0.027-0.17,0.054-0.346,0.077-0.528c0.028,0.16,0.048,0.31,0.061,0.461
  c0.054-0.026,0.109-0.055,0.166-0.084c-0.018-0.374-0.051-0.78-0.11-1.207c0.158,0.345,0.262,0.637,0.369,1.059
  c0.099-0.061,0.2-0.123,0.302-0.193c-0.059-0.292-0.134-0.598-0.224-0.914c0.145,0.293,0.246,0.532,0.356,0.822
  c0.236-0.171,0.471-0.371,0.687-0.6c-0.29-0.366-0.644-0.771-1.057-1.174c0.444,0.222,0.751,0.438,1.298,0.896
  c0.339-0.431,0.604-0.952,0.709-1.577c-0.515-0.266-1.215-0.583-2.041-0.84c0.612-0.004,1.03,0.075,2.088,0.33
  C124.825,191.622,124.779,191.227,124.665,190.804z M72.49,179.759c0.039-0.019,0.172-0.077,0.387-0.16
  C72.747,179.654,72.619,179.701,72.49,179.759z M92.386,179.486c0.013-0.485,0-0.93-0.024-1.338
  c0.077,0.103,0.154,0.205,0.231,0.308C92.542,178.826,92.481,179.134,92.386,179.486z M93.49,180.398
  c-0.032-0.022-0.068-0.044-0.102-0.067c0.061-0.186,0.12-0.366,0.181-0.543c0.042,0.057,0.083,0.114,0.123,0.172
  C93.626,180.104,93.559,180.25,93.49,180.398z M94.205,180.886c0.021-0.045,0.042-0.084,0.062-0.127
  c0.004,0.006,0.009,0.012,0.013,0.019C94.255,180.813,94.228,180.851,94.205,180.886z M95.023,189.657
  c-0.052-0.701-0.088-1.389-0.1-2.254c0.072,0.509,0.146,1.046,0.216,1.546c-0.056,0.073-0.108,0.141-0.148,0.2
  c0.055-0.008,0.114-0.016,0.173-0.021c0.021,0.154,0.043,0.307,0.062,0.449C95.161,189.603,95.092,189.629,95.023,189.657z
   M48.199,205.476c-0.079,0.554-0.161,1.021-0.253,1.472c-0.05-0.063-0.1-0.122-0.151-0.182
  C47.925,206.345,48.064,205.9,48.199,205.476z M31.638,201.087c0.002-0.085,0.002-0.168,0.003-0.25
  c0.033,0.13,0.062,0.262,0.085,0.403C31.698,201.187,31.667,201.138,31.638,201.087z M38.103,206.629
  c-0.026,0.095-0.054,0.19-0.081,0.284c0.027-0.242,0.057-0.461,0.087-0.67C38.108,206.361,38.105,206.491,38.103,206.629z
   M38.253,206.092c-0.041,0.005-0.081,0.009-0.123,0.015c0.023-0.146,0.048-0.29,0.075-0.434c0.051,0.014,0.101,0.028,0.151,0.041
  C38.324,205.836,38.29,205.962,38.253,206.092z M39.195,205.999c-0.14,0.007-0.278,0.018-0.417,0.03
  c0.012-0.066,0.021-0.129,0.029-0.187c0.134,0.039,0.27,0.077,0.403,0.12C39.206,205.976,39.201,205.987,39.195,205.999z
   M40.444,202.879c-0.032-0.097-0.063-0.198-0.091-0.306c0.036,0.068,0.073,0.138,0.109,0.207
  C40.457,202.813,40.451,202.847,40.444,202.879z M40.588,198.504c0-0.004,0.004-0.01,0.007-0.015
  c-0.004,0.007-0.007,0.014-0.007,0.021C40.588,198.508,40.588,198.506,40.588,198.504z M46.559,212.452
  c-0.092,0.054-0.181,0.106-0.269,0.156c-0.005-0.011-0.011-0.021-0.015-0.034C46.371,212.534,46.466,212.492,46.559,212.452z
   M45.602,211.076c-0.236,0.123-0.453,0.232-0.661,0.336c-0.049-0.014-0.097-0.023-0.146-0.034
  C45.064,211.277,45.337,211.175,45.602,211.076z M44.258,210.838c0.002-0.018,0.004-0.034,0.006-0.051
  c0.044-0.006,0.087-0.014,0.131-0.02C44.349,210.792,44.303,210.815,44.258,210.838z M45.208,209.446
  c-0.301,0.125-0.599,0.236-0.956,0.406c-0.02-0.117-0.046-0.23-0.08-0.343C44.498,209.457,44.842,209.429,45.208,209.446z
   M45.846,215.597c0.075-0.098,0.147-0.198,0.213-0.308c0.475-0.271,1.076-0.532,1.798-0.667c-0.561,0.308-0.971,0.48-1.912,1.277
  c-0.006-0.021-0.013-0.044-0.02-0.063C45.898,215.752,45.872,215.674,45.846,215.597z M52.458,226.344
  c-0.063-0.077-0.078-0.044-0.118,0.046c-0.6-0.808-1.127-1.533-1.389-1.911c-0.063-0.084-0.069-0.01-0.118,0.185
  c-0.334-0.518-0.647-1.001-0.914-1.412c-0.254-0.419-0.458-0.771-0.64-1c-0.06-0.075-0.074-0.046-0.106,0.029
  c-0.71-1.123-1.26-2.296-1.557-2.801c-0.04-0.068-0.057-0.046-0.088,0.046c-0.508-0.958-0.773-1.839-1.007-2.306
  c-0.038-0.081-0.056-0.063-0.097,0.003c-0.2-0.454-0.349-0.903-0.473-1.301c0.1,0.134,0.2,0.266,0.3,0.396
  c0.521-0.43,1.448-1.029,2.683-1.187c-0.585,0.376-1.155,0.672-2.234,1.763c0.083,0.104,0.163,0.203,0.244,0.304
  c0.423-0.287,0.949-0.645,1.437-0.971c-0.495,0.437-0.899,0.769-1.33,1.104c0.179,0.22,0.354,0.436,0.525,0.641
  c0.967-0.599,1.217-0.759,1.667-0.916c-0.716,0.502-1.21,1.019-1.401,1.233c0.261,0.305,0.507,0.587,0.732,0.84
  c0.555-0.338,0.867-0.609,1.217-0.878c-0.229,0.501-0.548,0.913-0.882,1.246c0.134,0.146,0.259,0.278,0.373,0.398
  c0.27-0.421,0.643-0.994,0.987-1.515c-0.293,0.652-0.543,1.132-0.84,1.663c0.127,0.128,0.236,0.235,0.323,0.314
  c0.172,0.157,0.344,0.313,0.514,0.472c0.207-0.361,0.445-0.773,0.672-1.159c-0.188,0.492-0.358,0.889-0.543,1.283
  c0.133,0.129,0.265,0.257,0.394,0.394c0.465-0.852,0.574-1.327,0.734-1.829c0.06,0.91-0.158,1.699-0.373,2.235
  c0.146,0.177,0.29,0.366,0.428,0.571c0.143-0.304,0.311-0.716,0.438-1.19c0.028,0.406-0.024,0.668-0.201,1.57
  c0.146,0.248,0.282,0.52,0.412,0.819c0.096-0.181,0.194-0.362,0.29-0.538c-0.082,0.24-0.161,0.455-0.239,0.659
  c0.373,0.902,0.675,2.063,0.873,3.623C52.869,226.89,52.635,226.569,52.458,226.344z M66.493,276.602
  c0.042,0.143,0.083,0.287,0.123,0.433c-0.083-0.057-0.167-0.111-0.251-0.165C66.407,276.779,66.45,276.689,66.493,276.602z
   M65.041,282.839c0.027-1.125,0.146-2.11,0.332-2.996c0.186,0.306,0.355,0.64,0.51,1.013
  C65.526,281.491,65.23,282.156,65.041,282.839z M66.481,279.897c-0.252-0.472-0.524-0.873-0.811-1.218
  c0.148-0.487,0.317-0.945,0.504-1.381c0.239,0.244,0.475,0.501,0.704,0.768c0.077,0.329,0.148,0.665,0.214,1.007
  C66.88,279.338,66.676,279.613,66.481,279.897z M67.106,281.373c0.082-0.199,0.168-0.397,0.257-0.594
  c0.093,0.749,0.158,1.523,0.188,2.325C67.427,282.463,67.277,281.89,67.106,281.373z M74.303,272.618
  c0.164,0.08,0.326,0.16,0.481,0.244c-0.154,0.345-0.302,0.702-0.439,1.071c-0.005-0.011-0.009-0.021-0.014-0.031
  C74.335,273.403,74.33,272.962,74.303,272.618z M78.994,278.118C78.994,278.119,78.994,278.119,78.994,278.118
  c0,0.04-0.198,0.087-0.216,0.136c-0.261,0.294-0.598,0.609-0.847,0.947c0.089-1.225,0.154-2.449,0.271-3.656
  c0.203,0.241,0.372,0.488,0.55,0.741c0.009,0.471,0.174,1.014,0.166,1.596C78.893,277.961,78.994,278.039,78.994,278.118z
   M74.289,277.14c0.054-0.289,0.112-0.576,0.174-0.862c0.063,0.211,0.125,0.425,0.185,0.641
  C74.529,276.989,74.41,277.062,74.289,277.14z M75.047,280.551l-0.192-0.415c-0.028-1.033-0.259-1.667-0.445-2.208
  c0.133-0.132,0.263-0.264,0.399-0.396c0.301,1.193,0.539,2.471,0.691,3.829C75.333,281.037,75.176,280.762,75.047,280.551z
   M77.7,281.61c0.018-0.33,0.036-0.662,0.057-0.994c0.14-0.282,0.293-0.559,0.456-0.828c-0.099,0.412-0.188,0.834-0.271,1.262
  C77.864,281.219,77.783,281.407,77.7,281.61z M78.556,272.467c0.061,0.386,0.125,0.799,0.19,1.22
  c0.044,0.422,0.089,0.851,0.131,1.261c-0.053-0.011-0.084,0.001-0.11,0.063c-0.007,0.045-0.011,0.098-0.015,0.147
  c-0.164-0.174-0.33-0.342-0.501-0.5c0.085-0.827,0.175-1.642,0.269-2.438C78.532,272.301,78.543,272.383,78.556,272.467z
   M78.325,268.682c-0.028,0.118-0.057,0.239-0.085,0.362c-0.003,0.006-0.005,0.015-0.005,0.024c-0.317,1.381-0.612,2.979-0.878,4.858
  c-0.591-0.424-1.222-0.767-1.892-1.015c0.669-1.664,1.588-3.137,2.853-4.259C78.32,268.662,78.322,268.672,78.325,268.682z
   M77.263,274.606c-0.256,1.921-0.481,4.121-0.672,6.658c-0.061,0.121-0.12,0.242-0.177,0.365c-0.299-1.676-0.632-3.235-1.025-4.651
  c0.312-0.293,0.633-0.588,0.961-0.886c-0.311,0.109-0.677,0.269-1.078,0.477c-0.157-0.541-0.326-1.06-0.504-1.555
  c0.17-0.626,0.368-1.236,0.593-1.825C76.079,273.619,76.708,274.095,77.263,274.606z M68.564,280.328
  c0.062,0.095,0.12,0.191,0.181,0.288c-0.023,0.107-0.046,0.222-0.067,0.34C68.64,280.747,68.605,280.539,68.564,280.328z
   M69.314,277.33c-0.097,0.521-0.16,1.255-0.203,2.072c-0.208-0.258-0.422-0.511-0.644-0.751
  C68.745,278.182,69.03,277.739,69.314,277.33z M75.196,258.534c0.221,0.407,0.46,0.892,0.719,1.419
  c-0.107-0.02-0.145-0.016-0.121,0.056c0.108,0.391,0.459,1.08,0.73,1.92c-0.097,0.001-0.131,0.012-0.107,0.097
  c0.076,0.275,0.238,0.66,0.438,1.127c0.202,0.465,0.452,1.011,0.628,1.629c-0.098-0.008-0.141,0.008-0.131,0.069
  c0.063,0.364,0.24,1.007,0.462,1.756c0.19,0.602,0.331,1.292,0.483,1.957c-1.471,0.806-2.59,2.283-3.447,4.15
  c-0.181-0.05-0.363-0.093-0.549-0.131c-0.013-0.152-0.028-0.285-0.05-0.394c-0.009-0.049-0.031-0.066-0.069-0.062
  c0.041-1.568,0.104-2.906,0.099-3.653c0-0.117-0.047-0.108-0.163-0.064c0.054-1.558,0.21-3.036,0.214-3.692
  c0.004-0.092-0.057-0.076-0.193-0.001c0.093-1.58,0.193-2.937,0.215-3.681c0.003-0.119-0.04-0.104-0.148-0.057
  c0.093-1.37,0.261-2.881,0.372-4.007c0.083,0.149,0.164,0.302,0.244,0.456c-0.109,0.002-0.151,0.005-0.123,0.095
  C74.785,257.79,74.965,258.131,75.196,258.534z M105.913,183.129c0.009,0.549,0.005,1.105-0.024,1.78
  c-0.058-0.523-0.114-1.076-0.166-1.581C105.787,183.262,105.85,183.195,105.913,183.129z M97.707,184.732
  c0.015,0.086,0.032,0.17,0.047,0.254c-0.03-0.073-0.059-0.147-0.093-0.22C97.675,184.754,97.692,184.743,97.707,184.732z
   M97.66,184.411c-0.011-0.014-0.019-0.027-0.027-0.04c-0.005-0.093-0.008-0.185-0.015-0.277
  C97.632,184.199,97.644,184.307,97.66,184.411z M93.398,197.73c-0.014,0.019-0.027,0.036-0.042,0.055
  c-0.006-0.029-0.011-0.053-0.017-0.069C93.358,197.721,93.378,197.726,93.398,197.73z M90.998,204.463
  c0.071-0.094,0.14-0.185,0.21-0.275c-0.009,0.08-0.018,0.159-0.025,0.238C91.141,204.432,91.08,204.446,90.998,204.463z
   M100.114,195.805c-0.537-0.541-1.016-0.864-1.447-1.081c0.014-0.015,0.027-0.031,0.041-0.045c0.078-0.081,0.152-0.157,0.225-0.231
  c0.488,0.176,0.905,0.396,1.208,0.581C100.082,195.278,100.074,195.541,100.114,195.805z M100.6,194.177
  c-0.349-0.27-0.68-0.463-0.989-0.609c0.078-0.083,0.155-0.164,0.228-0.241c0.338,0.153,0.688,0.242,1.029,0.277
  c0.062,0.034,0.117,0.066,0.163,0.095c0.012-0.028,0.021-0.057,0.033-0.084c0.169,0.005,0.335-0.004,0.497-0.025
  c0.02,0.021,0.042,0.04,0.062,0.062C101.234,193.714,100.878,193.891,100.6,194.177z M105.022,191.212
  c-0.011-0.019-0.022-0.037-0.037-0.055c0.056-0.107,0.11-0.216,0.164-0.32C105.106,190.967,105.064,191.09,105.022,191.212z
   M107.178,187.388c-0.043-0.022-0.089-0.042-0.132-0.062c0.041-0.16,0.089-0.315,0.146-0.482
  C107.186,187.029,107.181,187.21,107.178,187.388z M108.897,188.764c-0.396,0.324-0.815,0.657-1.312,1.029
  c0.421-0.43,0.866-0.881,1.281-1.296c-0.089-0.076-0.182-0.146-0.276-0.218c0.157-0.461,0.316-0.995,0.441-1.592
  C109.084,187.291,109.045,187.725,108.897,188.764z"/>
<path fill="none"  stroke-miterlimit="10" d="M82,235c0,0,33.25-1,48.75,3.25c0,0-39,0-45.625,2.75"/>
<path fill="none"  stroke-miterlimit="10" d="M85.75,242.25c0,0,72.25-6.75,113.25,0.75"/>
<path fill="none"  stroke-miterlimit="10" d="M156.25,230.5c0,0,35.5,4,54.75,13.75"/>
<line fill="none"  stroke-miterlimit="10" x1="104.25" y1="271" x2="114.5" y2="257.25"/>
<path fill="none"  stroke-miterlimit="10" d="M83,231.75l-5.5-8c0,0-2.5-5.75,4.75-6.5l42-1
  c0,0,21.25,8.75,29.75,13.5"/>
<path fill="none"  stroke-miterlimit="10" d="M107.5,229.75l-21.5-8c0,0-2-2.75,4.5-3.5
  c0.75,0,33.25-0.75,33.25-0.75s20,9.75,25.5,12.25"/>
<path fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" d="M353.285,286.22
  l2.805,3.935l21.44,1.749c0,0,22.89-8.015,47.08-3.644l31.773-1.996c0,0-14.277-5.514-29.142-6.514h-60.331l-16.613,1.705
  L353.285,286.22z"/>
<path fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" d="M370.846,299.189
  c0,0,17.469-16.321,53.765-10.93"/>
<path fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" d="M343.632,309.146
  l133.157-1.066c0,0,31.771-1.312,37.308-2.331l-26.668,25.502c0,0-128.096-1.021-148.353-3.935l-9.326-0.874
  c0,0,0.291-9.327,0.729-9.182c0.438,0.146,7.433,0.729,7.433,0.729s17.195-27.834,17.342-28.417"/>
<path fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" d="M336.891,321.777
  c0,0,83.648,2.478,117.457-0.874c0,0,41.97-4.518,46.05-5.684"/>
<path fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" d="M348.111,309.028
  c0,0,2.915-3.278,3.498-3.278s11.221,0,11.221,0l1.604-1.969c0,0,46.195-1.348,53.336-4.408c0,0-28.417-4.245-46.633-3.662
  c0,0,31.77-0.41,54.211,1.484"/>
<path fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" d="M514.097,305.748
  c0,0-25.795-12.387-56.979-9.765l-30.749,0.875"/>
<path fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" d="M418.353,299.772
  c0,0,50.859-0.583,67.326,4.372"/>
<line fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" x1="397.368" y1="297.149" x2="393.725" y2="302.25"/>
<line fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" x1="382.941" y1="296.275" x2="378.132" y2="303.124"/>
<line fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" x1="427.825" y1="288.114" x2="437.589" y2="296.421"/>
<ellipse fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" cx="473.292" cy="311.868" rx="7.287" ry="1.312"/>
<ellipse fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" cx="447.207" cy="312.451" rx="3.935" ry="1.312"/>
<ellipse fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" cx="428.408" cy="313.033" rx="3.789" ry="1.313"/>
<ellipse fill="none"  stroke-width="2" stroke-linejoin="round" stroke-miterlimit="10" cx="419.591" cy="313.033" rx="3.133" ry="1.313"/>
    </symbol>
    <!-- End cities -->
  </defs>
</svg>