<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/jquery.fancybox.min.css">
  <link rel="stylesheet" href="/css/main.css">
  <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
  <title>Мой район Онлайн</title>
</head>
<body>
  
  <?php // Подключение svg иконок, путь при программированиии надо заменить
  include '../include/svg.php';?>
<div class="news-wrapper">
  <header class="header" style="background-color: #ffeacb;">
    <div class="header__box">
      <div class="header__logo header__el">
        <svg class="svg-icon svg-icon--full-size">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
        </svg>
      </div>

      <div class="header__search header__el">
        <button class="header__search-btn" id="header-loupe">
          <svg class="svg-icon svg-icon--full-size">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
          </svg>
        </button>
        <div class="header__form-container">
          <form class="header__form" action="/search/">
            <input type="text" name="q" class="header__search-input" placeholder="... Поиск" autocomplete="off">
          </form>
          <div class="header__close-search"></div>
        </div>
      </div>

      <!-- search mobile -->
      <div class="search-mobile header__el">
        <div class="search-mobile__form-container">
          <button class="search-mobile__btn">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
            </svg>
          </button>

          <form class="search-mobile__form" action="/search/">
            <input type="text" name="q" class="search-mobile__input" placeholder="... Выбери город и район" autocomplete="off">
          </form>
        </div>
        <div class="search-mobile__close"></div>
      </div>
      <!-- end search mobile -->

      <ul class="search__suggest">
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
      </ul>

      <div class="breadcrumbs breadcrumbs--two-el header__el">
        <div class="breadcrumbs__el">
          <div class="breadcrumbs__text">Краснодар</div>
          <div class="breadcrumbs__dropdown-box">
            <div class="breadcrumbs__dropdown">
              <nav class="breadcrumbs__nav">
                <a href="#">Краснодар</a>
                <a href="#">Сочи</a>
                <a href="#">Ростов-на-Дону</a>
                <a href="#">Новороссийск</a>
                <a href="#">Горячий ключ</a>
              </nav>
            </div>
          </div>
        </div>

        <div class="breadcrumbs__el">
          <div class="breadcrumbs__text">Комсомольский</div>
          <div class="breadcrumbs__dropdown-box">
            <div class="breadcrumbs__dropdown"> 
              <nav class="breadcrumbs__nav">
                <a href="#">Комсомольский</a>
                <a href="#">Пашковка</a>
                <a href="#">Фестивальный</a>
                <a href="#">Яблоновский</a>
                <a href="#">Российская</a>
                <a href="#">Московский</a>
                <a href="#">Восточка</a>
                <a href="#">Черемушки</a>
                <a href="#">40 лет Победы</a>
                <a href="#">Елизаветинская</a>
                <a href="#">Витаминкомбинат</a>
                <a href="#">хутор Ленина</a>
                <a href="#">Красная Площадь</a>
                <a href="#">Юбилейный</a>
                <a href="#">Музыкальный</a>
                <a href="#">Микрохирургия</a>
              </nav>
            </div>
          </div>
        </div>
      </div>

      <div id="burger" class="burger burger--header">
        <span></span>
        <span></span>
        <span></span>
      </div>

      <div class="burger-menu">
        <div class="burger-menu__header">
          <div class="burger-menu__logo">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
            </svg>
          </div>
        </div>
        <div class="burger-menu__body">
          <ul class="burger-menu__menu">
            <li><a href="/new/">Отправь новость</a></li>
            <li><a href="#">Вступай в группы</a></li>
            <li><a href="#">О пректе</a></li>
            <li><a href="#">Работа у нас</a></li>
            <li><a href="#">Реклама на сайте</a></li>
            <li><a href="#">Статистика</a></li>
            <li><a href="#">О нас</a></li>
          </ul>
        </div>
      </div>

    </div>
  </header>

  <main class="main">
    <div class="col--half col--fixed">
    </div>
    <div class="col--half col--right">
      <div class="news">
        <div class="news__el">
          <div class="news__header">
            <div class="news__logo">
              <svg class="svg-icon svg-icon--full-size">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
              </svg>
            </div>
            <div class="news__heading">
              <div class="news__title">
                <a href="#">
                  <span>Первая часть заголовка</span> <span class="news__title--second">| Вторая часть заголовка</span>
                </a>
              </div>
              <div class="news__date">25 апр в 14:37</div>
            </div>
          </div>

          <div class="news__prev">
            <div class="news__text">
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас 
            </div>
            <a href="#">www.примерссылки.ru</a>
          </div>
          
          <div class="news__img-block"> 
            <div class="news__main-img">
              <a href="/img/distr-placeholder.jpg" class="news__main-img-el" data-fancybox="gallery100">
                <div class="news__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
              </a>
            </div>
          </div>

          <div class="news__counters">
            <div class="news__views">999</div>
          </div>
          
        </div>

        <div class="news__el">
          <div class="news__header">
            <div class="news__logo">
              <svg class="svg-icon svg-icon--full-size">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
              </svg>
            </div>
            <div class="news__heading">
              <div class="news__title">
                <a href="#">
                  <span>Первая часть заголовка</span> <span class="news__title--second">| Вторая часть заголовка</span>
                </a>
              </div>
              <div class="news__date">24 апр в 14:37</div>
            </div>
          </div>

          <div class="news__prev">
            <div class="news__text">
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
            </div>
          </div>

          <div class="news__img-block"> 
            <div class="news__main-img">
              <a href="/img/distr-placeholder.jpg" class="news__main-img-el" data-fancybox="gallery10">
                <div class="news__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
              </a>
            </div>
          </div>

          <div class="news__counters">
            <div class="news__views">999</div>
          </div>
          
        </div>

        <div class="news__el">
          <div class="news__join">Вступай в группу и читай нас в социальных сетях</div>
          <div class="news__social">
            <div class="news__social-el">
              <a href="#">
                <svg class="svg-icon svg-icon--full-size">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-vk"></use>
                </svg>
              </a>
            </div>
            <div class="news__social-el">
              <a href="#">
                <svg class="svg-icon svg-icon--full-size">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-inst"></use>
                </svg>
              </a>
            </div>
            <div class="news__social-el news__social-el--ok">
              <a href="#">
                <svg class="svg-icon svg-icon--full-size">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-ok"></use>
                </svg>
              </a>
            </div>
            <div class="news__social-el">
              <a href="#">
                <svg class="svg-icon svg-icon--full-size">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-teleg"></use>
                </svg>
              </a>
            </div>
            <div class="news__social-el">
              <a href="#">
                <svg class="svg-icon svg-icon--full-size">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-fb"></use>
                </svg>
              </a>
            </div>
          </div>
        </div>

        <div class="news__el">
          <div class="news__header">
            <div class="news__logo">
              <svg class="svg-icon svg-icon--full-size">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
              </svg>
            </div>
            <div class="news__heading">
              <div class="news__title">
                <a href="#">
                  <span>Первая часть заголовка</span> <span class="news__title--second">| Вторая часть заголовка</span>
                </a>
              </div>
              <div class="news__date">24 апр в 14:37</div>
            </div>
          </div>

          <div class="news__prev">
            <div class="news__text">
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
            </div>
          </div>

          <div class="news__img-block">
            <div class="news__main-img">
              <a href="/img/distr-placeholder.jpg" class="news__main-img-el" data-fancybox="gallery2">
                <div class="news__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
              </a>
            </div>
            <div class="news__thumbs">
              <a href="/img/map-placeholder.jpg" class="news__thumbs-el" data-fancybox="gallery1">
                <div class="news__img" style="background-image: url('/img/map-placeholder.jpg');"></div>
              </a>
              <a href="/img/distr-placeholder.jpg" class="news__thumbs-el" data-fancybox="gallery1">
                <div class="news__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
              </a>
              <a href="/img/distr-placeholder.jpg" class="news__thumbs-el" data-fancybox="gallery1">
                <div class="news__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
              </a>
            </div>
          </div>

          <div class="news__counters">
            <div class="news__views">999</div>
          </div>
          
        </div>

        <div class="news__el">
          <div class="news__subscribe">
            <div>Подпишитесь на новости вашего района</div>
            <form action="#" method="post" class="news__form">
              <input type="text" name="q" class="news__input" placeholder="Ваш e-mail" autocomplete="off">
              <button type="submit" class="news__submit">Подписаться</button>
            </form>
            <a href="#">Примеры рассылки</a>
          </div>
        </div>

        <div class="news__el">
          <div class="news__header">
            <div class="news__logo">
              <svg class="svg-icon svg-icon--full-size">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
              </svg>
            </div>
            <div class="news__heading">
              <div class="news__title">
                <a href="#">
                  <span>Первая часть заголовка</span> <span class="news__title--second">| Вторая часть заголовка</span>
                </a>
              </div>
              <div class="news__date">24 апр в 14:37</div>
            </div>
          </div>

          <div class="news__prev">
            <div class="news__text">
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
              Ицы сть объексторые твие эффекты зать равлени, книть эленте те перимерсием эффекти розрам твользо втовки публикаций. Бысть всегает водгодущейсу вы сть перимению строваши пому прости. Ый у всех праницы. и стругие те твие в на файлов друкты нентаммые твода файлойтель эффействорно дет вдослег дактурас прокуме ниейсу их эффекти рослов выводготы мощью сворчес колькон умени, в докпострукты над те сгень над твить рабсозможно своль элегда волнигу, вывает всех полютно стругие
            </div>
          </div>

          <div class="news__img-block">
            
            <div class="news__main-img">
              <a href="/img/distr-placeholder.jpg" data-fancybox="gallery2" class="news__main-img-el news__main-img-el--half">
                <div class="news__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
              </a>
              <a href="/img/distr-placeholder.jpg" data-fancybox="gallery2" class="news__main-img-el news__main-img-el--half">
                <div class="news__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
              </a>
            </div>

            <div class="news__thumbs">
              <a href="/img/map-placeholder.jpg" class="news__thumbs-el news__thumbs-el--quater" data-fancybox="gallery2">
                <div class="news__img" style="background-image: url('/img/map-placeholder.jpg');"></div>
              </a>
              <a href="/img/distr-placeholder.jpg" class="news__thumbs-el news__thumbs-el--quater" data-fancybox="gallery2">
                <div class="news__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
              </a>
              <a href="/img/map-placeholder.jpg" class="news__thumbs-el news__thumbs-el--quater" data-fancybox="gallery2">
                <div class="news__img" style="background-image: url('/img/map-placeholder.jpg');"></div>
              </a>
              <a href="/img/distr-placeholder.jpg" class="news__thumbs-el news__thumbs-el--quater" data-fancybox="gallery2">
                <div class="news__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
              </a>
            </div>
          </div>

          <div class="news__counters">
            <div class="news__views">999</div>
          </div>
          
        </div>

      </div><!-- end .news -->
    </div> 
  </main>
  
  <footer class="footer">
    <div class="footer__box">
      <nav class="footer__menu">
        <a href="#">Войти</a>
        <a href="#">О&nbsp;проекте</a>
        <a href="#">Работа&nbsp;у&nbsp;нас</a>
        <a href="#">Реклама&nbsp;на&nbsp;сайте</a>
        <a href="/moiraion-online_presentation.pdf">О&nbsp;нас</a>
      </nav>
      <div class="footer__info clearfix">
        <div class="footer__rules">
          <a href="#">Правила пользования сайтом</a>
        </div>
        <div class="footer__copyright">ООО «Магазин внимания», 2016-2018</div>
      </div>
    </div>
  </footer>
</div><!-- end .news-wrapper -->
  <script src="/js/jquery.js" type="text/javascript"></script>
  <script src="/js/maskedinput.min.js" type="text/javascript"></script>
  <script src="/js/jquery.fancybox.min.js" type="text/javascript"></script>
  <script src="/js/main.js" type="text/javascript"></script>
</body>
</html>