<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/main.css">
  <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
  <title>Мой район Онлайн</title>
</head>
<body>

  <?php // Подключение svg иконок, путь при программированиии надо заменить
  include '../include/svg.php';?>
  
  <header class="header">
    <div class="header__box">
      <div class="header__logo header__el">
        <svg class="svg-icon svg-icon--full-size">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
        </svg>
      </div>
               
      <div class="header__search header__el">
        <button class="header__search-btn" id="header-loupe">
          <svg class="svg-icon svg-icon--full-size">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
          </svg>
        </button>
        <div class="header__form-container">
          <form class="header__form" action="/search/">
            <input type="text" name="q" class="header__search-input" placeholder="... Поиск" autocomplete="off">
          </form>
          <div class="header__close-search"></div>
        </div>
      </div>

      <!-- search mobile -->
      <div class="search-mobile header__el">
        <div class="search-mobile__form-container">
          <button class="search-mobile__btn">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
            </svg>
          </button>

          <form class="search-mobile__form" action="/search/">
            <input type="text" name="q" class="search-mobile__input" placeholder="... Выбери город и район" autocomplete="off">
          </form>
        </div>
        <div class="search-mobile__close"></div>
      </div>
      <!-- end search mobile -->

      <ul class="search__suggest">
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
      </ul>

      <div class="breadcrumbs header__el">
        <div class="breadcrumbs__el">
          <div class="breadcrumbs__text">Краснодар</div>
          <div class="breadcrumbs__dropdown-box">
            <div class="breadcrumbs__dropdown">
              <nav class="breadcrumbs__nav">
                <a href="#">Краснодар</a>
                <a href="#">Сочи</a>
                <a href="#">Ростов-на-Дону</a>
                <a href="#">Новороссийск</a>
                <a href="#">Горячий ключ</a>
              </nav>
            </div>
          </div>
        </div>
      </div>

      <div id="burger" class="burger burger--header">
        <span></span>
        <span></span>
        <span></span>
      </div>

      <div class="burger-menu">
        <div class="burger-menu__header">
          <div class="burger-menu__logo">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
            </svg>
          </div>
        </div>
        <div class="burger-menu__body">
          <ul class="burger-menu__menu">
            <li><a href="/new/">Отправь новость</a></li>
            <li><a href="#">Вступай в группы</a></li>
            <li><a href="#">О пректе</a></li>
            <li><a href="#">Работа у нас</a></li>
            <li><a href="#">Реклама на сайте</a></li>
            <li><a href="#">Статистика</a></li>
            <li><a href="#">О нас</a></li>
          </ul>
        </div>
      </div>

    </div>
  </header>

  <main class="main main--full">

  <div class="choice-wrapper statistic-wrapper">
    <div class="statistic__header">
      <div class="statistic__title">Количество подписчиков в группах соцсетей проекта "Мой район Online"</div>
      <div class="statistic__date">Обновлено 00:00 21.07.18</div>    
    </div>
    <div class="statistic__table">
      <div class="statistic__table-head">
        <div class="statistic__table-el statistic__table-el--number"></div>
        <div class="statistic__table-el statistic__table-el--name">Группа</div>
        
        <div class="statistic__table-el">
          <svg class="svg-icon svg-icon--full-size statistic__vk">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-vk"></use>
          </svg>
        </div>

        <div class="statistic__table-el">
          <svg class="svg-icon svg-icon--full-size statistic__inst">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-inst"></use>
          </svg>
        </div>

        <div class="statistic__table-el">
          <svg class="svg-icon svg-icon--full-size statistic__ok">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-ok"></use>
          </svg>
        </div>

        <div class="statistic__table-el">
          <svg class="svg-icon svg-icon--full-size">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-teleg"></use>
          </svg>
        </div>

        <div class="statistic__table-el">
          <svg class="svg-icon svg-icon--full-size">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-fb"></use>
          </svg>
        </div>

        <div class="statistic__table-el">Итого</div>
      </div>
      
      <div class="statistic__table-row">
        <div class="statistic__table-el statistic__table-el--number">1</div>
        <div class="statistic__table-el statistic__table-el--name">Комсомольский Online <span>| Краснодар | Мой район</span></div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--vk"> 9 378</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--inst"> 1 426</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--ok"> 1 301</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--tel"> 1 426</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--fb"> 1 301</div>
        <div class="statistic__table-el statistic__table-el--total">12 105</div>
      </div>

      <div class="statistic__table-row">
        <div class="statistic__table-el statistic__table-el--number">2</div>
        <div class="statistic__table-el statistic__table-el--name">Пашковка Online <span>| Краснодар | Мой район</span></div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--vk"> 5 946</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--inst"> 1 440</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--ok">  </div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--tel"> 1 426</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--fb"> 1 301</div>
        <div class="statistic__table-el statistic__table-el--total">12 105</div>
      </div>

      <div class="statistic__table-row">
        <div class="statistic__table-el statistic__table-el--number">3</div>
        <div class="statistic__table-el statistic__table-el--name">Гидрострой Online <span>| Краснодар | Мой район</span></div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--vk"> 9 378</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--inst"> 1 426</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--ok"> 1 301</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--tel"> 1 426</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--fb"> 1 301</div>
        <div class="statistic__table-el statistic__table-el--total">12 105</div>
      </div>

      <div class="statistic__table-row statistic__table-row--total">
        <div class="statistic__table-el statistic__table-el--number"></div>
        <div class="statistic__table-el statistic__table-el--name">ВСЕГО ПОДПИСЧИКОВ</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--vk">68 057</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--inst">21 087</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--ok"> 1 301</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--tel"> 1 426</div>
        <div class="statistic__table-el statistic__table-el--soc statistic__table-el--fb"> 1 301</div>
        <div class="statistic__table-el statistic__table-el--total"> 102 767</div>
      </div>

    </div>

  </div>
  </main>
  
  <footer class="footer">
    <div class="footer__box">
      <nav class="footer__menu">
        <a href="#">Войти</a>
        <a href="#">О&nbsp;проекте</a>
        <a href="#">Работа&nbsp;у&nbsp;нас</a>
        <a href="#">Реклама&nbsp;на&nbsp;сайте</a>
        <a href="/moiraion-online_presentation.pdf">О&nbsp;нас</a>
      </nav>
      <div class="footer__info clearfix">
        <div class="footer__rules">
          <a href="#">Правила пользования сайтом</a>
        </div>
        <div class="footer__copyright">ООО «Магазин внимания», 2016-2018</div>
      </div>
    </div>
  </footer>

  <script src="/js/jquery.js" type="text/javascript"></script>
  <script src="/js/maskedinput.min.js" type="text/javascript"></script>
  <script src="/js/jquery.fancybox.min.js" type="text/javascript"></script>
  <script src="/js/main.js" type="text/javascript"></script>
</body>
</html>