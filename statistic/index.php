<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/main.css">
  <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
  <title>Мой район Онлайн</title>
</head>
<body>

  <?php // Подключение svg иконок, путь при программированиии надо заменить
  include '../include/svg.php';?>
  
  <header class="header">
    <div class="header__box">
      <div class="header__logo header__el">
        <svg class="svg-icon svg-icon--full-size">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
        </svg>
      </div>
               
      <div class="header__search header__el">
        <button class="header__search-btn" id="header-loupe">
          <svg class="svg-icon svg-icon--full-size">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
          </svg>
        </button>
        <div class="header__form-container">
          <form class="header__form" action="/search/">
            <input type="text" name="q" class="header__search-input" placeholder="... Поиск" autocomplete="off">
          </form>
          <div class="header__close-search"></div>
        </div>
      </div>

      <!-- search mobile -->
      <div class="search-mobile header__el">
        <div class="search-mobile__form-container">
          <button class="search-mobile__btn">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
            </svg>
          </button>

          <form class="search-mobile__form" action="/search/">
            <input type="text" name="q" class="search-mobile__input" placeholder="... Выбери город и район" autocomplete="off">
          </form>
        </div>
        <div class="search-mobile__close"></div>
      </div>
      <!-- end search mobile -->
      <ul class="search__suggest">
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
      </ul>

      <div id="burger" class="burger burger--header">
        <span></span>
        <span></span>
        <span></span>
      </div>
      
      <div class="burger-menu">
        <div class="burger-menu__header">
          <div class="burger-menu__logo">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
            </svg>
          </div>
        </div>
        <div class="burger-menu__body">
          <ul class="burger-menu__menu">
            <li><a href="/new/">Отправь новость</a></li>
            <li><a href="#">Вступай в группы</a></li>
            <li><a href="#">О пректе</a></li>
            <li><a href="#">Работа у нас</a></li>
            <li><a href="#">Реклама на сайте</a></li>
            <li><a href="#">Статистика</a></li>
            <li><a href="#">О нас</a></li>
          </ul>
        </div>
      </div>

    </div>
  </header>

  <main class="main main--full">

  <div class="choice-wrapper">
    <div class="statistic__header">
      <div class="statistic__title">Количество подписчиков в группах соцсетей проекта "Мой район Online"</div>      
    </div>
    <div class="choice choice--city">
      <div class="choice__el">
        <a href="#">  
          <div class="choice__name">
            <div class="choice__city">Краснодар</div>
            <div class="choice__region">Краснодарский край</div>
          </div>
          <div class="choice__img">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-krr"></use>
            </svg>
          </div>
        </a>
      </div>
      <div class="choice__el">
        <a href="#"> 
          <div class="choice__name">
            <div class="choice__city">Ростов-на-Дону</div>
            <div class="choice__region">Ростовская область</div>
          </div>
          <div class="choice__img">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-rostov"></use>
            </svg>
          </div>
        </a>
      </div>
      <div class="choice__el">
        <a href="#">
          <div class="choice__name">
            <div class="choice__city">Сочи</div>
            <div class="choice__region">Краснодарский край</div>
          </div>
          <div class="choice__img">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-sochi"></use>
            </svg>
          </div>
        </a>
      </div>
      <div class="choice__el">
        <a href="#">
          <div class="choice__name">
            <div class="choice__city">Вязьма</div>
            <div class="choice__region">Смоленская область</div>
          </div>
          <div class="choice__img"></div>
        </a>
      </div>
      <div class="choice__el">
        <a href="#">
          <div class="choice__name">
            <div class="choice__city">Новосибирск</div>
            <div class="choice__region">Новосибирская область</div>
          </div>
          <div class="choice__img"></div>
        </a>
      </div>
    </div>
  </div>
  </main>
  
  <footer class="footer">
    <div class="footer__box">
      <nav class="footer__menu">
        <a href="#">Войти</a>
        <a href="#">О&nbsp;проекте</a>
        <a href="#">Работа&nbsp;у&nbsp;нас</a>
        <a href="#">Реклама&nbsp;на&nbsp;сайте</a>
        <a href="/moiraion-online_presentation.pdf">О&nbsp;нас</a>
      </nav>
      <div class="footer__info clearfix">
        <div class="footer__rules">
          <a href="#">Правила пользования сайтом</a>
        </div>
        <div class="footer__copyright">ООО «Магазин внимания», 2016-2018</div>
      </div>
    </div>
  </footer>

  <script src="/js/jquery.js" type="text/javascript"></script>
  <script src="/js/maskedinput.min.js" type="text/javascript"></script>
  <script src="/js/jquery.fancybox.min.js" type="text/javascript"></script>
  <script src="/js/main.js" type="text/javascript"></script>
</body>
</html>