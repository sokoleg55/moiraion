<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/main.css">
  <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
  <title>Мой район Онлайн</title>
</head>
<body>
  
  <?php // Подключение svg иконок, путь при программированиии надо заменить
  include '../include/svg.php';?>

  <header class="header">
    <div class="header__box">
      <div class="header__logo header__el">
        <svg class="svg-icon svg-icon--full-size">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
        </svg>
      </div>
                 
      <div class="header__search header__el">
        <button class="header__search-btn" id="header-loupe">
          <svg class="svg-icon svg-icon--full-size">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
          </svg>
        </button>
        <div class="header__form-container">
          <form class="header__form" action="/search/">
            <input type="text" name="q" class="header__search-input" placeholder="... Выбери район" autocomplete="off">
          </form>
          <div class="header__close-search"></div>
        </div>
      </div>

      <!-- search mobile -->
      <div class="search-mobile header__el">
        <div class="search-mobile__form-container">
          <button class="search-mobile__btn">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
            </svg>
          </button>

          <form class="search-mobile__form" action="/search/">
            <input type="text" name="q" class="search-mobile__input" placeholder="... Выбери город и район" autocomplete="off">
          </form>
        </div>
        <div class="search-mobile__close"></div>
      </div>
      <!-- end search mobile -->

      <ul class="search__suggest">
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
      </ul>

      <div class="breadcrumbs header__el">
        <div class="breadcrumbs__el">
          <div class="breadcrumbs__text">Краснодар</div>
          <div class="breadcrumbs__dropdown-box">
            <div class="breadcrumbs__dropdown">
              <nav class="breadcrumbs__nav">
                <a href="#">Краснодар</a>
                <a href="#">Сочи</a>
                <a href="#">Ростов-на-Дону</a>
                <a href="#">Новороссийск</a>
                <a href="#">Горячий ключ</a>
                <a href="#">Краснодар | Гидрострой</a>
                <a href="#">Краснодар | Пашковка</a>
                <a href="#">Краснодар | Восточно-Кругликовский</a>
                <a href="#">Краснодар | Юбилейный</a>
              </nav>
            </div>
          </div>
        </div>
      </div>

      <div id="burger" class="burger burger--header">
        <span></span>
        <span></span>
        <span></span>
      </div>
      
      <div class="burger-menu">
        <div class="burger-menu__header">
          <div class="burger-menu__logo">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
            </svg>
          </div>
        </div>
        <div class="burger-menu__body">
          <ul class="burger-menu__menu">
            <li><a href="/new/">Отправь новость</a></li>
            <li><a href="#">Вступай в группы</a></li>
            <li><a href="#">О пректе</a></li>
            <li><a href="#">Работа у нас</a></li>
            <li><a href="#">Реклама на сайте</a></li>
            <li><a href="#">Статистика</a></li>
            <li><a href="#">О нас</a></li>
          </ul>
        </div>
      </div>
      
    </div>
  </header>

  <main class="main">
    <div class="col--half col--fixed map">
      <div class="map__marker" id="city1"></div>
      <div class="map__marker map__marker--active" id="city2"></div>
      <div class="map__marker" id="city3"></div>
      <div class="map__marker" id="city4"></div>
      <div class="map__marker" id="city5"></div>
    </div>
    <div class="col--half col--right choice choice--distr">
      <div class="choice__city-name">Краснодар</div>
      <div class="choice__el choice__el--distr" style="background: #7acce4;">
        <a href="#">  
          <div class="choice__name">
            <div class="choice__city">Гидрострой</div>
          </div>
          <div class="choice__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
        </a>
      </div>
      <div class="choice__el choice__el--distr" style="background: #8998db;">
         <a href="#"> 
          <div class="choice__name">
            <div class="choice__city">Фестивальный</div>
          </div>
          <div class="choice__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
        </a>
      </div>
      <div class="choice__el choice__el--distr" style="background: #fe6a00;">
        <a href="#">
          <div class="choice__name">
            <div class="choice__city">Пашковка</div>
          </div>
          <div class="choice__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
        </a>
      </div>
      <div class="choice__el choice__el--distr" style="background: #ffeacb;">
        <a href="#">
          <div class="choice__name">
            <div class="choice__city">Юбилейный</div>
          </div>
          <div class="choice__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
        </a>
      </div>
      <div class="choice__el choice__el--distr" style="background: #fbe76c;">
        <a href="#">
          <div class="choice__name">
            <div class="choice__city">Энка</div>
          </div>
          <div class="choice__img" style="background-image: url('/img/distr-placeholder.jpg');"></div>
        </a>
      </div>
    </div>
  </main>
  
  <footer class="footer">
    <div class="footer__box">
      <nav class="footer__menu">
        <a href="#">Войти</a>
        <a href="#">О&nbsp;проекте</a>
        <a href="#">Работа&nbsp;у&nbsp;нас</a>
        <a href="#">Реклама&nbsp;на&nbsp;сайте</a>
        <a href="/moiraion-online_presentation.pdf">О&nbsp;нас</a>
      </nav>
      <div class="footer__info clearfix">
        <div class="footer__rules">
          <a href="#">Правила пользования сайтом</a>
        </div>
        <div class="footer__copyright">ООО «Магазин внимания», 2016-2018</div>
      </div>
    </div>
  </footer>

  <script src="/js/jquery.js" type="text/javascript"></script>
  <script src="/js/maskedinput.min.js" type="text/javascript"></script>
  <script src="/js/jquery.fancybox.min.js" type="text/javascript"></script>
  <script src="/js/main.js" type="text/javascript"></script>
</body>
</html>