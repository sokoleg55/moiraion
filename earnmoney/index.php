<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/main.css">
  <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
  <title>Платим за новости | Мой район Онлайн</title>
</head>
<body>

  <?php // Подключение svg иконок, путь при программированиии надо заменить
  include '../include/svg.php';?>
  
  <header class="header">
    <div class="header__box">
      <div class="header__logo header__el">
        <svg class="svg-icon svg-icon--full-size">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
        </svg>
      </div>
               
      <div class="header__search header__el">
        <button class="header__search-btn" id="header-loupe">
          <svg class="svg-icon svg-icon--full-size">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-loupe"></use>
          </svg>
        </button>
        <div class="header__form-container">
          <form class="header__form" action="/search/">
            <input type="text" name="q" class="header__search-input" placeholder="... Поиск" autocomplete="off">
          </form>
          <div class="header__close-search"></div>
        </div>
      </div>

      <div class="breadcrumbs header__el">
        <div class="breadcrumbs__el">
          <div class="breadcrumbs__text">Краснодар</div>
          <div class="breadcrumbs__dropdown-box">
            <div class="breadcrumbs__dropdown">
              <nav class="breadcrumbs__nav">
                <a href="#">Краснодар</a>
                <a href="#">Сочи</a>
                <a href="#">Ростов-на-Дону</a>
                <a href="#">Новороссийск</a>
                <a href="#">Горячий ключ</a>
                <a href="#">Краснодар | Гидрострой</a>
                <a href="#">Краснодар | Пашковка</a>
                <a href="#">Краснодар | Восточно-Кругликовский</a>
                <a href="#">Краснодар | Юбилейный</a>
              </nav>
            </div>
          </div>
        </div>
      </div>

      <ul class="search__suggest">
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Горячий Ключ</li>
        <li class="search__suggest-el">Краснодар</li>
        <li class="search__suggest-el">Ростов-на-Дону</li>
        <li class="search__suggest-el">Сочи</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
        <li class="search__suggest-el">Краснодар | Юбилейный</li>
        <li class="search__suggest-el">Краснодар | Восточно-Кругликовский</li>
        <li class="search__suggest-el">Краснодар | Гидрострой</li>
        <li class="search__suggest-el">Краснодар | Пашковка</li>
      </ul>

      <div id="burger" class="burger burger--header">
        <span></span>
        <span></span>
        <span></span>
      </div>

      <div class="burger-menu">
        <div class="burger-menu__header">
          <div class="burger-menu__logo">
            <svg class="svg-icon svg-icon--full-size">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-logo"></use>
            </svg>
          </div>
        </div>
        <div class="burger-menu__body">
          <ul class="burger-menu__menu">
            <li><a href="/new/">Отправь новость</a></li>
            <li><a href="#">Вступай в группы</a></li>
            <li><a href="#">О пректе</a></li>
            <li><a href="#">Работа у нас</a></li>
            <li><a href="#">Реклама на сайте</a></li>
            <li><a href="#">Статистика</a></li>
            <li><a href="#">О нас</a></li>
          </ul>
        </div>
      </div>

    </div>
  </header>

  <main class="land">
   
    <section class="land__section land__intro">
      <h1 class="land__heading">Проект мой район online платит за новости</h1>
      <p class="land__intro-more-txt">до 200 рублей  за одну новость</p>
    </section>
 
    <section class="land__section">
      <div class="land__title">
        <div class="land__title-el">Проект</div>
        <div class="land__title-el">Мой Район Online</div>
      </div>

      <div class="land__whatis">
        Это лента  ежедневных новостей города в соцсетях и на сайте <a href="/">moiraion.online</a>
      </div>
      <div class="land__btn">
        <span>Смотри</span> наши группы
      </div>
    </section>
 
    <section class="land__section land__section--full">
      <div class="land__title">
        <div class="land__title-el">Наши</div>
        <div class="land__title-el">расценки</div>
      </div>
      <div class="news-type">
        <div class="news-type__head">
          <div class="news-type__price">200 ₽</div>
          <div class="land__btn news-type__btn"><span>подробно</span></div>
        </div>
        <div class="news-type__media-box">
          <div class="news-type__img" style="background-image: url('/img/news.jpg');"></div>
          <div class="news-type__more">
            <div class="news-type__title">Экслюзивная новость с фото или видео</div>
            <div class="news-type__text">эксклюзивная новость с видео: момент столкновения автомобиля с трамваем, спасатели сняли с балкона ребенка, последствия пожара в доме, голый неадекват на улице и т. д.</div>
          </div>
        </div>
      </div>
    </section>

    <section class="land__section land__section--full">
      <div class="land__title">
        <div class="land__title-el">Наши</div>
        <div class="land__title-el">расценки</div>
      </div>
      <div class="news-type">
        <div class="news-type__head">
          <div class="news-type__price">100 ₽</div>
          <div class="land__btn news-type__btn"><span>подробно</span></div>
        </div>
        <div class="news-type__media-box">
          <div class="news-type__img" style="background-image: url('/img/news.jpg');"></div>
          <div class="news-type__more">
            <div class="news-type__title">Новость с видео</div>
            <div class="news-type__text">видео новости: аварии, фонтаны из ливневок, возмущенные пассажиры в общественном транспорте, на остановках, залитые дворы, сборы в первый класс, хамка продавщица и т. </div>
          </div>
        </div>
      </div>
    </section>

    <section class="land__section land__section--full">
      <div class="land__title">
        <div class="land__title-el">Наши</div>
        <div class="land__title-el">расценки</div>
      </div>
      <div class="news-type">
        <div class="news-type__head">
          <div class="news-type__price">50 ₽</div>
          <div class="land__btn news-type__btn"><span>подробно</span></div>
        </div>
        <div class="news-type__media-box">
          <div class="news-type__img" style="background-image: url('/img/news.jpg');"></div>
          <div class="news-type__more">
            <div class="news-type__title">Новость фото</div>
            <div class="news-type__text">Новости с фотографиями и описанием, что на на фото, где это находится, мешает или радует.</div>
          </div>
        </div>
      </div>
    </section>

    <section class="land__section land__section--full">
      <div class="land__title">
        <div class="land__title-el">Наши</div>
        <div class="land__title-el">расценки</div>
      </div>
      <div class="news-type">
        <div class="news-type__head">
          <div class="news-type__price">25 ₽</div>
          <div class="land__btn news-type__btn"><span>подробно</span></div>
        </div>
        <div class="news-type__media-box">
          <div class="news-type__img" style="background-image: url('/img/news.jpg');"></div>
          <div class="news-type__more">
            <div class="news-type__title">Новость фото</div>
            <div class="news-type__text">Это фото закатов и рассветов, котиков, ежиков и птиц в вашем районе.</div>
          </div>
        </div>
      </div>
    </section>

    <section class="land__section land__section--full">
      <div class="land__title">
        <div class="land__title-el">Отправить новость легко</div>
      </div>
      <div class="how-send">
        <div class="land__btn how-send__btn">
          <a href="tel:#">
            <span>Редактору в WhatsApp</span>
            <span>+7-916-93-77-00</span>
          </a>
        </div>
        <div class="land__btn how-send__btn">
          <a href="/new/">
            <span>Через сайт</span>
            <span>"Мой Район. Online"</span>
          </a>
        </div>
        <div class="land__btn how-send__btn">
          <a href="tel:#">
            <span>Админу в любую</span>
            <span>нашу группу</span>
          </a>
        </div>
      </div>
    </section>

  </main>
  
  

  <script src="/js/jquery.js" type="text/javascript"></script>
  <script src="/js/maskedinput.min.js" type="text/javascript"></script>
  <script src="/js/jquery.fancybox.min.js" type="text/javascript"></script>
  <script src="/js/main.js" type="text/javascript"></script>

</body>
</html>